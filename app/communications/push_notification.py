from itertools import groupby
import requests
import simplejson as sj

from app import config
from app.log import get_logger
LOG = get_logger()


class Push(object):

    session = None      # the requests session

    def __init__(self):
        # create a session which we can re-use.
        self.session = requests.Session()
        self.session.headers.update({
            'X-CleverTap-Account-Id': config.CLEVERTAP_ACCOUNT_ID,
            'X-CleverTap-Passcode': config.CLEVERTAP_PASSCODE,
            'Content-Type': 'application/json'
        })
        self.key_func = lambda r: r['amount']

    def send_bulk_push_notifications(self, task_name, records, title, message_body, product_id=None,
                                     tag_group=None, deep_link_android=None, deep_link_ios=None,
                                     title_personalized=False, message_body_personalized=False,
                                     ** kwargs):
        """
        Sends out push notifications in bulk.
        """
        LOG.debug('Executing task: {}, sending PUSH NOTIFICATIONS to {} customers.'
                  .format(task_name, len(records)))

        # we can batch API calls by amount
        results = sorted(records, key=self.key_func)

        kwargs['product_id'] = product_id

        successful_recipients = 0

        for amount, group in groupby(results, self.key_func):
            # extract mobiles to which this amount is to be sent

            intended_customers = list(group)
            # LOG.debug("Intended Customers: {}".format(intended_customers))
            # LOG.debug("Amount: {}".format(amount))
            emails = [r['customer_email'] for r in intended_customers]
            # LOG.debug("Emails: {}".format(emails))
            identities = [r['customer_mobile'] for r in intended_customers]
            # LOG.debug('Identities: {}'.format(identities))

            if (not deep_link_android) and 'notification_type' in kwargs:
                notification_type = kwargs['notification_type']
                try:
                    # Not overriding the provided value; if given any, which denotes any customized amount.
                    if amount not in kwargs:
                        kwargs['amount'] = amount

                    # LOG.debug("Kwargs in send_bulk_push_notifications: {}".format(kwargs))

                    deep_link_android = notification_type.format(**kwargs)
                except Exception as e:
                    LOG.warn("Failed to create the deeplink url. An exception occurred: {}".format(e))

            if (not deep_link_ios) and deep_link_android:
                deep_link_ios = deep_link_android

            # construct request payload

            payload = {
                'name': str(task_name),
                'to': {
                    'Email': emails,
                    'Identity': identities
                },
                'respect_frequency_caps': False,
                'content': {
                    'title': str(title),
                    'body': str(message_body),
                    'platform_specific': {
                        'android': {
                            'background_image': None, # config.PUSH_1_ANDROID_IMG,
                            'deep_link': deep_link_android
                        },
                        'ios': {
                            'deep_link': deep_link_ios
                        }
                    }
                }
            }

            if tag_group:
                payload['tag_group'] = str(tag_group)

            # TODO: this API allows max 1000 requests in a second.
            # handle this limit later.

            result = self.session.\
                post(config.CLEVERTAP_API_PUSH_URL, sj.dumps(payload))

            api_response = result.json()
            LOG.debug("API Response: {}".format(api_response))

            if result.status_code != 200 or api_response['status'] != 'success':
                LOG.warn('Push API failed to execute the task: {}'.format(task_name),
                         extra={
                             'status_code': result.status_code,
                             'payload': payload,
                             'api_response': api_response})
            else:
                successful_recipients += len(intended_customers)

        if successful_recipients:
            LOG.info('Push notifications sent successfully to {} customers out'
                     ' of {}, regarding {}'.format(successful_recipients,
                                                   len(records), task_name))
        else:
            LOG.error("Error in sending Push notifications to {} customers, "
                      "regarding {}".format(len(records), task_name),
                      exc_info=True,
                      extra={
                          "task_name": task_name,
                          "records": records,
                          "api_response": api_response})

        return True
