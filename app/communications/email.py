from postmarker.core import PostmarkClient
from app.config import POSTMARK_API_TOKEN, POSTMARK_ACCOUNT_TOKEN, DEBUG
from .email_constants import mail_types
from app import log

LOG = log.get_logger()


class Email():

    def __init__(self, API_TOKEN=None, ACCOUNT_TOKEN=None,
                 max_retries=0, log_level=None, base_sender=None):
        """
        Initialize the Postmark client with the provided API Token.

        :param API_TOKEN: A string, which is the server api token.
        :param ACCOUNT_TOKEN: An optional value.
        :param max_retries: Number of reattempts to be made if
                            mail delivery fails in first attempt.
        :param log_level: An integer, where 0 - OFF, 1 - INFO, 2 - DEBUG, 3 - FULL.
        :param base_sender: A fallback email address, to be used for 'From'.
        """

        API_TOKEN = API_TOKEN if API_TOKEN else POSTMARK_API_TOKEN
        ACCOUNT_TOKEN = ACCOUNT_TOKEN if ACCOUNT_TOKEN else POSTMARK_ACCOUNT_TOKEN

        if DEBUG:
            log_level = log_level if log_level else 2
        else:
            log_level = log_level if log_level else 1


        self.postmark_client = PostmarkClient(server_token=API_TOKEN,
                                              account_token=ACCOUNT_TOKEN,
                                              verbosity=log_level,
                                              max_retries=max_retries)
        self.base_sender = base_sender


    def send_template_mail(self, mail_type, recipient, sender=None, cc=None, bcc=None,
                           subject= None, tag=None, attachments=None, **kwargs):
        """
        Send a single template email, to an individual whose email id is
        specified as a receipt value. The type of template to be used is given
        by the value of mail_type.

        :param mail_type: Email constant, denoting which category of mail to be sent.
        :param recipient: A string, containing the email address of the receipt of this mail.
        :param sender: An optional value containing the email address of the sender.
                       If not specified, a global value is used.
        :param cc: An optional field, for the email addresses of the people to be kept
                   in cc of this mail.
        :param bcc: String, optional, email id of all those to be kept in bcc of the mail.
        :param subject: If a subject is provided, it overrides the subject which
                        may have been specified during template creation.
        :param tag: Optional, a string denoting the tag to be
        :param attachments: A list of attachments to be sent with this mail.
        :param kwargs: Key worded argument, to be passed to TemplateModel,
                       which are supplied to the template's HTML/Text body of this mail.
        :return: None
        """
        # Attachments is a list. For reference, see: http://developer.postmarkapp.com/developer-api-templates.html

        if recipient:
            try:
                email_response = self.postmark_client.emails.send_with_template(
                    TemplateId = mail_types[mail_type]['template_id'],
                    From =(sender if sender else
                            mail_types[mail_type]['from'] if mail_types[mail_type]['from']
                            else self.base_sender),
                    To = recipient,
                    Cc = cc if cc else mail_types[mail_type]['cc'],
                    Bcc = bcc if bcc else mail_types[mail_type]['bcc'],
                    Subject = subject,
                    Tag = tag if tag else mail_types[mail_type]['tag'],
                    Attachments = attachments,
                    TemplateModel = kwargs
                )

                if not email_response['ErrorCode'] and email_response['Message'] == 'OK':
                    LOG.info('Email Sending Activity: {} email sent successfully to {}'.format(mail_type, recipient))
                    return True
                else:
                    LOG.info('Email Sending Activity: {} email not sent to {}, with error code: {} and '
                             'message: {} from Postmark.'
                          .format(mail_type, recipient, email_response['ErrorCode'], email_response['Message']))
                    return False

            except Exception as e:
                LOG.error('Email Sending Activity: An exception occurred while sending the {} email to {}, '
                         'with exception being: {}'
                      .format(mail_type, recipient, e))

        return False


    def send_mail_in_bulk(self, mail_type, recipients, sender=None):
        pass
