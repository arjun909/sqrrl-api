from app.config import SMS_API_AUTHKEY, SMS_SENDER, DEBUG, SMS_API_URL
from .sms_constants import sms_templates
import requests
import json
from app.log import get_logger

LOG = get_logger()


headers = {
    'Content-Type': 'application/json',
    'authkey': SMS_API_AUTHKEY
}


def is_sender_valid(sender):
    if isinstance(sender, str) and len(sender) == 6 and sender.isalpha():
        return True
    return False


def send_bulk_transaction_sms(sms_type, *mobile_numbers, **message_params):
    """
    Uses msg91 API to send transaction sms.

    Sample POST Body -

    {
	"authkey" : AUTH_KEY,
	"sms": [
	    {
	        "message" : MESSAGE_CONTENT,
		    "to" : [PHONE_NUMBERS]
	    }
	],
	"country": "91",
	"route": "4",
	"flash": 0,
	"sender": "TSQRRL"
    }

    @params
    sms - array in the format [{'message': 'hello', 'to': ['99xxx']}]
    """

    if not message_params:
        message_params = {}

    assert(isinstance(mobile_numbers, tuple) or
           isinstance(mobile_numbers, list) or
           isinstance(mobile_numbers, set))

    mobile_numbers = list(mobile_numbers)
    sms = [{
        'message': sms_templates[sms_type].format(**message_params),
        'to': list(mobile_numbers)
    }]

    # API request body
    body = {
        'sender': SMS_SENDER,
        'country': '91',
        'route': '4',
        'flash': 0,
        'sms': sms
    }

    if 'campaign' in message_params:
        body['campaign'] = message_params['campaign']
    else:
        body['campaign'] = sms_type

    if not DEBUG:
        try:
            r = requests.post(url=SMS_API_URL,
                              json=body,
                              headers=headers)

            if r.status_code == 200 and r.json()['type'] == 'success':
                LOG.info("{} sms sent successfully to {}".
                         format(sms_type, mobile_numbers))
            else:
                LOG.error("An error occurred while sending {} sms, failed "
                          "due to: {}".format(sms_type,r.json()['message']))

            return r.status_code

        except Exception as e:
            LOG.error("An exception occurred while sending {} sms, with the"
                      " exception being: {}".format(sms_type, e),
                      exc_info=True)
            raise e
    else:
        LOG.debug("Debug mode on. Cannot send SMS but assume it is sent.")
        return 200
