from app import config as c
TYPE_1 = 'First Successful Sqrrl Away Transaction'
TYPE_2 = 'First Successful BYOD Transaction'
TYPE_3 = 'First Successful Axe Tax Transaction'
TYPE_4 = 'Sqrrl Monthly Account Statements'
TYPE_5 = 'UTR files as per AMC codes'

mail_types = {

    TYPE_1: {
        'template_id': c.TEMPLATE_ID_MAIL_TYPE_SQRRL_AWAY,
        'from': 'sanjeev@sqrrl.in',
        'cc': None,
        'bcc': None,
        'tag': 'Sqrrl Away'
    },

    TYPE_2: {
        'template_id': c.TEMPLATE_ID_MAIL_TYPE_BYOD,
        'from': 'sanjeev@sqrrl.in',
        'cc': None,
        'bcc': None,
        'tag': 'BYOD'
    },

    TYPE_3: {
        'template_id': c.TEMPLATE_ID_MAIL_TYPE_AXE_TAX,
        'from': 'sanjeev@sqrrl.in',
        'cc': None,
        'bcc': None,
        'tag': 'Axe Tax'
    },
    TYPE_4: {
        'template_id': c.TEMPLATE_ID_MAIL_TYPE_STATEMENTS,
        'from': 'sanjeev@sqrrl.in',
        'cc': None,
        'bcc': None,
        'tag': 'statements'
    },
    TYPE_5: {
        'template_id': c.TEMPLATE_ID_MAIL_TYPE_UTR,
        'from': 'dev@sqrrl.in',
        'cc': None,
        'bcc': None,
        'tag': 'utr'
    },

}
