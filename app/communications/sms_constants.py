SUCCESSFUL_TRANSACTION_GENERAL = 'Successful transaction(general)'
FAILED_TRANSACTION_GENERAL = 'Failed transaction(general)'

SQRRL_AWAY_SUCCESSFUL_INVESTMENT = 'Successful Sqrrl Away transaction(investment)'
SQRRL_AWAY_SUCCESSFUL_REDEEM = 'Successful Sqrrl Away transaction(redeem)'
SQRRL_AWAY_UNITS_ALLOTTED = 'Sqrrl Away Units allotted'
SQRRL_AWAY_FAILED_INVESTMENT = 'Sqrrl Away purchase failed'
SQRRL_AWAY_FAILED_REDEEM = 'Sqrrl Away redeem failed'

BYOD_SUCCESSFUL_INVESTMENT = 'Successful BYOD transaction(investment)'
BYOD_TRANSACTION_REJECTED_BY_AMC = 'Rejected BYOD transaction'
BYOD_UNITS_ALLOTTED = 'BYOD Units allotted'

AXE_TAX_SUCCESSFUL_INVESTMENT = 'Successful Axe Tax transaction'
AXE_TAX_TRANSACTION_REJECTED_BY_AMC = 'Rejected Axe Tax transaction'
AXE_TAX_UNITS_ALLOTTED = 'Axe Tax Units allotted'


sms_templates = {

    SUCCESSFUL_TRANSACTION_GENERAL:
        'INR {amount} successfully {transaction_type} {product} via '
        'ref: {group_id}. It will be processed within 1-2 business days.',

    FAILED_TRANSACTION_GENERAL:
        'Your transaction of INR {amount} with Sqrrl(ref: {group_id}) was not '
        'confirmed by your bank. If any amount was debited, '
        'your bank will refund it within 1-2 days.',

    SQRRL_AWAY_SUCCESSFUL_INVESTMENT:
        'INR {amount} successfully added to your Sqrrl Away account. '
        'Ref: {group_id}. It will be processed within 1-2 business days.',

    SQRRL_AWAY_SUCCESSFUL_REDEEM:
        'INR {amount} successfully withdrawn from your Sqrrl Away account. '
        'Ref: {group_id}. It will be processed within 1 business day.',

    SQRRL_AWAY_UNITS_ALLOTTED:
        'For your investment in Sqrrl Away, transaction ref: {group_id}, the '
        'AMC(Reliance) has allotted {units} units in your folio.',

    SQRRL_AWAY_FAILED_INVESTMENT:
        'Your transaction of Rs {amount} into Sqrrl Away, ref: {group_id}, '
        'was not confirmed by your bank. '
        'If any amount was debited, your bank will refund it within 1-2 days.',

    SQRRL_AWAY_FAILED_REDEEM:
        'Your Sqrrl Away transaction ref: {group_id} was declined by AMC'
        '(Reliance). Please retry in some time.',


    BYOD_SUCCESSFUL_INVESTMENT:
        'Congrats, you’re one step closer to living your dream! INR {amount} '
        'successfully added to BYOD. Ref: {group_id}. '
        'It will be processed within 1-2 business days.',


    BYOD_UNITS_ALLOTTED:
        'Cheers. For your investment in BYOD, transaction ref: {group_id}, '
        'units have been successfully allotted by the AMCs in your folios.',

    BYOD_TRANSACTION_REJECTED_BY_AMC: None,

    AXE_TAX_SUCCESSFUL_INVESTMENT:
        'INR {amount} successfully added to Axe Tax. Ref: {group_id}. '
        'It will be processed within 1-2 business days. '
        'Congrats on saving your hard earned money.',

    AXE_TAX_TRANSACTION_REJECTED_BY_AMC: None,
    AXE_TAX_UNITS_ALLOTTED: None

}
