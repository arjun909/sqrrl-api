######################
# feed_out.py
######################
from datetime import datetime, timedelta
from itertools import groupby

from sparkpost import SparkPost

from app import model as M
from app import config
from app.constants import (TR_STATUS_PROCESSING, INVEST, REDEEM,
                           KARVY_OCCUPATION, OCCUPATION, KARVY, CAMS,
                           FT, INCOME_GROUP, LIQUID,
                           NACH_REGISTRATION_INITIATED,
                           NACH_REGISTRATION_PROCESSING,
                           FT_MIN_TR_ID, REDIS_CAMS_FEEDFILE_PREFIX)
from app.utils.helpers import s3_file_upload
from app.log import get_logger
from itertools import groupby
from openpyxl import Workbook
from lxml import etree

from sqlalchemy import or_

import dbf
import io
import redis

import xlsxwriter
LOG = get_logger()

wb = Workbook()
sp = SparkPost()

redis_client = redis.StrictRedis.from_url(config.REDIS_URL)

# use to convert dates/times from UTC to IST
IST_OFFSET = timedelta(hours=5, minutes=30)


class FeedOutWorker(object):
    """
    Encapsulates the workers that generate transaction feeds
    for CAMS and Karvy.
    """
    def __init__(self, session):
        self.session = session

    def prevent_duplicate_folio_creation(self, rows, rta):
        """
        This function filters rows that do not have scheme folios.
        Creates a group of transactions in an investment with no folio number,
        and adds only the row with maximum amount.
        """
        if rta == KARVY:
            keyfunc = lambda r: r.e6  # NOQA
            column_name = 'folio_number'
        elif rta == CAMS:
            keyfunc = lambda r: r.dummy4_c  # NOQA
            column_name = 'folio_no'
        elif rta == FT:
            keyfunc = lambda r: r.investment_id  # NOQA
            column_name = 'folio_no'

        # get rows which have folio number
        parsed_rows = [row for row in rows if getattr(row, column_name)]

        # get rows which do not have folio number.
        no_folio_rows = [row for row in rows if not getattr(row, column_name)]

        # group rows by investment id
        for row, group in groupby(no_folio_rows, keyfunc):
            no_folio_groups = list(group)

            # if a group already has a processed transaction, then continue
            has_processed_trxn = self.session.query(M.Transaction).filter(
                                    M.Transaction.investment_id == row,
                                    M.Transaction.is_processed.is_(True)
                                ).count()
            if has_processed_trxn:
                continue
            # find row with max amount in no_folio_groups to be sent to RTA

            max_amount_row = max(
                                 no_folio_groups,
                                 key=lambda row: row.amount
                            )
            # only add maximum amount row per group
            parsed_rows.append(max_amount_row)
        return parsed_rows

    def insert_karvy_transactions(self):
        """
        Starting with the scheme table, gets all PROCESSING
        transactions and inserts into KarvyFeedOut
        """
        LOG.debug('Starting worker for [ Karvy ] feed out...')
        # ordering by investment.id, as we need it sorted for group_by
        # in prevent_duplicate_folio
        q = self.session.query(
            M.Scheme, M.Investment, M.Transaction, M.Customer, M.Product
        ).filter(
            M.Scheme.id == M.Investment.scheme_id,
            M.Investment.id == M.Transaction.investment_id,
            M.Investment.customer_id == M.Customer.id,
            M.Transaction.status == TR_STATUS_PROCESSING,
            M.Transaction.transaction_type.in_((INVEST, REDEEM)),
            or_(M.Transaction.is_processed.is_(False),
                M.Transaction.is_processed.is_(None)),
            M.Scheme.rta == KARVY,
            M.Investment.product_id == M.Product.id
        ).order_by(M.Investment.id)
        new_rows = []
        for row in q:
            # Redemption from Reliance Money Manager Fund should not be
            # included in Karvy Feed Out
            if row.Transaction.transaction_type == REDEEM and \
               row.Product.internal_name == LIQUID:
                continue

            # the new row to insert
            nr = M.KarvyFeedOut()
            # copy-pasted and adapted from KarvyFeedOut

            # Mandatory except for fresh purchase
            nr.folio_number = row.Investment.scheme_folio or ''

            # Mandatory, round off to 2 decimal places
            nr.amount = round(row.Transaction.amount, 2)

            # Mandatory
            # if no folio number then it means first time investment
            if not nr.folio_number and nr.amount < 5000:
                nr.sub_transaction_type = 'S'
            elif nr.folio_number and nr.amount < 1000:
                nr.sub_transaction_type = 'S'
            else:
                nr.sub_transaction_type = 'Normal'

            if row.Investment.product_id == 1:
                nr.sub_transaction_type = 'Normal'
            # convert dates to IST, db data is in UTC
            dt = row.Transaction.created_on
            ist_dt = dt + IST_OFFSET
            # Mandatory, MM/dd/yyyy
            nr.transaction_date = ist_dt
            # Mandatory format hh:mm:ss
            nr.transaction_time = ist_dt.strftime(r'%H:%M:%S')

            # Mandatory
            if nr.sub_transaction_type == 'S':
                nr.sip_reference_number = row.Transaction.id
                # Mandatory
                nr.sip_registration_date = nr.transaction_date
                nr.sip_stdt = nr.transaction_date
                nr.sip_endt = nr.transaction_date + timedelta(days=180)
                nr.sip_freq = 'M'
                nr.sip_amount = nr.amount
            # Mandatory
            nr.amc_code = row.Scheme.amc_code_out or row.Scheme.amc_code
            # Mandatory
            nr.broker_code = 'ARN-{}'.format(config.ARN)
            # Mandatory
            nr.user_code = config.KARVY_FEED_USERCODE
            # Mandatory
            nr.user_transaction_number = row.Transaction.id
            # this will come back to us in reverse feed
            nr.application_number = '{}'.format(row.Transaction.id)
            # Mandatory, P or R
            nr.transaction_type = 'P' \
                if row.Transaction.transaction_type == INVEST else 'R'
            # Mandatory, 4 character code.
            nr.scheme_code = row.Scheme.rta_code
            # Mandatory
            nr.first_name = row.Customer.pan_name
            # Mandatory
            nr.address1 = (row.Customer.correspondence_address_1
                           or row.Customer.permanent_address_1)
            nr.address2 = (row.Customer.correspondence_address_2
                           or row.Customer.permanent_address_2)

            # all address fields can be max 40 characters
            if len(nr.address1 or '') >= 40:
                idx = nr.address1.strip().rfind(' ', 0, 39)
                if idx == -1:
                    idx = 39
                address = ''.join(nr.address1)
                nr.address1 = address[:idx].strip()
                nr.address2 = '{} {}'.format(address[idx+1:], nr.address2)

            # do the same test with nr.address2 now, spilling over to address3
            if len(nr.address2 or '') >= 40:
                idx = nr.address2.strip().rfind(' ', 0, 39)
                if idx == -1:
                    idx = 39
                address = ''.join(nr.address2)
                nr.address2 = address[:idx].strip()
                nr.address3 = address[idx+1:]

            if len(nr.address3 or '') >= 40:
                nr.address3 = nr.address3[:39]

            # Mandatory
            nr.city = (row.Customer.correspondence_city
                       or row.Customer.permanent_city)
            # Mandatory
            nr.pincode = (row.Customer.correspondence_pin
                          or row.Customer.permanent_pin)
            nr.mobile_number = row.Customer.mobile

            # format : MM/dd/yyyy
            nr.date_of_birth = row.Customer.date_of_birth
            # Mandatory
            nr.pan_number = row.Customer.pan_number
            if row.Customer.is_email_verified:
                nr.email = row.Customer.email
            else:
                nr.email = 'customer+{}@sqrrl.in'.format(row.Customer.uid)

            # Mandatory
            nr.account_number = row.Customer.bank_account_no
            # Mandatory, 'SB' for Savings.
            nr.account_type = 'SB'
            # Mandatory
            nr.bank_name = row.Customer.bank_name
            # Mandatory
            nr.branch_name = row.Customer.bank_branch_name
            # Mandatory
            nr.bank_city = row.Customer.bank_branch_city
            # Mandatory
            nr.reinvest_tag = row.Scheme.dividend_reinvestment_option
            # Mandatory, Single
            nr.hold_nature = 'SI'
            nr.neft_code = row.Customer.bank_ifsc
            nr.rtgs_code = row.Customer.bank_ifsc

            # Mandatory: Get from lookup / constant
            try:
                k = OCCUPATION[row.Customer.occupation_code]
                if k not in KARVY_OCCUPATION.keys():
                    k = 'Others'
            except KeyError:
                k = 'Others'
            nr.occupation_code = KARVY_OCCUPATION[k]

            # Mandatory - '01' is Individual
            nr.tax_status = '01'
            # always send the code for New Delhi
            nr.location = 'N3'
            # Mandatory - D for Direct credit
            nr.payment_mechanism = 'D'

            if row.Transaction.transaction_type == INVEST:
                # Mandatory
                nr.dep_bank = row.Scheme.bank_name
                # Mandatory
                nr.dep_acno = row.Scheme.bank_account_no
                # Mandatory format : MM/dd/YYYY
                nr.dep_date = ist_dt.strftime(r'%m/%d/%Y')
                # Mandatory for purchases: TODO
                nr.dep_reference_number = str(row.Transaction.id)
            # Mandatory
            nr.kyc_flag = 'Y' if row.Customer.is_kyc_verified else 'N'
            # Mandatory
            nr.poa_stat = 'N'
            # Mandatory, X = paperless mode
            nr.mode_transaction = 'X'
            # Mandatory
            nr.sign_verification = 'Y'
            # Mandatory
            nr.customer_id = row.Customer.uid
            # Mandatory if mode transation is W or T
            # ddmmyyyy$hh:mm:ss$user IP$Server Ip$Usrtrxnnumbe
            params = {
                'dt': ist_dt.strftime(r'%d%m%Y$%H:%M:%S'),
                'ip': row.Transaction.ip_address or row.Investment.ip_address,
                'server_ip': config.SERVER_IP,
                'trx': row.Transaction.id
            }
            nr.log_WT = '{dt}${ip}${server_ip}${trx}'.format(**params)
            # Mandatory if PAN of the 1st holder is provided
            nr.pan_validation = 'Y'

            nr.euin = config.EUIN
            nr.euin_optin = 'Y'
            nr.income_group = INCOME_GROUP[
                                          str(row.Customer.annual_income_code)
                                          ]
            nr.e6 = row.Investment.id
            new_rows.append(nr)

        parsed_rows = self.prevent_duplicate_folio_creation(new_rows, KARVY)
        self.session.query(M.KarvyFeedOut).delete()
        self.session.add_all(parsed_rows)
        self.session.commit()
        LOG.info('Finished worker; transaction rows - {}'
                 .format(len(parsed_rows)))
        return len(parsed_rows or [])

    def insert_cams_transactions(self):
        """
        Starting with the scheme table, gets all PROCESSING
        transactions and inserts into CamsFeedOut
        """
        LOG.debug('Starting worker for [ CAMS ] feed out...')
        q = self.session.query(
            M.Scheme, M.Investment, M.Transaction, M.Customer
        ).filter(
            M.Scheme.id == M.Investment.scheme_id,
            M.Investment.id == M.Transaction.investment_id,
            M.Investment.customer_id == M.Customer.id,
            M.Transaction.status == TR_STATUS_PROCESSING,
            M.Transaction.transaction_type.in_((INVEST, REDEEM)),
            or_(M.Transaction.is_processed.is_(False),
                M.Transaction.is_processed.is_(None)),
            M.Scheme.rta == CAMS
        ).order_by(M.Investment.id).all()

        # the new rows will be inserted here.
        new_rows = []

        for row in q:
            nr = M.CamsFeedOut()
            nr.amc_code = row.Scheme.amc_code
            nr.broker_code = config.CAMS_BROKER_CODE
            nr.user_code = config.KARVY_FEED_USERCODE
            nr.user_transaction_no = row.Transaction.id
            if row.Investment.scheme_folio:
                if len(row.Investment.scheme_folio.split('/')) > 1:
                    nr.folio_no = row.Investment.scheme_folio.split('/')[0]
                    nr.check_digit_numn = row.Investment.scheme_folio.\
                        split('/')[1]
                else:
                    nr.folio_no = row.Investment.scheme_folio
            if row.Transaction.transaction_type == INVEST:
                nr.transaction_type = 'P'
            else:
                nr.transaction_type = 'R'
            nr.scheme_code = row.Scheme.rta_code
            nr.first_name = row.Customer.pan_name

            # convert dates to IST, db data is in UTC
            dt = row.Transaction.created_on
            ist_dt = dt + IST_OFFSET
            # Mandatory, MM/dd/yyyy
            nr.transaction_date = ist_dt
            nr.start_date = nr.transaction_date
            nr.end_date = nr.start_date + timedelta(days=6*365/12)
            nr.install_no = 1
            # Mandatory format hh:mm:ss
            nr.transaction_time = ist_dt.strftime(r'%H:%M:%S')

            city = (row.Customer.correspondence_city
                    or row.Customer.permanent_city)
            # for fresh purchases only
            if row.Transaction.is_first:
                # Mandatory
                nr.address_1 = (row.Customer.correspondence_address_1
                                or row.Customer.permanent_address_1
                                or '')
                nr.address_2 = (row.Customer.correspondence_address_2
                                or row.Customer.permanent_address_2
                                or '')

                # all address fields can be max 40 characters
                nr.address_1 = nr.address_1[:40]
                nr.address_2 = nr.address_2[:40]

                nr.city = city
                nr.pincode = (row.Customer.correspondence_pin
                              or row.Customer.permanent_pin)

            nr.amount = row.Transaction.amount
            if not nr.folio_no and nr.amount < 5000:
                nr.sub_transaction_type = 'S'
            elif nr.folio_no and nr.amount < 1000:
                nr.sub_transaction_type = 'S'
            else:
                nr.sub_transaction_type = 'Normal'

            if nr.sub_transaction_type == 'S':
                nr.sip_registration_number = row.Transaction.id
                nr.sip_registration_date = ist_dt
                nr.sip_registration_number = row.Transaction.id
                nr.sip_freq = 'OM'
                nr.number_of_installments = 6   # hard-coded.

            nr.pan_number = row.Customer.pan_number
            if row.Customer.is_email_verified:
                nr.email = row.Customer.email
            else:
                nr.email = 'customer+{}@sqrrl.in'.format(row.Customer.uid)

            # bank details
            nr.account_number = row.Customer.bank_account_no
            nr.account_type = 'SB'  # Savings
            nr.bank_name = row.Customer.bank_name
            nr.branch_name = row.Customer.bank_branch_name
            nr.bank_city = row.Customer.bank_branch_city
            nr.reinv_tag = row.Scheme.dividend_reinvestment_option
            nr.neft_code = row.Customer.bank_ifsc
            nr.rtgs_code = row.Customer.bank_ifsc
            # occupation_code
            nr.occupation_code = row.Customer.occupation_code
            nr.tax_status = '01'    # individual

            # state code, location code
            state = (row.Customer.correspondence_state
                     or row.Customer.permanent_state)
            nr.state = M.CamsStateLookup.get_code(self.session,
                                                  state, nr.amc_code)

            # always send code for New Delhi
            nr.location_code = 'D1'
            nr.dob = row.Customer.date_of_birth
            if 'liquid' in row.Scheme.category.lower():
                nr.pricing = 'H'
            else:
                nr.pricing = 'F'
            nr.mobile_no = row.Customer.mobile

            # For purchase / additional purchase only
            if row.Transaction.transaction_type == INVEST:
                nr.holding_nature = 'SI'    # Single holding

            # insert
            if nr.transaction_date:
                date = nr.transaction_date.strftime('%d/%m/%y')
                time = nr.transaction_date.strftime('%H:%M:%S')
            else:
                date = ''
                time = ''
            nr.remarks = '$'.join([str(nr.user_transaction_no), date, time])
            nr.dummy4_c = row.Investment.id
            new_rows.append(nr)

        parsed_rows = self.prevent_duplicate_folio_creation(new_rows, CAMS)
        # truncate table, then push data
        self.session.query(M.CamsFeedOut).delete()
        self.session.add_all(parsed_rows)
        self.session.commit()
        LOG.info('Finished CAMS worker; transactions - {}'
                 .format(len(parsed_rows or []))
                 )
        return len(parsed_rows or [])

    def create_karvy_dbf(self):
        """
        Generates dbf for KarvyFeedOut as per a given format,
        and emails it to operations.
        """
        utc_n = datetime.utcnow().isoformat()
        file_name = '/tmp/karvy{}'.format(utc_n)
        dbf_file = dbf.Table(file_name,
                             'AMC_CODE C(3);\
                              BROKE_CD C(20);\
                              SBBR_CODE C(15);\
                              USER_CODE C(10);\
                              USR_TXN_NO N(10, 0);\
                              APPL_NO  C(10);\
                              FOLIO_NO C(15);\
                              CK_DIG_NO C(10);\
                              TRXN_TYPE C(7);\
                              SCH_CODE C(5);\
                              FIRST_NAME C(70);\
                              JONT_NAME1 C(35);\
                              JONT_NAME2 C(35);\
                              ADD1 C(40);\
                              ADD2 C(40);\
                              ADD3 C(40);\
                              CITY C(35);\
                              PINCODE C(15);\
                              PHONE_OFF C(15);\
                              MOBILE_NO C(15);\
                              TRXN_DATE D;\
                              TRXN_TIME C(10);\
                              UNITS N(19, 3);\
                              AMOUNT N(16, 5);\
                              CLOS_AC_CH C(1);\
                              DOB D;\
                              GUARDIAN C(35);\
                              TAX_NUMBER C(40);\
                              PHONE_RES C(15);\
                              FAX_OFF C(15);\
                              FAX_RES C(15);\
                              EMAIL C(50);\
                              ACCT_NO C(40);\
                              ACCT_TYPE C(5);\
                              BANK_NAME C(40);\
                              BR_NAME C(40);\
                              BANK_CITY C(35);\
                              REINV_TAG C(1);\
                              HOLD_NATUR C(2);\
                              OCC_CODE C(2);\
                              TAX_STATUS C(2);\
                              REMARKS C(250);\
                              STATE C(2);\
                              PAN_2_HLDR C(40);\
                              PAN_3_HLDR C(40);\
                              GUARD_PAN C(40);\
                              LOCATION C(10);\
                              UINNO C(10);\
                              FORM6061 C(1);\
                              FORM6061J1 C(1);\
                              FORM6061J2 C(1);\
                              PAY_MEC C(1);\
                              RTGS_CD C(11);\
                              NEFT_CD C(11);\
                              MICR_CD C(9);\
                              DEPBANK C(40);\
                              DEP_ACNO C(50);\
                              DEP_DATE D;\
                              DEP_RFNO C(25);\
                              SUB_TRXN_T C(6);\
                              SIP_RFNO C(8);\
                              SIP_RGDT D;\
                              NOM_NAME C(40);\
                              NOM_RELA C(40);\
                              KYC_FLG C(1);\
                              POA_STAT C(1);\
                              MOD_TRXN C(1);\
                              SIGN_VF C(1);\
                              CUST_ID C(12);\
                              LOG_WT C(70);\
                              LOG_PE C(70);\
                              DPID C(25);\
                              CLIENTID C(25);\
                              NRI_SOF C(25);\
                              EUIN C(16);\
                              EUIN_OPT C(1);\
                              INCSLAB C(30);\
                              NET_WOR_ N(18, 2);\
                              NETWOR_DT D;\
                              INCSLAB_J1 C(30);\
                              NET_WOR_J1 N(18, 2);\
                              NETDATE_J1 D;\
                              PEP_J1 C(1);\
                              INCSLAB_J2 C(30);\
                              NET_WOR_J2 N(18, 2);\
                              NETDATE_J2 D;\
                              PEP_J2 C(1);\
                              INCSLAB_J3 C(30);\
                              NET_WOR_J3 N(18,2);\
                              NETDATE_J3 D;\
                              PEP_J3 C(1);\
                              INCSLAB_GR C(30);\
                              NET_WOR_GR N(18, 2);\
                              NETDATE_GR D;\
                              PEP_GR C(1);\
                              FOREX_MCS C(1);\
                              GAME_GABLE C(1);\
                              LS_ML_PA C(1);\
                              SIP_ST_DT D;\
                              SIP_END_DT D;\
                              SIP_FQ C(3);\
                              SIP_AMT N(16, 5);\
                              NRI_ADD1 C(40);\
                              NRI_ADD2 C(40);\
                              NRI_ADD3 C(40);\
                              NRI_CITY C(35);\
                              NRI_STATE C(35);\
                              NRI_CON C(35);\
                              NRI_PIN C(15);\
                              NOM2_NAME C(40);\
                              NOM2_REL C(40);\
                              NOM3_NAME C(40);\
                              NOM3_REL C(40);\
                              NOM_PER N(7, 2);\
                              NOM2_PER N(7, 2);\
                              NOM3_PER N(7, 2);\
                              EMP_CODE C(1);\
                              SUB_ARN C(20);\
                              FATCA_FLAG C(1);\
                              DUMMY1 C(20);\
                              DUMMY2 C(20);\
                              DUMMY3 C(20);\
                              DUMMY4 N(14, 0);\
                              DUMMY5 N(14, 0);\
                              DUMMY6 N(14, 0);\
                              DUMMY7 N(14, 0);\
                              DUMMY8 D;\
                              DUMMY9 D;\
                              DUMMY10 D;\
                              DUMMY11 C(20);'
                             )
        dbf_file.open(dbf.READ_WRITE)
        for row in self.session.query(M.KarvyFeedOut).all():
            dbf_file.append(
                    (row.amc_code,
                     row.broker_code,
                     row.sub_broker_code,
                     row.user_code,
                     row.user_transaction_number,
                     row.application_number,
                     row.folio_number,
                     row.cheque_digit_number,
                     row.transaction_type,
                     row.scheme_code,
                     row.first_name,
                     row.joint_name1,
                     row.joint_name2,
                     row.address1,
                     row.address2,
                     row.address3,
                     row.city,
                     row.pincode,
                     row.phone_num_office,
                     row.mobile_number,
                     dbf.Date(row.transaction_date.year, row.transaction_date.month, row.transaction_date.day),  # NOQA
                     row.transaction_time,
                     row.units,
                     row.amount,
                     row.close_account_ch,
                     dbf.Date(row.date_of_birth.year, row.date_of_birth.month, row.date_of_birth.day),  # NOQA
                     row.guardian_name,
                     row.pan_number,
                     row.phone_residence,
                     row.office_fax_num,
                     row.residence_fax_num,
                     row.email,
                     row.account_number,
                     row.account_type,
                     row.bank_name,
                     row.branch_name,
                     row.bank_city,
                     row.reinvest_tag,
                     row.hold_nature,
                     row.occupation_code,
                     row.tax_status,
                     row.remarks,
                     row.state,
                     row.second_pan_holder,
                     row.third_pan_holder,
                     row.guardian_name,
                     row.location,
                     row.unino,
                     row.form6061,
                     row.form6061j1,
                     row.form6061j2,
                     row.payment_mechanism,
                     row.rtgs_code,
                     row.neft_code,
                     row.micr_code,
                     row.dep_bank,
                     row.dep_acno,
                     dbf.Date(row.dep_date.year, row.dep_date.month, row.dep_date.day) if row.dep_date else None,  # NOQA
                     row.dep_reference_number,
                     row.sub_transaction_type,
                     row.sip_reference_number,
                     dbf.Date(row.sip_registration_date.year, row.sip_registration_date.month, row.sip_registration_date.day) if row.sip_registration_date else None,  # NOQA
                     row.nominee_name,
                     row.nominee_relation,
                     row.kyc_flag,
                     row.poa_stat,
                     row.mode_transaction,
                     row.sign_verification,
                     row.customer_id,
                     row.log_WT,
                     row.log_PE,
                     row.dpid,
                     row.client_id,
                     row.nri_source_of_fund,
                     row.euin,
                     row.euin_optin,
                     '',
                     None,
                     None,
                     None,
                     None,
                     None,
                     row.politically_exposed,
                     '',
                     None,
                     None,
                     '',
                     '',
                     None,
                     None,
                     '',
                     '',
                     0.00,
                     None,
                     '',
                     row.occupation_code_flag,
                     'N',
                     'N',
                     dbf.Date(row.sip_stdt.year, row.sip_stdt.month, row.sip_stdt.day) if row.sip_stdt else None,  # NOQA
                     dbf.Date(row.sip_endt.year, row.sip_endt.month, row.sip_endt.day) if row.sip_endt else None,  # NOQA
                     '',
                     row.sip_amount,
                     row.nri_address1,
                     row.nri_address2,
                     row.nri_address3,
                     row.nri_city,
                     row.nri_state,
                     row.nri_country,
                     row.nri_pincode,
                     row.nominee2_name,
                     row.nominee2_relation,
                     '',
                     '',
                     None,
                     None,
                     None,
                     None,
                     None,
                     'Y',
                     None,
                     None,
                     None,
                     None,
                     None,
                     None,
                     None,
                     None,
                     None,
                     None,
                     None,)
                )
        dbf_file.close()
        # email it to operations
        if not config.DEBUG:
            sp.transmissions.send(
                recipients=['operations@sqrrl.in'],
                bcc=['dev@sqrrl.in'],
                text='PFA.',
                subject='Karvy dbf feed out',
                from_email='dev@sqrrl.in',
                attachments=[
                    {
                        'name': 'karvy-feed-out.dbf',
                        'type': 'application/dbf',
                        'filename': file_name + '.dbf'
                    }
                ]
            )
        return file_name+'.dbf'

    def create_karvy_xml(self):
        """
        returns xml file containing transactions of particular
        sub_transaction_type of a particular AMC.

        STEP 1:
        call construct_karvy_xml_for_trxn to create XML code for a particular
        transaction.
        STEP 2:
        Group the transactions according to amc_code and sub_transaction_type
        using if else loop.
        """
        # Generate one xml per AMC-Subtrx combination
        rows = self.session.query(M.KarvyFeedOut).order_by(
            M.KarvyFeedOut.amc_code,
            M.KarvyFeedOut.sub_transaction_type
        ).all()

        # extract rows by AMC, then generate one XML per AMC
        root = etree.Element('file')

        # will return all AMC codes and file names to caller
        result = []

        for key, grouper in groupby(
            rows,
            lambda r: r.amc_code
        ):
            # list of transactions for this xml
            tg_id_lst = []

            # generate xml for this AMC
            amc_rows = list(grouper)
            LOG.debug('key: {}, rows: {}'.format(key, len(amc_rows)))
            # create a file name for XML using datetime
            utc_n = datetime.utcnow().isoformat()
            file_name = '/tmp/karvy{}.xml'.format(utc_n)
            LOG.debug('Writing to {}'.format(file_name))

            # outer tag for xml with file name
            root = etree.Element('file')
            tree = etree.ElementTree(root)

            # insert xml rows for each transaction
            for row in amc_rows:
                # LOG.debug('appending row {}'.format(row.id))
                xml_tree = self.construct_karvy_xml_for_trxn(row)
                root.append(xml_tree)
                # maintain a list of transaction id for each file
                tg_id_lst.append(row.user_transaction_number)
            tree.write(file_name)
            # LOG.debug('Written')
            # adding all trx ids to redis key
            redis_client.sadd(file_name, *tg_id_lst)
            result.append((key, file_name))

            # upload to S3
            key = file_name.split('/', 1)[-1]
            try:
                s3_file_upload(key, file_name)
            except Exception as ex:
                LOG.error(ex, exc_info=True)
        return result

    def construct_karvy_xml_for_trxn(self, row):
        """
        constructs XML for a transaction rows
        """
        parent = etree.Element('request_dtl')
        AMC_CODE = etree.SubElement(parent, 'AMC_CODE')
        BROKE_CD = etree.SubElement(parent, 'BROKE_CD')
        SBBR_CODE = etree.SubElement(parent, 'SBBR_CODE')
        USER_CODE = etree.SubElement(parent, 'USER_CODE')
        USR_TXN_NO = etree.SubElement(parent, 'USR_TXN_NO')
        APPL_NO = etree.SubElement(parent, 'APPL_NO')
        FOLIO_NO = etree.SubElement(parent, 'FOLIO_NO')
        CK_DIG_NO = etree.SubElement(parent, 'CK_DIG_NO')
        TRXN_TYPE = etree.SubElement(parent, 'TRXN_TYPE')
        SCH_CODE = etree.SubElement(parent, 'SCH_CODE')
        FIRST_NAME = etree.SubElement(parent, 'FIRST_NAME')
        JONT_NAME1 = etree.SubElement(parent, 'JONT_NAME1')
        JONT_NAME2 = etree.SubElement(parent, 'JONT_NAME2')
        ADD1 = etree.SubElement(parent, 'ADD1')
        ADD2 = etree.SubElement(parent, 'ADD2')
        ADD3 = etree.SubElement(parent, 'ADD3')
        CITY = etree.SubElement(parent, 'CITY')
        PINCODE = etree.SubElement(parent, 'PINCODE')
        PHONE_OFF = etree.SubElement(parent, 'PHONE_OFF')
        MOBILE_NO = etree.SubElement(parent, 'MOBILE_NO')
        TRXN_DATE = etree.SubElement(parent, 'TRXN_DATE')
        TRXN_TIME = etree.SubElement(parent, 'TRXN_TIME')
        UNITS = etree.SubElement(parent, 'UNITS')
        AMOUNT = etree.SubElement(parent, 'AMOUNT')
        CLOS_AC_CH = etree.SubElement(parent, 'CLOS_AC_CH')
        DOB = etree.SubElement(parent, 'DOB')
        GUARDIAN = etree.SubElement(parent, 'GUARDIAN')
        TAX_NUMBER = etree.SubElement(parent, 'TAX_NUMBER')
        PHONE_RES = etree.SubElement(parent, 'PHONE_RES')
        FAX_OFF = etree.SubElement(parent, 'FAX_OFF')
        FAX_RES = etree.SubElement(parent, 'FAX_RES')
        EMAIL = etree.SubElement(parent, 'EMAIL')
        ACCT_NO = etree.SubElement(parent, 'ACCT_NO')
        ACCT_TYPE = etree.SubElement(parent, 'ACCT_TYPE')
        BANK_NAME = etree.SubElement(parent, 'BANK_NAME')
        BR_NAME = etree.SubElement(parent, 'BR_NAME')
        BANK_CITY = etree.SubElement(parent, 'BANK_CITY')
        REINV_TAG = etree.SubElement(parent, 'REINV_TAG')
        HOLD_NATUR = etree.SubElement(parent, 'HOLD_NATUR')
        OCC_CODE = etree.SubElement(parent, 'OCC_CODE')
        TAX_STATUS = etree.SubElement(parent, 'TAX_STATUS')
        REMARKS = etree.SubElement(parent, 'REMARKS')
        STATE = etree.SubElement(parent, 'STATE')
        PAN_2_HLDR = etree.SubElement(parent, 'PAN_2_HLDR')
        PAN_3_HLDR = etree.SubElement(parent, 'PAN_3_HLDR')
        GUARD_PAN = etree.SubElement(parent, 'GUARD_PAN')
        LOCATION = etree.SubElement(parent, 'LOCATION')
        UINNO = etree.SubElement(parent, 'UINNO')
        FORM6061 = etree.SubElement(parent, 'FORM6061')
        FORM6061J1 = etree.SubElement(parent, 'FORM6061J1')
        FORM6061J2 = etree.SubElement(parent, 'FORM6061J2')
        PAY_MEC = etree.SubElement(parent, 'PAY_MEC')
        RTGS_CD = etree.SubElement(parent, 'RTGS_CD')
        NEFT_CD = etree.SubElement(parent, 'NEFT_CD')
        MICR_CD = etree.SubElement(parent, 'MICR_CD')
        DEPBANK = etree.SubElement(parent, 'DEPBANK')
        DEP_ACNO = etree.SubElement(parent, 'DEP_ACNO')
        DEP_DATE = etree.SubElement(parent, 'DEP_DATE')
        DEP_RFNO = etree.SubElement(parent, 'DEP_RFNO')
        SUB_TRXN_T = etree.SubElement(parent, 'SUB_TRXN_T')
        SIP_RFNO = etree.SubElement(parent, 'SIP_RFNO')
        SIP_RGDT = etree.SubElement(parent, 'SIP_RGDT')
        NOM_NAME = etree.SubElement(parent, 'NOM_NAME')
        NOM_RELA = etree.SubElement(parent, 'NOM_RELA')
        KYC_FLG = etree.SubElement(parent, 'KYC_FLG')
        POA_STAT = etree.SubElement(parent, 'POA_STAT')
        MOD_TRXN = etree.SubElement(parent, 'MOD_TRXN')
        SIGN_VF = etree.SubElement(parent, 'SIGN_VF')
        CUST_ID = etree.SubElement(parent, 'CUST_ID')
        LOG_WT = etree.SubElement(parent, 'LOG_WT')
        LOG_PE = etree.SubElement(parent, 'LOG_PE')
        DPID = etree.SubElement(parent, 'DPID')
        CLIENTID = etree.SubElement(parent, 'CLIENTID')
        NRI_SOF = etree.SubElement(parent, 'NRI_SOF')
        GUARD_RELA = etree.SubElement(parent, 'GUARD_RELA')
        PAN_VALI = etree.SubElement(parent, 'PAN_VALI')
        GPAN_VALI = etree.SubElement(parent, 'GPAN_VALI')
        J1PAN_VALI = etree.SubElement(parent, 'J1PAN_VALI')
        J2PAN_VALI = etree.SubElement(parent, 'J2PAN_VALI')
        NRI_ADD1 = etree.SubElement(parent, 'NRI_ADD1')
        NRI_ADD2 = etree.SubElement(parent, 'NRI_ADD2')
        NRI_ADD3 = etree.SubElement(parent, 'NRI_ADD3')
        NRI_CITY = etree.SubElement(parent, 'NRI_CITY')
        NRI_STATE = etree.SubElement(parent, 'NRI_STATE')
        NRI_COUN = etree.SubElement(parent, 'NRI_COUN')
        NRI_PIN = etree.SubElement(parent, 'NRI_PIN')
        NOM_DOB = etree.SubElement(parent, 'NOM_DOB')
        NOM_PERC = etree.SubElement(parent, 'NOM_PERC')
        NOM_ADD1 = etree.SubElement(parent, 'NOM_ADD1')
        NOM_ADD2 = etree.SubElement(parent, 'NOM_ADD2')
        NOM_ADD3 = etree.SubElement(parent, 'NOM_ADD3')
        NOM_CITY = etree.SubElement(parent, 'NOM_CITY')
        NOM_STATE = etree.SubElement(parent, 'NOM_STATE')
        NOM_COUN = etree.SubElement(parent, 'NOM_COUN')
        NOM_PIN = etree.SubElement(parent, 'NOM_PIN')
        NOM_EMAIL = etree.SubElement(parent, 'NOM_EMAIL')
        NOM_MOB = etree.SubElement(parent, 'NOM_MOB')
        NOM1_NAME = etree.SubElement(parent, 'NOM1_NAME')
        NOM1_RELA = etree.SubElement(parent, 'NOM1_RELA')
        NOM1_DOB = etree.SubElement(parent, 'NOM1_DOB')
        NOM1_PERC = etree.SubElement(parent, 'NOM1_PERC')
        NOM1_ADD1 = etree.SubElement(parent, 'NOM1_ADD1')
        NOM1_ADD2 = etree.SubElement(parent, 'NOM1_ADD2')
        NOM1_ADD3 = etree.SubElement(parent, 'NOM1_ADD3')
        NOM1_CITY = etree.SubElement(parent, 'NOM1_CITY')
        NOM1_STATE = etree.SubElement(parent, 'NOM1_STATE')
        NOM1_COUN = etree.SubElement(parent, 'NOM1_COUN')
        NOM1_PIN = etree.SubElement(parent, 'NOM1_PIN')
        NOM1_EMAIL = etree.SubElement(parent, 'NOM1_EMAIL')
        NOM1_MOB = etree.SubElement(parent, 'NOM1_MOB')
        NOM2_NAME = etree.SubElement(parent, 'NOM2_NAME')
        NOM2_RELA = etree.SubElement(parent, 'NOM2_RELA')
        NOM2_DOB = etree.SubElement(parent, 'NOM2_DOB')
        NOM2_PERC = etree.SubElement(parent, 'NOM2_PERC')
        NOM2_ADD1 = etree.SubElement(parent, 'NOM2_ADD1')
        NOM2_ADD2 = etree.SubElement(parent, 'NOM2_ADD2')
        NOM2_ADD3 = etree.SubElement(parent, 'NOM2_ADD3')
        NOM2_CITY = etree.SubElement(parent, 'NOM2_CITY')
        NOM2_STATE = etree.SubElement(parent, 'NOM2_STATE')
        NOM2_COUN = etree.SubElement(parent, 'NOM2_COUN')
        NOM2_PIN = etree.SubElement(parent, 'NOM2_PIN')
        NOM2_EMAIL = etree.SubElement(parent, 'NOM2_EMAIL')
        NOM2_MOB = etree.SubElement(parent, 'NOM2_MOB')
        FIRC_STA = etree.SubElement(parent, 'FIRC_STA')
        SIP_STDT = etree.SubElement(parent, 'SIP_STDT')
        SIP_ENDT = etree.SubElement(parent, 'SIP_ENDT')
        SIP_NOINST = etree.SubElement(parent, 'SIP_NOINST')
        SIP_FREQ = etree.SubElement(parent, 'SIP_FREQ')
        EUIN = etree.SubElement(parent, 'EUIN')
        EUIN_OPTIN = etree.SubElement(parent, 'EUIN_OPTIN')
        E1 = etree.SubElement(parent, 'E1')
        E2 = etree.SubElement(parent, 'E2')
        E3 = etree.SubElement(parent, 'E3')
        E4 = etree.SubElement(parent, 'E4')
        E5 = etree.SubElement(parent, 'E5')
        E6 = etree.SubElement(parent, 'E6')

        AMC_CODE.text = row.amc_code
        BROKE_CD.text = row.broker_code
        SBBR_CODE.text = row.sub_broker_code
        USER_CODE.text = row.user_code
        USR_TXN_NO.text = str(row.user_transaction_number)
        APPL_NO.text = row.application_number
        if not config.DEBUG:
            # in debug env the folio may not exist
            FOLIO_NO.text = row.folio_number
        CK_DIG_NO.text = row.cheque_digit_number
        TRXN_TYPE.text = row.transaction_type
        SCH_CODE.text = row.scheme_code
        FIRST_NAME.text = row.first_name
        JONT_NAME1.text = row.joint_name1
        JONT_NAME2.text = row.joint_name2
        ADD1.text = row.address1
        ADD2.text = row.address2
        ADD3.text = row.address3
        CITY.text = row.city
        PINCODE.text = row.pincode
        PHONE_OFF.text = row.phone_num_office
        MOBILE_NO.text = row.mobile_number
        if row.transaction_date:
            TRXN_DATE.text = row.transaction_date.strftime('%m/%d/%Y')
        TRXN_TIME.text = row.transaction_time
        UNITS.text = row.units
        AMOUNT.text = str(row.amount)
        CLOS_AC_CH.text = row.close_account_ch
        if row.date_of_birth:
            DOB.text = row.date_of_birth.strftime('%m/%d/%Y')
        GUARDIAN.text = row.guardian_name
        TAX_NUMBER.text = row.pan_number
        PHONE_RES.text = row.phone_residence
        FAX_OFF.text = row.office_fax_num
        FAX_RES.text = row.residence_fax_num
        EMAIL.text = row.email
        ACCT_NO.text = row.account_number
        ACCT_TYPE.text = row.account_type
        BANK_NAME.text = row.bank_name
        BR_NAME.text = row.branch_name
        BANK_CITY.text = row.bank_city
        REINV_TAG.text = row.reinvest_tag
        HOLD_NATUR.text = row.hold_nature
        OCC_CODE.text = row.occupation_code
        TAX_STATUS.text = row.tax_status
        REMARKS.text = row.remarks
        STATE.text = row.state
        PAN_2_HLDR.text = row.second_pan_holder
        PAN_3_HLDR.text = row.third_pan_holder
        GUARD_PAN.text = row.guardian_pan
        LOCATION.text = row.location
        UINNO.text = row.unino
        FORM6061.text = row.form6061
        FORM6061J1.text = row.form6061j1
        FORM6061J2.text = row.form6061j2
        PAY_MEC.text = row.payment_mechanism
        RTGS_CD.text = row.rtgs_code
        NEFT_CD.text = row.neft_code
        MICR_CD.text = row.micr_code
        DEPBANK.text = row.dep_bank
        DEP_ACNO.text = row.dep_acno
        if row.dep_date:
            DEP_DATE.text = row.dep_date.strftime('%m/%d/%Y')
        DEP_RFNO.text = row.dep_reference_number
        SUB_TRXN_T.text = row.sub_transaction_type
        SIP_RFNO.text = row.sip_reference_number
        if row.sip_registration_date:
            SIP_RGDT.text = row.sip_registration_date.strftime('%m/%d/%Y')
        NOM_NAME.text = row.nominee_name
        NOM_RELA.text = row.nominee_relation
        KYC_FLG.text = row.kyc_flag
        POA_STAT.text = row.poa_stat
        MOD_TRXN.text = row.mode_transaction
        SIGN_VF.text = row.sign_verification
        CUST_ID.text = row.customer_id
        LOG_WT.text = row.log_WT
        LOG_PE.text = row.log_PE
        DPID.text = row.dpid
        CLIENTID.text = row.client_id
        NRI_SOF.text = row.nri_source_of_fund
        GUARD_RELA.text = row.guardian_relation
        PAN_VALI.text = row.pan_validation
        GPAN_VALI.text = row.gpan_validation
        J1PAN_VALI.text = row.j1pan_validation
        J2PAN_VALI.text = row.j2pan_validation
        NRI_ADD1.text = row.nri_address1
        NRI_ADD2.text = row.nri_address2
        NRI_ADD3.text = row.nri_address3
        NRI_CITY.text = row.nri_city
        NRI_STATE.text = row.nri_state
        NRI_COUN.text = row.nri_country
        NRI_PIN.text = row.nri_pincode
        if row.nominee_date_of_birth:
            NOM_DOB.text = row.nominee_date_of_birth.strftime('%m/%d/%Y')
        NOM_PERC.text = row.nom_perc
        NOM_ADD1.text = row.nominee_address1
        NOM_ADD2.text = row.nominee_address2
        NOM_ADD3.text = row.nominee_address3
        NOM_CITY.text = row.nominee_city
        NOM_STATE.text = row.nominee_state
        NOM_COUN.text = row.nominee_country
        NOM_PIN.text = row.nominee_pin
        NOM_EMAIL.text = row.nominee_email
        NOM_MOB.text = row.nominee_mobile
        NOM1_NAME.text = row.nominee1_name
        NOM1_RELA.text = row.nominee1_relation
        if row.nominee1_date_of_birth:
            NOM1_DOB.text = row.nominee1_date_of_birth.strftime('%m/%d/%Y')
        NOM1_PERC.text = row.nominee1_perc
        NOM1_ADD1.text = row.nominee1_address1
        NOM1_ADD2.text = row.nominee1_address2
        NOM1_ADD3.text = row.nominee1_address3
        NOM1_CITY.text = row.nominee1_city
        NOM1_STATE.text = row.nominee1_state
        NOM1_COUN.text = row.nominee1_country
        NOM1_PIN.text = row.nominee1_pincode
        NOM1_EMAIL.text = row.nominee1_email
        NOM1_MOB.text = row.nominee1_mobile
        NOM2_NAME.text = row.nominee2_name
        NOM2_RELA.text = row.nominee2_relation
        if row.nominee2_date_of_birth:
            NOM2_DOB.text = row.nominee2_date_of_birth.strftime('%m/%d/%Y')
        NOM2_PERC.text = row.nominee2_perc
        NOM2_ADD1.text = row.nominee2_address1
        NOM2_ADD2.text = row.nominee2_address2
        NOM2_ADD3.text = row.nominee2_address3
        NOM2_CITY.text = row.nominee2_city
        NOM2_STATE.text = row.nominee2_state
        NOM2_COUN.text = row.nominee2_country
        NOM2_PIN.text = row.nominee2_pincode
        NOM2_EMAIL.text = row.nominee2_email
        NOM2_MOB.text = row.nominee2_mobile
        FIRC_STA.text = row.firc_sta
        if row.sip_stdt:
            SIP_STDT.text = row.sip_stdt.strftime('%m/%d/%Y')
        if row.sip_endt:
            SIP_ENDT.text = row.sip_endt.strftime('%m/%d/%Y')
        SIP_NOINST.text = row.sip_noinst
        SIP_FREQ.text = row.sip_freq
        EUIN.text = row.euin
        EUIN_OPTIN.text = row.euin_optin
        E1.text = row.e1
        E2.text = row.e2
        E3.text = row.e3
        E4.text = row.e4
        E5.text = row.e5
        E6.text = row.e6
        return parent

    def create_cams_dbf(self):
        """
        Generates feed out file for CAMS as per a specified format, and
        emails it to operations.

        Returns: None
        """
        LOG.info('starting CAMS dbf creation.........')
        pattern = REDIS_CAMS_FEEDFILE_PREFIX + '*'
        r_keys = redis_client.keys(pattern)

        # delete all pre-existing keys
        for key in r_keys:
            redis_client.delete(key)

        amc_code_flag = ''
        # creates a list of AMC codes for all records in M.CamsFeedOut
        rows = self.session.query(M.CamsFeedOut).order_by(
                        M.CamsFeedOut.amc_code).all()
        amc_codes_list = [row.amc_code for row in rows]
        # Intra-day serial no of the file
        file_num = 1
        # Boolean to check if a file has been created
        file_obj_created = False

        for row in rows:
            # if amc_code is different create a new file
            if row.amc_code != amc_code_flag:
                # if there is an open file close the file
                if file_obj_created:
                    dbf_file.close() # NOQA

                amc_code_flag = row.amc_code
                # count number of records in a feed file
                record_count = '{0:04d}'.format(
                        amc_codes_list.count(amc_code_flag))

                amc_code_str = '{:<04}'.format(amc_code_flag)
                file_no = '{0:04d}'.format(file_num)
                # get time in IST
                timestamp = datetime.utcnow() + timedelta(hours=5, minutes=30)
                timestr = timestamp.strftime('%Y%m%d%H%M%S')
                file_name = ('/tmp/' + amc_code_str + record_count
                             + file_no + timestr + '.dbf')

                key = REDIS_CAMS_FEEDFILE_PREFIX + file_name
                redis_client.set(key, file_name)
                # increase the file_num for next file
                file_num += 1
                dbf_file = dbf.Table(file_name,
                            'AMC_CODE C(3);\
                              BROKE_CD C(20);\
                              SBBR_CODE C(15);\
                              USER_CODE C(10);\
                              USR_TXN_NO N(10, 0);\
                              APPL_NO  C(10);\
                              FOLIO_NO C(15);\
                              CK_DIG_NO C(10);\
                              TRXN_TYPE C(7);\
                              SCH_CODE C(5);\
                              FIRST_NAME C(70);\
                              JONT_NAME1 C(35);\
                              JONT_NAME2 C(35);\
                              ADD1 C(40);\
                              ADD2 C(40);\
                              ADD3 C(40);\
                              CITY C(35);\
                              PINCODE C(15);\
                              PHONE_OFF C(15);\
                              TRXN_DATE D;\
                              TRXN_TIME C(10);\
                              UNITS N(19, 3);\
                              AMOUNT N(16, 5);\
                              CLOS_AC_CH C(1);\
                              DOB D;\
                              GUARDIAN C(35);\
                              TAX_NUMBER C(40);\
                              PHONE_RES C(15);\
                              FAX_OFF C(15);\
                              FAX_RES C(15);\
                              EMAIL C(50);\
                              ACCT_NO C(40);\
                              ACCT_TYPE C(5);\
                              BANK_NAME C(40);\
                              BR_NAME C(40);\
                              BANK_CITY C(35);\
                              REINV_TAG C(1);\
                              HOLD_NATUR C(2);\
                              OCC_CODE C(2);\
                              TAX_STATUS C(2);\
                              REMARKS C(250);\
                              STATE C(2);\
                              SUB_TRXN_T C(6);\
                              DIV_PY_MEC C(7);\
                              ECS_NO C(1);\
                              BANK_CODE C(15);\
                              ALT_FOLIO C(1);\
                              ALT_BROKER C(20);\
                              LOCATION C(10);\
                              RED_PY_MEC C(7);\
                              PRICING C(1);\
                              PAN_2_HLDR C(40);\
                              PAN_3_HLDR C(40);\
                              NOM_NAME C(40);\
                              NOM_RELAT C(40);\
                              GUARDIAN_P C(45);\
                              INSTRMNO C(15);\
                              UINNO C(10);\
                              VALID_PAN C(1);\
                              GVALID_PAN C(1);\
                              JH1VALPAN C(1);\
                              JH2VALPAN C(1);\
                              FORM6061RE C(1);\
                              JH1F6061RE C(1);\
                              JH2F6061RE C(1);\
                              GF6061RE C(1);\
                              GSIP_CODE C(30);\
                              SIP_REG_DA D;\
                              FIRST_HLDR C(10);\
                              JH1_MIN C(10);\
                              JH2_MIN C(10);\
                              GH_MIN C(10);\
                              NEFT_CODE C(11);\
                              RTGS_CODE C(11);\
                              EMAIL_ACST C(1);\
                              MOBILE_NO C(15);\
                              DP_ID C(16);\
                              POA_TYPE C(1);\
                              TRXN_MODE C(1);\
                              TRXN_SIGN_ C(1);\
                              ADDL_ADDRE C(55);\
                              ADDL_ADDR2 C(55);\
                              ADDL_ADDR3 C(55);\
                              CITY2 C(35);\
                              STATE2 C(2);\
                              COUNTRY C(35);\
                              PINCODE2 C(35);\
                              NOM1_APPLI C(3);\
                              NOM2_NAME C(40);\
                              NOM2_RELAT C(40);\
                              NOM2_APPLI C(1);\
                              NOM3_NAME C(40);\
                              NOM3_RELAT C(40);\
                              NOM3_APPLI C(1);\
                              CHECK_FLAG C(1);\
                              TH_P_PYT C(1);\
                              KYC C(1);\
                              FIRC_STA C(1);\
                              SIP_REG_NO N(15, 0);\
                              NO_OF_INST N(10, 0);\
                              SIP_FREQ C(5);\
                              START_DATE D;\
                              END_DATE D;\
                              INSTALL_NO N(10, 0);\
                              NOM_DOB_D D;\
                              NOM_MINOR_ C(1);\
                              NOM_GUARDI C(35);\
                              DUMMY1_C C(10);\
                              DUMMY2_N C(1);\
                              DUMMY3_N C(1);\
                              DUMMY4_C C(5);\
                              DUMMY5_C C(1);\
                              DUMMY6_N C(1);\
                              FH_PAN_EXM C(1);\
                              JH1_PAN_EX C(1);\
                              JH1_PAN_E2 C(1);\
                              GUARD_PAN_ C(1);\
                              FH_EXMPT_C C(2);\
                              JH1_EXMPT_ C(2);\
                              JH2_EXMPT_ C(2);\
                              GUAR_EXMPT C(2);\
                              FH_KRA_EXM C(10);\
                              JH1_KRA_EX C(10);\
                              JH2_KRA_EX C(10);\
                              GUARD_EXMP C(10);\
                              EUIN_OPTED C(1);\
                              EUIN C(10);\
                              NOMINATION C(1);\
                              SUB_BROKER C(20);\
                              BANK_MANDA C(1);\
                              FH_CKYC N(14,0);\
                              JH1_CKYC N(14,0);\
                              JH2_CKYC N(14,0);\
                              GRDN_CKYC N(14,0);\
                              JH1_DOB D;\
                              JH2_DOB D;\
                              GRDN_DOB D;'
                                     )
                dbf_file.open(dbf.READ_WRITE)
                file_obj_created = True
            dbf_file.append(
                    (row.amc_code,
                     row.broker_code,
                     row.sub_broker_code,
                     row.user_code,
                     row.user_transaction_no,
                     row.application_no,
                     row.folio_no,
                     row.check_digit_numn,
                     row.transaction_type,
                     row.scheme_code,
                     row.first_name,
                     row.joint_name1,
                     row.joint_name2,
                     row.address_1,
                     row.address_2,
                     None,
                     row.city,
                     row.pincode,
                     row.phone_off,
                     row.transaction_date,
                     row.transaction_time,
                     row.units,
                     row.amount,
                     row.clos_ac_ch,
                     dbf.Date(row.dob.year, row.dob.month, row.dob.day),  # NOQA
                     row.guardian,
                     row.pan_number,
                     row.phone_res,
                     row.fax_off,
                     row.fax_res,
                     row.email,
                     row.account_number,
                     row.account_type,
                     row.bank_name,
                     row.branch_name,
                     row.bank_city,
                     row.reinv_tag,
                     row.holding_nature,
                     row.occupation_code,
                     row.tax_status,
                     row.remarks,
                     row.state,
                     row.sub_transaction_type,
                     row.div_payout_mechanism,
                     row.ecs_number,
                     row.bank_code,
                     row.alt_folio,
                     row.alt_broker,
                     row.location_code,
                     row.red_payout_mechanism,
                     row.pricing,
                     row.pan_2_holder,
                     row.pan_3_holder,
                     row.nominee_name,
                     row.nominee_relation,
                     row.guardian_pan,
                     row.instrmno,
                     row.uin_number,
                     row.valid_pan,
                     row.guardian_pan_valid,
                     row.jh1valpan,
                     row.jh2valpan,
                     row.form6061re,
                     row.jh1f6061re,
                     row.jh2f6061re,
                     row.gf6061re,
                     row.gsip_code,
                     row.sip_registration_date,
                     row.first_hldr,
                     row.jh1_min,
                     row.jh2_min,
                     row.gh_min,
                     row.neft_code,
                     row.rtgs_code,
                     row.email_acst,
                     row.mobile_no,
                     row.dp_id,
                     row.poa_type,
                     row.trxn_mode,
                     row.trxn_sign,
                     row.addl_addre,
                     row.addl_addr2,
                     row.addl_addr3,
                     row.city2,
                     row.state2,
                     row.country,
                     row.pincode2,
                     row.nom1_appli,
                     row.nom2_name,
                     row.nom2_relat,
                     row.nom2_appli,
                     row.nom3_name,
                     row.nom3_relat,
                     row.nom3_appli,
                     row.check_flag,
                     row.third_party_payment,
                     row.kyc_status,
                     None,
                     row.sip_registration_number,
                     row.number_of_installments,
                     row.sip_freq,
                     row.start_date,
                     row.end_date,
                     row.install_no,
                     row.nominee_date_of_birth,
                     row.nominee_minor_flag,
                     row.nominee_guardian,
                     row.dummy1_c,
                     row.dummy2_n,
                     row.dummy3_n,
                     row.dummy4_c,
                     row.dummy5_c,
                     row.dummy6_n,
                     row.fh_pan_exm,
                     None,
                     None,
                     row.guardian_pan_exempt,
                     row.fh_exmpt_c,
                     row.jh1_exmpt_c,
                     row.jh2_exmpt_c,
                     row.guar_exmpt_c,
                     row.fh_kra_exmpt_ref,
                     row.jh1_kra_exmpt_ref,
                     row.jh2_kra_exmpt_ref,
                     row.guard_exmpt_ref,
                     row.euin_opted,
                     row.euin,
                     row.nomination,
                     row.sub_broker,
                     row.bank_mandate_proof_submit)
                    )
        # email cams files to operations
        FeedOutWorker.upload_to_s3_and_mail_files(REDIS_CAMS_FEEDFILE_PREFIX)

    @staticmethod
    def upload_to_s3_and_mail_files(key_pattern):
        """
        Mails files returned by the key_pattern in redis_client to operations.
        """
        search_str = key_pattern + '*'
        r_keys = redis_client.keys(search_str)
        try:
            file_list = [key.decode().split(key_pattern)[-1] for key in r_keys]
        except Exception as ex:
            LOG.error(ex, exc_info=True)
            raise ex

        attachments = []

        for filepath in file_list:
            upload_key = filepath.split('/', 1)[-1] # NOQA
            try:
                LOG.debug('Uploading {} to {}'.format(filepath, upload_key))
                s3_file_upload(upload_key, filepath) # NOQA
            except Exception as ex:
                LOG.error(ex, exc_info=True)
                raise ex

        if not config.DEBUG:
            for filepath in file_list:
                attachmnt = {
                    'name': filepath,
                    'type': 'application/dbf',
                    'filename': filepath
                }
                attachments.append(attachmnt)
            response = sp.transmission.send(
                recipients=['operations@sqrrl.in'],
                bcc=['dev@sqrrl.in'],
                text='Have a Nice day ahead !',
                subject='CAMS dbf feed out',
                from_email='dev@sqrrl.in',
                attachments=attachments
            )
            if response and response['id']:
                import os
                for f in file_list:
                    os.remove(f)

    def insert_FT_transactions(self):
        """
        Starting with the scheme table, gets all PROCESSING
        transactions and inserts into KarvyFeedOut
        """
        LOG.debug('Starting worker for FT feed out...')
        q = self.session.query(
            M.Scheme, M.Investment, M.Transaction, M.Customer, M.Product
        ).filter(
            M.Scheme.id == M.Investment.scheme_id,
            M.Investment.id == M.Transaction.investment_id,
            M.Investment.customer_id == M.Customer.id,
            M.Transaction.status == TR_STATUS_PROCESSING,
            or_(M.Transaction.is_processed.is_(False),
                M.Transaction.is_processed.is_(None)),
            M.Scheme.rta == FT,
            M.Investment.product_id == M.Product.id
        ).order_by(M.Investment.id).all()
        new_rows = []
        for row in q:
            nr = M.IntFTFeedOut()
            nr.amc_code = row.Scheme.amc_code_out or row.Scheme.amc_code
            nr.broker_code = config.CAMS_BROKER_CODE
            nr.user_code = config.KARVY_FEED_USERCODE
            # For FT, usr_txn_no must start from 200001. Hence the offset.
            nr.usr_txn_no = FT_MIN_TR_ID + row.Transaction.id

            if row.Customer.is_email_verified:
                nr.email = row.Customer.email
            else:
                nr.email = 'customer+{}@sqrrl.in'.format(row.Customer.uid)

            if row.Transaction.transaction_type == INVEST:
                nr.trxn_type = 'P'
            else:
                nr.trxn_type = 'R'

            if row.Investment.scheme_folio:
                if len(row.Investment.scheme_folio.split('/')) > 1:
                    nr.folio_no = row.Investment.scheme_folio.split('/')[0]
                    nr.ck_dig_no = row.Investment.scheme_folio.\
                        split('/')[1]
                else:
                    nr.folio_no = row.Investment.scheme_folio
            nr.scheme_code = row.Scheme.rta_code
            nr.first_name = row.Customer.pan_name
            city = (row.Customer.correspondence_city
                    or row.Customer.permanent_city)
            nr.address1 = (row.Customer.correspondence_address_1
                           or row.Customer.permanent_address_1
                           or '')
            nr.address2 = (row.Customer.correspondence_address_2
                           or row.Customer.permanent_address_2
                           or '')
            nr.add1 = nr.address1[:40]
            nr.add2 = nr.address2[:40]
            nr.add3 = ''
            nr.city = city
            nr.pincode = (row.Customer.correspondence_pin
                          or row.Customer.permanent_pin)
            # nr.phone_off
            if row.Transaction.bank_transaction_on:
                nr.trxn_date = row.Transaction.bank_transaction_on.date()
                nr.trxn_time = row.Transaction.bank_transaction_on.time().\
                    strftime(r'%H:%M:%S')
            else:
                nr.trxn_date = None
            nr.amount = row.Transaction.amount
            if row.Customer.date_of_birth:
                nr.date_of_birth = row.Customer.date_of_birth
            else:
                nr.date_of_birth = None
            nr.guardian_name = ''
            nr.tax_number = row.Customer.pan_number
            nr.bank_account_no = row.Customer.bank_account_no
            nr.account_type = 'SB'  # Savings
            nr.bank_name = row.Customer.bank_name
            nr.br_name = row.Customer.bank_branch_name
            nr.bank_city = row.Customer.bank_branch_city
            nr.reinvestment_tag = row.Scheme.dividend_reinvestment_option
            nr.hold_nature = 'SI'
            nr.occ_code = row.Customer.occupation_code
            nr.tax_status = '01'
            # nr.remarks
            state = (row.Customer.correspondence_state
                     or row.Customer.permanent_state)
            nr.state = M.CamsStateLookup.get_code(self.session,
                                                  state, nr.amc_code)
            if row.Transaction.transaction_type == REDEEM:
                nr.sub_trxn_type = 'Normal'
            elif row.Transaction.transaction_type == INVEST:
                if not nr.folio_no and nr.amount < 5000:
                    nr.sub_transaction_type = 'S'
                elif nr.folio_number and nr.amount < 1000:
                    nr.sub_transaction_type = 'S'
                else:
                    nr.sub_transaction_type = 'Normal'
                if nr.sub_transaction_type == 'S':
                    nr.sipreg_num = row.Transaction.id

            nr.div_payout_code = 'DC'
            nr.redemption_payout_mec = 'DC'
            nr.location_code = 'D1'     # always send the code for Delhi
            nr.pan_validity_code = 'Y'
            if row.Transaction.transaction_type == INVEST:
                if row.Customer.modified_on:
                    nr.reg_date = row.Customer.modified_on
                else:
                    nr.reg_date = None
            else:
                nr.reg_date = None
            nr.neft_code = row.Customer.bank_ifsc
            nr.rtgs_code = row.Customer.bank_ifsc
            nr.kyc_flag = 'Y'
            nr.poa_stat = 'N'
            nr.mod_trxn = 'W'
            nr.sign_verification = 'Y'
            log_date = row.Transaction.modified_on
            date_str = log_date.strftime('%d%m%Y')
            time_str = log_date.strftime('%H:%M:%S')
            log_str = '$'.join([date_str, time_str,
                                str(nr.usr_txn_no)])
            nr.log_str = log_str
            if row.Transaction.transaction_type == INVEST:
                nr.th_p_pyt = 'N'
            nr.kyc = 'Y'
            nr.instl_no = ((row.Investment.completed_instalment_count or 0)
                           + 1)
            nr.total_inst = row.Investment.target_instalment_count or 12
            if row.Transaction.is_first:
                nr.new_flag = 'Y'
            else:
                nr.new_flag = 'N'
            nr.euin_statu = 'Y'
            nr.euin_no = config.EUIN
            nr.mob_no = row.Customer.mobile
            nr.nomi_avbl = 'N'
            nr.kra_verified = 'Y'
            nr.kra_agency = row.Customer.kra_name
            nr.fhld_ckyc = row.Customer.ckyc_id
            nr.investment_id = row.Investment.id
            new_rows.append(nr)

        parsed_rows = self.prevent_duplicate_folio_creation(new_rows, FT)
        if new_rows:
            # truncate table, then push data
            self.session.query(M.IntFTFeedOut).delete()
            self.session.add_all(parsed_rows)
            self.session.commit()
            LOG.info('Finished Franklin Templeton worker; transactions - {}'
                     .format(len(parsed_rows)))
        return len(parsed_rows or [])

    def create_FT_txtfile(self):
        """
        creates text file from int_FT_feedout table
        """
        amc_code = 'FTMF'
        today = datetime.utcnow()
        file_num = '0001'
        date_str = today.strftime('%Y%m%d%H%M%S')
        num_rec = len(self.session.query(M.IntFTFeedOut).all())
        if not num_rec:
            return
        num_rec = '{num:04d}'.format(num=num_rec)
        filename = '/tmp/' + amc_code + num_rec + file_num + date_str + '.txt'
        with io.open(filename, 'w') as text_file:
            rows = self.session.query(M.IntFTFeedOut).all()
            for i, row in enumerate(rows):
                lst = [row.amc_code, row.broker_code, '', row.user_code,
                       row.usr_txn_no, '',
                       row.folio_no, row.ck_dig_no, row.trxn_type,
                       row.scheme_code,
                       row.first_name, row.jont_name1, row.jont_name2,
                       row.add1, row.add2, row.add2, row.city, row.pincode,
                       '', row.trxn_date,
                       row.trxn_time, row.units, row.amount, row.clos_ac_ch,
                       row.date_of_birth, row.guardian_name, row.tax_number,
                       '', '', '', row.email, row.bank_account_no,
                       row.account_type,
                       row.bank_name, row.br_name, row.bank_city,
                       row.reinvestment_tag, row.hold_nature, row.occ_code,
                       row.tax_status, '', row.state, row.sub_trxn_type,
                       row.div_payout_code, row.ecs_no, '', row.alt_folio_no,
                       '', row.location_code, row.redemption_payout_mec, '',
                       row.pan_2_holder, row.pan_3_holder, row.nom_name,
                       row.nom_relation, row.guardian_pan, '', '',
                       row.pan_validity_code, row.grdn_pan_validity_code,
                       row.jh1valpan,
                       row.jh2valpan, '', '', '', '', row.reg_date, '', '',
                       '', '', row.neft_code, row.rtgs_code, row.kyc_flag,
                       row.poa_stat, row.mod_trxn, row.sign_verification,
                       row.log_str, row.addl_addr1, row.addl_addr2,
                       row.addl_addr3, row.addl_city, row.addl_state,
                       row.addcountry, row.addpincode, row.nom1_appli,
                       row.nom2_name, row.nom2_relat, row.nom2_appli,
                       row.nom3_name, row.nom3_relat,
                       row.nom3_appli, row.check_flag, row.th_p_pyt,
                       row.kyc, row.firc_sta, row.instl_no,
                       row.total_inst, row.new_flag, row.euin_statu,
                       row.euin_no, '', row.sipreg_num, '', '', row.mob_no,
                       row.nomi_avbl, row.kra_verified, row.kra_agency,
                       row.fhld_ckyc, '', '', '', '', '', '', '', '', '', '',
                       '']
                # LOG.debug('{} columns'.format(len(lst)))
                if i != 0:
                    text_file.write('\n')
                to_insert = (str(x or '').replace('|', '.') for x in lst)
                text_file.write('|'.join(to_insert))
        return filename

    def create_FT_dbf(self):
        """
        creates a dbf for FT feed
        """
        amc_code = 'FTMF'
        today = datetime.utcnow()
        file_num = '0001'
        date_str = today.strftime('%Y%m%d%H%M%S')
        num_rec = len(self.session.query(M.IntFTFeedOut).all())
        if not num_rec:
            return
        num_rec = '{num:04d}'.format(num=num_rec)
        filename = amc_code + num_rec + file_num + date_str
        file_name = '/tmp/{}'.format(filename)
        dbf_file = dbf.Table(file_name,
                             'AMC_CODE C(3);\
                              BROKE_CD C(20);\
                              SBBR_CODE C(15);\
                              USER_CODE C(10);\
                              USR_TXN_NO N(10, 0);\
                              APPL_NO  C(10);\
                              FOLIO_NO C(15);\
                              CK_DIG_NO C(10);\
                              TRXN_TYPE C(7);\
                              SCH_CODE C(5);\
                              FIRST_NAME C(70);\
                              JONT_NAME1 C(35);\
                              JONT_NAME2 C(35);\
                              ADD1 C(40);\
                              ADD2 C(40);\
                              ADD3 C(40);\
                              CITY C(35);\
                              PINCODE C(15);\
                              PHONE_OFF C(15);\
                              TRXN_DATE D;\
                              TRXN_TIME C(10);\
                              UNITS N(19, 3);\
                              AMOUNT N(16, 5);\
                              CLOS_AC_CH C(1);\
                              DOB D;\
                              GUARDIAN C(35);\
                              TAX_NUMBER C(40);\
                              PHONE_RES C(15);\
                              FAX_OFF C(15);\
                              FAX_RES C(15);\
                              EMAIL C(50);\
                              ACCT_NO C(40);\
                              ACCT_TYPE C(5);\
                              BANK_NAME C(40);\
                              BR_NAME C(40);\
                              BANK_CITY C(35);\
                              REINV_TAG C(1);\
                              HOLD_NATUR C(2);\
                              OCC_CODE C(2);\
                              TAX_STATUS C(2);\
                              REMARKS C(250);\
                              STATE C(2);\
                              SUB_TRXN_T C(6);\
                              DIV_PY_MEC C(7);\
                              ECS_NO C(1);\
                              BANK_CODE C(15);\
                              ALT_FOLIO C(1);\
                              ALT_BROKER C(20);\
                              LOCATION C(10);\
                              RED_PY_MEC C(7);\
                              PRICING C(1);\
                              PAN_2_HLDR C(40);\
                              PAN_3_HLDR C(40);\
                              NOM_NAME C(40);\
                              NOM_RELAT C(40);\
                              GUARDIAN_P C(45);\
                              INSTRMNO C(15);\
                              UINNO C(10);\
                              VALID_PAN C(1);\
                              GVALID_PAN C(1);\
                              JH1VALPAN C(1);\
                              JH2VALPAN C(1);\
                              FORM6061RE C(1);\
                              JH1F6061RE C(1);\
                              JH2F6061RE C(1);\
                              GF6061RE C(1);\
                              REG_DATE D;\
                              FH_MIN C(10);\
                              JH1_MIN C(10);\
                              JH2_MIN C(10);\
                              GH_MIN C(10);\
                              NEFT_CODE C(11);\
                              RTGS_CODE C(11);\
                              KYC_FLAG C(1);\
                              POA_STAT C(1);\
                              MOD_TRXN C(1);\
                              SIGN_VF C(1);\
                              LOG C(255);\
                              ADDL_ADDR1 C(40);\
                              ADDL_ADDR2 C(40);\
                              ADDL_ADDR3 C(40);\
                              ADDL_CITY C(35);\
                              ADDL_STATE C(35);\
                              AddCountry C(35);\
                              AddPincode C(35);\
                              NOM1_APPLI N(8,2);\
                              NOM2_NAME C(40);\
                              NOM2_RELAT C(40);\
                              NOM2_APPLI N(8,2);\
                              NOM3_NAME C(40);\
                              NOM3_RELAT C(40);\
                              NOM3_APPLI C(1);\
                              CHECK_FLAG C(1);\
                              TH_P_PYT C(1);\
                              KYC C(1);\
                              FIRC_STA C(1);\
                              INSTL_NO N(6, 0);\
                              TOTAL_INST N(6, 0);\
                              NEW_FLAG C(1);\
                              EUIN_statu C(50);\
                              EUIN_No C(30);\
                              SBBR0KR_AR C(30);\
                              SIPREG_NUM N(19, 0);\
                              Fres_Exi C(1);\
                              Che_copy C(1);\
                              Mob_no C(15);\
                              Nomi_avbl C(15);\
                              KRA_Verifd C(5);\
                              KRA_agency C(30);\
                              FHLD_CKYC N(14, 0);\
                              SHLD_CKYC N(14, 0);\
                              THLD_CKYC N(14, 0);\
                              GURD_CKYC N(14, 0);\
                              JH1_DOB D;\
                              JH2_DOB D;\
                              GUAR_DOB D;\
                              FHLD_AADHR N(12, 0);\
                              SHLD_AADHR N(12, 0);\
                              THLD_AADHR N(12, 0);\
                              GURD_AADHR N(12, 0);\
                              AADHR_STA C(1)'
                             )
        dbf_file.open(dbf.READ_WRITE)
        for row in self.session.query(M.IntFTFeedOut).all():
            if row.trxn_date:
                txn_dt_yr = row.trxn_date.year
                txn_dt_mnth = row.trxn_date.month
                txn_dt_dy = row.trxn_date.day
            else:
                txn_dt_yr = None
                txn_dt_mnth = None
                txn_dt_dy = None
            if row.date_of_birth:
                dob_yr = row.date_of_birth.year
                dob_mnth = row.date_of_birth.month
                dob_dy = row.date_of_birth.day
            else:
                dob_yr = None
                dob_mnth = None
                dob_dy = None
            if row.reg_date:
                reg_date_yr = row.reg_date.year
                reg_date_mnth = row.reg_date.month
                reg_date_dy = row.reg_date.day
            else:
                reg_date_yr = None
                reg_date_mnth = None
                reg_date_dy = None
            dbf_file.append(
                      (row.amc_code, row.broker_code, '', row.user_code,
                       row.usr_txn_no, '', row.folio_no, row.ck_dig_no,
                       row.trxn_type, row.scheme_code,
                       row.first_name, row.jont_name1, row.jont_name2,
                       row.add1, row.add2, row.add3, row.city, row.pincode,
                       '', dbf.Date(txn_dt_yr, txn_dt_mnth, txn_dt_dy),
                       row.trxn_time, row.units, row.amount, row.clos_ac_ch,
                       dbf.Date(dob_yr, dob_mnth, dob_dy),
                       row.guardian_name, row.tax_number, '', '', '', '',
                       row.bank_account_no, row.account_type, row.bank_name,
                       row.br_name, row.bank_city,
                       row.reinvestment_tag, row.hold_nature, row.occ_code,
                       row.tax_status, '', row.state, row.sub_trxn_type,
                       row.div_payout_code, row.ecs_no, '', row.alt_folio_no,
                       '', row.location_code, '', '',
                       row.pan_2_holder, row.pan_3_holder, row.nom_name,
                       row.nom_relation, row.guardian_pan, '', '',
                       row.pan_validity_code,
                       row.grdn_pan_validity_code, row.jh1valpan,
                       row.jh2valpan, '', '', '', '',
                       dbf.Date(reg_date_yr, reg_date_mnth, reg_date_dy),
                       '', '', '', '', row.neft_code, row.rtgs_code,
                       row.kyc_flag,
                       row.poa_stat, row.mod_trxn, row.sign_verification,
                       row.log_str, row.addl_addr1, row.addl_addr2,
                       row.addl_addr3, row.addl_city, row.addl_state,
                       row.addcountry, row.addpincode,
                       row.nom1_appli, row.nom2_name, row.nom2_relat,
                       row.nom2_appli, row.nom3_name, row.nom3_relat,
                       row.nom3_appli, row.check_flag, row.th_p_pyt,
                       row.kyc, row.firc_sta, row.instl_no,
                       row.total_inst, row.new_flag, row.euin_statu,
                       row.euin_no, '', row.sipreg_num, None, None, row.mob_no,
                       row.nomi_avbl, row.kra_verified, row.kra_agency,
                       row.fhld_ckyc, None, None, None, None, None, None,
                       None, None, None, None, None)
                    )
        LOG.info('Successfuly created the FT dbf file')
        dbf_file.close()

    def create_nach_registration_feed(self):
        date = datetime.utcnow().strftime('%d_%m_%y')
        filename = '/tmp/registration_client' + str(date)
        column_headers = [
            'S.No', 'Unique ID', 'Investor Name', 'Upper Limit Amount',
            "Bank Account Holder's Name (as In Bank records)",
            'Bank Account Number', 'Bank Name', 'MICR', 'IFSC Code',
            'Bank Account Type', 'Start date ( DDMMYYYY)',
            'End Date (DDMMYYYY)']
        row = 0
        col = 0
        workbook = xlsxwriter.Workbook(filename)
        worksheet = workbook.add_worksheet()
        # Used for styling column_headers in bold
        bold = workbook.add_format({'bold': True})

        for header in column_headers:
            worksheet.write(row, col, header, bold)
            col += 1

        rows = self.session.query(M.Customer).filter(
                M.Customer.nach_mandate_status == NACH_REGISTRATION_INITIATED)

        for row_num, row in enumerate(rows, 1):
            row.nach_mandate_status = NACH_REGISTRATION_PROCESSING
            col = 0
            excel_row = [row_num, row.nach_mandate_id, row.name,
                         config.NACH_UPPER_LIMIT_AMOUNT, row.bank_account_name,
                         row.bank_account_no, row.bank_name, '', row.bank_ifsc,
                         'Saving', row.nach_created_on]

            for element in excel_row:
                worksheet.write(row, col, element)
                col += 1
        workbook.close()
        self.session.commit()
        return filename
