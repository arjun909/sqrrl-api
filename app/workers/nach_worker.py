# nach_registration_worker.py
from datetime import datetime, timedelta

from sparkpost import SparkPost

from app.model import (Customer, Transaction, Investment, IntNachTrxnFeedOut,
                       BilldeskAmcLookup)
from app.utils.helpers import s3_file_upload
from app import constants as C
from app.config import SERVER_IP, DEBUG, NACH_RECIPIENTS, NACH_CC
from app.log import get_logger

import io

LOG = get_logger()
sp = SparkPost()


class NachWorker(object):
    def __init__(self, session):
        self.session = session

    def mail_to_billdesk(self, controlfile_name):
        """
        Sends NACH registration file to Billdesk.
        """
        rows = self.session.query(Customer).filter(
                    Customer.nach_mandate_status == C.NACH_PROCESSING)
        attachments = []
        file = {
            'name': controlfile_name,
            'type': 'xlsx',
            'filename': controlfile_name,
        }
        attachments.append(file)
        for row in rows:
            filename = '/tmp/' + 'NACH_' + row.nach_mandate_id
            file = {
                'name': filename,
                'type': 'jpg',
                'filename': filename
            }
            attachments.append(file)
        subject_str = 'Nach Registration Files'
        response = sp.transmissions.send(
                recipients=['operations@sqrrl.in'],
                text='Billdesk Documents',
                subject=subject_str,
                from_email='dev@sqrrl.in',
                attachments=attachments
            )

        if response and response['id']:
            # it has been emailed, delete it.
            import os
            for file_obj in attachments:
                key = file_obj['name'][1:]
                s3_file_upload(key, file_obj['name'])
                os.remove(file_obj['name'])
            LOG.info('NACH registration file and forms uploaded to s3.....')

    def create_nach_trxns(self, product_id):
        """
        Creates transaction rows in a product for all customers who have NACH.
        """
        num_days = 2
        date_check = datetime.utcnow() + timedelta(num_days)
        # get all investments within next 2 days
        rows = self.session.query(Investment).filter(
                    Customer.nach_mandate_status == C.NACH_REGISTRATION_SUCCESS,  # NOQA
                    Investment.next_investment_on < date_check,
                    Investment.product_id == product_id,
                    Customer.id == Investment.customer_id,
                    Investment.target_instalment_amount.isnot(None),
                    Investment.id == Transaction.investment_id
                ).all()

        LOG.debug('Found {} investments'.format(len(rows)))
        for row in rows:
            # to avoid duplicate transactions, there shouldn't be any
            # existing NACH transactions for this investment
            has_nach_trx = self.session.query(Transaction).filter(
                Transaction.investment_id == row.id,
                Transaction.mode == C.NACH, Transaction.status.in_(
                    (C.TR_STATUS_INITIATED, C.TR_STATUS_PENDING)
                )).count()
            if has_nach_trx:
                LOG.warn('investment already has NACH trxn, skipping...',
                         extra={'row_investment_id': row.id})
                continue
            try:
                Transaction.initiate(row.group_id,
                                     row.target_instalment_amount,
                                     C.INVEST, self.session, SERVER_IP, C.NACH)
                self.session.commit()
            except Exception as ex:
                LOG.error('Could not create NACH transaction',
                          exc_info=True,
                          extra={
                              'customer_id': row.customer_id,
                              'investment_id': row.id
                          })
                self.session.rollback()

    def create_nach_feedout(self):
        """
        creates a txt file for all transactions with mode = NACH
        and emails it to operations.
        """
        q = self.session.query(IntNachTrxnFeedOut).all()
        if q:
            date = datetime.utcnow().strftime('%d:%m:%Y')
            filename = '/tmp' + '/Transaction-Client-Sqrrl' + date + '.txt'
            with io.open(filename, 'w', newline='\r\n') as text_file:
                header_str = ('Auto TranID,Unique ID,InvName,AMC,SchmeCode,'
                              'FolioNumber,Debit Date (YYYYMMDD),Debit Amount')
                text_file.write(header_str)
                text_file.write('\n')

                for s_no, row in enumerate(q, 1):
                    lst = [s_no, row.nach_mandate_id, row.name, row.amc_code,
                           row.rta_code, row.folio_no,
                           row.debit_date.strftime('%Y%m%d'), row.amount]
                    text_file.write(','.join(map(lambda x: str(x) if x else '',
                                                 lst)))
                    text_file.write('\n')
            # email it to operations
            if not DEBUG:
                date = date.replace(':', '/')
                name = 'nach-trxn-feed-out-{}.txt'.format(date)
                response = sp.transmissions.send(
                    recipients=NACH_RECIPIENTS,
                    bcc=NACH_CC,
                    text='PFA',
                    subject='NACH Debit Instruction Sqrrl Fintech {}'.format(
                                                                       date),
                    from_email='operations@sqrrl.in',
                    attachments=[
                        {
                            'name': name,
                            'type': 'application/txt',
                            'filename': filename
                        }
                    ]
                )
                if response and response['id']:
                    # it has been emailed, delete it.
                    import os
                    os.remove(filename)
            LOG.debug('Finished sending Nach transaction feedout {}'.
                      format(filename))
        else:
            LOG.debug('No transactions were found in IntNachTrxnFeedOut.'
                      ' Nach transaction feed out was not generated')

    def insert_nach_transactions(self):
        """
        Starting with scheme table gets all INITIATED transactions
        with mode = NACH and inserts them in int_nach_trxn_feedout table.
        Status of these transactions is then changed to PENDING.
        """
        # truncate the table
        self.session.query(IntNachTrxnFeedOut).delete()

        LOG.info('Inserting NACH transactions...')
        q = self.session.query(Transaction).filter(
                        Transaction.status == C.TR_STATUS_INITIATED,
                        Transaction.mode == C.NACH).all()
        new_rows = []
        for row in q:
            nr = IntNachTrxnFeedOut()
            nr.nach_mandate_id = row.customer.nach_mandate_id
            nr.name = row.customer.pan_name
            bd_amc_code = BilldeskAmcLookup.find_by_amc(
                        row.investment.scheme.amc_name, self.session)
            nr.amc_code = bd_amc_code
            # folio no is set to Transaction.id which is then used
            # to find transaction and update status from nach feed in.
            nr.folio_no = row.id
            nr.rta_code = row.investment.scheme.rta_code
            nr.amount = row.amount
            debit_date = datetime.utcnow() + timedelta(days=1)
            nr.debit_date = debit_date

            # change status of transction row to PENDING
            row.status = C.TR_STATUS_PENDING
            new_rows.append(nr)
        if new_rows:
            # truncate table, then push data
            self.session.query(IntNachTrxnFeedOut).delete()
            self.session.add_all(new_rows)
            LOG.info('Finished Nach Feed Out worker; transactions - {}'
                     .format(len(new_rows)))
        else:
            LOG.info('No NACH transactions were found for insertion')
        self.session.commit()
