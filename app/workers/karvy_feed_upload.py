from os import remove
from datetime import timedelta
import redis
import requests
from zeep import Client
from zeep.transports import Transport
from lxml import objectify

from app.log import get_logger
from app import config as conf
from app.model import Transaction
from app.utils.helpers import s3_file_download

LOG = get_logger()

redis_client = redis.StrictRedis.from_url(conf.REDIS_URL)


class KarvyFeedUpload(object):
    """
    Worker to upload Karvy Feed to the Karvy service URL
    """
    @staticmethod
    def upload_file(session, amc_code, filepath):
        """
        Uploads xml file to Karvy STPUpload service at
        http://karvymfs.com/CP_STP_SERVICE/STPService.svc
        """
        key = filepath.split('/', 1)[-1]
        s3_file_download(key, filepath)
        xml_str = ''
        try:
            xml_file = open(filepath, 'rt')
            xml_str = xml_file.read()
            # delete local file
            remove(filepath)
        except Exception as ex:
            LOG.error('Could not read/remove XML file', exc_info=True)
            raise ex

        # SOAP service method STPUpload (FundCode, UserID,
        # Password, Branch, ARNCode, IPAddress, XMLFile)
        arn_code = 'ARN-'+conf.ARN

        # Using requests session as transport
        r_session = requests.Session()

        # using QUOTAGUARDSTATIC as HTTP Proxy in debug
        if conf.DEBUG and conf.QUOTAGUARDSTATIC_URL:
            # ref: https://pritschet.me/wiki/python/python-suds-using-proxy/
            proxy = {
                'http': conf.QUOTAGUARDSTATIC_URL,
                'https': conf.QUOTAGUARDSTATIC_URL
            }
            r_session.proxies = proxy

        # OP-86: use a timeout for requests
        transport = Transport(session=r_session, timeout=60)
        cl = Client(conf.KARVY_FEED_UPLOAD_URL, transport=transport)

        try:
            resp = cl.service.STPUpload(
                                amc_code, conf.KARVY_FEED_UPLOAD_USERID,
                                conf.KARVY_FEED_UPLOAD_PASSWORD,
                                conf.KARVY_FEED_UPLOAD_BRANCH,
                                arn_code, '', xml_str)
            # LOG.debug(resp)
            success, msg = KarvyFeedUpload.process_response(resp)
            if success:
                # get transactions id's from redis key
                LOG.info('XML upload successful', extra={
                    'upload_filepath': filepath
                })
                redis_key_set = redis_client.smembers(filepath)
                if not redis_key_set:
                    raise Exception('No trxn ids found for {}'.format(
                                                                filepath))
                trxn_id_lst = [int(key.decode('utf-8'))
                               for key in redis_key_set]
                LOG.debug('Transaction ids: {}'.format(trxn_id_lst))
                try:
                    session.query(Transaction).filter(
                        Transaction.id.in_(trxn_id_lst)).update(
                        {'is_processed': True}, synchronize_session=False)
                    session.commit()
                    redis_client.delete(filepath)
                except Exception as ex:
                    LOG.error('XML upload failed!', exc_info=True)
            else:
                LOG.error('XML upload got unsuccessful response', extra={
                    'err_filepath': filepath,
                    'err_message': msg
                })
                redis_client.expire(filepath, timedelta(days=1))
            return resp
        except Exception as e:
            LOG.error('Karvy XML feed upload failed', exc_info=True, extra={
                'err__amc_code': amc_code,
                'err__filepath': filepath
            })
            raise e

    @staticmethod
    def process_response(xml_string):
        """
        Process the XML response from the file upload
        """
        result = objectify.fromstring(xml_string)
        return_code = result.Information.Return_Code
        return_message = result.Information.Return_Msg
        LOG.debug('{} - {}'.format(return_code, return_message))
        success = return_code == 0
        # TODO: OP-81
        return success, return_message
