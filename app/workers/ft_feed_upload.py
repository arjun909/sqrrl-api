# ft_feed_upload_worker
from os.path import basename
from xml.etree import ElementTree
import requests

from app.log import get_logger
from app.config import FT_URL, FT_USER_ID, FT_PASS

LOG = get_logger()


class FTFeedUpload(object):
    """
    worker uploads FT feed text file to the FT Upload service URL
    """
    @classmethod
    def upload_FT_txtfile(cls, path):
        # function creates an HTTP POST request to the FT URL. Here we send the
        # created FT feed text file
        if not path:
            LOG.warn('No path provided, no file to upload')
            return
        LOG.info('uploading to FT', extra={'path': path})
        url = FT_URL
        payload = {
            'UserID': FT_USER_ID,
            'Password': FT_PASS
        }
        # LOG.debug('payload: {}'.format(payload))
        try:
            file_name = basename(path)
            # create a tuple (filename, data, content_type, headers)
            # for the file
            file_tuple = (file_name, open(path, 'rb'), 'text/plain')
            # LOG.debug(file_tuple)
            r = requests.post(url, data=payload,
                              files={'File1': file_tuple})
            # parse the xml, check if all records succeeded
            tree = ElementTree.fromstring(r.text)
            total = int(tree.find('Total-records').text)
            success = int(tree.find('Success-records').text)
            if total == success:
                LOG.info('{} rows uploaded to FT successfully'.format(total))
            else:
                LOG.error('Error uploading FT feed', extra={
                    'response_text': r.text,
                    'total_records': total,
                    'successful_records': success
                })
        except Exception as e:
            # Houston, we have a problem, over !
            LOG.error(e, exc_info=e)
