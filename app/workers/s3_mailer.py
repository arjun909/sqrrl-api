# s3_mailer.py
import boto3
import boto3.session
from app.model import Customer
from app.config import (AWS_REGION, ATTACHMENT_BUCKET)
from sparkpost import SparkPost
from app import log

LOG = log.get_logger()
sp = SparkPost()

# s3 variables
session = boto3.session.Session(region_name=AWS_REGION)
config = boto3.session.Config(signature_version='s3v4')
s3 = session.resource('s3', config=config)


class S3MailerWorker(object):
    """
    Mails S3 customer documents to operations@sqrrl.in using
    an s3 presigned_url()
    """
    def __init__(self, session):
        self.session = session

    def generate_presigned_url(self, key):
        # returns an s3 presigned url for s3_objects valid for 2 days
        session = boto3.session.Session(region_name=AWS_REGION)
        config = boto3.session.Config(signature_version='s3v4')
        s3 = session.client('s3', config=config)
        # kwarg ExpiresIn is URL validity time in seconds
        url = s3.generate_presigned_url(
                       'get_object',
                       Params={'Bucket': ATTACHMENT_BUCKET,
                               'Key': key},
                       ExpiresIn=172800)
        return url

    def get_s3_objects(self):
        # gets s3_objects from bucket and mails them to operations
        # using SparkPost API.
        my_bucket = s3.Bucket(ATTACHMENT_BUCKET)
        s3_keys = []
        # gets all keys in the s3 bucket and appends it to a list.
        for obj in my_bucket.objects.filter(Prefix='customer-'):
            s3_keys.append(obj.key)
        # get all rows of Customer table where pan_kyc_docs haven't been mailed
        LOG.debug('s3_keys: {}'.format(s3_keys))
        rows = self.session.query(Customer).\
            filter(
                Customer.pan_kyc_docs_mailed.is_(False),
                Customer.pan_kyc_docs_uploaded.is_(True),
                Customer.attachments.isnot(None)
                ).all()
        # find all keys for selected customers and get a presigned_url
        # for all such keys of that customer.
        for customer in rows:
            cust_keys = []
            # find all keys of this customer in the keys_list
            # and create a list of objects.
            for key in s3_keys:
                if customer.uid in key:
                    obj = {
                        'key': key,
                        'url': self.generate_presigned_url(key)
                    }
                    cust_keys.append(obj)
            # if we have a list of keys, mail it to operations@sqrrl.in
            if cust_keys:
                # LOG.debug('cust_keys: {}'.format(cust_keys))
                response = sp.transmissions.send(
                    recipients=['operations@sqrrl.in'],
                    template='ops-pan-kyc',
                    substitution_data={
                        'uid': customer.uid,
                        'pan_name': customer.pan_name or customer.name,
                        'pan':  customer.pan_number,
                        'date_of_birth': str(customer.date_of_birth),
                        'attachments': cust_keys,
                    }
                )
                # if the sparkpost transmission.send was successful set
                # pan_kyc_docs_mailed to True
                if response and response['id']:
                    customer.pan_kyc_docs_mailed = True
                    LOG.info('S3 mail for customer {} sent successfully...'.
                             format(customer.pan_name))
        try:
            self.session.commit()
        except Exception as ex:
            LOG.error(ex, exc_info=True)
            self.session.rollback()
