# billdesk_worker
import os
import csv
import zipfile
import xlrd
from datetime import datetime, timedelta
from app.model import (Customer, Investment, Transaction, Scheme)
from app.config import REDIS_URL, DEBUG
from app.constants import TR_STATUS_FAILURE, REDIS_UTR_PVN_PREFIX
from app.log import get_logger
from itertools import groupby
from sparkpost import SparkPost
from app.config import BASE_SENDER
from app.communications import Email
from app.communications.email_constants import TYPE_5
import redis

redis_client = redis.StrictRedis.from_url(REDIS_URL)
LOG = get_logger()
sp = SparkPost()
email = Email(log_level=1, base_sender=str(BASE_SENDER))


class BilldeskWorker(object):
    """
    This worker creates an async task for all the Billdesk_query_transaction
    processes.
    """
    def __init__(self, session):
        self.session = session

    def get_billdesk_failed_transactions(self):
        """
        We query all the '0002' status trxns for last 48 hours. For these trxns
        Billdesk.query_transaction is called and status is updated. If the trxn
        still returns failed after 48 hours it is updated to be Failed
        Permanently.
        """
        weekday = datetime.utcnow().weekday()
        # although we need to test for 2 business days and assume transactions
        # are failed beyond that, sometimes there are long weekends and what
        # not. Hence increasing the time to 4/6 days
        if weekday == 0:
            # Monday
            time_delta = 6
        else:
            # other days
            time_delta = 4
        last_check = datetime.utcnow() - timedelta(days=time_delta)
        rows = self.session.query(Customer, Transaction).filter(
                      Transaction.note.like('%0002%')).filter(
                      Transaction.status == TR_STATUS_FAILURE,
                      Customer.id == Transaction.customer_id,
                      Transaction.bank_transaction_on > last_check).all()
        return rows

    def process_mis_pv_number(self, dir1path):
        """
        processes the UTR from the directory and updates it in
        customer transactions
        """
        # LOG.info('Processing pv number.............')
        # Delicate code. Handle with extreme care !
        # Alert: Variable army ahead, proceed with caution.
        dir1_contents = os.listdir(dir1path)
        zip_folder = [file for file in dir1_contents if '.zip' in file]
        zip_path = dir1path + '/' + zip_folder[0]

        # find the zip file inside this zipfile, dir2
        dir2path = zip_path.split('.zip')[0]
        z = zipfile.ZipFile(zip_path)
        z.extractall(dir2path)
        dir2_contents = os.listdir(dir2path)

        # This is the master transaction file and contains all the info
        # about trxns. We need to get PV_NO against each trxns.
        # this is the only file that has sqrrl_ in its name in dir2.
        trxn_file = [file for file in dir2_contents if 'SQRRL_' in file][0]
        master_file_path = dir2path + '/' + trxn_file
        # open the xls file using xlrd
        book = xlrd.open_workbook(master_file_path)
        work_sheet = book.sheet_by_index(0)
        max_rows = work_sheet.nrows

        for row in range(max_rows)[1:]:
            bd_rta_code = work_sheet.cell_value(row, 8)
            scheme_code = work_sheet.cell_value(row, 10)
            trxn_group_id = work_sheet.cell_value(row, 5)
            if work_sheet.cell_value(row, 14) != 'SUCCESS':
                LOG.warn('Failed status for transaction', extra={
                    'data': {
                        'bd_rta_code': bd_rta_code,
                        'scheme_code': scheme_code
                    }
                })
                continue
            # read values from file
            file_search_key = bd_rta_code + scheme_code

            # find the file matching the file search key and starting with PV_
            pv_number_file = [file for file in dir1_contents
                              if file_search_key in file and 'PV_' in file][0]
            if not pv_number_file:
                LOG.error('File containing PV number not found', extra={
                    'data': {
                        'bd_rta_code__err': bd_rta_code,
                        'scheme_code__err': scheme_code
                    }
                })
                continue
            # read the file having pv number, we are inching closer !
            pv_number_file_path = dir1path + '/' + pv_number_file
            book2 = xlrd.open_workbook(pv_number_file_path)
            pv_worksheet = book2.sheet_by_index(0)
            # PV number is in C7 cell.
            pv_number = pv_worksheet.cell_value(6, 2)
            # update pv_number against the transaction
            record = self.session.query(
                    Investment, Scheme, Transaction).filter(
                    Scheme.rta_code == scheme_code,
                    Transaction.group_id == trxn_group_id,
                    Transaction.investment_id == Investment.id,
                    Investment.scheme_id == Scheme.id
                    ).first()

            rd_key = REDIS_UTR_PVN_PREFIX + pv_number

            # check if UTR has been received for this PV-number
            if redis_client.get(rd_key):
                utr = redis_client.get(rd_key).decode()
                # update utr number in Transaction
                record.Transaction.utr_no = utr
                # delete the redis key
                redis_client.delete(rd_key)
            try:
                record.Transaction.extras['pv_number'] = pv_number
            except:
                LOG.warn('No transaction record found', extra={
                    'data': {
                        'trxn_group_id': trxn_group_id,
                        'scheme_code': scheme_code
                    }
                })
        self.session.commit()
        self.email_utr_rows()
        # LOG.info('PV number processing finished.....')

    def process_utr(self, filepath):
        """
        process utr for every pv_number and update it against transactions
        """
        LOG.info('Processing utr', extra={'data': {'filepath': filepath}})
        book = xlrd.open_workbook(filepath)
        work_sheet = book.sheet_by_index(0)
        max_rows = work_sheet.nrows
        for row in range(max_rows):
            # this cell contains the UTR record
            utr_record = work_sheet.cell_value(row, 2)
            pv_number = work_sheet.cell_value(row, 1)
            rows = self.session.query(Transaction).filter(
                Transaction.extras['pv_number'] == pv_number).all()
            if rows:
                for record in rows:
                    record.utr_no = utr_record
            else:
                rd_key = REDIS_UTR_PVN_PREFIX + pv_number
                redis_client.set(rd_key, utr_record)
        self.session.commit()
        self.email_utr_rows()
        # LOG.info('Finished processing UTR.....')

    def email_utr_rows(self):
        """
        sends utr for transactions to operations grouping them according
        to the AMC.
        """
        file_list = []
        attachments = []
        # LOG.info('Sending email for processed utrs.....')
        # fetch all transactions whose utr has been updated but hasnt
        # been mailed to amc's yet, order them by amc_code.
        rows = self.session.query(Transaction, Investment, Scheme, Customer).\
            filter(
                Transaction.investment_id == Investment.id,
                Investment.scheme_id == Scheme.id,
                Transaction.customer_id == Customer.id,
                Transaction.is_utr_processed.isnot(True),
                Transaction.utr_no.isnot(None)).\
            order_by(Scheme.amc_code).all()
        if rows:
            LOG.info('UTR rows found, emailing...', extra={
                'data': {'count': len(rows)}
            })
        else:
            LOG.warn('No unsent UTR rows found, returning.')
            return

        # group by AMC, then mail.
        for key, grouper in groupby(
            rows,
            lambda r: r.Scheme.amc_code
        ):
            amc_rows = list(grouper)
            # headers for csv file
            file_headers = ['Transaction Reference Number', 'Investor Name',
                            'Investor PAN', 'Scheme Code', 'AMC Code',
                            'Amount', 'Date of Posting', 'UTR']
            date = datetime.utcnow().strftime(r'%d:%m:%Y')
            file_name = '/tmp/' + key + '_' + date + '.csv'
            with open(file_name, 'w', newline='') as csvfile:
                spamwriter = csv.writer(csvfile, delimiter=',',
                                        quoting=csv.QUOTE_MINIMAL)
                spamwriter.writerow(file_headers)
                for row in amc_rows:
                    record_info = [row.Transaction.id, row.Customer.pan_name,
                                   row.Customer.pan_number,
                                   row.Scheme.rta_code, key,
                                   row.Transaction.amount, '',
                                   row.Transaction.utr_no]
                    spamwriter.writerow(record_info)
                    row.Transaction.is_utr_processed = True
            file_list.append(file_name)

        if file_list and not DEBUG:
            for file in file_list:
                attachments.append(file)
            recipient = 'operations@sqrrl.in'
            LOG.debug('sending email to {}'.format(recipient))
            success = email.send_template_mail(TYPE_5, recipient,
                                               attachments=attachments)
            if success:
                # only if email is sent should we set is_utr_processed
                self.session.commit()
            else:
                LOG.warn('UTR email was not sent successfully')
                self.session.rollback()
        else:
            LOG.debug('No UTR to mail, or DEBUG=True')
