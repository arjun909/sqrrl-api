from app.model import Customer
from app.log import get_logger
from datetime import datetime
import dbf
from app import config
from sparkpost import SparkPost
from app.config import ARN, SERVER_IP
import io
import csv

LOG = get_logger()
sp = SparkPost()


class SetupLogWorker(object):
    """
    Encapsulates the workers to generate e-logs for customers on successful
    completion of KYC, Bank and Fatca setup.
    """
    def __init__(self, session):
        self.session = session

    def create_setup_log(self):
        """
        gets all customers who have updated all the info
        """
        unprocessed_customers = self.session.query(Customer).\
            filter(Customer.setup_feed_generated == False).all()  # NOQA
        today = datetime.today()
        date_str = today.strftime('%d%m%Y')
        file_name = ''.join(['DOC_', ARN, date_str, '_001'])
        fatca_file_name = ''.join(['FATCA_', ARN, date_str, '_001', '.csv'])

        customers = []
        for customer in unprocessed_customers:
            setup_status = customer.get_profile()
            if all((setup_status['is_kyc_verified'],
                    setup_status['bank_updated'],
                    setup_status['fatca_updated'])
                   ):
                customers.append(customer)
                customer.setup_feed_generated = True
        try:
            self.write_to_karvy(file_name, customers)
            self.write_to_cams(file_name, customers)
            self.write_to_fatca(fatca_file_name, customers)
            self.session.commit()
        except Exception as ex:
            LOG.error('setup feed generation failed. {}'.format(ex))

    def write_to_karvy(self, file_name, customers):
        dbf_file = dbf.Table(file_name,
                             'FIELD1 C(20); FIELD2 N(15,0); FIELD3 C(25);\
                             FIELD4 C(3); FIELD5 C(20); FIELD6 C(20);\
                             FIELD7 C(15); FIELD8 C(18); FIELD9 C(70);\
                             FIELD10 C(1); FIELD11 C(1); FIELD12 C(2);\
                             FIELD13 C(30); FIELD14 C(10); FIELD15 C(20);\
                             FIELD16 C(1)')
        dbf_file.open(dbf.READ_WRITE)
        serial_num = 1
        for customer in customers:
            try:
                arn = ''.join(['ARN-', ARN])
                arn_pan = ''.join([arn, customer.pan_number])
                dbf_file.append(('001', serial_num, arn_pan, '', arn, arn, '',
                                 '', customer.pan_name, '1', 'N', 'ON',
                                 'AOF,POA,PAN,KYC', customer.pan_number,
                                 customer.uid, 'N',
                                 ))
                serial_num += 1
            except Exception as ex:
                LOG.error('{} - {}'.format(customer, ex))
        dbf_file.close()
        # email it to operations
        subject_str = '[Karvy customer elog file]'
        file_type = 'application/dbf'
        self.email_and_delete_file(file_name+'.dbf', subject_str, file_type)

    def write_to_cams(self, file_name, customers):
        with io.open(''.join([file_name, '.txt']), 'w',
                     newline='\r\n') as text_file:
            for customer in customers:
                setup_date = customer.modified_on
                setup_date_str = setup_date.strftime('%d%m%Y')
                time_str = setup_date.strftime('%H:%M:%S')
                log_str = '$'.join([setup_date_str, time_str,
                                    customer.ip_address, SERVER_IP])
                if customer.pan_number and customer.pan_name:
                    text_file.write('|'.join(['ARN-112848',
                                              customer.pan_number,
                                              customer.pan_name, 'All', '',
                                              log_str]))
                else:
                    LOG.error('customers pan info could not be found',
                              exc_info=True)
                text_file.write('\n')
        subject_str = '[CAMS customer elog file]'
        file_type = 'text'
        self.email_and_delete_file(file_name+'.txt', subject_str, file_type)

    def write_to_fatca(self, file_name, customers):
        """
        creates E-log for customers who have created the fatca setup.
        """
        with open(file_name, 'w', newline='') as csvfile:
            fatca_file = csv.writer(csvfile, delimiter=',')
            header = ['PAN_RP', 'PEKRN', 'INV_NAME', 'DOB',
                      'FR_NAME', 'SP_NAME', 'TAX_STATUS', 'DATA_SRC',
                      'ADDR_TYPE', 'PO_BIR_INC', 'CO_BIR_INC', 'TAX_RES1',
                      'TPIN1', 'ID1_TYPE', 'TAX_RES2', 'TPIN2', 'ID2_TYPE',
                      'TAX_RES3', 'TPIN3', 'ID3_TYPE', 'TAX_RES4', 'TPIN4',
                      'ID4_TYPE', 'SRCE_WEALT', 'CORP_SERVS', 'INC_SLAB',
                      'NET_WORTH', 'NW_DATE', 'PEP_FLAG', 'OCC_CODE',
                      'OCC_TYPE', 'EXEMP_CODE', 'FFI_DRNFE', 'GIIN_NO',
                      'SPR_ENTITY', 'GIIN_NA', 'GIIN_EXEMC', 'NFFE_CATG',
                      'ACT_NFE_SC', 'NATURE_BUS', 'REL_LISTED', 'EXCH_NAME',
                      'UBO_APPL', 'UBO_COUNT', 'UBO_NAME', 'UBO_PAN',
                      'UBO_NATION', 'UBO_ADD1', 'UBO_ADD2', 'UBO_ADD3',
                      'UBO_CITY', 'UBO_PIN', 'UBO_STATE', 'UBO_CNTRY',
                      'UBO_ADD_TY', 'UBO_CTR', 'UBO_TIN', 'UBO_ID_TY',
                      'UBO_COB', 'UBO_DOB', 'UBO_GENDER', 'UBO_FR_NAM',
                      'UBO_OCC', 'UBO_OCC_TY', 'UBO_TEL', 'UBO_MOBILE',
                      'UBO_CODE', 'UBO_HOL_PC', 'SDF_FLAG', 'UBO_DF',
                      'AADHAAR_RP', 'NEW_CHANGE', 'LOG_NAME', 'FILLER1',
                      'FILLER2']
            fatca_file.writerow(header)
            for customer in customers:
                setup_date = customer.modified_on
                setup_date_str = setup_date.strftime('%d%m%Y')
                time_str = setup_date.strftime('%H:%M:%S')
                log_str = '$'.join([setup_date_str, time_str,
                                    customer.ip_address, ''])
                if customer.occupation_code in ['02', '03', '04', '09', '41',
                                                '42', '44']:
                    occ_type = 'S'
                elif customer.occupation_code in ['01', '43']:
                    occ_type = 'B'
                else:
                    occ_type = 'O'
                if customer.politically_exposed:
                    pep_flag = 'Y'
                else:
                    pep_flag = 'N'
                fatca_file.writerow([
                    customer.pan_number, '', customer.pan_name,
                    '', '', '', '01', 'E', '1', 'IN', 'IN', 'IN',
                    customer.pan_number, 'C', '', '', '', '', '', '', '',
                    '', '', customer.source_of_wealth_code, '04',
                    str(customer.annual_income_code), '', '', pep_flag,
                    customer.occupation_code, occ_type, '', '', '', '', '',
                    '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                    '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                    '', '', '', '', '', 'N', '', 'N', log_str, '', ''
                ])
        # email it to operations
        subject_str = '[FATCA customer elog file]'
        file_type = 'application/csv'
        self.email_and_delete_file(
                                   file_name,
                                   subject_str,
                                   file_type
                                   )

    def email_and_delete_file(self, file_name, subject_str, file_type):
        """
        email the file to operations@sqrrl.in and on successful send,
        delete the file from memory.
        """
        if not config.DEBUG:
            response = sp.transmissions.send(
                recipients=['operations@sqrrl.in'],
                text='customer elogs',
                subject=subject_str,
                from_email='dev@sqrrl.in',
                attachments=[
                    {
                        'name': file_name,
                        'type': file_type,
                        'filename': file_name,
                    }
                ]
            )
            if response and response['id']:
                import os
                os.remove(file_name)
