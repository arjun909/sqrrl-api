from app.log import get_logger
from app.config import (CAMS_FEED_UPLOAD_URL, CAMS_FEED_UPLOAD_USER_CODE,
                        CAMS_FEED_UPLOAD_PASS, REDIS_URL)
from app.utils.helpers import s3_file_download
from lxml import objectify
from app.model import Transaction
import redis
import dbf
import requests

LOG = get_logger()

redis_client = redis.StrictRedis.from_url(REDIS_URL)


class CamsWorker(object):
    """
    Worker to upload Karvy Feed to the Karvy service URL
    """
    def __init__(self, session):
        self.session = session

    def upload_file(self, key):
        """
        uploads the file to the cams feed upload service URL.
        """
        # step 1: Download file from s3 bucket
        file_name = '/' + key
        try:
            s3_file_download(key, file_name)
        except Exception as ex:
            LOG.error(ex)

        """
        params for CAMS feed upload service URL
        {
            Usrcode : Varchar(20),
            pass : Varchar(50),
            remarks : Varchar(250),
            mailreq : Char,
            Uploadfile : FILE type control (ENCTYPE="multipart/form-data")
        }
        """

        # step 2: get all transaction ids from feed file. These ids
        # are used to update is_processed column for transaction rows
        table = dbf.Table(file_name).open()
        trxn_id_lst = [record[4] for record in table]
        files = {'Uploadfile': open(file_name, 'rb')}
        payload = {
            'Usrcode': CAMS_FEED_UPLOAD_USER_CODE,
            'pass': CAMS_FEED_UPLOAD_PASS,
            'mailreq': 'Y'  # always a Y
        }
        try:
            resp = requests.post(CAMS_FEED_UPLOAD_URL, data=payload,
                                 files=files)
            if resp.status_code == 200:
                # step 3: parse the response URL and update is_processed
                # LOG.debug(resp.text)
                result_rows = self.process_response(resp.text)

                # iterate through results
                for record in result_rows:
                    index = int(record.recordnumber) - 1
                    if record.code == 0:
                        # fetch row and update flag
                        row = self.session.query(Transaction).\
                            get(trxn_id_lst[index])
                        row.is_processed = True
                        self.session.add(row)
                    else:
                        LOG.warn('Transaction upload unsuccessful', extra={
                            'transaction_id': trxn_id_lst[index],
                            'error_msg': record.message
                        })
                # commit to database
                self.session.commit()
            else:
                LOG.error('CAMS XML request failed', extra={
                    'resp_status_code': resp.status_code,
                    'resp_content': resp.content
                })
        except Exception as ex:
            LOG.error('Could not upload file to CAMS', exc_info=True, extra={
                'request_payload': payload,
                'request_files': files
            })
            raise ex

    def process_response(self, xml_string):
        """
        Parses XML response of CAMS upload
        """
        try:
            status = objectify.fromstring(xml_string)
            total = status['Total-records']
            successful = status['Success-records']
            filename = status.FileName
            LOG.info('XML response status', extra={
                'total_records': total,
                'success_records': successful,
                'file_name': filename
            })
            return status.result
        except Exception as ex:
            LOG.error('Could not parse XML', exc_info=True, extra={
                'xml_string': xml_string
            })
            raise ex
