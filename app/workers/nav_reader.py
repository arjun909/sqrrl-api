# nav_reader.py
from app.model import StgNavDaily
from app.log import get_logger
from bs4 import BeautifulSoup
import requests

LOG = get_logger()


class NavReader(object):
    """
    NavReader - extracts NAV from a given files using Karvy and CAMS formats
    respectively, and updates them in the database.
    """

    def __init__(self, session):
        self.session = session

    def get_nav_amfi(self, AMFIINDIA_URL, AMFIINDIA_PART_URL):

        try:
            response = requests.get(AMFIINDIA_URL)
            if response.status_code != 200:
                raise Exception('Unable to open url: {}'.format(AMFIINDIA_URL))
        except requests.exceptions.Timeout as e:
            LOG.error('Request to the URL at amfi india timed out')
        except requests.exceptions.TooManyRedirects as e:
            LOG.error('Too many redirects for the amfiindia URL')
        except Exception as e:
            # catastrophic error. bail.
            LOG.error(e)
            raise e

        soup = BeautifulSoup(response.text, 'lxml')
        href = ''

        for link in soup.find_all('a', href=True,
                                  text='Open Ended NAV Report in Text Format',
                                  limit=1):
            href = (link['href'])

        url = ''.join([AMFIINDIA_PART_URL, href])

        try:
            response = requests.get(url)
        except requests.exceptions.Timeout as e:
            LOG.error('Request to the URL at amfi india timed out')
        except requests.exceptions.TooManyRedirects as e:
            LOG.error('Too many redirects for the amfiindia URL')
        except Exception as e:
            # catastrophic error. bail.
            LOG.error(e)
            raise e

        # Truncate table before insert
        self.session.query(StgNavDaily).delete()

        nav_records = {}
        for line in response.iter_lines():
            if len(line.decode('utf-8').split(';')) > 4:
                line_records = str(line).split(';')

                name = line_records[3]
                nav = line_records[4]
                isin = line_records[1]
                date_of_nav = line_records[-1][:-1]
                scheme_record = {
                    'name': name,
                    'nav': nav,
                    'isin': isin,
                    'date': date_of_nav,
                }
                nav_records[isin] = scheme_record
        # Insert values in StgNavDaily Table
        for record in nav_records:
            new_row = StgNavDaily(name=nav_records[record]['name'],
                                  nav=nav_records[record]['nav'],
                                  isin=nav_records[record]['isin'],
                                  nav_date=nav_records[record]['date'])
            self.session.add(new_row)
        self.session.commit()
