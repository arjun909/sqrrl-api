# app/workers/reliance_switch.py
# a worker dedicated to the task of switching Reliance folios
# from Reliance Money Manager to Reliance Liquid Fund - Treasury Plan
from sqlalchemy import func, String
from app import model as M
from app.constants import TR_STATUS_PROCESSING
from app.log import get_logger
from app.utils.reliance import RelianceMoneyManager
LOG = get_logger()


class RelianceSwitchManager():

    def __init__(self, session):
        # the database session
        self.session = session

    def get_customers_for_switch(self):
        """
        Returns list of customers who have opted for switch
        """
        # later we can add more filters for diff. schemes, or date etc.
        rows = self.session.query(M.CustomerAction).\
            filter(M.CustomerAction.params['switch'] == '1').\
            filter(M.Investment.customer_id == M.CustomerAction.customer_id).\
            filter(func.cast(M.Investment.scheme_id, String) ==
                   M.CustomerAction.params['from_scheme']).all()
        return rows

    def switch_customer(self, customer_id, ip, from_scheme_id, to_scheme_id):
        """
        Performs the switch for a given customer_id
        """
        LOG.debug('switching {} for scheme {} -> {}'.
                  format(customer_id, from_scheme_id, to_scheme_id))
        customer = self.session.query(M.Customer).get(customer_id)
        row = None
        tr_out = None
        tr_in = None
        try:
            row, tr_out, tr_in = M.Investment.switch_fund(
                self.session,
                customer,
                from_scheme_id,
                to_scheme_id,
                ip
            )
        except StopIteration as s_err:
            # no investment row found
            LOG.warn('No investment row found, skipping')
            return False
        except Exception as ex:
            LOG.error('Could not switch fund', exc_info=True)
            raise ex

        if row and tr_out and tr_in:
            # now we call Reliance API
            rmm = RelianceMoneyManager(
                session=self.session,
                transaction_group_id=None,
                investment_group_id=row.group_id
            )
            try:
                # update balance
                rmm.get_withdrawal_balance()
                success, reference = rmm.switch_fund()
                if success:
                    # update switch transactions with reference
                    M.Transaction.update_group(
                        tr_out, self.session,
                        reference=reference,
                        status=TR_STATUS_PROCESSING
                    )
                    M.Transaction.update_group(
                        tr_in, self.session,
                        reference=reference,
                        status=TR_STATUS_PROCESSING
                    )
                else:
                    row.undo_switch()
                    self.session.add(row)
                    self.session.commit()
                return success
            except Exception as ex:
                LOG.error('Switch failed', exc_info=True)
                row.undo_switch()
                self.session.add(row)
                self.session.commit()
                raise ex
        else:
            # Transaction rows were not successfully created, undo
            LOG.warn('Rows were not created properly, undo switch changes')
            if row:
                row.undo_switch()
                self.session.add(row)
                self.session.commit()
