# nav_history.py
from datetime import datetime, timedelta
from decimal import Decimal
import re
from sqlalchemy.exc import IntegrityError

from app.model import StgNavDaily, Scheme, NavHistory, NavIntermediate
from app.log import get_logger

LOG = get_logger()


class NavUpdate(object):
    """
    NavReader - extracts NAV from a given files using Karvy and CAMS formats
    respectively, and updates them in the database.
    """

    def __init__(self, session):
        self.session = session

    def create_intermediate_table(self):
        stage_table_records = self.session.query(StgNavDaily).all()

        # regex to check if date is in dd-mon-yyyy format
        date_regex = re.compile("^((([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4})|(T\+[0-9]+)$")  # NOQA

        self.session.query(NavIntermediate).delete()

        for record in stage_table_records:
            nav_date_string = record.nav_date

            # check if date is formatted and nav is a (float or int)
            if date_regex.match(nav_date_string) \
                    and record.nav.replace('.', '', 1).isdigit():
                nav_date_object =\
                    (datetime.strptime(nav_date_string, '%d-%b-%Y') +
                     timedelta(microseconds=999999))
                new_row = NavIntermediate(name=record.name,
                                          nav=Decimal(record.nav),
                                          isin=record.isin,
                                          nav_date=nav_date_object)
                try:
                    self.session.add(new_row)
                except IntegrityError as e:
                    if 'unique constraint' in e.message:
                        continue
                        # duplicate value found
                        # LOG.debug("duplicate found")
        self.session.commit()

    def update_records_in_navhistory(self):
        """
        updates records from scheme table in the nav_history table
        """
        schemes = self.session.query(Scheme).all()
        for scheme in schemes:
                new_row = NavHistory(name=scheme.name, nav=scheme.nav,
                                     isin=scheme.isin,
                                     nav_date=scheme.nav_last_updated_on)
                self.session.add(new_row)

        self.session.commit()

    def update_navs_in_scheme(self):
        schemes = self.session.query(Scheme).all()
        nav_intermediate_table_records =\
            self.session.query(NavIntermediate).all()

        for scheme in schemes:
            for record in nav_intermediate_table_records:
                # update scheme row if date is newer than nan_last_updated_on in scheme table  # NOQA
                if scheme.isin == record.isin and\
                    (scheme.nav_last_updated_on is None
                     or record.nav_date > scheme.nav_last_updated_on):
                        scheme.nav = record.nav
                        scheme.nav_last_updated_on = record.nav_date
                        LOG.debug('scheme nav: {} - {}'.format(scheme.id,
                                                               scheme.nav))
        self.session.commit()
