from sqlalchemy import Column, String, DateTime, Numeric, ForeignKey, Integer
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import HSTORE

from app.model import Base, Customer


class CustomerSms(Base):
    """
    Model to describe storage of customer sms-es for insights
    """
    __tablename__ = 'customer_sms'

    # establish relationship with customer table
    customer_id = Column(Integer, ForeignKey('customer.id'))
    customer = relationship(
        Customer,
        backref=backref('texts', uselist=True, cascade='delete,all'))
    mobile = Column(String)
    sms_date = Column(DateTime)
    raw_text = Column(String)
    sms_shortcode = Column(String)
    bank_name = Column(String)

    # Important - never store monetary values in anything other than
    # numeric (or decimal, they're same) - or we will get rounding errors.
    # Also, scale = number of digits to the right of decimal point.
    # amounts are to be displayed to 2 decimals, stored till 4.
    transaction_amount = Column(Numeric(scale=4))
    balance_amount = Column(Numeric(scale=4))
    transaction_mode = Column(String)
    merchant_code = Column(String)
    merchant_category = Column(String)
    # dba means "Doing Business As" - what it is called by customers
    merchant_dba = Column(String)
    merchant_extra = Column(HSTORE)
    transaction_date = Column(DateTime)
    # for any other info gleaned from the sms, use extra to store key-values
    extra = Column(HSTORE)
