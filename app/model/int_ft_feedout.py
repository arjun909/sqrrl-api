##################################
#
# int_ft_feedout.py
#
##################################

from sqlalchemy import (
    Column,
    String,
    Date,
    Numeric,
    Integer
)
from app.model import Base
from app import log

LOG = log.get_logger()


class IntFTFeedOut(Base):

    __tablename__ = 'int_ft_feedout'
    amc_code = Column(String(3))
    broker_code = Column(String(20))
    user_code = Column(String(10))
    usr_txn_no = Column(Integer)
    folio_no = Column(String(13))
    ck_dig_no = Column(String(2))
    trxn_type = Column(String(7))
    scheme_code = Column(String(5))
    first_name = Column(String(70))
    jont_name1 = Column(String(35))
    jont_name2 = Column(String(35))
    add1 = Column(String(40))
    add2 = Column(String(40))
    add3 = Column(String(40))
    city = Column(String(35))
    pincode = Column(String(15))
    trxn_date = Column(Date)
    trxn_time = Column(String(20))
    units = Column(Numeric)
    amount = Column(Numeric)
    clos_ac_ch = Column(String(1))
    date_of_birth = Column(Date)
    guardian_name = Column(String(35))
    tax_number = Column(String(40))
    email = Column(String(50))
    bank_account_no = Column(String(40))
    account_type = Column(String(5))
    bank_name = Column(String(40))
    br_name = Column(String(40))
    bank_city = Column(String(35))
    reinvestment_tag = Column(String(1))
    hold_nature = Column(String(2))
    occ_code = Column(String(2))
    tax_status = Column(String(2))
    state = Column(String(2))
    sub_trxn_type = Column(String(6))
    div_payout_code = Column(String(7))
    ecs_no = Column(Numeric)
    alt_folio_no = Column(String(40))
    location_code = Column(String(10))
    redemption_payout_mec = Column(String(7))
    pan_2_holder = Column(String(10))
    pan_3_holder = Column(String(10))
    nom_name = Column(String(70))
    nom_relation = Column(String(40))
    guardian_pan = Column(String(45))
    pan_validity_code = Column(String(1))
    grdn_pan_validity_code = Column(String(1))
    jh1valpan = Column(String(1))
    jh2valpan = Column(String(1))
    reg_date = Column(Date)
    neft_code = Column(String(11))
    rtgs_code = Column(String(11))
    kyc_flag = Column(String(1))
    poa_stat = Column(String(1))
    mod_trxn = Column(String(1))
    sign_verification = Column(String(1))
    log_str = Column(String(70))
    addl_addr1 = Column(String(40))
    addl_addr2 = Column(String(40))
    addl_addr3 = Column(String(40))
    addl_city = Column(String(35))
    addl_state = Column(String(2))
    addcountry = Column(String(35))
    addpincode = Column(String(35))
    nom1_appli = Column(Numeric)
    nom2_name = Column(String(40))
    nom2_relat = Column(String(40))
    nom2_appli = Column(Numeric)
    nom3_name = Column(String(40))
    nom3_relat = Column(String(40))
    nom3_appli = Column(Numeric)
    check_flag = Column(String(1))
    th_p_pyt = Column(String(1))
    kyc = Column(String(1))
    firc_sta = Column(String(1))
    instl_no = Column(String(6))
    total_inst = Column(String(6))
    new_flag = Column(String(1))
    euin_statu = Column(String(1))
    euin_no = Column(String(10))
    sipreg_num = Column(String(20))
    nomi_avbl = Column(String, default='N')
    kra_verified = Column(String, default='Y')
    mob_no = Column(String)
    kra_agency = Column(String)
    fhld_ckyc = Column(String)
    investment_id = Column(Integer)
