from app.model import Base
from sqlalchemy import (Column, Integer, String, Date, Boolean,
                        func, text, ForeignKey)
from datetime import datetime
import random
from os import urandom
from base64 import b64encode
import string
from app import log


LOG = log.get_logger()


class ReferralCode(Base):

    __tablename__ = 'referral_code'

    referral_code = Column(String(10),
                           unique=True, index=True, nullable=False)

    start_date = Column(Date, default=func.now(), server_default=text("now()"))
    expiry_date = Column(Date)
    is_valid = Column(Boolean, default=True, server_default=text("true"))

    downloads_count = Column(Integer, default=0)

    referrer_id = Column(Integer, ForeignKey('referrer.id'), nullable=True)

    @staticmethod
    def is_referral_code_valid(session, code):

        rc_row = ReferralCode.get_referral_code_row(session, code)

        if rc_row:
            if rc_row.expiry_date:
                has_expired = rc_row.expiry_date <= datetime.now().date()

                if has_expired:
                    return False, 'Referral code has expired'

            # Valid referral code
            return True, 'Valid referral code'

        else:
            return False, 'Invalid referral code'

    @staticmethod
    def get_referral_code_row(session, code, regard_validity=True):
        rc_query = session.query(ReferralCode).\
            filter(func.upper(ReferralCode.referral_code) == code.upper())

        if regard_validity:
            return rc_query.filter(ReferralCode.is_valid).first()
        else:
            return rc_query.first()

    @staticmethod
    def increment_downloads_count(session, code):
        """
        # Increase the downloads_count by +1 against the Referral Code
        """

        rc_row = ReferralCode.get_referral_code_row(session, code)

        if not rc_row.downloads_count:
            rc_row.downloads_count = 1
        else:
            rc_row.downloads_count = ReferralCode.downloads_count + 1

        session.add(rc_row)
        session.commit()

    @staticmethod
    def random_referral_code_generator(seed=None, size=6):
        chars = string.ascii_uppercase + string.digits + string.ascii_lowercase

        if not seed:
            seed = b64encode(urandom(size)).decode('ascii')

        ref_code_generated = ''.join(random.SystemRandom(x=seed).choice(chars)
                                     for _ in range(size))

        # TODO: Add profanity testing on the generated Referral Code, so as to
        # check for cuss words. You don't want the customer's referral code to
        # to contain any of the cuss words, since it is visible to all.

        return ref_code_generated
