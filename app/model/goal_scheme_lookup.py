# goal_scheme_lookup.py
from sqlalchemy import (
    Column,
    String,
    Integer,
    Numeric,
    ForeignKey,
)
from sqlalchemy.orm import relationship

from app.model import Base, Scheme


class GoalSchemeLookup(Base):
    """
    A mapping of duration buckets, ROI, scheme, allocations and
    aggressive/moderate portfolio characteristics.

    This table helps us decide for a given portfolio, which schemes to pick
    and their allocation.
    """

    __tablename__ = 'goal_scheme_lookup'

    # 0 stands for 0 - 6 months, 1 stands for 6-12 months,
    # 3: 12-24 months, 4: 24-36 months, 5: > 36 months
    # maps to app.utils.helpers.get_duration_bucket
    duration_bucket_code = Column(Integer, nullable=False)

    # 'M' means Moderate, 'A' means Aggressive
    aggression_code = Column(String, default='M', nullable=False)

    # ROI expressed as a fraction between 0 and 1
    target_roi = Column(Numeric(scale=4))

    # scheme reference
    scheme_rta_code = Column(String, ForeignKey('scheme.rta_code'))
    scheme = relationship(Scheme)

    # allocation as a percent between 0 - 100
    allocation_percent = Column(Numeric(scale=4))

    @classmethod
    def get_allocations(cls, duration_bucket_code, aggression_code, session):
        """
        For given duration and aggression_code, return allocation of schemes
        """
        # for 72 months and beyond the results are same.
        if duration_bucket_code > 5:
            duration_bucket_code = 5
        rows = session.query(cls).\
            filter(cls.duration_bucket_code == duration_bucket_code,
                   cls.aggression_code == aggression_code).all()
        result = []
        for r in rows:
            result.append({
                'scheme_id': r.scheme.id,
                'allocation_percent': r.allocation_percent,
                'target_roi': r.target_roi,
                'min_sip_amount': r.scheme.minimum_sip_amount
            })
        return result
