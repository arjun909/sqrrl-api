###############################################################
#
# stg_ft_feedin.py
#
# stores response from FT
###############################################################

from sqlalchemy import (
    Column, String, Numeric, Date, Boolean
)
from sqlalchemy.exc import IntegrityError
import dbf

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class FTFeedInMixin():
    """
    Common columns for StgFTFeedIn and IntFTFeedIn are defined in
    this mixin.
    """
    comp_code = Column(String)
    scheme_co0 = Column(String)
    scheme_na1 = Column(String)
    folio_no = Column(String)
    trxn_type = Column(String)
    trxn_no = Column(String)
    investor_2 = Column(String)
    trxn_date = Column(Date)
    postdt_da3 = Column(Date)
    nav = Column(Numeric)
    pop = Column(Numeric)
    units = Column(Numeric)
    amount = Column(Numeric)
    check_no = Column(String)
    dividend_4 = Column(String)
    joint_nam1 = Column(String)
    joint_nam2 = Column(String)
    address1 = Column(String)
    address2 = Column(String)
    address3 = Column(String)
    city = Column(String)
    pin_code = Column(String)
    state = Column(String)
    country = Column(String)
    d_birth = Column(String)
    phone_res = Column(String)
    phone_off = Column(String)
    tax_status = Column(String)
    occu_code = Column(String)
    email = Column(String)
    trxn_mode = Column(String)
    trxn_stat = Column(String)
    isc_code = Column(String)
    brok_code = Column(String)
    sub_broke5 = Column(String)
    brok_perc = Column(String)
    brok_comm = Column(String)
    invest_id = Column(String)
    crea_date = Column(String)
    crea_time = Column(String)
    trxn_sub = Column(String)
    appl_no = Column(String)
    trxn_id = Column(String)
    stt = Column(String)
    customer_6 = Column(String)
    direct_fl7 = Column(String)
    it_pan_no1 = Column(String)
    it_pan_no2 = Column(String)
    it_pan_no3 = Column(String)
    pan_statu8 = Column(String)
    pan_statu9 = Column(String)
    pan_stat10 = Column(String)
    regd_date = Column(Date)
    isin = Column(String)
    family_s11 = Column(String)
    goal_sheet = Column(String)
    goal = Column(String)
    goal_det12 = Column(String)
    txn_chg = Column(Numeric)
    gross_am13 = Column(Numeric)
    allotmen14 = Column(String)
    sip_inst15 = Column(String)
    account_16 = Column(String)
    fund_opt17 = Column(String)
    remarks = Column(String)
    social_s18 = Column(String)
    kyc_id = Column(String)
    kyc_status = Column(String)
    holding_19 = Column(String)
    folio_so20 = Column(String)
    plan_type = Column(String)
    branch_c21 = Column(String)
    karvy_ac22 = Column(String)
    pbank_name = Column(String)
    personal23 = Column(String)
    payment_24 = Column(String)
    account_25 = Column(String)
    branch_n26 = Column(String)
    payment_27 = Column(String)
    customer28 = Column(String)
    dd_payab29 = Column(String)
    bank_code = Column(String)
    ifsc_code = Column(String)
    payment_30 = Column(String)
    neft_code = Column(String)
    default_31 = Column(String)
    euin = Column(String)
    sub_brok32 = Column(String)
    euin_inv33 = Column(String)
    b15_t15_34 = Column(String)
    commissi35 = Column(String)
    switch_r36 = Column(String)
    load_amo37 = Column(Numeric)
    tds_amount = Column(String)
    elecmode38 = Column(String)
    nominee1 = Column(String)
    nominee2 = Column(String)
    nominee3 = Column(String)
    nominee_39 = Column(String)
    dp_id = Column(String)
    benf_id = Column(String)
    stp_regn40 = Column(String)

    # how we track whether this row has been 'processed'
    is_processed = Column(Boolean, default=False)


class StgFTFeedIn(Base, FTFeedInMixin):
    """
    The structure of this table is translated from Karvy's reverse feed
    dbf file. The column names are same as far as possible.
    """
    __tablename__ = 'stg_ft_feedin'

    @staticmethod
    def import_dbf(session, file_name):
        """
        Imports a dbf into the table
        """
        LOG.info('importing {}...'.format(file_name))
        table = dbf.Table(file_name.split('.dbf')[0])
        table.open()
        for record in table:
            r = StgFTFeedIn()
            # iterate through all columns, pick the values from record
            # and set attributes.
            for key in table.field_names:
                try:
                    value = record[key]
                    if isinstance(value, str):
                        # remove all spaces around value if str
                        value = value.strip()
                    # LOG.debug('{}: {}'.format(key, value))
                    setattr(r, key, value)
                except KeyError:
                    pass
            # LOG.debug('\n ------ \n')
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                session.rollback()
            except Exception as ex:
                LOG.error(ex, exc_info=True)
                session.rollback()
        LOG.info('importing done!')
