##################################
#
# int_cams_reject_feedin.py
#
##################################
from sqlalchemy.exc import IntegrityError

from app import model as M
from app import constants as C
from app.log import get_logger
LOG = get_logger()


class IntCamsRejectFeedIn(M.Base, M.CamsRejectFeedInMixin):
    """
    Intermediate table for rejected CAMS transactions.
    Exactly like stg_cams_reject_feedin, except table name.
    """
    __tablename__ = 'int_cams_reject_feedin'

    @staticmethod
    def import_stg(session):
        """
        Imports unprocessed rows into int_cams_reject_feedin

        TODO: Re-use code from IntCamsFeedIn
        """
        LOG.info('importing into int_cams_reject_feedin')
        rows = session.query(M.StgCamsRejectFeedIn).\
            filter(M.StgCamsRejectFeedIn.is_processed.is_(False)).all()

        for r in rows:
            nr = IntCamsRejectFeedIn()
            for key in dir(nr):
                if key[0] == '_' or key in ('id', 'created_on', 'modified_on'):
                    continue
                try:
                    setattr(nr, key, getattr(r, key, None))
                except KeyError:
                    pass
            session.add(nr)
            r.is_processed = True
            try:
                session.commit()
            except IntegrityError:
                # ignores duplicate rows.
                session.rollback()
            except Exception as ex:
                LOG.error(ex)
                session.rollback()

        LOG.info('Finished importing into int_cams_reject_feedin')

    @staticmethod
    def update_transactions(session):
        """
        Based on rejected rows in int_cams_reject_feedin update
        customer_transactions
        """
        rows = session.query(IntCamsRejectFeedIn).\
            filter(IntCamsRejectFeedIn.is_processed.is_(False)).all()

        LOG.info('CAMS Rejected Feed In: Processing {} rows'.
                 format(len(rows)))

        for r in rows:
            try:
                tr = session.query(M.Transaction).get(r.user_trxnn)
                if tr:
                    # LOG.debug('{} - {}'.format(tr.id, tr.amount))
                    tr.note = r.remarks
                    tr.status = C.TR_STATUS_FAILURE
                    session.add(tr)
                r.is_processed = True
                session.add(r)
            except Exception as ex:
                LOG.error(ex)
                raise ex

        # commit all updated rows.
        try:
            session.commit()
        except Exception as ex:
            LOG.error(ex)
            session.rollback()

        return len(rows or [])
