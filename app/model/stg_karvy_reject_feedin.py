###############################################################
#
# stg_karvy_reject_feedin.py
#
# stores raw dump from Karvy rejection dbf.
###############################################################


from sqlalchemy import (
    Column, String, Numeric, Boolean
)
from sqlalchemy.exc import IntegrityError
import dbf

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class KarvyRejectFeedInMixin(object):
    """
    Columns for Karvy rejection feed defined here.
    """
    fund = Column(String)
    scheme = Column(String)
    pln = Column(String)
    option = Column(String)
    schcode = Column(String)
    acno = Column(Numeric)
    branch = Column(String)
    trno = Column(Numeric, unique=True)
    name = Column(String)
    trtype = Column(String)
    trdate = Column(String)
    amount = Column(Numeric)
    units = Column(Numeric)
    agent = Column(String)
    batchno = Column(String)
    remarks = Column(String)

    # how we track whether this row has been 'processed'
    is_processed = Column(Boolean, default=False)


class StgKarvyRejectFeedIn(Base, KarvyRejectFeedInMixin):
    """
    The structure of this table is translated from Karvy's reverse feed
    dbf file. The column names are same as far as possible.
    """
    __tablename__ = 'stg_karvy_reject_feedin'

    @staticmethod
    def import_dbf(session, file_name):
        """
        Imports a dbf into the table
        """
        LOG.info('importing {} ...'.format(file_name))
        table = dbf.Table(file_name.split('.dbf')[0])
        table.open()
        for record in table:
            r = StgKarvyRejectFeedIn()
            # iterate through all columns, pick the values from record
            # and set attributes.
            for key in table.field_names:
                try:
                    value = record[key]
                    if isinstance(value, str):
                        # remove all spaces around value if str
                        value = value.strip()
                    # LOG.debug('{}: {}'.format(key, value))
                    setattr(r, key, value)
                except KeyError:
                    pass
            # LOG.debug('\n ------ \n')
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                # ignore rows that are duplicates.
                # TODO: Use DO NOTHING constructs later.
                # LOG.warn('Unique error: Ignoring {}'.format(record.unqno))
                session.rollback()
            except Exception as ex:
                # other exceptions.
                LOG.error(ex)
                session.rollback()
        LOG.info('importing {}: FINISHED!'.format(file_name))
