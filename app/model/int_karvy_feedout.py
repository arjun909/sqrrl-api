from sqlalchemy import (
    Column,
    String,
    Numeric,
    DateTime,
    Date
)
from app.model import Base
from app import log

LOG = log.get_logger()


class KarvyFeedOut(Base):

    __tablename__ = 'int_karvy_feedout'
    # Mandatory
    amc_code = Column(String(3))
    # Mandatory
    broker_code = Column(String(20))
    sub_broker_code = Column(String(15))
    # Mandatory
    user_code = Column(String(10))
    # Mandatory
    user_transaction_number = Column(Numeric)
    application_number = Column(String(10))
    # Mandatory
    folio_number = Column(String(15))
    cheque_digit_number = Column(String(10))
    # Mandatory
    transaction_type = Column(String(7))
    # Mandatory
    scheme_code = Column(String(5))
    # Mandatory
    first_name = Column(String(70))
    joint_name1 = Column(String(35))
    joint_name2 = Column(String(35))
    # Mandatory
    address1 = Column(String(40))
    # Mandatory
    address2 = Column(String(40))
    # Mandatory
    address3 = Column(String(40))
    # Mandatory
    city = Column(String(35))
    # Mandatory
    pincode = Column(String(15))
    phone_num_office = Column(String(15))
    mobile_number = Column(String(15))
    # Mandatory
    transaction_date = Column(Date)
    # Mandatory
    transaction_time = Column(String(10))
    # Mandatory
    units = Column(Numeric)
    # Mandatory
    amount = Column(Numeric)
    # Mandatory
    close_account_ch = Column(String(1))
    # format : MM/dd/yyyy
    date_of_birth = Column(Date)
    guardian_name = Column(String(35))
    # Mandatory
    pan_number = Column(String(40))

    phone_residence = Column(String(15))
    office_fax_num = Column(String(15))
    residence_fax_num = Column(String(15))
    email = Column(String(50))
    # Mandatory
    account_number = Column(String(40))
    # Mandatory
    account_type = Column(String(5))
    # Mandatory
    bank_name = Column(String(40))
    # Mandatory
    branch_name = Column(String(40))
    # Mandatory
    bank_city = Column(String(35))
    # Mandatory
    reinvest_tag = Column(String(1))
    # Mandatory
    hold_nature = Column(String(2))
    # Mandatory
    occupation_code = Column(String(2))
    # Mandatory
    tax_status = Column(String(2))
    remarks = Column(String(250))
    state = Column(String(2))
    # Mandatory
    second_pan_holder = Column(String(40))
    # Mandatory
    third_pan_holder = Column(String(40))
    # Mandatory
    guardian_pan = Column(String(40))
    # Mandatory
    location = Column(String(10))
    unino = Column(String(10))
    form6061 = Column(String(1))
    form6061j1 = Column(String(1))
    form6061j2 = Column(String(1))
    # Mandatory
    payment_mechanism = Column(String(1))
    rtgs_code = Column(String(11))
    neft_code = Column(String(11))
    micr_code = Column(String(9))
    # Mandatory
    dep_bank = Column(String(40))
    # Mandatory
    dep_acno = Column(String(50))
    # Mandatory format : MM/dd/YYYY
    dep_date = Column(DateTime)
    # Mandatory for purchases
    dep_reference_number = Column(String(25))
    # Mandatory
    sub_transaction_type = Column(String(6))
    # Mandatory
    sip_reference_number = Column(String(8))
    # Mandatory
    sip_registration_date = Column(DateTime)
    nominee_name = Column(String(40))
    # Mandatory
    nominee_relation = Column(String(40))
    # Mandatory
    kyc_flag = Column(String(1))
    # Mandatory
    poa_stat = Column(String(1))
    # Mandatory
    mode_transaction = Column(String(1))
    # Mandatory
    sign_verification = Column(String(1))
    # Mandatory
    customer_id = Column(String(20))
    # Mandatory if mode transation is W or T
    log_WT = Column(String(70))
    # Mandatory if mode transation is P or E
    log_PE = Column(String(70))
    dpid = Column(String(25))
    client_id = Column(String(25))
    # Mandatory in case purchase by nri's
    nri_source_of_fund = Column(String(25))
    # Mandatory in case of Minor
    guardian_relation = Column(String(40))
    # Mandatory if PAN of the 1st holder is provided
    pan_validation = Column(String(1))
    # Mandatory for MINOR Investors
    gpan_validation = Column(String(1))
    # Mandatory if PAN of the 1st Joint holder is provided
    j1pan_validation = Column(String(1))
    # Mandatory if PAN of the 2nd Joint holder is provided
    j2pan_validation = Column(String(1))
    nri_address1 = Column(String(40))
    nri_address2 = Column(String(40))
    nri_address3 = Column(String(40))
    nri_city = Column(String(35))
    nri_state = Column(String(2))
    nri_country = Column(String(35))
    nri_pincode = Column(String(35))
    nominee_date_of_birth = Column(DateTime)
    nom_perc = Column(Numeric)
    nominee_address1 = Column(String(40))
    nominee_address2 = Column(String(40))
    nominee_address3 = Column(String(40))
    nominee_city = Column(String(35))
    nominee_state = Column(String(2))
    nominee_country = Column(String(35))
    nominee_pin = Column(String(35))
    nominee_email = Column(String(50))
    nominee_mobile = Column(String(20))
    nominee1_name = Column(String(40))
    # Mandatory if nominee 2 exists
    nominee1_relation = Column(String(40))
    nominee1_date_of_birth = Column(DateTime)
    # Mandatory if nominee 1 exists
    nominee1_perc = Column(Numeric)
    nominee1_address1 = Column(String(40))
    nominee1_address2 = Column(String(40))
    nominee1_address3 = Column(String(40))
    nominee1_city = Column(String(35))
    nominee1_state = Column(String(2))
    nominee1_country = Column(String(35))
    nominee1_pincode = Column(String(35))
    nominee1_email = Column(String(50))
    nominee1_mobile = Column(String(20))
    nominee2_name = Column(String(40))
    # Mandatory if nominee 2 exists
    nominee2_relation = Column(String(40))
    nominee2_date_of_birth = Column(DateTime)
    nominee2_perc = Column(Numeric)
    nominee2_address1 = Column(String(40))
    nominee2_address2 = Column(String(40))
    nominee2_address3 = Column(String(40))
    nominee2_city = Column(String(35))
    nominee2_state = Column(String(2))
    nominee2_country = Column(String(35))
    nominee2_pincode = Column(String(35))
    nominee2_email = Column(String(50))
    nominee2_mobile = Column(String(20))
    # Mandatory for NRI Investors for Fresh / Additional Purchases
    firc_sta = Column(String(1))
    sip_stdt = Column(DateTime)
    sip_endt = Column(DateTime)
    sip_noinst = Column(Numeric)
    sip_freq = Column(String(1))
    euin = Column(String(20))
    euin_optin = Column(String(1))
    e1 = Column(String(50))
    e2 = Column(String(50))
    e3 = Column(String(50))
    e4 = Column(String(50))
    e5 = Column(String(50))
    e6 = Column(String(50))
    income_group = Column(String(30))
    politically_exposed = Column(String(1), default='N')
    occupation_code_flag = Column(String(1), default='N')
    sip_amount = Column(Numeric)
