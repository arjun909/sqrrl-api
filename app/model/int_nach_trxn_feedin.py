from sqlalchemy import (
    Column, DateTime, String, Integer, Boolean
    )
from datetime import datetime
from app.model import StgNachTrxnFeedIn, Transaction
from app.model import Base
from app.log import get_logger
from app import constants as C
LOG = get_logger()


class IntNachTrxnFeedIn(Base):
    """
    Loads values from stg table to int table
    """
    __tablename__ = 'int_nach_trxn_feedin'

    mandate_id = Column(String)
    bank_account_holder_name = Column(String)
    bd_amc_code = Column(String)
    rta_code = Column(String)
    folio_no = Column(String)
    debit_date = Column(DateTime)
    amount = Column(Integer)
    status = Column(String)
    reason = Column(String)
    trxn_id = Column(String)
    date_received = Column(DateTime)
    pv_no = Column(String)

    # how to track if the row has been processed
    is_processed = Column(Boolean, default=False)

    @staticmethod
    def import_stg(session):
        session.query(IntNachTrxnFeedIn).delete()
        q = session.query(StgNachTrxnFeedIn).\
            filter(StgNachTrxnFeedIn.is_processed.is_(False)).all()
        new_rows = []
        for row in q:
            nr = IntNachTrxnFeedIn()
            nr.mandate_id = row.mandate_id
            nr.bank_account_holder_name = row.bank_account_holder_name
            nr.bd_amc_code = row.bd_amc_code
            nr.rta_code = row.rta_code
            nr.folio_no = row.folio_no
            nr.debit_date = datetime.strptime(
                                row.debit_date.split('.')[0], r'%Y%m%d'
                                              )
            nr.amount = int(row.amount.split('.')[0])
            nr.status = row.status
            nr.reason = row.reason
            nr.trxn_id = row.trxn_id
            nr.date_received = datetime.strptime(
                            row.date_received.split('.')[0], r'%d%m%Y')
            nr.pv_no = row.pv_no
            new_rows.append(nr)
            row.is_processed = True
            session.add(row)
        if new_rows:
            session.add_all(new_rows)
            try:
                session.commit()
            except Exception as ex:
                LOG.error(ex, exc_info=True)
                session.rollback()

    @staticmethod
    def update_transaction(session):
        q = session.query(IntNachTrxnFeedIn).\
            filter(IntNachTrxnFeedIn.is_processed.is_(False)).all()

        updated_transactions = []

        for row in q:
            trxn_id = row.folio_no
            trxn_row = session.query(Transaction).get(trxn_id)

            if not trxn_row:
                LOG.warn('NACH transaction row not found', extra={
                    'trxn_id': trxn_id
                })
                continue

            # update status - ready for RTA processing.
            if row.status == 'PAID':
                trxn_row.status = C.TR_STATUS_PROCESSING
                trxn_row.extras.update({
                    'PV_NO': row.pv_no,
                    'NACH_TRXN_ID': row.trxn_id
                })
                updated_transactions.append(trxn_row)
            else:
                trxn_row.status = C.TR_STATUS_FAILURE
                trxn_row.note = row.reason

            if trxn_row.status == C.TR_STATUS_PROCESSING:
                # is_first
                t_first_count = session.query(Transaction).\
                    filter(Transaction.investment_id == trxn_row.investment_id,
                           Transaction.is_first).count()
                if not t_first_count:
                    trxn_row.is_first = True

            # mark row as processed.
            row.is_processed = True
            session.add(row)

            # update this transaction.
            session.add(trxn_row)
        try:
            # commit to db.
            session.flush()
            session.commit()
        except Exception as ex:
            LOG.error('there was an exception!')
            session.rollback()

        # update connected investments
        for r in updated_transactions:
            r.investment.update_totals()
