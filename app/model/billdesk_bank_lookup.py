#################################################################
# billdesk_bank_lookup.py
# Stores mapping of bank codes with Billdesk codes as per the
# lookup table provided by them.
#################################################################
from sqlalchemy import Column, String
from fuzzywuzzy import process

from app.model import Base
from app.utils import RedisWrapper
from app.log import get_logger
LOG = get_logger()

CACHE_PREFIX = '_cached:bank:'
REDIS_IFSC_KEY = '_cached:ifsc'


class BilldeskBankLookup(Base):
    __tablename__ = 'billdesk_bank_lookup'
    code = Column(String)
    value = Column(String)
    ifsc = Column(String, nullable=False)

    @staticmethod
    def find_by_bank(bank_name, session):
        """
        Does a fuzzy match on the bank name and returns the corresponding code
        http://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python
        """
        # Check cache in redis.
        rdb = RedisWrapper().get_redis_client()
        key = CACHE_PREFIX + bank_name.lower().replace(' ', '_')
        code = rdb.get(key)
        if code:
            LOG.debug('Returning cached value')
            return code.decode('ascii')

        rows = session.query(BilldeskBankLookup).all()

        # clean up the query string a bit
        bank_name = bank_name.lower().\
            replace('limited', '').replace('ltd', '').replace('pvt', '').\
            strip()

        banks = [{r.code: r.value} for r in rows]
        result, score = process.extractOne(bank_name, banks)
        LOG.debug('{}, score: {}'.format(result, score))
        if score >= 90:
            # found a match of the form {code: value}
            code, value = result.popitem()
            # add to cache for 3 days
            rdb.set(key, code, 86400*3)
            return code
        else:
            LOG.warn('No match for {}'.format(bank_name))
            return ''

    @staticmethod
    def is_supported(ifsc, session):
        """
        Returns true if this IFSC code is supported, i.e., it exists
        in this table as a supported net banking bank.
        """
        if not ifsc or len(ifsc) != 11 or ifsc[4] != '0':
            LOG.warn('Invalid IFSC', extra={'ifsc': ifsc})
            raise ValueError('Invalid IFSC provided')

        # the first four characters of an IFSC code form the bank code
        ifsc_bank_code = ifsc[:4]

        # check if this code is in our redis set
        rdb = RedisWrapper().get_redis_client()

        # first we need to check if redis has members in this set
        if not rdb.scard(REDIS_IFSC_KEY):
            # cache all unique ifsc codes into redis set
            LOG.debug('updating cache {}'.format(REDIS_IFSC_KEY))
            for row in session.query(BilldeskBankLookup.ifsc).distinct():
                rdb.sadd(REDIS_IFSC_KEY, row.ifsc)
        result = rdb.sismember(REDIS_IFSC_KEY, ifsc_bank_code)
        return result

    @staticmethod
    def find_by_ifsc(ifsc, session):
        """
        Returns the code for a given bank IFSC
        """
        if not ifsc:
            raise ValueError('ifsc is missing')

        # first four characters are bank code
        ifsc_bank_code = ifsc[:4]
        result = session.query(BilldeskBankLookup).\
            filter(BilldeskBankLookup.ifsc == ifsc_bank_code).first()
        code = result.code if result else None
        if not code:
            LOG.warn('No match found', extra={'ifsc': ifsc})
        return code

    @staticmethod
    def get_supported_ifsc_codes(session):
        """
        Returns the list of all supported IFSC codes, which apps can use
        to stop invalid data right when it is typed in.
        """
        query = session.query(BilldeskBankLookup.ifsc).distinct()
        codes = [r.ifsc for r in query.all()]
        return codes
