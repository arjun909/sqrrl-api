from sqlalchemy import (
    Column, String, Integer, DateTime
)
# from app.model import Base
from app import log
from app.model import Base, StgNachRegisterFeedIn, Customer
from app.constants import (
    NACH_REGISTRATION_SUCCESS, NACH_REGISTRATION_FAILURE)
from datetime import datetime

LOG = log.get_logger()


class IntNachRegisterFeedIn(Base):
    __tablename__ = 'int_nach_register_feedin'
    mandate_id = Column(String)
    investor_name = Column(String)
    upper_limit_amount = Column(Integer)
    bank_account_holder_name = Column(String)
    bank_account_number = Column(String)
    bank_name = Column(String)
    ifsc_code = Column(String)
    bank_account_type = Column(String)
    start_date = Column(DateTime)
    end_date = Column(String)
    status = Column(String)
    umrn = Column(String)
    remarks = Column(String)

    @staticmethod
    def import_stg(session):
        q = session.query(StgNachRegisterFeedIn).\
            filter(StgNachRegisterFeedIn.is_processed.is_(False)).all()
        new_records = []
        for row in q:
            nr = IntNachRegisterFeedIn()
            nr.mandate_id = row.mandate_id
            nr.investor_name = row.investor_name
            nr.upper_limit_amount = row.upper_limit_amount
            nr.bank_account_holder_name = row.bank_account_holder_name
            nr.bank_account_number = row.bank_account_number
            nr.bank_name = row.bank_name
            nr.ifsc_code = row.ifsc_code
            nr.bank_account_type = row.bank_account_type
            nr.start_date = datetime.strptime(row.start_date, '%d%m%Y')
            nr.end_date = row.end_date
            nr.status = row.status
            nr.umrn = row.umrn
            nr.remarks = row.remarks
            new_records.append(nr)
            row.is_processed = True
        if new_records:
            LOG.info('int nach register feed in new records found.....')
            session.query(IntNachRegisterFeedIn).delete()
            session.add_all(new_records)
            try:
                session.commit()
            except Exception as ex:
                LOG.error(ex)
                session.rollback()

    @staticmethod
    def update_customer_nach_status(session):
        q = session.query(IntNachRegisterFeedIn).all()
        for row in q:
            customer = session.query(Customer).filter(
                    Customer.nach_mandate_id == row.mandate_id).first()
            # if status is valid update status and nach UMRN
            if row.status == 'Valid':
                customer.nach_mandate_status = NACH_REGISTRATION_SUCCESS
                customer.nach_umrn = row.umrn
            # if status is invalid update the reason of failure in
            # nach_mandate_notes
            elif row.status == 'Failed':
                customer.nach_mandate_status = NACH_REGISTRATION_FAILURE
                customer.nach_mandate_note = row.remarks
        try:
            session.commit()
        except Exception as ex:
            LOG.error(ex)
            session.rollback()
