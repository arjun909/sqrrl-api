#################################################################
# cams_location_lookup.py
# Stores mapping of location codes with AMC
#################################################################
from sqlalchemy import Column, String

from app.model import Base
from app.constants import OTHERS
from app.log import get_logger
LOG = get_logger()


class CamsLocationLookup(Base):
    __tablename__ = 'cams_location_lookup'
    code = Column(String)
    value = Column(String)

    # not attaching any ForeignKey here so that recreating Scheme table
    # is easy (otherwise we'd have to cascade etc.)
    amc_code = Column(String)

    @classmethod
    def get_code(cls, session, location_name, amc_code):
        """
        Given a location_name (i.e., a city) and an amc_code, returns the code.
        """
        # here we're doing a full-text search for the given location,
        # as well as "others" which is provided by some AMCs.
        # using Postgres full text search
        city_qs = ' & '.join(location_name.split(' '))
        results = session.query(cls).\
            filter(cls.amc_code == amc_code).\
            filter(
                (cls.value.match(city_qs)) | (cls.value.match(OTHERS))
            ).all()
        if len(results) == 2:
            # found exact match, as well as 'other'
            code = next(r.code for r in results
                        if OTHERS.lower() not in r.name.lower())
        elif len(results) == 1:
            # only one match, return code.
            code = results[0].code
        else:
            # No match?!?! WHATTODO?
            LOG.warn('No match for {} | {}'.format(amc_code, location_name))
            # if no match - using hard-coded value for New Delhi.
            code = 'D1'

        # if code:
        #     LOG.debug('{} | {} | {}'.format(amc_code, location_name, code))
        return code
