# tips.py
from itertools import groupby
import requests
import simplejson

from app.config import TIPS_GSHEETS
from app.constants import TIPS_CACHE
from app.database import rdb
from app.log import get_logger
LOG = get_logger()


class Tips(object):
    @staticmethod
    def get_all():
        """
        Fetches tips from google sheets
        """
        # if there's something in the cache, return it
        cached = rdb.get(TIPS_CACHE)
        if cached:
            return simplejson.loads(cached)  # NOQA

        r = requests.get(TIPS_GSHEETS)
        if r.status_code == 200:
            json = r.json()
            result = {}
            # sort entries by title
            entries = sorted(json['feed']['entry'],
                             key=lambda e: e['title']['$t'])
            # group by category (general, liquid etc.)
            for k, v in groupby(entries, lambda e: e['title']['$t']):
                # Google prefixes "text: " in front of the text.
                # hence extracting the tip text from this string.
                try:
                    v = (e['content']['$t'].split('text: ')[1] for e in v)
                except Exception:
                    pass
                result[k] = list(v)
            # cache for one day
            rdb.set(TIPS_CACHE, simplejson.dumps(result), ex=60*60*24)
            return result
