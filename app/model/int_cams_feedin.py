##################################
#
# int_cams_feedin.py
#
##################################
from sqlalchemy.exc import IntegrityError

from app import model as M
from app import constants as C
from app.log import get_logger
LOG = get_logger()


class IntCamsFeedIn(M.Base, M.CamsFeedInMixin):
    """
    Intermediate table for CAMS feed in.
    Exactly like stg_cams_feedin, except table name.
    """
    __tablename__ = 'int_cams_feedin'

    @staticmethod
    def import_stg(session):
        """
        Imports unprocessed rows of interest into int_cams_feedin
        """
        rows = session.query(M.StgCamsFeedIn).\
            filter(M.StgCamsFeedIn.is_processed.is_(False)).all()

        # LOG.debug('Found {} rows'.format(len(rows)))
        for r in rows:
            # new row with identical data
            nr = IntCamsFeedIn()
            for key in dir(nr):
                if key[0] == '_' or key in ('id', 'created_on', 'modified_on'):
                    continue
                try:
                    setattr(nr, key, getattr(r, key, None))
                except KeyError:
                    pass
            session.add(nr)
            r.is_processed = True
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                # ignores duplicate rows.
                session.rollback()
            except Exception as ex:
                LOG.error(ex)
                session.rollback()

    @staticmethod
    def update_transactions(session):
        """
        mothership.

        We have rows in int_karvy_feedin - need to create or update
        corresponding Transactions.
        """
        rows = session.query(IntCamsFeedIn).\
            filter(IntCamsFeedIn.is_processed.is_(False)).all()

        LOG.info('CAMS feedin - Processing {} rows'.format(len(rows)))

        updated_investment_ids = []
        for r in rows:
            try:
                # the id we supplied is in inwardnum1
                tr = M.Transaction.find_by_feed_row(session, r, C.CAMS)

                # mark rows as processed
                r.is_processed = True
                session.add(r)

                # we are only interested in processing transactions
                if not tr or not tr.status == C.TR_STATUS_PROCESSING:
                    continue

                # TODO: Check amounts.
                LOG.debug('update {}'.format(tr.id))

                # 1. update folio no. if needed
                if not tr.investment.scheme_folio:
                    tr.investment.scheme_folio = r.folio_no
                # 2. update units, status
                tr.confirm_and_update(
                    status=C.TR_STATUS_SUCCESS,
                    units=r.units,
                    transaction_subtype=r.trxntype
                )
                session.add(tr)

                # track updated investment ids
                updated_investment_ids.append(tr.investment_id)
            except Exception as ex:
                LOG.error(ex)
                raise ex

        # update investment rows
        if updated_investment_ids:
            investment_rows = session.query(M.Investment.group_id).\
                filter(M.Investment.id.in_(updated_investment_ids)).\
                distinct().all()
            for group_id in investment_rows:
                M.Investment.post_verification(session, group_id)
        try:
            session.commit()
        except Exception as ex:
            LOG.error(ex)
            session.rollback()
        return len(rows or [])
