from sqlalchemy import (
    Column,
    String,
    Numeric,
    Date
)
from app.model import Base
from app import log

LOG = log.get_logger()


class IntNachRegisterFeedOut(Base):

    __tablename__ = 'int_nach_register_feedout'
    nach_mandate_id = Column(String)
    name = Column(String)
    amc_code = Column(String)
    folio_no = Column(String)
    rta_code = Column(String)
    amount = Column(Numeric(scale=4))
    debit_date = Column(Date)
