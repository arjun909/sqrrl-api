# karvy_location_lookup
from sqlalchemy import Column, String
from app.model import Base


class KarvyLocationLookup(Base):
    """
    Lookup table for Karvy feed location codes.
    """
    __tablename__ = 'karvy_location_lookup'
    code = Column(String)
    value = Column(String)

    @classmethod
    def get_code_by_value(cls, value, session):
        """
        For a given value like 'Ahmedabad'
        return the location code.
        """
        # we store values in lowercase
        value = value.lower()
        if not value:
            # the fallback value
            value = 'others'

        row = session.query(cls).filter(cls.value == value).first()
        if not row:
            # nothing found, search for 'others'
            value = 'others'
            row = session.query(cls).filter(cls.value == value).first()
        return row.code
