# transaction.py
from datetime import datetime
import datetime as DATETIME_PKG
from sqlalchemy import (
    Column,
    String,
    Boolean,
    Integer,
    Numeric,
    DateTime,
    ForeignKey,
    cast,
    Date,
    func
)
from sqlalchemy.orm import relationship, backref, object_session
from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict

from app.utils.shortuuid import generate
from app.utils.helpers import send_transaction_sms
from app.model import Base, Customer, Investment, Scheme, Product
from app.constants import (
    NACH, INVEST, TR_STATUS_INITIATED, TR_STATUS_PENDING,
    TR_STATUS_PROCESSING, TR_STATUS_SUCCESS, TR_STATUS_FAILURE,
    TR_NOTIFIED_CUSTOMER, CAMS, KARVY, REDEEM, FT, FT_MIN_TR_ID,
    SMS_SUCCESS_TEMPLATE, SMS_FAILURE_TEMPLATE,
    PAYMENT_MODE_NB, SWITCH_IN, SWITCH_OUT, LIQUID, TAX, GOAL
)
from app.communications.email_constants import TYPE_1, TYPE_2, TYPE_3
# from app.tasks import communications_mail
import app
from app import log
LOG = log.get_logger()


class Transaction(Base):
    """
    Stores every (money) transaction
    """
    # TRANSACTION is a reserved word in SQL, using customer_transaction
    __tablename__ = 'customer_transaction'

    # establish relationship with customer table
    customer_id = Column(Integer, ForeignKey('customer.id'))
    customer = relationship(
        Customer,
        backref=backref('transactions', uselist=True, cascade='delete,all')
    )

    # relationship with Investment table
    investment_id = Column(Integer, ForeignKey('investment.id'))
    investment = relationship(
        Investment,
        backref=backref('transactions', uselist=True, cascade='delete,all')
    )

    # transaction_group_id
    # one transaction per investment.group_id will be split into multiple
    # transactions per scheme - hence we keep track of the transaction group.
    group_id = Column(String)

    # the transaction amount and fund units
    amount = Column(Numeric(scale=4))
    units = Column(Numeric(scale=4))

    # this is the amount that the customer sees - the total for this group.
    group_amount = Column(Numeric(scale=4))

    # INVEST or REDEEM
    transaction_type = Column(String, default=INVEST)
    # transaction sub-type. 'S' for SIP, 'Normal',
    # 'DR' for Dividend reinvestments
    transaction_subtype = Column(String)

    # transaction status - pending, success, failure etc.
    status = Column(String)
    # mode (Netbanking, Debit, NACH)
    mode = Column(String)

    # transaction reference - usually PG transaction reference
    transaction_reference = Column(String)

    # PG reference
    bank_transaction_id = Column(String)
    bank_transaction_on = Column(DateTime)

    # note - can be used to track failure reason etc.
    note = Column(String)

    # tracks the IP address of customers for each transaction
    ip_address = Column(String)

    # when this transaction was verified (using feed)
    verified_on = Column(DateTime)

    # if this is the first transaction for this investment.
    is_first = Column(Boolean, default=False)

    # a catch-all key-value store for some minor associated data.
    extras = Column(MutableDict.as_mutable(HSTORE), default={
        TR_NOTIFIED_CUSTOMER: '0'
    })
    is_processed = Column(Boolean, default=False)
    utr_no = Column(String)
    is_utr_processed = Column(Boolean, default=False)

    @staticmethod
    def initiate(investment_group_id, amount, transaction_type, session, ip,
                 tr_mode=PAYMENT_MODE_NB):
        """
        Initiates a transaction group for a given investment group id.

        NOTE: For a given transaction group, we create a transaction row
        per investment row in the group.
        """
        LOG.debug('initiating transaction for: {}'.format(investment_group_id))
        investment_rows = session.query(Investment).\
            filter(Investment.group_id == investment_group_id)

        if not investment_rows:
            raise Exception('No such investment_group_id {}'.
                            format(investment_group_id))

        # create a new group for these transactions
        # trxn_mode_nach = None
        tr_group_id = generate(prefix='TG')
        t_rows = []
        # this is not needed, but keeping for compatibility
        tr_status = TR_STATUS_INITIATED
        # ivt = session.query(Investment).\
        #     filter(Investment.group_id == investment_group_id).first()
        # cust = ivt.customer
        # if transaction_type == INVEST:
        #     LOG.debug('mandate_status: {}'.format(cust.nach_mandate_status))
        #     # if customer's mandate status is SUCCESS then mode is NACH
        #     if cust.nach_mandate_status == NACH_REGISTRATION_SUCCESS:
        #         # if mode is NACH update tr_status as QUEUED else INITIATED
        #         tr_status = TR_STATUS_QUEUED
        #         tr_mode = NACH

        for ir in investment_rows:
            tr = Transaction()
            tr.customer_id = ir.customer.id
            tr.ip_address = ip
            tr.investment_id = ir.id
            tr.group_id = tr_group_id
            tr.status = TR_STATUS_INITIATED
            tr.group_amount = amount
            tr.transaction_type = transaction_type
            tr.amount = amount * ir.allocation_percent / 100
            # for investments / purchases, we need to populate mode
            tr.mode = tr_mode if tr.transaction_type == INVEST else None
            # validate amount
            if (tr.transaction_type == INVEST
                    and tr.amount < ir.scheme.minimum_sip_amount):
                raise ValueError('min. sip amount is higher '
                                 'than allocated amount')
            t_rows.append(tr)

        # Add to db session
        session.add_all(t_rows)

        # NOTE: do not commit here, commit in the caller
        # so that we can rollback if some other failure happens outside
        # this method.
        LOG.debug('Created transaction group {}'.format(tr_group_id))
        return tr_group_id, tr_status, tr_mode

    @staticmethod
    def update_group(tr_group_id, session, **kwargs):
        """
        For a given transaction group, update the reference number.
        This reference is provided by third-party (reliance, billdesk)
        """
        try:
            LOG.debug('updating transaction group {}'.format(tr_group_id))
            updates = {}
            reference = kwargs.pop('reference', None)
            status = kwargs.pop('status', None)
            note = kwargs.pop('note', None)
            # this is for Reliance / Sqrrl Away, where group = single row
            units = kwargs.pop('units', None)
            extras = kwargs.pop('extras', None)
            mode = kwargs.pop('mode', None)
            if reference:
                updates['transaction_reference'] = reference
            if status:
                # validate passed status
                if status not in (
                    TR_STATUS_INITIATED,
                    TR_STATUS_PROCESSING,
                    TR_STATUS_SUCCESS,
                    TR_STATUS_FAILURE,
                    TR_STATUS_PENDING
                ):
                    raise Exception('invalid status {}'.format(status))
                updates['status'] = status
            bank_transaction_id = kwargs.pop('bank_transaction_id', None)
            if bank_transaction_id:
                updates['bank_transaction_id'] = bank_transaction_id
            bank_transaction_on = kwargs.pop('bank_transaction_on', None)
            if bank_transaction_on:
                updates['bank_transaction_on'] = bank_transaction_on
            if note:
                updates['note'] = note
            if units:
                updates['units'] = units
            if extras:
                # NOTE: This overwrites previous extras. Need to implement
                # mutable dictionary here.
                updates['extras'] = extras
            if mode:
                updates['mode'] = mode
            # if a transaction is set as PROCESSING (i.e., customer completed
            # the transaction) - set additional flag is_first
            if status and status == TR_STATUS_PROCESSING:
                # if there are no other "first" transactions, then this is it!
                investment_id = session.query(Transaction.investment_id).\
                    filter(Transaction.group_id == tr_group_id).\
                    first().investment_id
                t_first_count = session.query(Transaction).\
                    filter(Transaction.investment_id == investment_id,
                           Transaction.is_first).count()
                # LOG.debug('t_first_count: {}'.format(t_first_count))

                updates['is_first'] = True if (not t_first_count) else False

            # LOG.debug('Updates: {}'.format(updates))
            query = session.query(Transaction).\
                filter(Transaction.group_id == tr_group_id)
            transaction_rows = query.all()
            query.update(updates)

            session.commit()

            # update next investment date, totals for investment rows
            if status:
                for r in transaction_rows:
                    r.investment.update_totals()
                    if status in (TR_STATUS_PROCESSING, TR_STATUS_SUCCESS):
                        r.investment.update_next_investment_on()

            # checks if this transaction is IS_PROCESSING,
            # and sends an sms.
            Transaction.notify_customer(session, tr_group_id)

            # Transaction is first, sent mailers for first transaction.
            # Check for is_first flag and to be double sure, check transaction_type also.  # NOQA
            if ('is_first' in updates and updates['is_first'] and
                    transaction_rows[0].transaction_type == INVEST):
                # Make this send_first_successful_transaction_mail function an async function
                # Or use Gevent, to run this as an independent async task.
                # Following is a blocking call..!!
                Transaction.send_first_successful_transaction_mail(session, transaction_rows)

        except Exception as ex:
            # If this breaks, we need to be able to retry in the future.
            LOG.error('update_group failed', exc_info=True)
            session.rollback()
            raise ex

    @staticmethod
    def belongs_to_customer(customer_id, tr_group_id, session):
        """
        Return if row exists for customer_id + transaction_group_id
        """
        q = session.query(Transaction.id).\
            filter(Transaction.group_id == tr_group_id,
                   Transaction.customer_id == customer_id)
        is_valid = session.query(q.exists()).scalar()
        return is_valid

    @staticmethod
    def fetch(customer_id, tr_group_id, session, grouped=False):
        """
        Returns transaction rows for a group_id

        The customer_id filters is a sort of security to make sure
        we don't return rows for the wrong customer.
        """
        rows = session.query(Transaction).\
            filter(Transaction.group_id == tr_group_id,
                   Transaction.customer_id == customer_id).all()
        if not rows:
            return {} if grouped else []
        if not grouped:
            # return a row for each transaction.
            result = []
            for r in rows:
                result.append({
                    'group_id': r.group_id,
                    'investment_group_id': r.investment.group_id,
                    'investment_id': r.investment_id,
                    'customer_uid': r.customer.uid,
                    'amount': r.amount,
                    'status': r.status,
                    'reference': r.transaction_reference,
                    'created_on': r.created_on.isoformat(),
                    'modified_on': r.modified_on.isoformat(),
                    'note': r.note,
                    'bank_transaction_id': r.bank_transaction_id,
                    'units': r.units,
                    'transaction_type': r.transaction_type,
                    'transaction_subtype': r.transaction_subtype
                })
        else:
            # return a single dictionary with grouped information.
            # TODO: rewrite without so much repetition
            r = rows[0]
            result = {
                'group_id': r.group_id,
                'investment_group_id': r.investment.group_id,
                'customer_uid': r.customer.uid,
                'amount': r.group_amount,
                'status': r.status,
                'reference': r.transaction_reference,
                'created_on': r.created_on.isoformat(),
                'modified_on': r.modified_on.isoformat(),
                'note': r.note,
                'bank_transaction_id': r.bank_transaction_id,
                'units': r.units,
                'transaction_type': r.transaction_type,
                'transaction_subtype': r.transaction_subtype
            }
        return result

    @staticmethod
    def can_redeem(transaction_group_id, session):
        """
        For a REDEEM transaction, this method returns True if each
        of the redemption amounts are within the available_withdrawal_balance
        for the corresponding investment
        """
        t_rows = session.query(Transaction).\
            filter(Transaction.group_id == transaction_group_id)

        # check 1 - rows should exist!
        if not t_rows:
            raise ValueError('No rows for {}'.format(transaction_group_id))
        for row in t_rows:
            # re-compute everything to be double-sure
            Investment.post_verification(session, row.investment.group_id)
            available = row.investment.available_withdrawal_balance

            # check 2 - should have available_withdrawal_balance
            if not available:
                raise Exception('No available_withdrawal_balance for'
                                ' investment.id {}'.format(row.investment.id))

            # actual check - amount should be within what's available.
            if row.amount > available:
                LOG.warn('{} cannot be more than {} for {}'.
                         format(row.amount, available, transaction_group_id))
                # No can do, sir!
                return False
        return True

    @classmethod
    def summary_by_investment(cls, session, investment_group_id):
        """
        Returns the latest 10 transaction groups for an investment_group_id
        """
        # LOG.debug('summary by investment: {}'.format(investment_group_id))
        # NOTE: To handle some corner cases where individual transactions
        # within a group may have diff. statuses, we use min function as
        # luckily (F)AILURE, (P)ROCESSING and (S)UCCESS are already
        # alphabetically sorted.
        # within a trx group, if one is F and other is P/S, then show F
        # and so on.
        rows = session.query(
            cls.group_id,
            cls.group_amount,
            cls.transaction_type,
            func.min(cls.status).label('status'),
        ).join(Investment).filter(
            Investment.group_id == investment_group_id,
            cls.investment_id == Investment.id,
            cls.status.in_([
                TR_STATUS_PROCESSING, TR_STATUS_SUCCESS, TR_STATUS_FAILURE
            ]),
            cls.transaction_type.in_((INVEST, REDEEM))
        ).group_by(
            cls.group_id,
            cls.group_amount,
            cls.transaction_type,
            func.date_trunc('second', cls.created_on)
        ).having(
            func.min(cls.status).in_(
                [TR_STATUS_PROCESSING, TR_STATUS_SUCCESS]
            )
        ).order_by(
            func.date_trunc('second', cls.created_on).desc()
        ).limit(10).all()
        # LOG.debug(rows)
        return rows

    @classmethod
    def fetch_by_investment(cls, session, investment_group_id):
        """
        Returns the transaction groups for an investment_id
        """
        # fetch all the group-level info from transactions table
        # for a given investment.
        rows = session.query(
            cls.group_id,
            cls.group_amount,
            cls.transaction_type,
            func.min(cls.status).label('status'),
            func.min(cls.created_on).label('created_on')
        ).join(Investment).filter(
            Investment.group_id == investment_group_id,
            cls.investment_id == Investment.id,
            cls.status.in_([
                TR_STATUS_PROCESSING, TR_STATUS_SUCCESS, TR_STATUS_FAILURE,
                TR_STATUS_PENDING
            ]),
            cls.transaction_type.in_((INVEST, REDEEM))
        ).group_by(
            cls.group_id,
            cls.group_amount,
            cls.transaction_type
        ).order_by(func.min(cls.created_on).desc()).all()
        # LOG.debug(rows)
        return rows

    @classmethod
    def processing(cls, session, customer_id):
        # get all transaction groups for a customer indicating
        # transactions in processing state.
        # using min to get ANY investment_id, we use that to fetch the product
        rows = session.query(cls.group_id, cls.group_amount,
                             func.min(cls.investment_id).label('investment'))\
            .filter(cls.customer_id == customer_id,
                    cls.status == TR_STATUS_PROCESSING).\
            group_by(cls.group_id, cls.group_amount,
                     cast(cls.created_on, Date)).\
            order_by(cast(cls.created_on, Date)).all()
        results = []
        for row in rows:
            result = row._asdict()
            # get the product for this row
            # LOG.debug('{}'.format(row))
            investment = session.query(Investment).get(row.investment)
            product = investment.product
            result.update(product_id=product.id,
                          product_display_name=product.display_name,
                          investment_description=investment.description)
            results.append(result)
        return results

    def confirm_and_update(self, **params):
        """
        For a given transaction id - this method is called when
        the transaction is confirmed by validating it in the reverse feed,
        update the transaction and investment rows
        """
        session = object_session(self)
        # update verified_on, units
        self.verified_on = datetime.utcnow()
        self.status = params['status']
        self.transaction_subtype = params.get('transaction_subtype', None)
        if self.status == TR_STATUS_SUCCESS:
            self.units = params['units']

        try:
            session.commit()
        except Exception as ex:
            msg = 'Could not update txn id {}'.format(self.id)
            LOG.error('{}\n{}'.format(msg, ex))

    @classmethod
    def find_by_feed_row(cls, session, feed_row, feed_type):
        """
        Finds a transaction given a feed_row of feed_type CAMS or Karvy.

        1. First tries directly by id.
        2. then tries folio + transaction_reference as filter
        3. finally if folio exists but no transaction - creates a new row.
        """
        if feed_type not in (CAMS, KARVY, FT):
            raise ValueError('Invalid feed_type: {}'.format(feed_type))

        # try searching by id
        tr_id = 0
        if feed_type == KARVY:
            tr_id = int(feed_row.inwardnum1 or 0)
        elif feed_type == CAMS:
            tr_id = int(feed_row.usrtrxno or 0)
        else:
            # Franklin Templeton - APPL_NO contains the transaction id
            # but we need to subtract the initial offset we added while
            # sending the feed out.
            tr_id = int(feed_row.appl_no or 0)
            if tr_id:
                tr_id = tr_id - FT_MIN_TR_ID

        LOG.debug('tr_id: {}'.format(tr_id))
        row = session.query(cls).get(tr_id)
        if row:
            # this is the row of interest
            # LOG.debug('Found by id {}'.format(tr_id))
            return row
        else:
            # let's check folio
            if feed_type == KARVY:
                folio_no = str(feed_row.td_acno)
            else:
                # for CAMS and FT
                folio_no = str(feed_row.folio_no)

            # does this folio exist?
            has_folio = session.query(Investment.id).\
                filter(Investment.scheme_folio == folio_no).first()

            if not has_folio:
                # no folio found - ignore.
                return None
            else:
                # we have the folio - now we check the transaction using
                # IHNO (maps to transaction_reference).
                # This handles RMF scenario where since we're not sending
                # rows for feed-out, we don't receive id in reverse feed.
                if feed_type == KARVY:
                    if feed_row.td_purred in ('P', 'D'):
                        trxn_type = INVEST
                    else:
                        trxn_type = REDEEM
                    ihno = '%{}'.format(feed_row.ihno)
                    row = session.query(cls).filter(
                        cls.transaction_reference.ilike(ihno),
                        cls.transaction_type == trxn_type
                    ).first()
                    if row:
                        # found it!
                        LOG.debug('found folio for ihno {}'.
                                  format(feed_row.ihno))
                        return row

                # we have a folio but no corresponding row
                # for this guy we will need to create a new row.
                # LOG.debug('folio but no row {}, creating'.format(tr_id))
                row = Transaction.create_from_feed_row(session,
                                                       feed_row,
                                                       feed_type,
                                                       folio_no)
                return row

    @classmethod
    def create_from_feed_row(cls, session, feed_row, feed_type, folio_no):
        """
        Creates a new transaction for a given transaction in the reverse feed.
        This is needed to replicate any dividend reinvestment transactions,
        or other transactions that happen in our folios outside of our apps.
        """
        LOG.debug('Creating row: {}'.format(folio_no))
        if feed_type not in (CAMS, KARVY):
            raise ValueError('Invalid feed_type: {}'.format(feed_type))

        if not folio_no:
            raise ValueError('folio_no is required')

        transaction_type = transaction_subtype = None
        if feed_type == KARVY:
            amc_code = feed_row.td_fund
            rta_code = feed_row.schpln
            amount = feed_row.td_amt
            units = feed_row.td_units
            if feed_row.td_purred in ('P', 'D'):
                transaction_type = INVEST
                if feed_row.td_purred == 'D':
                    transaction_subtype = 'D'
            elif feed_row.td_purred == 'R':
                transaction_type = REDEEM
            # LOG.debug('td_purred: {},transaction_type: {}'.
            #           format(feed_row.td_purred, transaction_type))
            # prioritise IHNO as Reliance case is common here.
            transaction_reference = feed_row.ihno or feed_row.td_trno
            created_on = feed_row.td_trdt
            verified_on = datetime.utcnow()
            note = feed_row.nctremarks
        else:
            amc_code = feed_row.amc_code
            # prod_code = amc_code + rta_code
            rta_code = feed_row.prodcode[len(amc_code):]
            amount = feed_row.amount
            units = feed_row.units
            # will be treated as midnight UTC
            verified_on = datetime.utcnow()
            created_on = feed_row.traddate
            if feed_row.trxntype == 'R':
                transaction_type = REDEEM
            elif feed_row.trxntype in ('P', 'DR'):
                transaction_type = INVEST
                if feed_row.trxntype == 'DR':
                    transaction_subtype = 'DR'
            transaction_reference = feed_row.trxnno
            note = feed_row.remarks

        if not transaction_type:
            # this is not a purchase, redemption or dividend reinv, ignore.
            return

        # fetch investment row for the folio
        investment = session.query(Investment).\
            filter(Investment.scheme_folio == folio_no).\
            filter(Investment.scheme_id == Scheme.id).\
            filter(Scheme.amc_code == amc_code).\
            filter(Scheme.rta_code == rta_code).first()
        if not investment:
            LOG.warn('No investment for folio: {} / AMC: {} / Scheme: {}'.
                     format(folio_no, amc_code, rta_code))
            return

        # generating new row.
        tr = Transaction()
        tr.customer_id = investment.customer_id
        tr.investment_id = investment.id
        tr.group_id = generate(prefix='TG')
        tr.status = TR_STATUS_PROCESSING
        tr.created_on = created_on
        tr.verified_on = verified_on
        tr.amount = amount
        tr.group_amount = amount
        tr.transaction_type = transaction_type
        tr.transaction_subtype = transaction_subtype
        tr.transaction_reference = transaction_reference
        tr.units = units
        tr.note = note or 'via reverse feed'

        # add row to db
        # LOG.debug('adding row to db...')
        # from pprint import pprint
        # pprint(vars(tr))
        session.add(tr)
        try:
            session.commit()
            return tr
        except Exception as ex:
            LOG.error(ex)
            session.rollback()
            raise ex

    @classmethod
    def notify_customer(cls, session, tr_group_id):
        """
        Notify (via sms) when a transaction happens, i.e., transaction
        enters status TR_STATUS_PROCESSING.
        """
        t_group = session.query(cls).\
            filter(cls.group_id == tr_group_id).first()

        if not t_group.status == TR_STATUS_PROCESSING:
            # nothing to do.
            return

        if t_group.extras and t_group.extras[TR_NOTIFIED_CUSTOMER] == '1':
            # already notified, skip
            # LOG.warn('Already notified, skipping for {}'.format(tr_group_id))
            return

        # return a dict of things we need to send out the sms
        inv_prod = session.query(Investment, Product).\
            filter(Investment.id == t_group.investment_id,
                   Investment.product_id == Product.id).first()

        # LOG.debug(inv_prod)

        # customer row
        c = session.query(Customer).get(t_group.customer_id)

        # product display
        product_display = (inv_prod.Investment.description
                           or inv_prod.Product.display_name)[:30]

        if t_group.transaction_type == INVEST:
            transaction_type = 'added to'
        elif t_group.transaction_type == REDEEM:
            transaction_type = 'withdrawn from'
        else:
            # switch transaction - just go back.
            return

        # message params
        message = SMS_SUCCESS_TEMPLATE.format(
            amount=t_group.group_amount,
            product=product_display,
            transaction_type=transaction_type,
            group_id=t_group.group_id
        )
        # LOG.debug('message: {}'.format(message))
        sms_payload = [{'to': [c.mobile], 'message': message}]
        # TODO: Wrap in celery task
        send_transaction_sms(sms_payload)
        # update rows to mark as notified
        cls.update_group(tr_group_id, session,
                         extras={TR_NOTIFIED_CUSTOMER: '1'})

    @classmethod
    def notify_failed_transaction(cls, session, tr_group_id):
        """
        In case of Billdesk 0002 (pending) failures - amount is deducted
        from customer but reversed later (in 99% cases.)

        This method sends out an sms to such a customer.
        """
        row = session.query(cls).\
            filter(cls.group_id == tr_group_id).first()

        if row:
            # check if sms is already sent.
            if row.extras and row.extras[TR_NOTIFIED_CUSTOMER] == '1':
                # already notified, skip
                # LOG.warn('Already notified, skipping for {}'.
                #          format(tr_group_id))
                return

            c = session.query(Customer).get(row.customer_id)
            message = SMS_FAILURE_TEMPLATE.format(amount=row.group_amount)
            sms_payload = [{'to': [c.mobile], 'message': message}]
            # TODO: Wrap in celery task
            send_transaction_sms(sms_payload)
            # update rows to mark as notified
            cls.update_group(tr_group_id, session,
                             extras={TR_NOTIFIED_CUSTOMER: '1'})

    @classmethod
    def switch_fund(cls, session, inv_row, ip):
        """
        This will create two rows - switch out and switch in for the given
        investment_row.
        """
        LOG.debug('Inside switch_fund')
        # initiating the switch out, then switch in
        # assuming full switch (all units)
        tr_group_id = generate(prefix='TG')
        tr_out = cls(
            group_id=tr_group_id,
            transaction_type=SWITCH_OUT,
            units=inv_row.scheme_units,
            status=TR_STATUS_INITIATED,
            investment_id=inv_row.id,
            customer_id=inv_row.customer_id,
            ip_address=ip,
        )

        tr_group_id = generate(prefix='TG')
        tr_in = cls(
            group_id=tr_group_id,
            transaction_type=SWITCH_IN,
            status=TR_STATUS_INITIATED,
            investment_id=inv_row.id,
            customer_id=inv_row.customer_id,
            ip_address=ip,
        )

        session.add(tr_out)
        session.add(tr_in)

        LOG.info('created transactions for fund switch', extra={
            'tr_group_id': tr_group_id
        })
        return tr_out.group_id, tr_in.group_id

    @staticmethod
    def send_first_successful_transaction_mail(session, transaction_rows):
        # Step 1: Get the Investment rows to obtain scheme names.
        # Step 2: Using the investment, get the product row.
        # Step 3: Using the customer id from the transaction row, get the Customer row.

        try:
            communication_arguments = {}

            # Database queries
            investment_ids = [tr.investment_id for tr in transaction_rows]

            investment_rows = Investment.get_investment_rows_by_ids(session, *investment_ids)

            product_row = session.query(Product).get(investment_rows[0].product_id)

            customer = session.query(Customer).get(transaction_rows[0].customer_id)

            schemes = session.query(Scheme).filter(
                Scheme.id.in_([investment.scheme_id for investment in investment_rows])).all()

            expected_processing_time = '1-2 business days'
            transaction_amount = transaction_rows[0].group_amount
            scheme_names = ', '.join([scheme.name for scheme in schemes])

            communication_arguments['recipient_mail_id'] = customer.email
            communication_arguments['first_name'] = customer.pan_name.split(' ')[0].capitalize()
            communication_arguments['product_name'] = product_row.display_name
            communication_arguments['amount'] = "Rs. " + str(transaction_amount)
            communication_arguments['expected_processing_time'] = expected_processing_time
            communication_arguments['date_of_investment'] = str(DATETIME_PKG.date
                                                                .today()
                                                                .strftime('%d %B %Y'))

            if product_row.internal_name == LIQUID:
                # Sqrrl Away

                communication_arguments['mail_type'] = TYPE_1
                communication_arguments['scheme_name'] = scheme_names
                communication_arguments['first_minimum_transaction_amount'] = \
                                schemes[0].minimum_initial_investment

                communication_arguments['minimum_sip_amount'] = \
                                schemes[0].minimum_sip_amount

            elif product_row.internal_name == GOAL:
                # BYOD

                minimum_sip_amount = sum([scheme.minimum_sip_amount for scheme in schemes])
                minimum_initial_investment = sum([scheme.minimum_initial_investment for scheme in schemes])

                communication_arguments['mail_type'] = TYPE_2
                communication_arguments['byod_goal_name'] = investment_rows[0].description
                communication_arguments['schemes'] = scheme_names
                communication_arguments['minimum_initial_investment'] = minimum_initial_investment
                communication_arguments['minimum_sip_amount'] = minimum_sip_amount

            elif product_row.internal_name == TAX:
                # Axe Tax

                communication_arguments['mail_type'] = TYPE_3
                communication_arguments['schemes'] = scheme_names

            LOG.info("Sending a congratulatory mail to {} for his first successful "
                     "transaction in {}.".format(customer.pan_name, product_row.display_name))

            # Calling the celery mail task
            app.tasks.communications_mail.delay(**communication_arguments)

        except Exception as mailer_exception:
            LOG.error("First successful transaction mail could not be send "
                      "because an exception has occurred: {}".format(mailer_exception))
