###############################################################
#
# stg_cams_reject_feedin.py
#
# stores raw dump from CAMS rejection dbf.
###############################################################


from sqlalchemy import (
    Column, String, Numeric, Date, Boolean
)
from sqlalchemy.exc import IntegrityError
import dbf

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class CamsRejectFeedInMixin(object):
    """
    Columns for CAMS rejection feed defined here.
    """
    location = Column(String)
    applicatio = Column(String)
    folio_numb = Column(String)
    investor_n = Column(String)
    invest_ad1 = Column(String)
    invest_ad2 = Column(String)
    invest_ad3 = Column(String)
    investor_c = Column(String)
    investor_p = Column(String)
    phone_off = Column(String)
    phone_res = Column(String)
    investor_e = Column(String)
    broker_nam = Column(String)
    product_co = Column(String)
    scheme_nam = Column(String)
    amount = Column(Numeric)
    remarks = Column(String)
    location_c = Column(String)
    user_code = Column(String)
    user_trxnn = Column(Numeric, unique=True)
    scheme_cod = Column(String)
    trxn_type = Column(String)
    posted_dat = Column(Date)
    trade_date = Column(Date)
    entry_date = Column(Date)
    instrm_no = Column(String)
    reject_at = Column(String)
    euin = Column(String)

    # how we track whether this row has been 'processed'
    is_processed = Column(Boolean, default=False)


class StgCamsRejectFeedIn(Base, CamsRejectFeedInMixin):
    """
    The structure of this table is translated from CAMS's reverse feed
    dbf file. The column names are same as far as possible.
    """
    __tablename__ = 'stg_cams_reject_feedin'

    @staticmethod
    def import_dbf(session, file_name):
        """
        Imports a dbf into the table
        """
        LOG.info('importing {} ...'.format(file_name))
        table = dbf.Table(file_name.split('.dbf')[0])
        table.open()
        for record in table:
            r = StgCamsRejectFeedIn()
            # iterate through all columns, pick the values from record
            # and set attributes.
            for key in table.field_names:
                try:
                    value = record[key]
                    if isinstance(value, str):
                        # remove all spaces around value if str
                        value = value.strip()
                    # LOG.debug('{}: {}'.format(key, value))
                    setattr(r, key, value)
                except KeyError:
                    pass
            # LOG.debug('\n ------ \n')
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                # ignore rows that are duplicates.
                # TODO: Use DO NOTHING constructs later.
                # LOG.warn('Unique error: Ignoring {}'.format(record.unqno))
                session.rollback()
            except Exception as ex:
                # other exceptions.
                LOG.error(ex)
                session.rollback()
        LOG.info('importing {}: FINISHED!'.format(file_name))
