from app.model import Base
from sqlalchemy import (Column, String, Boolean, text)


class Referrer(Base):

    __tablename__ = 'referrer'

    name = Column(String)
    description = Column(String)
    link = Column(String)
    is_customer = Column(Boolean, default=False, server_default=text("false"))
    type = Column(String)
