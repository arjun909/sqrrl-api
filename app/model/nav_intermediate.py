from sqlalchemy import (
    Column, String, DateTime
)

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class NavIntermediate(Base):
    """
    Model to describe MF fund or scheme.
    """
    __tablename__ = 'int_nav_daily'

    name = Column(String, nullable=False)
    nav = Column(String)
    isin = Column(String, unique=True)
    nav_date = Column(DateTime)
