# investment.py
from decimal import Decimal, localcontext, ROUND_CEILING
import numpy as np
from datetime import datetime, timedelta, time
from sqlalchemy import (
    Column,
    String,
    Integer,
    Numeric,
    DateTime,
    ForeignKey,
    func,
    cast,
    Date,
    and_,
    Boolean
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm.session import object_session
from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict

from app.model import Base, Customer, Product, Scheme
from app import model as m
from app.utils.shortuuid import generate
from app.utils.helpers import monthdelta, weekdelta, round_up, xirr
from app.constants import (
    WEEKLY, MONTHLY, TR_STATUS_SUCCESS, INVEST, REDEEM,
    TR_STATUS_INITIATED, TR_STATUS_FAILURE, TR_STATUS_PROCESSING,
    GOAL, LIQUID, SWITCH_IN, SWITCH_OUT, NACH, TR_STATUS_PENDING
)
from app import log
LOG = log.get_logger()


class Investment(Base):
    """
    While "Product" represents what the customer sees as a product (e.g.,
    Sqrrl Away), "Investment" is what stores his actual portfolio. An
    Investment is one-per-scheme (or one-per-folio).

    The relationship may be visualised as:

        Product -< Investment -< Transaction

    where -< is a one-many relationship.
    """
    __tablename__ = 'investment'

    # establish relationship with customer table
    customer_id = Column(Integer, ForeignKey('customer.id'))
    customer = relationship(
        Customer,
        backref=backref('investments', uselist=True, cascade='delete,all')
    )

    # reference to Product
    product_id = Column(Integer, ForeignKey('product.id'))
    product = relationship(Product)

    # investment group - this is an important column.
    # this groups investments within a product - for e.g. if BYOD is actually
    # made of 3 funds/investments, then this group will club all three.
    # cannot use product_id for this grouping as a customer may have multiple
    # "groups" for same product. E.g., a BYOD for a Europe trip and a BYOD
    # for an iPhone purchase.
    group_id = Column(String, nullable=False)

    # any user-provided category, description - for BYOD
    category = Column(String)
    description = Column(String)

    # relationship with scheme table
    scheme_id = Column(Integer, ForeignKey('scheme.id'))
    scheme = relationship(Scheme)

    # allocation - percentage split for each scheme across an investment group
    allocation_percent = Column(Numeric(scale=4))

    # the actual folio number and units from the fund.
    scheme_folio = Column(String)
    # units can have 3 decimal points
    scheme_units = Column(Numeric(scale=3))
    # NAV and nav dates
    scheme_nav = Column(Numeric(scale=4))
    scheme_nav_date = Column(DateTime)

    # how much is planned (from customer perspective)
    total_target_amount = Column(Numeric(scale=4))
    # total of investment already made in this product.
    total_investment = Column(Numeric(scale=4), default=0)
    last_investment_on = Column(DateTime)
    next_investment_on = Column(DateTime)
    # completion date (target) chosen by customer.
    target_completion_on = Column(DateTime)
    estimated_completion_on = Column(DateTime)
    # how many instalments are planned, and how many completed.
    target_instalment_count = Column(Integer)
    completed_instalment_count = Column(Integer)
    # Use weekly or monthly.
    instalment_period = Column(String)

    # for BYOD
    target_roi = Column(Numeric(scale=4))

    # per-instalment amount
    # where we know total target investment and instalments,
    # this is total_target_amount / target_instalment_count
    target_instalment_amount = Column(Numeric(scale=4))

    # withdrawal related fields.
    available_withdrawal_balance = Column(Numeric(scale=4))
    # how much has already been withdrawn / redeemed.
    total_withdrawal = Column(Numeric(scale=4))
    # how many times customer has withdrawn
    total_withdrawal_count = Column(Integer)

    # net = total_investment - total_withdrawal
    net_investment = Column(Numeric(scale=4))
    # based on today's NAV, what the current worth is
    current_value = Column(Numeric(scale=4))
    # net gain = current_value - net_investment
    net_gain = Column(Numeric(scale=4))
    # net_gain / net_investment
    net_gain_percentage = Column(Numeric(scale=2))

    # any other stats can be stored as key-values
    stats = Column(HSTORE)

    # tracks the IP address of customers for new investments
    ip_address = Column(String)

    # boolean to track whether this investment row has processing transactions
    # this will help us with updating investment-level fields, as well as
    # determining which tranactions should go into feed-out.
    has_processing_transactions = Column(Boolean, default=False)

    # boolean to determine if this investment row is closed
    # this may happen in full redemption (i.e., reached goal)
    is_closed = Column(Boolean, default=False)

    # To check if a BYOD has completed or not
    is_completed = Column(Boolean, nullable=True)

    # extras - for any extra information e.g., fund switch details
    # a catch-all key-value store for some minor associated data.
    extras = Column(MutableDict.as_mutable(HSTORE), default={})

    @staticmethod
    def update_nav(session):
        """
        update nav columns after doing checks for None values on dates
        """
        investments = session.query(Investment).all()

        for investment in investments:
            # if investment nav_date is None check Scheme_nav_last_updated_on
            # and if it's True update investment nav_date and nav
            if investment.scheme_nav_date is None:
                if investment.scheme.nav_last_updated_on:
                    investment.scheme_nav_date = \
                        investment.scheme.nav_last_updated_on
                    investment.scheme_nav = investment.scheme.nav
            else:
                if investment.scheme.nav_last_updated_on:
                    if (investment.scheme.nav_last_updated_on >
                       investment.scheme_nav_date):
                        investment.scheme_nav_date = \
                            investment.scheme.nav_last_updated_on
                        investment.scheme_nav = investment.scheme.nav
        session.commit()

    @staticmethod
    def update_current_value(session):
        """
        Runs across all rows of investment and updates totals
        """
        investments = session.query(Investment).all()
        for investment in investments:
            investment.update_totals()

    @staticmethod
    def get_by_customer(session, customer_id):
        """
        For a given customer (id), return investments
        """
        assert session
        assert customer_id
        # group investments by product, and aggregate amounts
        # using sum or min/max (where they are identical)
        # uses outer join to include products for which customer has no
        # investments yet.
        investments = session.query(
                Investment.group_id,
                Product.id.label('product_id'),
                Investment.description,
                Investment.total_target_amount,
                Investment.target_instalment_amount,
                Investment.instalment_period,
                Investment.target_instalment_count,
                Investment.is_completed,
                func.min(Investment.total_withdrawal_count).
                label('total_withdrawal_count'),
                func.min(Investment.completed_instalment_count)
                .label('completed_instalment_count'),
                cast(Investment.target_completion_on, Date).
                label('target_completion_on'),
                cast(Investment.estimated_completion_on, Date).
                label('estimated_completion_on'),
                Product.display_name,
                Product.internal_name,
                Product.instalment_strategy,
                func.sum(Investment.total_investment).
                label('p_total_investment'),
                func.sum(Investment.total_withdrawal).
                label('p_total_withdrawal'),
                func.sum(Investment.available_withdrawal_balance).
                label('p_available_withdrawal_balance'),
                func.sum(Investment.net_investment).
                label('p_net_investment'),
                func.sum(Investment.current_value).
                label('p_current_value'),
                func.sum(Investment.net_gain).
                label('p_net_gain'),
                (func.sum(Investment.net_gain_percentage * Investment.allocation_percent) / 100).  # NOQA
                label('p_net_gain_percentage'),
                cast(func.max(Investment.created_on), Date).label('created_on')
            ).select_from(Product).\
            outerjoin(Investment,
                      and_(Investment.product_id == Product.id,
                           Investment.customer_id == customer_id)).\
            group_by(
                Investment.group_id,
                Product.id,
                Product.display_name,
                Product.internal_name,
                Product.instalment_strategy,
                Investment.description,
                Investment.total_target_amount,
                Investment.target_instalment_amount,
                Investment.instalment_period,
                Investment.target_instalment_count,
                Investment.is_completed,
                cast(Investment.target_completion_on, Date),
                cast(Investment.estimated_completion_on, Date),
            ).order_by(Product.id).all()
        return investments

    @staticmethod
    def create_new(customer, product, doc, session):
        """
        Create a new investment group for a customer + product
        """
        if not product.allow_multiple:
            # make sure there's no existing investment in this product
            # for this customer.
            if session.query(Investment).\
                      filter(Investment.product_id == product.id,
                             Investment.customer_id == customer.id,
                             Investment.is_closed.isnot(True)).count():
                msg = 'Cannot create multiple {} investments'.\
                      format(product.display_name)
                raise Exception(msg)

        # for each fund in product, create a row per scheme.
        # later, fund selection can come into play here.
        investments = []
        group_id = generate(prefix='IG')
        doc['group_id'] = group_id

        LOG.debug('Investment group {} for product {}'.format(group_id,
                                                              product.id))

        # get recommended schemes.
        allocation = Product.get_recommended_schemes(product, doc, session)
        # LOG.debug('allocation: \n{}'.format(allocation))
        total_target_amount = doc.get('total_target_amount')

        for scheme in allocation:
            investment = Investment()
            # we need created_on before first commit to db,
            # to do some date computations.
            investment.created_on = datetime.utcnow()
            # technically the next investment is scheduled for NOW!
            investment.next_investment_on = investment.created_on
            investment.customer_id = customer.id
            investment.product_id = product.id
            investment.scheme_id = scheme['scheme_id']
            investment.allocation_percent = scheme['allocation_percent']
            investment.group_id = group_id

            # for BYOD
            investment.category = doc.get('category')
            investment.description = doc.get('description')
            investment.target_roi = scheme.get('target_roi')

            # Setting the default value to be False for BYOD investment rows
            if product.internal_name == GOAL:
                investment.is_completed = False

            # this is the sum of all investments, not per scheme.
            if total_target_amount:
                min_sip_amount = scheme.get('min_sip_amount', None)
                if min_sip_amount:
                    # check that the allocated amount for each scheme
                    # is above min sip amount.
                    if (min_sip_amount > (total_target_amount *
                                          investment.allocation_percent/100)):
                        raise ValueError('For scheme id {}, min. sip amount'
                                         ' {} is higher than allocated amount'
                                         .format(scheme['scheme_id'],
                                                 scheme['min_sip_amount']))
                investment.total_target_amount = total_target_amount

            investment.target_completion_on = doc.get('target_completion_on')
            investment.estimated_completion_on =\
                investment.target_completion_on

            # TODO: This should be computed.
            investment.target_instalment_amount = \
                doc.get('target_instalment_amount')

            investment.ip_address = doc.get('ip_address')

            # investment period - weekly or monthly or None
            investment.instalment_period = doc.get('instalment_period')

            # target_instalment_count
            investment.target_instalment_count = \
                doc.get('target_instalment_count')

            # compute some values
            investment.compute_initial_values()

            # add new row to list of inserts
            investments.append(investment)
        # insert all
        session.add_all(investments)
        # NOTE: do not commit, the caller should commit if everything is fine.
        # this is done to support rollback-to-savepoint if a subsequent error
        # occurs in transactions.
        doc['investments'] = []
        # delete ip address from response
        doc.pop('ip_address', None)

        for i in investments:
            # LOG.debug('inserted investment.id {}'.format(i.id))
            doc['investments'].append({
                'allocation_percent': i.allocation_percent,
                'scheme_id': i.scheme_id
            })
        return doc

    @staticmethod
    def is_valid_group_id(investment_group_id, customer_id, session):
        """
        Validates if this investment group id exists.
        """
        # ref: http://docs.sqlalchemy.org/en/latest/orm/query.html
        q = session.query(Investment).\
            filter(Investment.group_id == investment_group_id,
                   Investment.customer_id == customer_id)
        is_valid = session.query(q.exists()).scalar()
        # LOG.debug('is_valid: {} {}'.format(investment_group_id, is_valid))
        return is_valid

    @staticmethod
    def fetch(investment_group_id, customer_id, session):
        """
        Fetches investment rows for a group_id. While is_valid_group_id
        does an 'EXISTS' check, this will actually return the rows.
        """
        rows = session.query(Investment).filter(
            Investment.group_id == investment_group_id,
            Investment.customer_id == customer_id).all()
        return rows

    @staticmethod
    def delete_group(investment_group_id, customer_id, session):
        """
        Deletes an investment group
        """
        investment_rows = session.query(Investment).\
            filter(Investment.group_id == investment_group_id,
                   Investment.customer_id == customer_id).all()
        if investment_rows:
            # make sure no transactions exist, except INITIATED
            ids = (r.id for r in investment_rows)
            t_rows = session.query(m.Transaction).\
                filter(m.Transaction.investment_id.in_(ids),
                       m.Transaction.status.notin_((TR_STATUS_INITIATED,TR_STATUS_FAILURE))).all()
            if t_rows:
                raise ValueError('Cannot delete investment with existing'
                                 ' transactions.')
            else:
                for r in investment_rows:
                    # this just marks rows for deletion
                    # actual deletion happens on flush/commit
                    session.delete(r)
                session.commit()
                return len(investment_rows)
        else:
            return 0

    def compute_initial_values(self):
        """
        For a given row, computes initial values for some fields
        """
        # everything using Decimal with precision 4
        with localcontext() as ctx:
            ctx.prec = 12

            # This covers all except regular Sqrrl Away investments
            if self.total_target_amount:
                # 1. if end date is known, compute number of instalments
                if self.target_completion_on:
                    if self.instalment_period == MONTHLY:
                        self.target_instalment_count =\
                            monthdelta(self.target_completion_on,
                                       self.created_on)
                    elif self.instalment_period == WEEKLY:
                        self.target_instalment_count =\
                            weekdelta(self.target_completion_on,
                                      self.created_on)
                # 2. else if number of instalments is known, compute end date
                elif self.target_instalment_count:
                    # count includes the first instalment on day 0
                    count = self.target_instalment_count - 1
                    if self.instalment_period == MONTHLY:
                        self.target_completion_on =\
                            self.created_on + timedelta(days=count*30)
                    elif self.instalment_period == WEEKLY:
                        self.target_completion_on =\
                            self.created_on + timedelta(days=count*7)
                # 3. We know total investment and instalments,
                # so we can compute instalment amount.
                # TODO: Test product.instalment_strategy
                if not self.target_instalment_amount:
                    # NOTE: Converting decimal to float so that we can use
                    # numpy
                    # LOG.debug('\n\ntarget_roi: {}'.format(self.target_roi))
                    # final value is the amount to save
                    amount = float(self.total_target_amount)
                    # no. of instalments
                    nper = self.target_instalment_count
                    if self.target_roi:
                        # BYOD
                        if self.instalment_period == MONTHLY:
                            rate = float(self.target_roi / 12)
                        elif self.instalment_period == WEEKLY:
                            rate = float(self.target_roi / 52)
                        else:
                            rate = 0

                        # compute instalment
                        inst = abs(np.pmt(rate, nper, 0, fv=amount))

                        # round up to nearest 100 (ref: API-91)
                        self.target_instalment_amount =\
                            Decimal(round_up(inst, 100))
                        # LOG.debug('rate: {:.4f}, nper: {}, amount: {}'.
                        #           format(rate, nper, amount))
                        # LOG.debug('inst - {}, target_instalment_amount: {}'.
                        #           format(inst, self.target_instalment_amount))
                    elif amount and nper:
                        # AXE-TAX
                        # compute instalment
                        inst = abs(amount/nper)
                        # round up to nearest 100 (ref: API-91)
                        self.target_instalment_amount = \
                            Decimal(round_up(inst, 1000))
                    else:
                        # Sqrrl Away Lumpsum
                        # target_instalment_count is null in this case
                        # target_instalment_amount would remain null
                        pass

    @classmethod
    def populate_transaction_summary(cls, arr, session):
        """
        Populate recent 10 transactions for each investment group_id in arr
        """
        results = []
        for item in arr:
            if item.group_id:
                row = session.query(cls).\
                    filter(cls.group_id == item.group_id).first()
                # in order to insert the transactions here, we will need to
                # convert named tuple into dict
                item = item._asdict()
                item['recent_transactions'] =\
                    m.Transaction.summary_by_investment(session, row.group_id)
            results.append(item)
        return results

    @classmethod
    def retry_transaction(cls, session, investment_group_id):
        """
        For a given group_id, fetch all transactions that he needs to "retry"

        These are the latest transactions for each investment that are in
        FAILURE or INITIATED or PENDING state.
        """
        result = None
        try:
            latest_transaction_row = session.query(
                cls,
                m.Transaction.group_amount,
                m.Transaction.status,
                m.Transaction.created_on,
                m.Transaction.transaction_type,
                m.Transaction.mode
            ).filter(
                cls.group_id == investment_group_id,
                cls.id == m.Transaction.investment_id,
            ).order_by(m.Transaction.created_on.desc()).first()

            # LOG.debug('{} - {}'
            #          .format(investment_group_id, latest_transaction_row))
            if latest_transaction_row:
                # only last one day to be included in this.
                # yesterday = datetime.utcnow() - timedelta(days=1)
                # if (latest_transaction_row.status == TR_STATUS_INITIATED
                #         and latest_transaction_row.created_on < yesterday):
                #     return

                # If mode is NACH, then retry_transaction should be null
                if latest_transaction_row.mode == NACH and \
                            latest_transaction_row.transaction_type == INVEST:
                    result = None

                elif latest_transaction_row.status in (
                    TR_STATUS_INITIATED,
                    TR_STATUS_FAILURE
                ) and latest_transaction_row.transaction_type == INVEST:
                    result = {
                        'amount': latest_transaction_row.group_amount,
                        'status': latest_transaction_row.status,
                        'created_on': latest_transaction_row.created_on
                    }
            else:
                # there is no transaction yet. return a dummy object
                # see issue #33.
                investment = session.query(cls).\
                    filter(cls.group_id == investment_group_id).first()
                result = {
                    'amount': (investment.target_instalment_amount
                               or investment.total_target_amount),
                    'status': 'INITIATED',
                    'created_on': investment.created_on
                }
        except Exception as ex:
            LOG.error(ex)
        return result

    @classmethod
    def upcoming_transactions(cls, session, customer_id, num_days):
        """
        For a given customer, get any upcoming transactions in the next
        num_days

        This should return all information needed to POST the transaction.
        """
        # the cutoff for the upcoming days.
        result = []
        try:
            # we should return a hit only if at least one transaction is
            # processed for a given investment, hence the filter on
            # total_investment

            # TODO: Add a filter for target amount > invested so far.
            upcoming_date = datetime.utcnow() + timedelta(days=num_days)
            # go back at most one day, or this becomes a nag.
            past_since = datetime.utcnow() - timedelta(days=1)
            result = session.query(
                cls.group_id,
                cast(cls.next_investment_on, Date).label('next_investment_on'),
                cls.target_instalment_amount,
            ).filter(
                cls.customer_id == customer_id,
                cls.next_investment_on is not None,
                cls.next_investment_on < upcoming_date,
                cls.next_investment_on > past_since,
                cls.total_investment is not None,
                cls.total_investment > 0,
                cls.target_instalment_amount is not None,
                cls.target_instalment_amount > 0,
            ).group_by(
                cls.group_id,
                cast(cls.next_investment_on, Date),
                cls.target_instalment_amount
            ).order_by(cast(cls.next_investment_on, Date)).all()
        except Exception as ex:
            LOG.error(ex)
        return result

    @classmethod
    def update_group(cls, session, investment_group_id, **kwargs):
        """
        updates the investment group with provided folio number.
        """
        updates = {}
        # only allow certain fields to update.
        for k in ('has_processing_transactions'):
            if k in kwargs:
                updates[k] = kwargs[k]
        # update in db.
        session.query(cls).filter(cls.group_id == investment_group_id).\
            update(updates)
        session.commit()

    @classmethod
    def post_verification(cls, session, investment_group_id):
        """
        Once a transaction group is verified, update several investment
        fields related to the investment group
        """
        rows = session.query(cls).filter(cls.group_id == investment_group_id).\
            all()
        # assume all rows have the same value for has_processing_transactions
        if not rows:
            raise Exception('Investment group not found: {}'.
                            format(investment_group_id))
        for r in rows:
            r.update_totals()

    def update_next_investment_on(self):
        """
        For a given investment row, update last and next investment dates.

        last_investment_on: most recent PROCESSING / SUCCESS row of INVEST type
        session: sqlalchemy session
        """
        # get latest INVEST in these statuses
        session = object_session(self)
        statuses = (TR_STATUS_PROCESSING, TR_STATUS_SUCCESS)
        recent_trx = session.query(m.Transaction).\
            filter(m.Transaction.investment_id == self.id).\
            filter(m.Transaction.transaction_type == INVEST).\
            filter(m.Transaction.status.in_(statuses)).\
            order_by(m.Transaction.created_on.desc()).first()

        if not recent_trx:
            # LOG.warn('inv. {} has no transactions'.format(self.id))
            return

        self.last_investment_on = (recent_trx.bank_transaction_on or
                                   recent_trx.modified_on)

        # if investment has reached goal, next_investment_on is None
        if self.has_reached_target(session, self.group_id):
            self.next_investment_on = None
        elif self.target_instalment_amount:
            # update next_investment_on
            if self.instalment_period == WEEKLY:
                today = datetime.today().date()

                # update next_investment_on if it is currently set
                # to a date in the past, or if the last payment was made
                # in the same week (e.g., a payment due for Thursday was
                # made on a Tuesday or Wednesday)

                # checking if two dates are in the same week
                # pardon the variable naming. :-/
                cal1 = self.last_investment_on.isocalendar()
                cal2 = self.next_investment_on.isocalendar()

                # LOG.debug('{} - {}'.format(self.last_investment_on, cal1))
                # LOG.debug('{} - {}'.format(self.next_investment_on, cal2))

                # last trx and next date in same week?
                same_week = cal1[0] == cal2[0] and cal1[1] == cal2[1]

                if (self.next_investment_on.date() < today or same_week):
                    # LOG.debug('Need to update next_investment_on')

                    # setting it to "next Thursday", whenever that may be
                    # how many days to next Thursday (from today)
                    from_today = (today - self.last_investment_on.date()).days
                    # weekday - 0 is Monday, 3 is Thursday
                    delta_days = 7 - today.weekday() + 3 + from_today
                    next_thursday = (self.last_investment_on +
                                     timedelta(delta_days)).date()
                    # set it for 1am UTC/6:30am IST on the given date
                    self.next_investment_on = datetime.combine(
                        next_thursday,
                        time(hour=1)
                    )
            elif self.instalment_period == MONTHLY:
                # 7th of next month is the SIP date.
                month = self.last_investment_on.month + 1
                year = self.last_investment_on.year
                if month > 12:
                    month = 1
                    year = year + 1

                # NOTE: Can adjust the date if 7 is not a business day.
                self.next_investment_on = datetime(year, month, 7)
        session.add(self)
        try:
            session.commit()
        except Exception as ex:
            LOG.error(ex, exc_info=True)
            session.rollback()

    def compute_xirr(self, session):
        """
        Returns annualised or absolute return for a given investment id.

        For short-term debt / liquid funds we can annualise returns. For
        longer term, we can only annualise if investment is over 1 year old.

        Args:
            session (object): the sqlalchemy session
        """
        try:
            # fetch all transactions for this investment group
            all_transactions = session.query(m.Transaction).\
                filter(m.Transaction.investment_id == self.id).\
                filter(m.Transaction.status == TR_STATUS_SUCCESS).\
                filter(m.Transaction.transaction_type.in_((INVEST, REDEEM))).\
                order_by(m.Transaction.created_on).all()

            if not all_transactions:
                # no transactions, no gain percentage to return
                # LOG.debug('No transactions for {}'.format(self.id))
                return

            # absolute or annualised?
            is_absolute = True
            year = timedelta(days=365)
            now = datetime.utcnow()
            first_transaction = all_transactions[0]

            # ignore if first transaction is < 2 days old
            if (first_transaction.verified_on and
                first_transaction.verified_on >
                    datetime.utcnow() - timedelta(days=5)):
                LOG.debug('Too soon, no returns to show')
                return

            # short term or beyond a year for ELSS/BYOD -> use annualised
            if (self.product.internal_name == LIQUID or
                (now - first_transaction.created_on > year) or
                (self.product.internal_name == GOAL and
                 self.target_completion_on - self.created_on <= year)):
                # short term, show annualised.
                is_absolute = False
                # LOG.debug('is_absolute for {}: {}'.
                #           format(self.id, is_absolute))

            if is_absolute:
                # simple gain expressed as a percentage
                if self.net_investment:
                    net_gain_percentage =\
                        (self.net_gain * Decimal(100) / self.net_investment)
                    # LOG.debug('absolute for {}: {}'.
                    #           format(self.id, net_gain_percentage))
                    return net_gain_percentage
            else:
                # compute annualised_return using xirr function.
                # extract the successful transactions, and flip the sign
                transactions = []
                for t in all_transactions:
                    sign = -1 if t.transaction_type == INVEST else 1
                    value = (t.created_on.date(), float(t.amount) * sign)
                    # LOG.debug('{}\t{}'.format(value[0], value[1]))
                    transactions.append(value)

                # LOG.debug('transaction count for {}: {}'.
                #           format(self.id, len(transactions)))
                # the final value in the array is the current value as per units  # NOQA
                transactions.append(
                    (self.scheme_nav_date.date(),
                     float(self.scheme_units * self.scheme_nav))
                )
                # from pprint import pprint
                # pprint(transactions)
                annualised_return = xirr(transactions)
                # LOG.debug('xirr for {}: {}'.
                #           format(self.id, annualised_return * 100))
                return Decimal(annualised_return * 100)
        except Exception as ex:
            LOG.error(ex, exc_info=True)

    @classmethod
    def populate_upcoming_nach_transactions(cls, investments, session):
        """
        Updates investments with upcoming nach transactions, such
        transactions have status : PENDING
        """
        for investment in investments:
            if 'group_id' in investment:
                upcoming_transaction = None
                row = session.query(
                    cls.next_investment_on,
                    m.Transaction.group_amount,
                    m.Transaction.group_id
                    ).filter(
                    cls.group_id == investment['group_id'],
                    cls.id == m.Transaction.investment_id,
                    m.Transaction.mode == NACH,
                    m.Transaction.status == TR_STATUS_PENDING
                    ).group_by(
                    cls.next_investment_on,
                    m.Transaction.group_amount,
                    m.Transaction.group_id
                    ).first()
                if row:
                    upcoming_transaction = {
                        'target_instalment_amount': row.group_amount,
                        'next_investment_on': row.next_investment_on,
                        'group_id': row.group_id
                    }
                investment['upcoming_nach_transaction'] =\
                    upcoming_transaction
        return investments

    @classmethod
    def populate_schemes(cls, investments, session):
        """
        Updates investments with scheme_info array
        The array contains scheme_id, scheme_name and allocation percentage
        in each scheme in an investment.
        """
        for row in investments:
            scheme_info = []
            if 'group_id' in row:
                scheme_rows = session.query(cls, Scheme).\
                    filter(cls.group_id == row['group_id'],
                           cls.scheme_id == Scheme.id).all()
                for r in scheme_rows:
                    record = {
                        'id': r.Scheme.id,
                        'name': r.Scheme.name,
                        'allocation_percent': r.Investment.allocation_percent
                    }
                    scheme_info.append(record)
                # LOG.info(row)
                row['schemes'] = scheme_info
        return investments

    @classmethod
    def populate_min_purchase_amount(cls, investments, session):
        """
        Updates investments with `p_min_purchase_amount`
        and `p_min_withdrawal_amount` using the min. SIP amounts in scheme
        table.

        Can be done in `get_by_customer` as well but we're outer-joining with
        product table so we'll need to do some if then case / subquery stuff.

        Makes the changes in place.

        args:
            investments - array returned by get_by_customer and converted into
            an editable list of objects.
            session - the sqlalchemy session.
        """

        # each row is actually investment group
        for row in investments:
            if 'group_id' in row:
                # LOG.debug('In group_id: {}'.format(row['group_id']))
                # to compute min_purchase_amount (for any transaction)
                # for each investment group - we need to get the max of
                # (min_sip_amount / allocation) across all schemes.
                scheme_rows = session.query(cls, Scheme).\
                    filter(cls.group_id == row['group_id']).\
                    filter(cls.scheme_id == Scheme.id).all()
                min_purchase_amount = max(
                    (sr.Scheme.minimum_sip_amount * 100
                     / sr.Investment.allocation_percent for sr in scheme_rows)
                )

                # round to next 100 for hygiene
                result =\
                    (min_purchase_amount / 100).to_integral_value(
                        rounding=ROUND_CEILING
                    ) * 100
                # LOG.debug('result: {}, rounded: {}'.
                #           format(min_purchase_amount, result))
                row['p_min_purchase_amount'] = int(result)

                # likewise - compute min. redemption amount.
                min_redemption_amount = max(
                    (sr.Scheme.minimum_redemption_amount * 100
                     / sr.Investment.allocation_percent for sr in scheme_rows)
                )
                # round to next 100
                result =\
                    (min_redemption_amount / 100).to_integral_value(
                        rounding=ROUND_CEILING
                    ) * 100
                row['p_min_withdrawal_amount'] = int(result)
        return investments

    @classmethod
    def due_today(cls, session, product_id=None):
        """
        Returns customer + investment info for those investments
        which are due today.
        """
        results = []
        today = datetime.utcnow().date()
        query = session.query(cls).\
            filter(cls.total_investment is not None).\
            filter(cls.total_investment > 0).\
            filter(cls.target_instalment_amount is not None).\
            filter(func.date_trunc('day', cls.next_investment_on) == today)

        if product_id:
            # if product id is not provided, customer may have more than
            # one result for an upcoming transaction
            query = query.filter(cls.product_id == product_id)

        rows = query.order_by(cls.group_id).all()

        # iterate through each investment row, and exclude those for which
        # a PURCHASE transaction is already PROCESSING
        for r in rows:
            # the corresponding customer
            if results and results[-1]['group_id'] == r.group_id:
                # ignore if this group has been considered.
                # LOG.debug('skipping as duplicate {}'.format(r.id))
                continue

            t_count = session.query(m.Transaction).\
                filter(m.Transaction.investment_id == r.id).\
                filter(m.Transaction.transaction_type == INVEST).\
                filter(m.Transaction.status == TR_STATUS_PROCESSING).count()
            if t_count:
                # LOG.debug('t_count: {}, inv: {}'.format(t_count, r.id))
                # ignore rows with non-zero processing purchases.
                LOG.debug('skipping due to processing trx {}'.format(r.id))
                continue

            # this record is of interest.
            c = session.query(Customer).get(r.customer_id)
            record = {
                'group_id': r.group_id,
                'customer_id': r.customer_id,
                'customer_email': c.email,
                'customer_mobile': c.mobile,
                'amount': r.target_instalment_amount,
                'next_investment_on': r.next_investment_on
            }
            # LOG.debug(record)
            results.append(record)
        return results

    def has_reached_target(self, session, investment_group_id):
        """
        Check whether a given investment has reached its target

        TODO: cache results?
        """
        if self.product.internal_name == GOAL:
            if self.is_completed:
                return True
            else:
                current_date = datetime.utcnow()
                target_amount_per_scheme = \
                    self.total_target_amount * self.allocation_percent / Decimal(100.0)
                if current_date > self.target_completion_on \
                        or (self.total_investment and
                            self.total_investment >= target_amount_per_scheme):
                    return True

        if self.total_investment and self.net_investment is not None:
            group_row = session.query(
                Investment.group_id,
                func.sum(Investment.net_investment).label('net_investment'),
                func.max(Investment.total_target_amount).label('total_target_amount')
            ).filter(Investment.group_id == investment_group_id).group_by(
                Investment.group_id).first()

            if (group_row and group_row.total_target_amount
                    and group_row.net_investment):
                # assume customer has reached target if within 500 Rs.
                return group_row.net_investment >= (
                    group_row.total_target_amount - 499
                )

        return False

    @classmethod
    def due_by_n_days_from_now(cls, session, product_id=None, days=0):
        """
        Returns customer + investment info for those investments
        which are due by n days from now.
        """
        results = []

        # datetime.datetime.now().date()
        # datetime.datetime.utcnow().date()
        date_n_days_from_now = datetime.utcnow().date()
        if days and isinstance(days, int):
            date_n_days_from_now += timedelta(days=days)

        LOG.debug("Date n = {} days from now: {}".format(days,date_n_days_from_now))

        query = session.query(cls).\
                        filter(cls.total_investment > 0).\
                        filter(cls.target_instalment_amount is not None).\
                        filter(func.date_trunc('day', cls.next_investment_on) == date_n_days_from_now)

        if product_id:
            # if product id is not provided, customer may have more than one result for an upcoming transaction
            query = query.filter(cls.product_id == product_id)

        # query = query.group_by(cls.group_id, cls.customer_id, cls.product_id)
        rows = query.order_by(cls.id).all()

        # iterate through each investment row, and exclude those for which
        # a PURCHASE transaction is already PROCESSING
        for r in rows:
            # the corresponding customer
            if results and results[-1]['group_id'] == r.group_id:
                # ignore if this group has been considered.
                # LOG.debug('skipping as duplicate {}'.format(r.id))
                continue

            t_count = session.query(m.Transaction).\
                        filter(m.Transaction.investment_id == r.id).\
                        filter(m.Transaction.transaction_type == INVEST).\
                        filter(m.Transaction.status == TR_STATUS_PROCESSING).count()

            if t_count:
                # LOG.debug('t_count: {}, inv: {}'.format(t_count, r.id))
                # ignore rows with non-zero processing purchases.
                LOG.debug('skipping due to processing trx {}'.format(r.id))
                continue

            c = session.query(Customer).get(r.customer_id)
            record = {
                'name' : c.pan_name,
                'customer_id': r.customer_id,
                'customer_email': c.email,
                'customer_mobile': c.mobile,
                'group_id': r.group_id,
                'amount': r.target_instalment_amount,
                'next_investment_on': r.next_investment_on
            }

            results.append(record)
        return results

    @staticmethod
    def switch_fund(session, customer, from_scheme_id, to_scheme_id, ip):
        """
        Switch funds - for a given customer find the investment row
        for a given scheme_id, then initiate switch transaction to
        a new scheme.

        :params
        session - the database session
        customer - Customer instance
        from_scheme_id - the scheme which needs to "switch out"
        to_scheme_id - the scheme id to which it should be switched to
        ip - the ip address that initiated this change (for audit)
        """
        if not customer:
            raise ValueError('No customer provided')

        # iterate through customer's investments,
        # find the one with from_scheme_id
        investment = None
        try:
            investment = next(i for i in customer.investments
                              if i.scheme_id == from_scheme_id)
        except StopIteration as s_err:
            LOG.error('No matching investment found', extra={
                'from_scheme_id': from_scheme_id
            })
            raise s_err

        # validate to_scheme_id
        target_scheme = session.query(Scheme).get(to_scheme_id)
        if not target_scheme:
            raise ValueError('No such scheme: {}'.format(to_scheme_id))

        # TODO: Check if there are existing processing transactions
        has_processing = next((t for t in investment.transactions
                               if t.status == TR_STATUS_PROCESSING), None)
        if has_processing:
            LOG.warn('Investment has processing transactions, skipping',
                     extra={'investment_id': investment.id})
            raise ValueError('Investment has processing transactions')

        tr_out = None
        tr_in = None
        try:
            # here is how we do it...
            # store old folio/scheme information in extras
            # and start storing new scheme_id, scheme_folio
            now_str = datetime.utcnow().isoformat()
            if not investment.extras:
                investment.extras = {}
            investment.extras.update({
                'switch_initiated_on': now_str,
                'switch_from_scheme_id': str(investment.scheme_id),
                'switch_from_scheme_folio': investment.scheme_folio,
                'switch_to_scheme_id': str(target_scheme.id),
                'switch_units': str(investment.scheme_units),
                'switch_from_nav': str(investment.scheme_nav),
                'switch_from_nav_date': investment.scheme_nav_date.isoformat()
            })

            investment.scheme_id = target_scheme.id

            # create the switch transactions
            # ip is to track the ip address which initiated this switch.
            tr_out, tr_in = m.Transaction.switch_fund(
                session=session,
                inv_row=investment,
                ip=ip
            )

            # for the investment row post-switch, update nav, units
            old_nav, investment.scheme_nav =\
                investment.scheme_nav, target_scheme.nav
            old_units = investment.scheme_units
            investment.scheme_nav_date =\
                target_scheme.nav_last_updated_on
            investment.scheme_units =\
                round(old_units * old_nav / investment.scheme_nav, 4)
            # also update the units in switch-in row
            updated_trx = session.query(m.Transaction).\
                filter(m.Transaction.group_id == tr_in).\
                filter(m.Transaction.transaction_type == SWITCH_IN).first()
            updated_trx.units = investment.scheme_units

            # commit the changes - all or nothing
            session.add(investment)
            session.add(updated_trx)
            session.commit()
        except Exception as ex:
            LOG.error('ERROR: Rolling back nested transaction', exc_info=True)
            session.rollback()
            raise ex
        return investment, tr_out, tr_in

    def undo_switch(self):
        """
        In case we need to revert the scheme and units etc. back to pre-switch
        """
        self.scheme_folio = self.extras['switch_from_scheme_folio']
        self.scheme_units = Decimal(self.extras['switch_units'])
        self.scheme_id = int(self.extras['switch_from_scheme_id'])
        self.scheme_nav = Decimal(self.extras['switch_from_nav'])
        dstr = self.extras['switch_from_nav_date'].split('.')[0]
        self.scheme_nav_date = datetime.strptime(dstr, r'%Y-%m-%dT%H:%M:%S')
        self.current_value = self.scheme_units * self.scheme_nav
        self.extras = {}

    @staticmethod
    def get_investment_rows_by_ids(session, *investment_ids):
        return session.query(Investment).\
                        filter(Investment.id.in_(investment_ids)).all()

    def update_totals(self):
        """
        For a given row, updates the totals.
        """
        LOG.debug('updating totals for {}'.format(self.id))
        session = object_session(self)
        try:
            # filter by these
            statuses = (TR_STATUS_PROCESSING, TR_STATUS_SUCCESS)
            # fetch transaction rows, reverse date ordering is important!
            t_rows = session.query(m.Transaction).\
                filter(m.Transaction.investment_id == self.id,
                       m.Transaction.status.in_(statuses)).\
                order_by(m.Transaction.created_on.desc()).all()

            if t_rows:
                sum_investments = sum_withdrawals = 0
                total_investment_count = total_withdrawal_count = 0
                total_purchase_processing = total_withdrawal_processing = 0
                units = 0
                ignore_units = False
                for t in t_rows:
                    if t.status == TR_STATUS_SUCCESS:
                        if t.transaction_type == INVEST:
                            total_investment_count = total_investment_count + 1
                            sum_investments = sum_investments + t.amount
                            # using a flag to stop updating units,
                            # to be used in case of switch
                            if not ignore_units:
                            # if transaction is successful, units must not be null  # NOQA
                                units = units + t.units
                        elif t.transaction_type == REDEEM:
                            total_withdrawal_count = total_withdrawal_count + 1
                            sum_withdrawals = sum_withdrawals + t.amount
                            if not ignore_units:
                                units = units - t.units
                        elif t.transaction_type == SWITCH_IN:
                            units = units + t.units
                            ignore_units = True
                    elif t.status == TR_STATUS_PROCESSING:
                        if t.transaction_type == INVEST:
                            total_purchase_processing = (
                                total_purchase_processing + t.amount
                            )
                            # count purchases towards completed instalments...
                            total_investment_count = total_investment_count + 1
                        elif t.transaction_type == REDEEM:
                            total_withdrawal_processing = (
                                total_withdrawal_processing + t.amount
                            )
                        elif t.transaction_type == SWITCH_IN:
                            if t.units:
                                units = units + t.units
                            else:
                                # we have a processing SWITCH OUT.
                                # need to translate units from old scheme
                                # to new scheme
                                old_nav = Decimal(self.extras['switch_from_nav'])  # NOQA
                                old_units = Decimal(self.extras['switch_units'])  # NOQA
                                new_units = round(
                                    old_nav * old_units / self.scheme_nav, 4
                                )
                                units = units + new_units
                            ignore_units = True

                # now we have all totals for processed and processing trx
                self.completed_instalment_count = total_investment_count
                self.total_withdrawal_count = total_withdrawal_count
                # we want to count both successful and processing
                self.total_investment =\
                    sum_investments + total_purchase_processing
                self.total_withdrawal =\
                    sum_withdrawals + total_withdrawal_processing
                self.net_investment =\
                    self.total_investment - self.total_withdrawal
                self.scheme_units = units
                if self.scheme_nav:
                    # only the processed units are available to withdraw, minus
                    # any withdrawals already processing.
                    if not self.scheme_nav:
                        self.scheme_nav = self.scheme.nav
                        self.scheme_nav_date = self.scheme.nav_last_updated_on
                    if self.scheme_units:
                        processed_amt = round(self.scheme_units * self.scheme_nav, 4)  # NOQA
                    else:
                        processed_amt = 0
                    self.available_withdrawal_balance =\
                        processed_amt - total_withdrawal_processing
                    # compute current value from both processed and processing
                    # assume processing trx to be at the purchase value for
                    # this computation
                    self.current_value = processed_amt + (
                        total_purchase_processing - total_withdrawal_processing
                    )
                    self.net_gain = self.current_value - self.net_investment
                    self.net_gain_percentage = self.compute_xirr(session)

                # legacy
                self.has_processing_transactions = False

                # Update is_completed
                # if self.product.internal_name == GOAL and not self.is_completed:
                #     current_date = datetime.utcnow()
                #     target_amount_per_scheme = \
                #         self.total_target_amount*self.allocation_percent/100.0
                #     if current_date > self.target_completion_on or \
                #             self.total_investment >= target_amount_per_scheme:
                #         self.is_completed = True
                self.is_completed = \
                    self.has_reached_target(session, self.group_id)

                # commit the changes
                session.commit()
        except Exception as ex:
            LOG.error('update_totals failed', exc_info=True, extra={
                'self_investment_id': self.id
            })
            session.rollback()

    @staticmethod
    def get_investment_rows_by_customer_ids(session, *customer_ids):
        return session.query(Investment).\
                        filter(Investment.customer_id.in_(customer_ids)).all()
