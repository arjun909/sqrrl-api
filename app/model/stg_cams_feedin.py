###############################################################
#
# stg_cams_feedin.py
#
# stores raw dump from Karvy dbf
###############################################################


from sqlalchemy import (
    Column, String, Numeric, Date, Boolean
)
from sqlalchemy.exc import IntegrityError
import dbf

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class CamsFeedInMixin(object):
    """
    Common columns for StgCamsFeedIn and IntCamsFeedIn are defined in
    this mixin.
    """
    amc_code = Column(String(3))
    folio_no = Column(String(20))
    prodcode = Column(String(7))
    scheme = Column(String(100))
    inv_name = Column(String(70))
    trxntype = Column(String(7))
    trxnno = Column(Numeric(16, 0), unique=True)
    trxnmode = Column(String(1))
    trxnstat = Column(String(1))
    usercode = Column(String(10))
    usrtrxno = Column(Numeric(16, 0))
    traddate = Column(Date)
    postdate = Column(Date)
    purprice = Column(Numeric(19, 6))
    units = Column(Numeric(19, 6))
    amount = Column(Numeric(19, 6))
    brokcode = Column(String(20))
    subbrok = Column(String(30))
    brokperc = Column(Numeric(19, 6))
    brokcomm = Column(Numeric(19, 6))
    altfolio = Column(Numeric(16, 0))
    rep_date = Column(Date)
    time1 = Column(String(5))
    trxnsubtyp = Column(String(7))
    applicatio = Column(String(20))
    trxn_natur = Column(String(80))
    tax = Column(Numeric(19, 6))
    total_tax = Column(Numeric(19, 6))
    te_15h = Column(String(2))
    micr_no = Column(String(40))
    remarks = Column(String(100))
    swflag = Column(String(12))
    old_folio = Column(String(30))
    seq_no = Column(Numeric(16, 0))
    reinvest_f = Column(String(1))
    mult_brok = Column(String(32))
    stt = Column(Numeric(19, 6))
    location = Column(String(30))
    scheme_typ = Column(String(15))
    tax_status = Column(String(35))
    load = Column(Numeric(19, 6))
    scanrefno = Column(String(50))
    pan = Column(String(20))
    inv_iin = Column(Numeric(16, 0))
    targ_src_s = Column(String(100))
    trxn_type_ = Column(String(30))
    ticob_trty = Column(String(2))
    ticob_trno = Column(String(2))
    ticob_post = Column(String(2))
    dp_id = Column(String(20))
    trxn_charg = Column(Numeric(19, 6))
    eligib_amt = Column(Numeric(19, 6))
    src_of_txn = Column(String(14))
    trxn_suffi = Column(String(150))
    siptrxnno = Column(Numeric(19, 6))
    ter_locati = Column(String(1))
    euin = Column(String(20))
    euin_valid = Column(String(1))
    euin_opted = Column(String(1))
    sub_brk_ar = Column(String(20))
    exch_dc_fl = Column(String(1))
    src_brk_co = Column(String(20))
    sys_regn_d = Column(Date)
    ac_no = Column(String(40))
    bank_name = Column(String(40))
    reversal_c = Column(Numeric(19, 6))
    exchange_f = Column(String(3))
    ca_initiat = Column(Date)

    # how we track whether this row has been 'processed'
    is_processed = Column(Boolean, default=False)


class StgCamsFeedIn(Base, CamsFeedInMixin):
    """
    The structure of this table is translated from CAMS's reverse feed
    dbf file. The column names are same as far as possible.
    """
    __tablename__ = 'stg_cams_feedin'

    @staticmethod
    def import_dbf(session, file_name):
        """
        Imports a dbf into the table
        """
        LOG.info('importing {}'.format(file_name))
        table = dbf.Table(file_name.split('.dbf')[0])
        table.open()
        for record in table:
            r = StgCamsFeedIn()
            # iterate through all columns, pick the values from record
            # and set attributes.
            for key in table.field_names:
                try:
                    value = record[key]
                    if isinstance(value, str):
                        # remove all spaces around value if str
                        value = value.strip()
                    # LOG.debug('{}: {}'.format(key, value))
                    setattr(r, key, value)
                except KeyError:
                    pass
            # LOG.debug('\n ------ \n')
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                # ignore rows that are duplicates.
                # TODO: Use DO NOTHING constructs later.
                # LOG.warn('Unique error: Ignoring {}'.format(record.unqno))
                session.rollback()
            except Exception as ex:
                # other exceptions.
                LOG.error(ex)
                session.rollback()
        LOG.info('importing done!')
