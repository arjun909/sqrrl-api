from datetime import datetime
from functools import partial
from distutils.util import strtobool
from sqlalchemy import (Column, Integer, String, DateTime, Date, Boolean, func,
                        ForeignKey, event, BigInteger)
from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.orm import relationship, object_session
from sqlalchemy.ext.hybrid import hybrid_property

from app.config import NACH_UPPER_LIMIT_AMOUNT
from app.config import BRANCH_API_BASE_URL, BRANCH_API_CREATE_LINK, BRANCH_API_KEY
import requests
from app.model import Base, CustomerLevelLookup
from app import model as m
from app.utils import shortuuid
from app.utils.helpers import mask_string
from app.constants import (SETTINGS_K_REMIND_DAYS,
                           SETTINGS_K_SUMMARY_DAY, SETTINGS_K_TIPS_ENABLED,
                           SETTINGS_K_PUSH_ENABLED, SETTINGS_K_PAYMENT_PREF,
                           PAYMENT_MODE_NB)
from app import log
from sparkpost import SparkPost

sp = SparkPost()
LOG = log.get_logger()


class Customer(Base):
    """
    The customer (or investor) table

    Note: After some research and deliberation, keeping a single customer table
     as premature optimization is not needed. In future, for performance
     optimization, vertical partitioning may be explored.
    """
    __tablename__ = 'customer'

    # Use short uuid - *never* expose Integer id to API
    # Uses partial to add prefix='C'
    uid = Column(String(16),
                 default=partial(shortuuid.generate, prefix='C'),
                 unique=True, index=True, nullable=False)

    # personal info
    name = Column(String)
    mobile = Column(String)
    mobile_verified = Column(Boolean, default=False)
    email = Column(String)
    is_active = Column(Boolean, default=True)  # is this account active?
    tax_status = Column(String)

    # PAN, KRA data
    pan_name = Column(String)   # this may be slightly diff than name.
    pan_number = Column(String, unique=True)
    kyc_status = Column(String)
    is_kyc_verified = Column(Boolean, default=False)
    kra_name = Column(String)
    kyc_created_on = Column(DateTime)
    kyc_updated_on = Column(DateTime)
    date_of_birth = Column(Date)
    gender = Column(String)
    # TODO: Use CountryType from utils?
    country_of_birth = Column(String)
    # last logged in at?
    last_login_on = Column(DateTime, default=func.now())

    # tracks user's state in onboarding journey
    # DEPRECATED: use customer_level instead
    onboarding_status = Column(String, default=' ')

    customer_level_lookup_id =\
        Column(Integer, ForeignKey('customer_level_lookup.id'), default=1)
    customer_level_lookup = relationship(CustomerLevelLookup)

    # tracks the IP address of new users.
    ip_address = Column(String)

    # address info
    type_of_address = Column(String)
    permanent_address_1 = Column(String)
    permanent_address_2 = Column(String)
    permanent_city = Column(String)
    permanent_state = Column(String)
    permanent_pin = Column(String)
    permanent_country = Column(String)
    correspondence_address_1 = Column(String)
    correspondence_address_2 = Column(String)
    correspondence_city = Column(String)
    correspondence_state = Column(String)
    correspondence_pin = Column(String)
    correspondence_country = Column(String)

    # bank info
    bank_account_name = Column(String)
    bank_account_no = Column(String)
    bank_ifsc = Column(String)
    bank_name = Column(String)
    bank_branch_name = Column(String)
    bank_branch_address = Column(String)
    bank_branch_city = Column(String)

    # other investor information
    mother_name = Column(String)
    marital_status = Column(String)

    occupation_code = Column(String)
    occupation = Column(String)

    annual_income_code = Column(Integer)
    annual_income = Column(String)

    source_of_wealth_code = Column(Integer)
    source_of_wealth = Column(String)

    tax_residency_status = Column(String)
    politically_exposed = Column(Boolean, default=False)

    # Nominee related informations
    nomination_status = Column(Boolean, default=False)
    nominee_name = Column(String)
    nominee_date_of_birth = Column(Date)
    nominee_guardian = Column(String)
    nominee_relationship = Column(String)

    # for new KYC applications
    kyc_transaction_id = Column(String)
    ekyc_submitted_on = Column(DateTime)

    # flag for s3 customer docs mailed to operations@sqrrl.in
    pan_kyc_docs_mailed = Column(Boolean, default=False)

    # flag for e-logs generated
    setup_feed_generated = Column(Boolean, default=False)

    # ckyc id
    ckyc_id = Column(BigInteger)

    # attachments - key-value pairs with attachment names and s3 locations
    # to be used to PAN based KYC or any document upload
    attachments = Column(HSTORE)

    # columns related to email verification
    email_verification_token = Column(String, unique=True)
    is_email_verified = Column(Boolean, default=False)

    pan_kyc_docs_uploaded = Column(Boolean, default=False)
    # user settings, with defaults
    settings = Column(HSTORE, default={
        SETTINGS_K_PUSH_ENABLED: str(True),
        SETTINGS_K_TIPS_ENABLED: str(True),
        SETTINGS_K_REMIND_DAYS: str(2),
        SETTINGS_K_SUMMARY_DAY: 'saturday',
        SETTINGS_K_PAYMENT_PREF: PAYMENT_MODE_NB,
    })

    # store aadhaar number - needed for ckyc customers
    aadhaar = Column(String)

    # columns related to NACH mandate
    nach_mandate_id = Column(String)
    nach_mandate_status = Column(String)
    nach_mandate_note = Column(String)
    nach_created_on = Column(DateTime)
    nach_umrn = Column(String)
    nach_upper_limit_amount = Column(Integer, default=NACH_UPPER_LIMIT_AMOUNT)

    # API-159
    # This column is populated via daily Clevertap webhook.
    # It should be noted that this is not foolproof - if user does not share
    # device tokens with Clevertap, they're unable to detect uninstalls.
    last_uninstalled_on = Column(DateTime)

    def __repr__(self):
        return 'Customer id: %s, uid: %s, name: %s' % \
                (self.id, self.uid, self.name)

    def get_profile(self, extended=False):
        """
        Returns a dictionary with profile info that is returned by API
        """
        # transform settings to native data types
        if self.settings:
            for k, v in self.settings.items():
                if '_enabled' in k and isinstance(v, str):
                    self.settings[k] = strtobool(v) == 1
                elif '_days' in k:
                    self.settings[k] = int(v)

        # includes certain boolean flags to check where the customer is
        # in the setup process.
        profile = {
            'id': self.uid,
            'mobile': self.mobile,
            'name': self.name,
            'email': self.email,
            'mobile_verified': self.mobile_verified,
            'level': self.customer_level_lookup.level,
            'level_display': self.customer_level_lookup.level_display,
            'is_kyc_verified': self.is_kyc_verified,
            'pan_kyc_docs_uploaded': self.pan_kyc_docs_uploaded,
            'is_email_verified': self.is_email_verified,
            'date_of_birth': self.date_of_birth or '',
            'settings': self.settings,
            'pan_updated': self.pan_number is not None,
            'nach_mandate_status': self.nach_mandate_status,
            'bank_ifsc': self.bank_ifsc,
            'bank_updated': all((self.bank_ifsc,
                                 self.bank_account_no,
                                 self.bank_account_name)),
            'fatca_updated': all((self.politically_exposed is not None,
                                  self.occupation_code is not None,
                                  self.annual_income_code is not None,
                                  self.source_of_wealth_code is not None))
        }
        # extended - provide bank, pan info
        if extended:
            # mask bank account number for security
            # unhide last four numbers for bank account
            unmasked_pos = [-1, -2, -3, -4]
            masked_bank_acno = mask_string(self.bank_account_no,
                                           unmasked_pos=unmasked_pos)
            profile.update({
                'pan_number': self.pan_number or '',
                'bank_name': self.bank_name,
                'bank_account_no': masked_bank_acno
            })

        profile['nominee_details'] = {
            'nominee_name': self.nominee_name,
            'nominee_date_of_birth': self.nominee_date_of_birth,
            'nominee_relationship': self.nominee_relationship,
            'nominee_guardian': self.nominee_guardian
        }

        referable_code, referable_link = self.get_customer_referable_items()
        profile['referable_code'] = referable_code
        profile['referable_link'] = referable_link

        return profile

    @staticmethod
    def get_profile_for_uid(uid, session):
        """
        Wraps over get_profile to be called directly with uid
        """
        c = session.query(Customer).filter(Customer.uid == uid).first()
        return c.get_profile()

    def ok_for_investment(self):
        """
        Checks if current customer is ok for investment.

        1. Should be KYC verified
        2. Should have all bank fields populated.
        3. Should have FATCA info populated.
        """
        # all following conditions must be truth-y
        # NOTE: must check for None where a boolean false value is allowed.
        return all((
            self.is_kyc_verified,
            self.bank_ifsc,
            self.bank_account_no,
            self.bank_name,
            self.occupation_code is not None,
            self.annual_income_code is not None,
            self.source_of_wealth_code is not None,
            self.politically_exposed is not None,
        ))

    @staticmethod
    def update_attachments(customer_uid, attachments, session):
        """
        Write attachments to the db.
        """
        c = session.query(Customer).filter(Customer.uid == customer_uid).\
            first()
        if not c.attachments:
            c.attachments = attachments
        else:
            c.attachments.update(attachments)
        session.commit()

    @staticmethod
    def post_ekyc_callback(kyc_transaction_id, is_success, session):
        """
        Post an Aadhar based ekyc, update customer row.
        """
        # since kyc_transaction_id is actually customer.id, we can use
        # this handy shortcut.
        if kyc_transaction_id:
            c = session.query(Customer).get(kyc_transaction_id)
        else:
            raise ValueError('No customer with kyc_transaction_id: {}'.
                             format(kyc_transaction_id))
        # simply update the flag, the KRA data will be fetched later.
        c.is_kyc_verified = is_success
        if not is_success:
            # Aadhar based ekyc is complete (though not successful)
            cll = CustomerLevelLookup.get_by_level_display(session,
                                                           'EKYC_COMPLETED')
            c.customer_level_lookup_id = cll.id

        session.commit()
        if is_success:
            sp.transmissions.send(
                    recipients=['operations@sqrrl.in'],
                    template='ops-e-kyc-completed',
                    substitution_data={
                        'pan_name': c.pan_name,
                        'mobile': c.mobile,
                        'pan_number': c.pan_number,
                        'date_of_birth': c.date_of_birth.strftime(r'%d-%m-%Y')
                    }
            )
        return c

    @hybrid_property
    def is_installed(self):
        """
        This is a computed property. last_uninstalled_on has the most recent
        recorded uninstall datetime. If there's not been a login since then,
        we can assume customer has uninstalled the app.
        """
        return (self.last_uninstalled_on is None or
                self.last_login_on > self.last_uninstalled_on)

    @staticmethod
    def update_uninstalls(clevertap_payload, db):
        """
        Uses Clevertap webhook payload to update customer.last_uninstalled_on

        :param clevertap_payload - parsed json payload from clevertap webhook
        :param db - sqlalchemy session
        """
        now = datetime.utcnow()
        profiles = clevertap_payload.get('profiles', [])
        if profiles:
            identities = (r['identity'] for r in profiles)
            LOG.debug('updating uninstall dates')
            db.query(Customer).\
                filter(Customer.mobile.in_(identities)).\
                update({'last_uninstalled_on': now},
                       synchronize_session='fetch')

    def get_customer_referable_items(self, session=None):

        session = session if session else object_session(self)
        referrer = session.query(m.Referrer). \
            filter(m.Referrer.is_customer,
                   m.Referrer.type == 'UID_{}'.format(self.uid),
                   m.Referrer.link.isnot(None)).first()

        if referrer:
            ref_code = session.query(m.ReferralCode). \
                filter(m.ReferralCode.referrer_id == referrer.id, m.ReferralCode.is_valid).first()
            return ref_code.referral_code, referrer.link

        else:
            return self.construct_customer_attribution_code(session)

    def construct_customer_attribution_code(self, session):
        # # Check if the customer already exists in the referrer table
        # referrer = session.query(Referrer).\
        #     filter(Referrer.is_customer,
        #            Referrer.type == 'UID_{}'.format(self.uid),
        #            Referrer.link.isnot(None)).first()
        #
        # # LOG.debug("{}".format(referrer))
        #
        # if referrer:
        #     ref_code = session.query(ReferralCode.referral_code).\
        #         filter(ReferralCode.referrer_id == referrer.id,
        #                ReferralCode.is_valid).first()
        #
        #     return ref_code, referrer.link

        # If referrer not found, then create a new one.
        referral_code = m.ReferralCode.random_referral_code_generator()
        referral_code = referral_code.upper()

        # Check if the referral code generated already exists or not
        # If duplicate referral code generated, then retry for a new code
        retry = 5
        while retry and m.ReferralCode. \
                get_referral_code_row(session, referral_code, False):
            referral_code = m.ReferralCode.random_referral_code_generator()
            referral_code = referral_code.upper()
            retry -= 1

        # If retry has reduced to 0, that means, the generator kept
        # producing duplicate codes, then use customer's UID as ref code
        if not retry:
            referral_code = str(self.uid).upper()[1:]

        # LOG.debug("Referral Code: {}".format(referral_code))

        branch_url = "{}{}".format(BRANCH_API_BASE_URL, BRANCH_API_CREATE_LINK)
        branch_link_data = {
            "branch_key": BRANCH_API_KEY,
            "campaign": "Referral",
            "channel": "App",
            "tags": ["customer", "referral code"],
            "alias": referral_code,

            "data": {
                # "name": self.name,
                # "email": self.email,
                "referral_code": referral_code,
                "referrer_mobile": self.mobile,
                "uid": self.uid
            }
        }

        # Using the referral code generated, create a Branch.io link
        branch_api_response = requests.post(url=branch_url,
                                            json=branch_link_data,
                                            timeout=10)

        if branch_api_response.status_code == 200:
            referral_link = branch_api_response.json()['url']
            # LOG.debug("Referral Link: {}".format(referral_link))

            r = m.Referrer(name=self.name, description='P2P referrer',
                           link=referral_link, is_customer=True,
                           type='UID_{}'.format(self.uid))
            try:
                # LOG.debug('Inserting referrer row')
                session.add(r)
                session.flush()
            except BaseException as ex:
                LOG.error('Could not create referrer row', exc_info=True,
                          extra={'data': {
                            'uid': self.uid,
                            'mobile': self.mobile
                          }})
                session.rollback()
                return

            # fetches r.id from db
            session.refresh(r)
            # LOG.debug("Referrer ID: {}".format(r.id))

            rc = m.ReferralCode(referral_code=referral_code)
            session.add(rc)
            # LOG.debug('rc.referral_code: {}'.format(rc.referral_code))
            try:
                rc.referrer_id = r.id
                session.commit()
            except BaseException as ex:
                LOG.error('Could not commit referral_code', exc_info=True)
                session.rollback()

        return referral_code, r.link if r else None


@event.listens_for(Customer.email, 'set', named=True)
def set_email(**kwargs):
    # when email is updated from a previous none, update customer_level
    # as well. Using sqlAlchemy hooks to make this happen behind the scenes.
    row = kwargs['target']
    email = kwargs['value']
    # LOG.debug(' -- set_email: {} --'.format(email))
    session = object_session(row)

    if not row.email and email:
        # we are setting a previously unset value of email for this customer,
        # hence she can be level'ed up to CONTACT_UPDATED
        c_level = CustomerLevelLookup.get_by_level_display(session,
                                                           'CONTACT_UPDATED')
        row.customer_level_lookup_id = c_level.id


@event.listens_for(Customer.pan_number, 'set')
def set_pan_number(row, pan_number, oldvalue, initiator):
    # when pan_number is updated and kyc is yet to be determined,
    # update customer_level to PAN_UPDATED
    # LOG.debug(' -- set_pan_number: {} --'.format(pan_number))
    session = object_session(row)

    if pan_number:
        # we are setting a previously unset value of email for this customer,
        # hence she can be level'ed up to CONTACT_UPDATED
        c_level = CustomerLevelLookup.get_by_level_display(session,
                                                           'PAN_UPDATED')
        row.customer_level_lookup_id = c_level.id


@event.listens_for(Customer.is_kyc_verified, 'set')
def set_is_kyc_verified(row, is_kyc_verified, oldvalue, initiator):
    # when setting is_kyc_verified to True, we can set customer_level
    # to KYC_OK
    # LOG.debug(' -- set_is_kyc_verified: {} --'.format(is_kyc_verified))
    session = object_session(row)

    if is_kyc_verified:
        # we are setting a previously unset value of email for this customer,
        # hence she can be level'ed up to CONTACT_UPDATED
        c_level = CustomerLevelLookup.get_by_level_display(session,
                                                           'KYC_OK')
        row.customer_level_lookup_id = c_level.id


@event.listens_for(Customer.kyc_transaction_id, 'set')
def set_kyc_transaction_id(row, kyc_transaction_id, oldvalue, initiator):
    # when setting kyc_transaction_id, we can set customer_level
    # to EKYC_STARTED
    # LOG.debug(' -- set_kyc_transaction_id: {} --'.format(kyc_transaction_id))
    session = object_session(row)

    if kyc_transaction_id:
        # we are setting a previously unset value of email for this customer,
        # hence she can be level'ed up to CONTACT_UPDATED
        c_level = CustomerLevelLookup.get_by_level_display(session,
                                                           'EKYC_STARTED')
        row.customer_level_lookup_id = c_level.id


@event.listens_for(Customer.bank_account_no, 'set')
@event.listens_for(Customer.bank_ifsc, 'set')
def set_bank_info(row, value, oldvalue, initiator):
    # if both ifsc and bank_account_no are set, then update level
    # to BANK_UPDATED
    if row.bank_ifsc and row.bank_account_no:
        # LOG.debug(' -- set_bank_info: {} --'.format(value))
        session = object_session(row)
        c_level = CustomerLevelLookup.get_by_level_display(session,
                                                           'BANK_UPDATED')
        row.customer_level_lookup_id = c_level.id


@event.listens_for(Customer.occupation_code, 'set')
@event.listens_for(Customer.annual_income_code, 'set')
@event.listens_for(Customer.source_of_wealth_code, 'set')
@event.listens_for(Customer.politically_exposed, 'set')
def set_fatca_info(row, value, oldvalue, initiator):
    # if all fatca fields are set, then update level
    # to FATCA_UPDATED
    fields = ('occupation_code', 'annual_income_code',
              'source_of_wealth_code', 'politically_exposed')

    # true if all fields are not None.
    all_fields_updated = all(getattr(row, k, None) is not None for k in fields)
    # LOG.debug('all_fields_updated: {}'.format(all_fields_updated))

    if all_fields_updated:
        session = object_session(row)
        c_level = CustomerLevelLookup.get_by_level_display(session,
                                                           'FATCA_UPDATED')
        row.customer_level_lookup_id = c_level.id
