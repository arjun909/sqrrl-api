###############################################
# scheme.py
# Stores fund/scheme information
###############################################
from urllib.parse import urljoin

from sqlalchemy import (
    Column, String, Numeric, DateTime,
    Boolean, Integer, CheckConstraint
)
from sqlalchemy.dialects.postgresql import ARRAY

from app.model import Base
from app.constants import RISKOMETER
from app.config import AMC_LOGO_PATH
from app.log import get_logger
LOG = get_logger()


class Scheme(Base):
    """
    Model to describe MF fund or scheme.
    """
    __tablename__ = 'scheme'

    amc_code = Column(String, nullable=False, default='NA')

    # For KARVY there is a diff amc code for feed out (why oh why?)
    amc_code_out = Column(String, default='NA')

    amc_name = Column(String, nullable=False, default='NA')
    # Karvy, CAMS?
    rta = Column(String, nullable=False, default='Karvy')

    # RTA code is unique only for a particular AMC. This may be treated
    # as temporary - as GoalSchemeLookup is using this as a foreign-key
    rta_code = Column(String, nullable=False, unique=True)
    status = Column(String, nullable=False, default='Active')
    name = Column(String, nullable=False)
    short_name = Column(String)
    category = Column(String)
    scheme_type = Column(String)
    scheme_url = Column(String)
    # scheme objective
    objective = Column(String)

    dividend_reinvestment_option = Column(String)
    # exit_load - 0.0025 means 0.25%
    exit_load = Column(Numeric(precision=5, scale=4),
                       CheckConstraint('exit_load between 0 and 1'))
    exit_load_age_days = Column(Integer)
    risko_meter = Column(Integer)   # between 1 and 5
    benchmark = Column(String)
    minimum_initial_investment = Column(Numeric(scale=4))
    minimum_subsequent_investment = Column(Numeric(scale=4))
    purchase_minimum_increment_amount = Column(Numeric(scale=4))
    minimum_redemption_amount = Column(Numeric(scale=4))
    minimum_redemption_units = Column(Numeric(scale=4))
    redemption_incremental_units = Column(Numeric(scale=4))
    sip_allowed = Column(Boolean)
    sip_frequency = Column(String)
    minimum_sip_amount = Column(Numeric(scale=4))
    # sip_dates - these are the days of the month
    # null / empty means "any day of the month".
    sip_dates = Column(ARRAY(Integer))
    switch_in_allowed = Column(Boolean)
    switch_out_allowed = Column(Boolean)
    switch_out_minimum_amount = Column(Numeric(scale=4))
    switch_in_minimum_amount = Column(Numeric(scale=4))
    stp_out_allowed = Column(Boolean)
    stp_out_frequency = Column(String)
    stp_out_minimum_amount = Column(Numeric(scale=4))
    stp_in_allowed = Column(Boolean)
    stp_in_frequency = Column(String)
    stp_in_minimum_amount = Column(Numeric(scale=4))
    swp_allowed = Column(Boolean)
    swp_minimum_amount = Column(Numeric(scale=4))
    swp_frequency = Column(String)

    # returns
    one_month_return = Column(Numeric(scale=2))
    three_months_return = Column(Numeric(scale=2))
    six_months_return = Column(Numeric(scale=2))
    one_year_return = Column(Numeric(scale=2))
    three_years_return = Column(Numeric(scale=2))
    five_years_return = Column(Numeric(scale=2))

    # NAV
    nav = Column(Numeric(scale=4))
    nav_last_updated_on = Column(DateTime)

    # Some more codes
    isin = Column(String)
    amfi_code = Column(String)

    # brand logo and colours
    brand_logo = Column(String)
    brand_primary_colour = Column(String)

    # bank account details
    bank_name = Column(String)
    bank_ifsc = Column(String)
    bank_account_no = Column(String)
    bank_account_name = Column(String)
    bank_neft = Column(String)

    @staticmethod
    def populate_summary(arr, session):
        """
        For an array of objects with "scheme_id",
        update scheme summary info in "scheme"

        E.g., [{'a':1, 'scheme_id': 1}] should transform to
        [{'a': 1, 'scheme': {'id':1, 'name': 'hello' ... }}]
        """
        # TODO: Use a memoizable function to cache responses
        for item in arr:
            scheme_id = item.pop('scheme_id')
            s = session.query(Scheme).get(scheme_id)
            item['scheme'] = s.get_json_dict()

    @staticmethod
    def get_details(session, **kwargs):
        """
        Returns mandatory details for given scheme(s)
        """
        scheme_id = kwargs.pop('scheme_id', None)
        scheme_ids = kwargs.pop('scheme_ids', None)
        # query and return a single scheme
        if scheme_id:
            s = session.query(Scheme).get(scheme_id)
            return s.get_json_dict()
        elif scheme_ids:
            # return selected ids
            # LOG.debug('query scheme_ids {}'.format(scheme_ids))
            rows = session.query(Scheme).\
                filter(Scheme.id.in_(scheme_ids)).all()
            return [r.get_json_dict() for r in rows]
        else:
            # return all rows
            rows = session.query(Scheme).all()
            return [r.get_json_dict() for r in rows]

    def get_json_dict(self):
        """
        For a given row of a scheme, returns the dict that we want to
        return in the json.
        """
        result = {
            'id': self.id,
            'name': self.name,
            'short_name': self.short_name,
            'objective': self.objective,
            'category': self.category,
            'scheme_url': self.scheme_url,
            'scheme_type': self.scheme_type,
            'risko_meter': self.risko_meter,
            'risko_meter_display': RISKOMETER[self.risko_meter],
            'exit_load': self.exit_load,
            'exit_load_age_days': self.exit_load_age_days,
            'minimum_initial_investment': self.minimum_initial_investment,
            'brand_primary_colour': self.brand_primary_colour,
            'minimum_sip_amount': self.minimum_sip_amount,
            'purchase_minimum_increment_amount':
                self.purchase_minimum_increment_amount
        }
        # full path of brand logo
        result['brand_logo'] = urljoin(AMC_LOGO_PATH, self.brand_logo)

        # some computed percentages
        # TODO: write it more pythonically?
        if self.exit_load:
            result['exit_load_percent'] = self.exit_load * 100
        else:
            result['exit_load_percent'] = None
        if self.one_year_return:
            result['one_year_return_percent'] =\
                self.one_year_return * 100
        if self.three_years_return:
            result['three_years_return_percent'] =\
                self.three_years_return * 100
        if self.five_years_return:
            result['five_years_return_percent'] =\
                self.five_years_return * 100

        # return
        return result
