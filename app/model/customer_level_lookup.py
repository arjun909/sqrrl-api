# customer_level_lookup.py
from sqlalchemy import (
    Column,
    String,
    Integer,
)

from app.model import Base


class CustomerLevelLookup(Base):
    """
    A table that holds all customer levels we want to track in their
    product journey. E.g., a customer will start out at level 0, then work
    their way up to say 100.

    This replaces the `customer.onboarding_status` field with more flexible
    values. We can always insert new levels when the product evolves.
    """
    __tablename__ = 'customer_level_lookup'

    # an integer that can be used to assign a "level" like a game
    # e.g., everyone begins from level 0, then we try to take them higher.
    level = Column(Integer, nullable=False)
    # a string value for easier interpretation of above level
    level_display = Column(String, nullable=False)

    @classmethod
    def get_by_level(cls, session, level):
        # returns the row corresponding to `level`
        return session.query(cls).\
            filter(cls.level == level).first()

    @classmethod
    def get_by_level_display(cls, session, level_display):
        # returns the row corresponding to `level_display`
        return session.query(cls).\
            filter(cls.level_display == level_display).first()
