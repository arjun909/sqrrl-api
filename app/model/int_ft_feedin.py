##################################
# int_ft_feedin.py
##################################
from sqlalchemy.exc import IntegrityError

from app import model as M
from app import constants as C
from app.log import get_logger
LOG = get_logger()


class IntFTFeedIn(M.Base, M.FTFeedInMixin):
    """
    Intermediate table for FT feed in.
    Exactly like stg_ft_feedin, except table name.
    """
    __tablename__ = 'int_ft_feedin'

    @staticmethod
    def import_stg(session):
        """
        Imports unprocessed rows of interest into int_ft_feedin
        """
        rows = session.query(M.StgFTFeedIn).\
            filter(M.StgFTFeedIn.is_processed.is_(False)).all()

        # LOG.debug('Found {} rows'.format(len(rows)))
        for r in rows:
            # new row with identical data
            nr = IntFTFeedIn()
            for key in dir(nr):
                if key[0] == '_' or key in ('id', 'created_on', 'modified_on'):
                    continue
                try:
                    setattr(nr, key, getattr(r, key, None))
                except KeyError:
                    pass
            session.add(nr)
            r.is_processed = True
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                # ignores duplicate rows.
                session.rollback()
            except Exception as ex:
                LOG.error(ex)
                session.rollback()

    @staticmethod
    def update_transactions(session):
        """
        mothership.

        We have rows in int_ft_feedin - need to create or update
        corresponding Transactions.
        """
        rows = session.query(IntFTFeedIn).\
            filter(IntFTFeedIn.is_processed.is_(False)).\
            order_by(IntFTFeedIn.trxn_date).all()

        updated_investment_ids = []

        LOG.info('FT feed-in - processing {} rows'.format(len(rows)))

        for r in rows:
            try:
                tr = M.Transaction.find_by_feed_row(session, r, C.FT)

                # mark this row as processed.
                r.is_processed = True
                session.add(r)

                # we are only interested in processing transactions
                if not tr or not tr.status == C.TR_STATUS_PROCESSING:
                    continue

                # TODO: Check amounts.
                # LOG.debug('update {}'.format(tr.id))

                # 1. update folio no. if needed
                if not tr.investment.scheme_folio:
                    tr.investment.scheme_folio = r.folio_no

                # 2. update units, status
                tr.confirm_and_update(
                    status=C.TR_STATUS_SUCCESS,
                    units=r.units,
                )
                # updates the folio
                session.add(tr)

                # track updated investment ids
                updated_investment_ids.append(tr.investment_id)
            except Exception as ex:
                LOG.error(ex)
                raise ex

        # update investment rows
        if updated_investment_ids:
            investment_rows = session.query(M.Investment.group_id).\
                filter(M.Investment.id.in_(updated_investment_ids)).\
                distinct().all()
            for group_id in investment_rows:
                try:
                    M.Investment.post_verification(session, group_id)
                except Exception as ex:
                    LOG.error('Investment.post_verification failed',
                              exc_info=True)
        try:
            session.commit()
        except Exception as ex:
            LOG.error(ex)
            session.rollback()
        return(len(rows or []))
