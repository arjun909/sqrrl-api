#################################################################
# app/model/customer_action.py
# Stores customer actions (usually via GET /u/{action})
# for audit purposes. Can be used to log any customer actions, e.g.,
# fund switch.
#################################################################

from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import HSTORE

from app.model import Base, Customer
from app.log import get_logger
LOG = get_logger()


class CustomerAction(Base):
    __tablename__ = 'customer_action'
    # establish relationship with customer table
    customer_id = Column(Integer, ForeignKey('customer.id'))
    customer = relationship(
        Customer,
        backref=backref('actions', uselist=True, cascade='delete,all')
    )
    # the action that is being logged. cannot be empty
    action = Column(String, nullable=False)
    # the provided parameters for this action
    # can use JSON or JSONB, but already using HSTORE which is good enough for
    # logs and queries.
    params = Column(HSTORE)
    ip_address = Column(String)
