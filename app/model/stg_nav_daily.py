from sqlalchemy import (
    Column, String
)

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class StgNavDaily(Base):
    """
    Model to describe MF fund or scheme.
    """
    __tablename__ = 'stg_nav_daily'

    name = Column(String, nullable=False)
    nav = Column(String)
    isin = Column(String)
    nav_date = Column(String)
