from sqlalchemy import (
    Column, String, Numeric, DateTime
)

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class NavHistory(Base):
    """
    Model to describe MF fund or scheme.
    """
    __tablename__ = 'nav_history'

    name = Column(String, nullable=False)
    nav = Column(Numeric(scale=4))
    isin = Column(String)
    nav_date = Column(DateTime)
