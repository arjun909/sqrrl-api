################################
# stg_nach_register_feedin.py  #
################################
import xlrd
from sqlalchemy import (
    Column, String, Integer, Boolean
)
from app.model import Base
from app import log

LOG = log.get_logger()


class StgNachRegisterFeedIn(Base):
    __tablename__ = 'stg_nach_register_feedin'
    mandate_id = Column(String)
    investor_name = Column(String)
    upper_limit_amount = Column(Integer)
    bank_account_holder_name = Column(String)
    bank_account_number = Column(String)
    bank_name = Column(String)
    micr = Column(String)
    ifsc_code = Column(String)
    bank_account_type = Column(String)
    start_date = Column(String)
    end_date = Column(String)
    status = Column(String)
    umrn = Column(String)
    remarks = Column(String)
    is_processed = Column(Boolean, default=False)

    @staticmethod
    def insert_data(session, filepath):
        book = xlrd.open_workbook(filepath)
        work_sheet = book.sheet_by_index(0)
        max_rows = work_sheet.nrows
        new_rows = []
        for row in range(max_rows)[1:]:
            nr = StgNachRegisterFeedIn()
            nr.mandate_id = work_sheet.cell_value(row, 1)
            nr.investor_name = work_sheet.cell_value(row, 2)
            nr.upper_limit_amount = work_sheet.cell_value(row, 3)
            nr.bank_account_holder_name = work_sheet.cell_value(row, 4)
            nr.bank_account_number = work_sheet.cell_value(row, 5)
            nr.bank_name = work_sheet.cell_value(row, 6)
            nr.micr = work_sheet.cell_value(row, 7)
            nr.ifsc_code = work_sheet.cell_value(row, 8)
            nr.bank_account_type = work_sheet.cell_value(row, 9)
            nr.start_date = work_sheet.cell_value(row, 10)
            nr.end_date = work_sheet.cell_value(row, 11)
            nr.status = work_sheet.cell_value(row, 12)
            nr.umrn = work_sheet.cell_value(row, 13)
            nr.remarks = work_sheet.cell_value(row, 14)
            new_rows.append(nr)
        if new_rows:
            session.add_all(new_rows)
            session.commit()
