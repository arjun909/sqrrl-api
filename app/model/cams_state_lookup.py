#################################################################
# cams_state_lookup.py
# Stores mapping of state codes with AMC
#################################################################
from sqlalchemy import Column, String

from app.model import Base
from app.constants import OTHERS
from app.log import get_logger
LOG = get_logger()


class CamsStateLookup(Base):
    __tablename__ = 'cams_state_lookup'
    code = Column(String(2))
    value = Column(String)

    # not attaching any ForeignKey here so that recreating Scheme table
    # is easy (otherwise we'd have to cascade etc.)
    amc_code = Column(String(4))

    # use this to look-up state for code from PAN KYC data.
    kyc_state_code = Column(String(3))

    @classmethod
    def get_code(cls, session, state, amc_code):
        """
        Given a state and an amc_code, returns the code.
        """
        if not state:
            LOG.warn('state is empty')
            return ''
        # here we're doing a full-text search for the given state,
        # as well as "others" which is provided by some AMCs.
        # using Postgres full text search
        city_qs = ' & '.join(state.split(' '))
        results = session.query(cls).\
            filter(cls.amc_code == amc_code).\
            filter(
                (cls.value.match(city_qs)) | (cls.value.match(OTHERS))
            ).all()
        if len(results) == 2:
            # found exact match, as well as 'other'
            code = next(r.code for r in results
                        if OTHERS.lower() not in r.value.lower())
        elif len(results) == 1:
            # only one match, return code.
            code = results[0].code
        else:
            # No match?!?! WHATTODO?
            LOG.warn('No match for {} | {}'.format(amc_code, state))
            code = ''

        # if code:
        #     LOG.debug('{} | {} | {}'.format(amc_code, state, code))
        return code

    @classmethod
    def get_state_from_pan_code(cls, session, kyc_state_code):
        """
        Return the state name given a KYC state code
        e.g., 007 -> Delhi
        """
        result = session.query(cls).\
            filter(cls.kyc_state_code == kyc_state_code).first()
        if result:
            return result.value
        return OTHERS
