from sqlalchemy import (
    Column,
    String,
    Integer,
    Numeric,
    DateTime,
    Date
)
from app.model import Base
from app.config import CAMS_BROKER_CODE, KARVY_FEED_USERCODE, EUIN
from app import log

LOG = log.get_logger()


class CamsFeedOut(Base):

    __tablename__ = 'int_cams_feedout'

    # from scheme
    amc_code = Column(String(3))

    # Broker Code
    broker_code = Column(String(20), default=CAMS_BROKER_CODE)
    sub_broker_code = Column(String(15))
    user_code = Column(String(10), default=KARVY_FEED_USERCODE)

    # txn.id
    user_transaction_no = Column(Integer)
    application_no = Column(String(20))

    # if fresh purchase then blank else mandatory
    folio_no = Column(String(25))
    # Two digit number appearing in the Folio after '/'.  Check digit is not
    # applicable for SBI / BIRLA AMCs. Mandatory for Funds where Check digit
    # is applicable. For SBI / BIRLA this field should be blank

    # scheme_folio
    check_digit_numn = Column(String(2))

    # transaction table P/R
    transaction_type = Column(String(7))

    # rta_code
    scheme_code = Column(String(5))

    # customer.name
    first_name = Column(String(70))

    # if we have a joint investor
    joint_name1 = Column(String(35))

    joint_name2 = Column(String(35))

    # mandatory
    address_1 = Column(String(55))

    address_2 = Column(String(55))

    # customer.permanenet_city
    city = Column(String(35))

    # customer.permanent_pin
    pincode = Column(String(15))
    # NULL
    phone_off = Column(String(35))

    # transaction table : missing
    # Time Stamp Date (Applicable NAV Date).  In case of FundsNet STP upload,
    # FundsNet Server date will be considered.
    transaction_date = Column(Date)

    # transaction table : missing
    # aa:bb:cc
    transaction_time = Column(String(8))

    # transaction.units
    units = Column(Numeric(scale=5))
    # transaction.amount
    amount = Column(Numeric(scale=5))

    # Null
    clos_ac_ch = Column(String(1))
    # Mandatory field for Fresh Purchases for the Minor Investments with
    # appropriate Tax status code as per CAMS Records which is specific
    # to each Fund house ( For tax status code refer field no 40 )
    dob = Column(Date)

    # Optional field but Mandatory for Minor
    guardian = Column(String(120))

    # Pan_number
    pan_number = Column(String(40))
    # Optional NULL
    phone_res = Column(String(35))

    # option NULL
    fax_off = Column(String(35))
    # optional NULL
    fax_res = Column(String(35))

    # Optional field can be blank. Mandatory if A/C Statement option is E
    email = Column(String(250))

    # Mandatory field: Investor Bank Account No
    account_number = Column(String(40))
    # Mandatory
    account_type = Column(String(5))
    # bank_name
    bank_name = Column(String(40))
    # bank_branch_name
    branch_name = Column(String(40))
    bank_city = Column(String(35))

    # Mandatory field  - Fresh Purchase.  For Dividend Schemes only ( Allowed
    # values - Y - Re-invest and N - For Payout ) For Growth schemes - the
    # allowed value is Z   Applicable for only Dividend schemes  ( Default
    # value is Y ) Allowed values are Y - FOR Re-Investment and N - For
    # Payout .  For Growth schemes - ' Z'
    reinv_tag = Column(String(1))

    # Mandatory field - For Fresh Purchase  "Mode of Holding
    # (Default Values 'SI' - Single
    #                 'JO' - Joint
    #                 'AS' - Anyone or Survivor )"

    holding_nature = Column(String(2))

    # Values to be populated for fresh / additional purchases as per CAMS
    # Maintained values. This will vary from fund house to Fund house.
    # customer
    occupation_code = Column(String(2))
    # Mandatory field  - Values to be populated for fresh purchase /
    # additional purchase as per the CAMS Maintained values which
    # will vary for fund house to fund house.
    tax_status = Column(String(2))

    # Optional if the transaction mode is physical / Email.  Electronic
    # log details to be populated mandatorily, if the trxn mode is
    # other than Physical / Email.
    remarks = Column(String(250))
    # Mandatory field - Values to be populated for fresh purchase /
    # Additional purchase as per the CAMS Maintained values which
    # will vary from fund house to fund house.
    state = Column(String(2))

    # Mandatory field  - This should be used only for Systematic
    # Refer systematic transaction process note
    sub_transaction_type = Column(String(6))
    # Mandatory field ( as per data maintained at CAMS which will
    # vary from fund house to fund house )
    # div payout mechanism
    div_payout_mechanism = Column(String(7))

    # Null
    ecs_number = Column(String(1))
    # Null
    bank_code = Column(String(15))
    # Null
    alt_folio = Column(String(1))
    # Null
    alt_broker = Column(String(20))
    # Mandatory field. Values to be populated for all types of transactions
    # as per the Fundwise Transaction Level Master data provided by CAMS.
    location_code = Column(String(10))
    # Mandatory field. Values to be populated for all types of transactions
    # as per the Fundwise Transaction Level Master data provided by CAMS.
    # For all Direct Credit Payouts, applicable payout mechanism is DC and the
    # Investor Bank name to be populated as per the Bank name list of CAMS
    red_payout_mechanism = Column(String(7), default='DC')
    # Mandatory field. For Liquid Schemes historic purchase / Addl. Purchase
    # this value will be H and for all other transactions  this will be F
    pricing = Column(String(1))
    # Mandatory field only if the order contains first Joint holder name
    pan_2_holder = Column(String(40))
    # Mandatory field only if the order contains second Joint holder name
    pan_3_holder = Column(String(40))
    # "Mandatory - If mode of holding is single for other than non-individual.
    nominee_name = Column(String(40))
    # Null
    nominee_relation = Column(String(40))
    # MANDATORY FOR MINOR investments only for all other tax
    # status codes this column should be kept as Blank
    guardian_pan = Column(String(45))
    # Null
    instrmno = Column(String(15))
    # Null
    uin_number = Column(String(15))
    # Shall be mandatory if PAN of the 1st holder is provided
    # ( V or Y is the allowed value to be populated )
    valid_pan = Column(String(1), default='Y')
    # Mandatory for MINOR Investments only.  Allowed values V or Y
    guardian_pan_valid = Column(String(1))
    # Shall be mandatory if PAN of the 1st Joint Holder is provided
    # Allowed values is V or Y
    jh1valpan = Column(String(1))
    # Shall be mandatory if PAN of the 2nd Joint Holder is provided
    # Allowed value is  V or Y
    jh2valpan = Column(String(1))
    # Null
    form6061re = Column(String(1))
    # Null
    jh1f6061re = Column(String(1))
    # Null
    jh2f6061re = Column(String(1))
    # NUll
    gf6061re = Column(String(1))
    # NUll
    gsip_code = Column(String(30))
    # There will be no value in this field for Normal orders and for Systematic
    # Transactions this field will be populated with 1ST Processed date for the
    # set of systematic transactions.  For detailed reference please refer
    # systematic transactions Process flow.
    sip_registration_date = Column(Date)
    # Null
    first_hldr = Column(String(10))
    # Null
    jh1_min = Column(String(10))
    # Null
    jh2_min = Column(String(10))
    # Null
    gh_min = Column(String(10))

    # Optional. NEFT_CODE is required for payment of redemption proceeds
    # through NEFT, if such payout mechanism is allowed by AMC for Redemption
    # payouts. Width should be equal to 11 digits and failure of the same will
    # result in import error
    neft_code = Column(String(11))

    # Optional. RTGS_CODE is required for payment of redemption proceeds
    # through RTGS, if such payout mechanism is allowed by AMC for Redemption
    # payouts. Width should be equal to 11 digits and failure of the same will
    # result in import error
    rtgs_code = Column(String(11))

    # Value  must be either P or E .  P denotes Physical Account Statement and
    # E  denotes Email Account Statement
    email_acst = Column(String(1), default='E')
    # mobile number of investor
    mobile_no = Column(String(15))

    # Beneficiary Owner ID allotted by NSDL or CDSL. Mandatory for Exchange
    # Traded Funds where allotment of Units will be in electronic form.
    dp_id = Column(String(16))

    # Mandatory   Y - Discretionary POA and N - Non-Discreationary
    poa_type = Column(String(1), default='N')

    # Mandatory P – Physical, E-  Email, W- Web, T- Telephone
    # S- Systematic Triggered
    trxn_mode = Column(String(1), default='W')

    # Value to be populated as Y mandatorily for all types of transaction
    trxn_sign = Column(String(1), default='Y')
    # Mandatory for NRI Investors
    addl_addre = Column(String(55))
    addl_addr2 = Column(String(55))
    addl_addr3 = Column(String(55))
    city2 = Column(String(35))
    state2 = Column(String(2))
    country = Column(String(35))
    pincode2 = Column(String(35))

    # Not applicable if there exists only one nominee or nomination to be
    # registered on successive basis. Investor can specify the percentage
    # if the benefits are to be shared among the nominees. Aggregate
    # percentage of all nominees must be equal to 100%
    nom1_appli = Column(String(3))
    nom2_name = Column(String(40))
    nom2_relat = Column(String(40))
    nom2_appli = Column(String(1))
    nom3_name = Column(String(40))
    nom3_relat = Column(String(40))
    nom3_appli = Column(String(1))

    # Mandatory in case of NRI and the values applicable are Y or N
    check_flag = Column(String(1))
    # Mandatory and applicable value is N
    third_party_payment = Column(String(1), default='N')
    # Mandatory and applicable value is Y
    kyc_status = Column(String(1), default='Y')
    # Mandatory and applicable value is Y
    firc_status = Column(String(1))
    # Mandatory for systematic trxn
    sip_registration_number = Column(Integer)
    # Mandatory for systematic trxn
    number_of_installments = Column(Integer)
    # Mandatory for systematic trxn
    sip_freq = Column(String(5))
    # Mandatory for systematic trxn
    start_date = Column(DateTime)
    # Mandatory for systematic trxn
    end_date = Column(DateTime)
    # Mandatory for systematic trxn
    install_no = Column(Numeric(scale=5))
    # Mandatory if the nominee is a minor
    nominee_date_of_birth = Column(DateTime)
    # Mandatory if the nominee is a minor  ( Y or N )
    nominee_minor_flag = Column(String(1))
    # Mandatory if the nominee is a minor
    nominee_guardian = Column(String(35))
    # Null
    dummy1_c = Column(String(10))
    dummy2_n = Column(String(1))
    dummy3_n = Column(String(1))
    dummy4_c = Column(String(5))
    dummy5_c = Column(String(1))
    dummy6_n = Column(String(1))
    # Y or N- Mandatory
    fh_pan_exm = Column(String(1), default='N')
    # Y or N- Mandatory
    jh1_pan_ex = Column(String(1))
    # Y or N- Mandatory
    jh1_pan_e2 = Column(String(1))
    # Y or N- Mandatory
    guardian_pan_exempt = Column(String(1), default='N')
    fh_exmpt_c = Column(String(2))
    jh1_exmpt_c = Column(String(2))
    jh2_exmpt_c = Column(String(2))
    guar_exmpt_c = Column(String(2))
    fh_kra_exmpt_ref = Column(String(10))
    jh1_kra_exmpt_ref = Column(String(10))
    jh2_kra_exmpt_ref = Column(String(10))
    guard_exmpt_ref = Column(String(10))
    # Mandatory-Y/N
    euin_opted = Column(String(1), default='Y')
    euin = Column(String(10), default=EUIN)

    # Mandatory-Y/N. In case of "Y" system will update the tag"No nomination".
    # Incase of "N" system will update the value in Nominee name (field 54)
    nomination = Column(String(1), default='N')
    sub_broker = Column(String(20))
    bank_mandate_proof_submit = Column(String(1), default='N')

    # columns for ckyc
    fh_ckyc_ref_no = Column(Integer)
    jh1_ckyc_ref_no = Column(Integer)
    jh2_ckyc_ref_no = Column(Integer)
    guardian_ckyc_refno = Column(Integer)
    jh1_dob = Column(Date)
    jh2_dob = Column(Date)
    guardian_dob = Column(Date)
