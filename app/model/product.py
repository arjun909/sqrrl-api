# product.py
from datetime import datetime, timedelta
from sqlalchemy import Column, String, Integer, Boolean, or_
from sqlalchemy.dialects.postgresql import ENUM, ARRAY
import numpy as np

from app import model
from app import constants as C
from app.utils.allocation_helper import be_aggressive
from app.utils.helpers import get_duration_bucket, monthdelta
from app.log import get_logger
LOG = get_logger()


class Product(model.Base):
    """
    Sqrrl products - Sqrrl away (liquid), goal-based, ELSS.

    This table merely acts as a "template" for the actual
    investment rows.
    """
    __tablename__ = 'product'

    internal_name = Column(String, nullable=False)
    display_name = Column(String, nullable=False)
    # product-type - MF, Insurance, etc.
    product_type = Column(ENUM('Mutual Fund', 'Insurance',
                               name='product_type_enum'))
    # cannot foreign-key an array of ids, hence keeping this
    # as a simple integer array. This should contain fund.id values.
    schemes = Column(ARRAY(Integer))
    # can a customer have multiple instances of this running?
    allow_multiple = Column(Boolean, default=True)
    # instalment strategy - whether to calculate instalments based on
    # principal or returned amount.
    instalment_strategy = Column(String, default=C.PRINCIPAL, nullable=False)
    # some products allow both weekly and monthly investments.
    allowed_periods = Column(ARRAY(String),
                             default=[C.MONTHLY],
                             nullable=False)

    @staticmethod
    def get_recommended_schemes(product, doc, session):
        """
        For a given product and request document (with user-inputted params),
        this method determines which schemes to invest in, along with the
        split of the allocation between them.

        While much of the logic is hard-coded for now, eventually it can be
        driven by a more generic rules-based engine.

        TODO: We can re-use code instead of doing everything twice.
        """
        # NOTE: For getting scheme ids, using text to query against database as
        # hard-coding IDs seems far riskier.

        # NOTE: There's no check by consumer if the allocations add up to 100%

        # liquid / Sqrrl Away, everything goes into Reliance Money Manager.
        if product.internal_name == C.LIQUID:
            scheme = session.query(model.Scheme).\
                filter(model.Scheme.isin == C.RELIANCE_LIQUID_FUND_ISIN).\
                first()
            return [{'scheme_id': scheme.id, 'allocation_percent': 100}]
        elif product.internal_name == C.TAX:
            # ELSS - split between two funds equally, Axis long term
            # and ICICI long term
            schemes = session.query(model.Scheme).\
                filter(or_(
                    model.Scheme.isin == C.AXIS_LONG_TERM_FUND_ISIN,
                    model.Scheme.isin == C.ICICI_LONG_TERM_FUND_ISIN
                ))
            ret_val = []
            for s in schemes:
                ret_val.append({
                    'scheme_id': s.id,
                    'allocation_percent': 50,
                    'min_sip_amount': s.minimum_sip_amount,
                    })
            return ret_val
        else:
            # BYOD or Goal based.
            # 1. Find out if aggressive or moderate
            category = doc.get('category', '').lower()
            description = doc.get('description', '')
            # for target_date, either get it directly from completion date,
            # or compute it from number of instalments
            target_date = doc.get('target_completion_on', None)
            if not target_date:
                intervals = int(doc.get('target_instalment_count', 1)) - 1
                if intervals:
                    # intervals = int(doc['target_instalment_count']) - 1
                    if doc['instalment_period'] == C.WEEKLY:
                        target_date = datetime.utcnow() +\
                            timedelta(days=7*intervals)
                    else:
                        target_date = datetime.utcnow() +\
                            timedelta(days=30*intervals)
                else:
                    # no end-date provided!
                    raise Exception('{} - needs target_instalment_count'
                                    ' or target_date to be provided'.
                                    format(product.display_name))

            is_aggressive = be_aggressive(category, target_date, description)
            aggression_code = 'A' if is_aggressive else 'M'
            duration_bucket_code = get_duration_bucket(target_date)
            # LOG.debug('aggression_code: {}, duration_bucket_code {}'.
            #           format(aggression_code, duration_bucket_code))

            # feed these, and figure out which schemes to use
            ret_val = model.GoalSchemeLookup.get_allocations(
                duration_bucket_code,
                aggression_code,
                session
            )

            # Check if ret_val is not [] -> this means no allocation
            # found *gasp* and shit is about to blow up.
            if not ret_val:
                raise ValueError('No allocations could be determined'
                                 ' for this input')

            # compute the min. SIP amount based on suggested allocation and
            # compare with the monthly ask from the customer.
            min_monthly_ask = max(
                (s['min_sip_amount'] * 100 / s['allocation_percent']
                 for s in ret_val)
            )
            # LOG.debug('min_monthly_ask: {0:02f}'.format(min_monthly_ask))

            # based on provided ROI and targets, determine the monthly asking
            # amount, and ensure it works above.
            rate = float(ret_val[0]['target_roi'] / 12)  # monthly
            amount = float(doc['total_target_amount'])
            nper = monthdelta(target_date, datetime.utcnow())
            inst = abs(np.pmt(rate, nper, 0, fv=amount))
            # LOG.debug('inst: {0:02f}'.format(inst))

            if inst < min_monthly_ask:
                LOG.warn('Monthly ask too low, trying other portfolio')
                # trying A1 / M1 variant
                aggression_code = '{}1'.format(aggression_code)
                ret_val = model.GoalSchemeLookup.get_allocations(
                    get_duration_bucket(target_date),
                    aggression_code,
                    session
                )

                if not ret_val:
                    raise ValueError('No allocations could be determined'
                                     ' for this input')

                # if now the monthly ask is still too low, then there's
                # nothing to do
                min_monthly_ask = max(
                    (s['min_sip_amount'] * 100 / s['allocation_percent']
                     for s in ret_val)
                )
                rate = float(ret_val[0]['target_roi'] / 12)
                inst = abs(np.pmt(rate, nper, 0, fv=amount))
                # LOG.debug('recomputed inst: {0:02f}'.format(inst))
                if inst < min_monthly_ask:
                    raise ValueError('No allocations could be determined'
                                     ' for this input')

            return ret_val


    @staticmethod
    def get_product_by_internal_name(session, product_internal_name):
        return session.query(Product)\
            .filter(Product.internal_name == product_internal_name).one()