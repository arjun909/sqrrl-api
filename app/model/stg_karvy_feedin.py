###############################################################
#
# stg_karvy_feedin.py
#
# stores raw dump from Karvy dbf
###############################################################

from sqlalchemy import (
    Column, String, Numeric, Date, Boolean
)
from sqlalchemy.exc import IntegrityError
import dbf

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class KarvyFeedInMixin(object):
    """
    Common columns for StgKarvyFeedIn and IntKarvyFeedIn are defined in
    this mixin.
    """
    fmcode = Column(String(10))
    td_fund = Column(String(4))
    td_scheme = Column(String(2))
    td_plan = Column(String(2))
    td_acno = Column(Numeric)
    schpln = Column(String(4))
    divopt = Column(String(1))
    funddesc = Column(String)
    td_purred = Column(String(1))
    td_trno = Column(Numeric)
    smcode = Column(Numeric)
    chqno = Column(String(20))
    invname = Column(String)
    trnmode = Column(String(1))
    trnstat = Column(String(1))
    td_branch = Column(String)
    isctrno = Column(Numeric)
    td_trdt = Column(Date)
    td_prdt = Column(Date)
    td_units = Column(Numeric(19, 3))
    td_amt = Column(Numeric(19, 2))
    td_agent = Column(String)
    td_broker = Column(String)
    brokper = Column(String)
    brokcomm = Column(String)
    invid = Column(String(1))
    crdate = Column(Date)
    crtime = Column(String(6))
    trnsub = Column(String(1))
    td_appno = Column(String)
    unqno = Column(String, unique=True)  # we will ignore duplicates using this
    trdesc = Column(String)
    td_trtype = Column(String(10))
    navdata = Column(Date)
    portdt = Column(Date)
    assettype = Column(String)
    subtrtype = Column(String)
    citycateg0 = Column(String)
    euin = Column(String(20))
    trcharges = Column(Numeric(19, 2))
    clientid = Column(String)
    dpid = Column(String)
    stt = Column(Numeric(19, 2))
    ihno = Column(Numeric(15))
    branchcode = Column(String)
    inwardnum1 = Column(String(15))
    pan1 = Column(String(20))
    pan2 = Column(String(20))
    pan3 = Column(String(20))
    tdsamount = Column(Numeric(19, 2))
    chqdate = Column(Date)
    chqbank = Column(String)
    trflag = Column(String(5))
    load1 = Column(Numeric(19, 2))
    brok_entdt = Column(Date)
    nctremarks = Column(String)
    prcode1 = Column(String(10))
    status = Column(String)
    schemeisin = Column(String)
    td_name = Column(String)
    insamount = Column(Numeric(19, 2))
    rejtrnoor2 = Column(String)
    evalid = Column(String)
    edeclflag = Column(String)
    subarncode = Column(String)
    atmcardre3 = Column(String)
    atmcardst4 = Column(String)
    sch1 = Column(String(4))
    pln1 = Column(String(4))
    td_trxnmo5 = Column(String)
    newunqno = Column(String)
    sipregdt = Column(Date)
    divper = Column(Numeric(19, 10))
    can = Column(String)
    exchorgtr6 = Column(String)
    electrxnf7 = Column(String(1))
    td_pop = Column(Numeric(19, 4))

    # how we track whether this row has been 'processed'
    is_processed = Column(Boolean, default=False)


class StgKarvyFeedIn(Base, KarvyFeedInMixin):
    """
    The structure of this table is translated from Karvy's reverse feed
    dbf file. The column names are same as far as possible.
    """
    __tablename__ = 'stg_karvy_feedin'

    @staticmethod
    def import_dbf(session, file_name):
        """
        Imports a dbf into the table
        TODO: re-use this function across CAMS/Karvy.
        """
        LOG.info('importing {}...'.format(file_name))
        table = dbf.Table(file_name.split('.dbf')[0])
        table.open()
        for record in table:
            r = StgKarvyFeedIn()
            # iterate through all columns, pick the values from record
            # and set attributes.
            for key in table.field_names:
                try:
                    value = record[key]
                    if isinstance(value, str):
                        # remove all spaces around value if str
                        value = value.strip()
                    # LOG.debug('{}: {}'.format(key, value))
                    setattr(r, key, value)
                except KeyError:
                    pass
            # LOG.debug('\n ------ \n')
            session.add(r)
            try:
                session.commit()
            except IntegrityError:
                # ignore rows that are duplicates.
                # TODO: Use DO NOTHING constructs later.
                # LOG.warn('Unique error: Ignoring {}'.format(record.unqno))
                session.rollback()
            except Exception as ex:
                # other exceptions.
                LOG.error(ex)
                session.rollback()
        LOG.info('importing done!')
