from app.model import Base, ReferralCode
from sqlalchemy import (Column, Integer, ForeignKey)
from sqlalchemy.exc import IntegrityError
from app import log


LOG = log.get_logger()


class ReferralCodeCustomerMapping(Base):

    __tablename__ = 'referral_code_customer_mapping'

    referral_code_id = Column(Integer, ForeignKey('referral_code.id'))
    customer_id = Column(Integer, ForeignKey('customer.id'), unique=True)

    @staticmethod
    def add_row(session, ref_code, customer_id):

        rc_row = ReferralCode.get_referral_code_row(session, code=ref_code)

        try:
            new_mapping = ReferralCodeCustomerMapping()
            new_mapping.customer_id = customer_id
            new_mapping.referral_code_id = rc_row.id

            # Increment the counter in the ReferralCode table.
            ReferralCode.increment_downloads_count(session, ref_code)

            session.add(new_mapping)
            session.commit()
        except IntegrityError:
            LOG.error("The mapping of ref_code to customer_id already exists"
                      " for the customer_id={}.".format(customer_id))

        except Exception as exc:
            LOG.error("An exception occurred while creating a mapping for the"
                      " customer(id={}) with ref_code={}. The exception is: {}"
                      .format(customer_id, ref_code, exc), exc_info=True)
            raise exc
