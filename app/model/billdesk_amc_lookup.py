#################################################################
# billdesk_amc_lookup.py
# Stores mapping of AMC codes with Billdesk codes as per the
# lookup table provided by them.
#################################################################
from sqlalchemy import Column, String
from fuzzywuzzy import fuzz

from app.model import Base
from app.log import get_logger
LOG = get_logger()


class BilldeskAmcLookup(Base):
    __tablename__ = 'billdesk_amc_lookup'
    code = Column(String)
    value = Column(String)

    @staticmethod
    def find_by_amc(amc_name, session):
        """
        Does a fuzzy match on the AMC name and returns the corresponding code
        http://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python
        """
        # TODO: Cache in redis.
        rows = session.query(BilldeskAmcLookup).all()
        # will pick the best match
        scores = {}
        # Clean up common words and phrases from both terms
        # to make sure they don't match on account of these phrases
        amc_name = amc_name.lower().\
            replace('ltd', '').replace('private', '').replace('limited', '').\
            replace('company', '').replace('fund', '').replace('asset', '').\
            replace('management', '').replace('services', '').\
            replace('mutual', '').strip()
        for r in rows:
            # TODO: Use token_set_ratio instead?
            score = fuzz.partial_token_set_ratio(amc_name, r.value)
            # TODO: may have to fine-tune threshold
            if score > 95:
                # it's a match!
                scores[r.code] = score
        if not scores:
            # no match!
            raise Exception('No match found for {}'.format(amc_name))
        else:
            sorted_scores = sorted(scores.items(), reverse=True)
            # LOG.info('sorted_scores {}'.format(sorted_scores))
            code, value = sorted_scores[0]
            # LOG.info('{} - {}'.format(amc_name, code))
            return code
