from sqlalchemy import Column, DateTime, func, Integer
from sqlalchemy.ext.declarative import declarative_base
from app import log

LOG = log.get_logger()

class BaseModel(object):
    """
    All models will inherit from this base model
    """
    id = Column(Integer, primary_key=True)
    created_on = Column(DateTime, default=func.now())
    modified_on = Column(DateTime, default=func.now(), onupdate=func.now())

Base = declarative_base(cls=BaseModel)