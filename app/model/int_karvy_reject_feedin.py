##################################
#
# int_karvy_reject_feedin.py
#
##################################
from datetime import datetime
from sqlalchemy.exc import IntegrityError
from sqlalchemy import func

from app import model as M
from app import constants as C
from app.log import get_logger
LOG = get_logger()


class IntKarvyRejectFeedIn(M.Base, M.KarvyRejectFeedInMixin):
    """
    Intermediate table for rejected Karvy transactions.
    Exactly like stg_karvy_reject_feedin, except table name.
    """
    __tablename__ = 'int_karvy_reject_feedin'

    @staticmethod
    def import_stg(session):
        """
        Imports unprocessed rows into int_karvy_reject_feedin

        TODO: Re-use code from IntKarvyFeedIn
        """
        LOG.info('importing into int_karvy_reject_feedin')
        rows = session.query(M.StgKarvyRejectFeedIn).\
            filter(M.StgKarvyRejectFeedIn.is_processed.is_(False)).all()

        for r in rows:
            nr = IntKarvyRejectFeedIn()
            for key in dir(nr):
                if key[0] == '_' or key in ('id', 'created_on', 'modified_on'):
                    continue
                try:
                    setattr(nr, key, getattr(r, key, None))
                except KeyError:
                    pass
            session.add(nr)
            r.is_processed = True
            try:
                session.commit()
            except IntegrityError:
                # ignores duplicate rows.
                session.rollback()
            except Exception as ex:
                LOG.error(ex)
                session.rollback()

        LOG.info('Finished importing into int_karvy_reject_feedin')

    @staticmethod
    def update_transactions(session):
        """
        Based on rejected rows in int_karvy_reject_feedin update
        customer_transactions
        """
        rows = session.query(IntKarvyRejectFeedIn).\
            filter(IntKarvyRejectFeedIn.is_processed.is_(False)).all()

        LOG.info('Karvy Rejected Feed In: Processing {} rows'.
                 format(len(rows)))

        for r in rows:
            try:
                tr = IntKarvyRejectFeedIn.get_rejected_tr(session, r)
                if tr:
                    # LOG.debug('{} - {}'.format(tr.id, tr.amount))
                    tr.note = r.remarks
                    tr.status = C.TR_STATUS_FAILURE
                    session.add(tr)
                r.is_processed = True
                session.add(r)
            except Exception as ex:
                LOG.error(ex)
                raise ex

        # commit all updated rows.
        try:
            session.commit()
        except Exception as ex:
            LOG.error(ex)
            session.rollback()

        return len(rows or [])

    @staticmethod
    def get_rejected_tr(session, record):
        """
        For the provided rejected feed record, returns the Transaction
        row which needs to be marked as failed.
        """
        result = None
        # for purchase rejections, match the provided trno as all purchases
        # are submitted to Karvy via out-feed and the ids will match.
        # LOG.debug('trtype {}'.format(record.trtype))
        if 'NEW' in record.trtype or 'ADD' in record.trtype:
            # this is a purchase rejection
            result = session.query(M.Transaction).\
                filter(M.Transaction.id == record.trno).\
                filter(M.Transaction.transaction_type == C.INVEST).\
                filter(M.Transaction.amount == record.amount).\
                filter(M.Transaction.status == C.TR_STATUS_PROCESSING).first()
        elif 'RED' in record.trtype:
            # for redemption we use Reliance API directly, by-passing out feed,
            # therefore we need to use a more complex mechanism to map the
            # record with the internal transaction row.
            tr_date = datetime.strptime(record.trdate, r'%d/%m/%Y')
            rows = session.query(M.Transaction, M.Investment).\
                filter(M.Transaction.transaction_type == C.REDEEM).\
                filter(M.Transaction.amount == record.amount).\
                filter(M.Transaction.status == C.TR_STATUS_PROCESSING).\
                filter(M.Investment.scheme_folio == str(record.acno)).\
                order_by(
                    func.abs(
                        func.extract('day', M.Transaction.created_on - tr_date)
                    )
                ).first()
            # TODO: verify that dates on the two are "close enough"?
            if rows:
                result = session.query(M.Transaction).get(rows.Transaction.id)
        return result
