################################
#
# stg_nach_trxn_feedin.py
#
################################
import xlrd
from sqlalchemy import (
    Column, String, Integer, Boolean
)
from app.model import Base, Transaction
from app import log
from app.constants import (PROCESSED, TR_BILLDESK_STATUS,
                           ACKNOWLEDGED, PAID)

LOG = log.get_logger()


class StgNachTrxnFeedIn(Base):
    __tablename__ = 'stg_nach_trxn_feedin'
    id = Column(Integer, primary_key=True)
    mandate_id = Column(String)
    bank_account_holder_name = Column(String)
    bd_amc_code = Column(String)
    rta_code = Column(String)
    folio_no = Column(String)
    debit_date = Column(String)
    amount = Column(String)
    status = Column(String)
    reason = Column(String)
    trxn_id = Column(String)
    date_received = Column(String)
    pv_no = Column(String)

    # how to track if the row has been processed
    is_processed = Column(Boolean, default=False)

    @staticmethod
    def import_data(session, filepath):
        """
        Reads the excel file and imports data.
        """
        book = xlrd.open_workbook(filepath)
        work_sheet = book.sheet_by_index(0)
        max_rows = work_sheet.nrows
        new_rows = []
        for row in range(max_rows)[1:]:
            nr = StgNachTrxnFeedIn()
            nr.mandate_id = work_sheet.cell_value(row, 1)
            nr.bank_account_holder_name = work_sheet.cell_value(row, 2)
            nr.bd_amc_code = work_sheet.cell_value(row, 3)
            nr.rta_code = work_sheet.cell_value(row, 4)
            nr.folio_no = work_sheet.cell_value(row, 5)
            nr.debit_date = work_sheet.cell_value(row, 6)
            nr.amount = work_sheet.cell_value(row, 7)
            nr.status = work_sheet.cell_value(row, 8)
            nr.reason = work_sheet.cell_value(row, 9)
            nr.trxn_id = work_sheet.cell_value(row, 10)
            nr.date_received = work_sheet.cell_value(row, 11)
            nr.pv_no = work_sheet.cell_value(row, 12)
            new_rows.append(nr)
        if new_rows:
            session.add_all(new_rows)
            try:
                session.commit()
            except Exception as ex:
                LOG.error(ex)
                session.rollback()
        LOG.info('Importing of feed in file into staging table finished !')

    @staticmethod
    def mark_as_acknowledged(session, filepath):
        LOG.info('Marking NACH transactions as acknowledged.')
        book = xlrd.open_workbook(filepath)
        work_sheet = book.sheet_by_index(0)
        max_rows = work_sheet.nrows

        for row in range(max_rows)[1:]:
            trxn_id = work_sheet.cell_value(row, 5)
            status = work_sheet.cell_value(row, 8)
            if status == PROCESSED:
                row = session.query(Transaction).get(trxn_id)
                if row:
                    row.extras[TR_BILLDESK_STATUS] = ACKNOWLEDGED
                else:
                    LOG.info('trxn id {} found in file {} does not exist in'
                             ' database'.format(trxn_id, filepath))

        try:
            session.commit()
            LOG.info('Process to finish NACH transactions as achnowledged '
                     'successfuly finished !')
        except Exception as ex:
            LOG.error(ex)
            session.rollback()

    @staticmethod
    def update_utr(session, filepath):
        """
        updates UTR against Transactions
        """
        LOG.info('Updating UTR in NACH transactions')
        book = xlrd.open_workbook(filepath)
        work_sheet = book.sheet_by_index(0)
        max_rows = work_sheet.nrows

        for row in range(max_rows)[1:]:
            status = work_sheet.cell_value(row, 5)
            trxn_id = work_sheet.cell_value(row, 8)
            utr = work_sheet.cell_value(row, 13)

            if status == PAID:
                row = session.query(Transaction).get(trxn_id)
                row.extras[TR_BILLDESK_STATUS] = PAID
                row.utr_no = utr

        try:
            session.commit()
            LOG.info('Process to update UTR in NACH transactions finished !')
        except Exception as ex:
            LOG.error(ex)
            session.rollback()
