# log.py
# sets up logging format for the app to be used across.

import sys
import logging
from raven.handlers.logging import SentryHandler
from app import config

INFO_FORMAT = '[%(asctime)s] %(message)s'
DEBUG_FORMAT = '[%(asctime)s %(filename)s:%(lineno)d] %(message)s'
SENTRY_FORMAT = ('[%(asctime)s][%(levelname)s] %(name)s '
                 '%(filename)s:%(funcName)s:%(lineno)d | %(message)s')
TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'

if config.DEBUG:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

LOG = logging.getLogger('API')
LOG.propagate = False

stream_handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter(DEBUG_FORMAT, TIMESTAMP_FORMAT)
stream_handler.setFormatter(formatter)
LOG.addHandler(stream_handler)

# Sentry logging, using additional handler
if config.SENTRY_DSN:
    sentry_handler = SentryHandler(config.SENTRY_DSN)
    sentry_formatter = logging.Formatter(SENTRY_FORMAT, TIMESTAMP_FORMAT)
    sentry_handler.setFormatter(sentry_formatter)
    LOG.addHandler(sentry_handler)

# if DEBUG boolean is true, set log levels for all loggers to DEBUG
if config.DEBUG:
    logging.getLogger('requests').setLevel(logging.DEBUG)
    logging.getLogger('suds').setLevel(logging.INFO)
    logging.getLogger('zeep').setLevel(logging.INFO)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('sqlalchemy.engine').setLevel(logging.WARNING)
else:
    logging.getLogger('raven').setLevel(logging.WARNING)
    # configuring Celery logging, else this gets very noisy
    logging.getLogger('celery').setLevel(logging.INFO)
    # disable logs from 'requests' library
    # ref: http://stackoverflow.com/a/11029841
    logging.getLogger('requests').setLevel(logging.INFO)
    # ref: https://stackoverflow.com/questions/11029717/
    logging.getLogger("urllib3").setLevel(logging.INFO)
    # do not want SOAP logs.
    # ref: https://fedorahosted.org/suds/wiki/Documentation
    logging.getLogger('zeep').setLevel(logging.WARNING)
    logging.getLogger('suds').setLevel(logging.WARNING)
    # turn off s3 logs
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('s3transfer').setLevel(logging.WARNING)
    logging.getLogger('sqlalchemy.engine').setLevel(logging.ERROR)


def get_logger():
    return LOG
