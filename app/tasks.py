# tasks.py
# holds all celery tasks to be run asynchronously
from math import ceil
from datetime import datetime, timedelta
import logging
import requests
import csv
import redis

from app.utils.os_utils import file_exists, dir_exists
from app.utils import PdfMaker
import os
import shutil
from celery import Task, Celery, group
from celery.exceptions import MaxRetriesExceededError
from celery.signals import worker_init
from celery_once import QueueOnce
from sqlalchemy import or_, and_, func
from sqlalchemy.orm import sessionmaker, scoped_session
from raven import Client
from raven.contrib.celery import register_signal, register_logger_signal

from app.database import get_engine
from app import config, log, celery_config
from app.model import Scheme, Investment, Customer, Product
from app.model import (StgCamsFeedIn, StgKarvyFeedIn,
                       IntCamsFeedIn, IntKarvyFeedIn,
                       StgCamsRejectFeedIn, IntCamsRejectFeedIn,
                       StgKarvyRejectFeedIn, IntKarvyRejectFeedIn,
                       StgNachRegisterFeedIn, IntNachRegisterFeedIn,
                       StgFTFeedIn, IntFTFeedIn
                       )
from app.utils.karvy import Karvy
from app.utils.helpers import (monthdelta, weekdelta, get_ifsc_info,
                               s3_file_download, s3_file_upload)
from app.utils.reliance import RelianceMoneyManager
from app.utils.billdesk import Billdesk
from app import constants as C
from app.workers import (NavReader, NavUpdate, FeedOutWorker,
                         S3MailerWorker, SetupLogWorker, FTFeedUpload,
                         KarvyFeedUpload, RelianceSwitchManager, NachWorker,
                         CamsWorker)
from app.config import BASE_SENDER

from sparkpost import SparkPost
from slacker import Slacker
import app.workers

from app.utils import RedisWrapper

from app.communications import Email, Push, push_notification_constants as NC
from app.communications import sms_constants as SMS_TYPE
from app.communications.sms import send_bulk_transaction_sms

sp = SparkPost()
email = Email(log_level=1, base_sender=str(BASE_SENDER))
mobile_push = Push()
statements_pdf_maker = PdfMaker(template_name='statement_template.html')
slack = Slacker(config.SLACKER_TOKEN)
celery_app = Celery('tasks')
celery_app.config_from_object(celery_config)


LOG = log.get_logger()

# initialize wdb_session, once for a worker.
# ref: http://stackoverflow.com/a/17224993
wdb_session = scoped_session(sessionmaker())


# Initializing the Redis client
redis_client = RedisWrapper().get_redis_client()


@worker_init.connect
def initialize_session(signal=None, sender=None, **kwargs):
    LOG.info('initialize worker wdb_session')
    engine = get_engine(config.DATABASE_URL)
    wdb_session.configure(bind=engine)


# sentry setup
# ref: https://docs.sentry.io/clients/python/integrations/celery/
sentry_client = Client(config.SENTRY_DSN)
# register a custom filter to filter out duplicate logs
register_logger_signal(sentry_client, loglevel=logging.WARNING)
# hook into the Celery error handler
register_signal(sentry_client, ignore_expected=True)


class DBTask(Task):
    """
    An abstract Celery Task that ensures that the connection to the
    database is closed on task completion.
    ref http://www.prschmid.com/2013/04/using-sqlalchemy-with-celery-tasks.html
    """
    abstract = True

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        wdb_session.remove()


class DBTaskOnce(QueueOnce):
    """
    Extends QueueOnce to enable db tasks that run only once.
    """
    abstract = True

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        super().after_return(status, retval, task_id, args, kwargs, einfo)
        wdb_session.remove()


# /auth tasks
@celery_app.task(priority=0, queue='priority_queue')
def send_otp(mobile):
    """
    asynchronously sends otp to a mobile using third-party service
    """
    # LOG.info('[send_otp] %s' % mobile)
    # ref: https://sendotp.msg91.com/doc
    headers = {'application-Key': config.OTP_APPLICATION_KEY}
    data = {
        'countryCode': '91',
        'mobileNumber': mobile,
        'getGeneratedOTP': False
    }
    r = requests.post(config.OTP_GENERATE_URL,
                      headers=headers,
                      json=data).json()
    # LOG.info('[send_otp] status {}'.format(r['status']))
    return r


@celery_app.task(base=DBTask, priority=7, queue='priority_queue')
def load_schemes():
    """
    Loads scheme data from schemes.csv to scheme table.
    Driven by boolean config var BOOTSTRAP_SCHEMES
    """
    # use a redis key to track whether this process is already
    # running.
    try:
        LOG.info('entering load_schemes')
        rdb = redis.StrictRedis.from_url(config.REDIS_URL)
        key = 'lock-scheme-import'
        if rdb.incr(key) > 1:
            LOG.warn('load_schemes already running')
            return
        LOG.info('[load_schemes] import schemes into table')
        with open('app/database/schemes.csv', newline='') as f:
            reader = csv.DictReader(f)
            for row in reader:
                # some cleanup for each row
                for k in row:
                    if row[k] == '':
                        row[k] = None
                    elif row[k] == 'Y':
                        row[k] = True
                    elif row[k] == 'N':
                        row[k] = False
                    elif k == 'sip_dates':
                        # need to convert array
                        arr_str = row[k][1:-1].split(',')
                        # LOG.debug('arr_str {}'.format(arr_str))
                        arr = [int(e) for e in arr_str]
                        row[k] = arr
                # insert or update
                if 'id' in row and row['id']:
                    wdb_session.query(Scheme).\
                        filter(Scheme.id == row['id']).update(**row)
                else:
                    r = Scheme(**row)
                    wdb_session.add(r)
            # commit changes to db
            wdb_session.commit()
    except Exception as ex:
        wdb_session.rollback()
        LOG.error(ex)
        raise ex
    finally:
        config.BOOTSTRAP_SCHEMES = False
        LOG.debug('resetting key {}'.format(key))
        rdb.getset(key, 0)


@celery_app.task(base=DBTask, priority=6, queue='priority_queue')
def update_investments(group_id):
    """
    Updates some computed fields when a new investment group is created.
    """
    investments = wdb_session.query(Investment).\
        filter(Investment.group_id == group_id).all()

    if investments:
        # will push computed fields here.
        computed = {}
        # all rows will have same values for target_instalment_amount etc.
        params = investments[0]
        if params.total_target_amount:
            # if we know the planned investment and one of planned
            # instalments or planned target date, we can compute the other.
            if params.target_completion_on:
                if params.instalment_period == C.WEEKLY:
                    num_inst = weekdelta(params.target_completion_on)
                else:
                    num_inst = monthdelta(params.target_completion_on)
                # NOTE: this will overwrite any existing value.
                computed['target_instalment_count'] = num_inst
                # compute target instalment based on above number
                if not params.target_instalment_amount:
                    computed['target_instalment_amount'] = ceil(
                        params.total_target_amount / num_inst
                    )


@celery_app.task(base=DBTask, priority=6, queue='priority_queue')
def refresh_pan_data(customer_id):
    """
    Utility task called via admin API to reload a customer's PAN details
    from Karvy. Typically for a scenario where PAN documents are uploaded and
    status needs to be refreshed.
    """
    c = wdb_session.query(Customer).get(customer_id)
    (validate_pan.s(c.uid, c.pan_number) | fetch_pan_data.s(c.uid))()


@celery_app.task(bind=True, base=DBTask, max_retries=2, priority=0,
                 queue='priority_queue')
def validate_pan(self, uid, pan):
    """
    Validates PAN information (KYC) using Karvy service.
    """
    try:
        if uid and pan:
            k = Karvy(wdb_session, uid)
            result = k.is_kyc_certified(pan)
            if not any((result['is_kyc_verified'],
                        result['ekyc_allowed'],
                        result['pan_kyc_allowed'])):
                # NOTE: This guy needs to be processed offline.
                # This will notify sentry and in-turn, slack.
                LOG.warn('[ALERT] KYC needs to be done offline',
                         extra={
                            'customer_uid': uid,
                            'PAN': pan
                         })
                payload = {
                    'channel': C.SLACK_OPS_CHANNEL,
                    'text': 'Need to do offline KYC',
                    'attachments': [{
                        'fields': [
                            {'title': 'User UID', 'value': uid, 'short': True},
                            {'title': 'PAN', 'value': pan, 'short': True}
                        ]
                    }],
                    'as_user': False,
                    'username': 'Cortana'
                }
                slack.chat.post_message(**payload)
            return result
    except Exception as ex:
        LOG.error('validate_pan failed', exc_info=True)
        if isinstance(ex, ValueError):
            # Don't want to retry this exception, rather return it
            raise ex
        else:
            self.retry(countdown=60, exc=ex)


@celery_app.task(bind=True, base=DBTask, max_retries=3, priority=6,
                 queue='priority_queue')
def fetch_pan_data(self, result, uid):
    """
    Fetches PAN data if user is kyc verified
    """
    try:
        if result and result['is_kyc_verified'] and not config.DEBUG:
            LOG.debug('Fetching PAN data for Customer UID: {}'.format(uid))
            k = Karvy(wdb_session, uid)
            k.get_all_pan_data()
    except Exception as ex:
        LOG.error('PAN data fetch and update failed for customer uid: {}'.
                  format(uid), exc_info=True)
        if isinstance(ex, ValueError):
            # Don't want to retry this exception, rather return it
            raise ex
        else:
            self.retry(countdown=60, exc=ex)


@celery_app.task(base=DBTask, bind=True, priority=3, queue='priority_queue')
def update_bank_from_ifsc(self, customer_id):
    """
    When a customer's bank_ifsc code is set, fetch related info
    like bank's name, branch etc and set it as well.
    """
    if customer_id:
        c = wdb_session.query(Customer).get(customer_id)
        wdb_session.refresh(c)  # get any pending data changes into c
        if not c.bank_ifsc:
            # bogus call, must have bank_ifsc in table.
            LOG.warn('Bank IFSC is null', extra={
                'customer_id': customer_id
            })
            return
        # using Razorpay IFSC service.
        try:
            bank_info = get_ifsc_info(c.bank_ifsc)
            # LOG.info('bank_info \n{}'.format(bank_info))
            if bank_info:
                c.bank_name = bank_info['BANK']
                c.bank_branch_name = bank_info['BRANCH']
                c.bank_branch_city = bank_info['CITY']
                c.bank_branch_address = bank_info['ADDRESS']
                wdb_session.commit()
        except Exception as ex:
            LOG.error('ifsc lookup failed', exc_info=True)
            # retry in 30 seconds
            self.retry(exc=ex, countdown=30)


@celery_app.task(base=DBTask, max_retries=5, priority=0,
                 queue='priority_queue')
def get_rmm_transaction_status(result,
                               investment_group_id,
                               transaction_group_id):
    """
    Fetches transaction status using reference number
    """
    # LOG.info('get_rmm_transaction_status: {}'.format(transaction_group_id))
    try:
        rmm = RelianceMoneyManager(investment_group_id,
                                   transaction_group_id,
                                   wdb_session)
        rmm.transaction_status()
    except Exception as ex:
        LOG.error('RMM - status fetch failed', exc_info=True)
        # starting with 2 minutes, keeps increasing delay between retries
        countdown = 2 * 60 * get_rmm_transaction_status.request.retries
        # LOG.debug('retry in {} seconds'.format(countdown))
        raise get_rmm_transaction_status.retry(exc=ex, countdown=countdown)


@celery_app.task(base=DBTask, max_retries=3, bind=True, priority=2,
                 queue='priority_queue')
def update_rmm_balance(self, investment_group_id, transaction_group_id):
    """
    Updates available balance (units + amount) from Reliance
    """
    rmm = RelianceMoneyManager(investment_group_id,
                               transaction_group_id,
                               wdb_session)
    try:
        rmm.get_withdrawal_balance()
    except Exception as ex:
        LOG.error(ex)
        # retry in 30 seconds
        self.retry(exc=ex, countdown=30)


@celery_app.task(base=DBTask, max_retries=3, bind=True, priority=0,
                 queue='priority_queue')
def rmm_redeem(self, investment_group_id, transaction_group_id, amount, ip):
    """
    Updates available balance (units + amount) from Reliance
    """
    redeem_result = None
    try:
        rmm = RelianceMoneyManager(investment_group_id,
                                   transaction_group_id,
                                   wdb_session)

        redeem_result = rmm.redeem(amount, ip)

    except Exception as ex:
        if self.request.retries == self.max_retries:
            LOG.error("Maximum retries({}) reached. Sqrrl Away REDEEM({}) "
                      "failed.".format(self.max_retries, self.name), exc_info=True)  # NOQA
            # ALERT OPS-TEAM over SLACK
            # Send SMS to the customer
            customer_mobile = wdb_session.query(Investment).\
                filter(Investment.group_id == investment_group_id).first().\
                customer.mobile

            # if isinstance(ex, requests.exceptions.ConnectionError):
            #     communications_sms.delay(SMS_TYPE.SQRRL_AWAY_FAILED_REDEEM,
            #                              customer_mobile,
            #                              group_id=transaction_group_id)
            # else:
            #     communications_sms.delay()
            communications_sms.delay(SMS_TYPE.SQRRL_AWAY_FAILED_REDEEM,
                                     customer_mobile,
                                     group_id=transaction_group_id)

        else:
            LOG.error('RMM Redeem failed. Retrying.')
            # retry in 5 minutes
            self.retry(exc=ex, countdown=300)

    return redeem_result


@celery_app.task(base=DBTask, max_retries=3, priority=0,
                 queue='priority_queue')
def get_billdesk_transaction_url(customer_uid, investment_group_id,
                                 transaction_group_id, amount,
                                 payment_mode=C.PAYMENT_MODE_NB, doc={}):
    """
    Same as get_rmm_transaction_url above, instead wraps around Billdesk API.
    """
    bd = Billdesk(customer_uid, wdb_session)
    url, params = bd.get_transaction_url(investment_group_id,
                                         transaction_group_id, amount,
                                         payment_mode=payment_mode)
    doc['transaction'] = {
        'group_id': transaction_group_id,
        'url': url,
        'params': params,
        'method': 'POST',
        'stop_url': config.BILLDESK_RETURN_URL
    }
    return doc


@celery_app.task(bind=True, base=DBTask, max_retries=5, priority=0,
                 queue='priority_queue')
def get_kyc_url(self, customer_uid, aadhaar, aadhaar_email, aadhaar_mobile):
    """
    Wraps Karvy.get_kyc_url in a task
    """
    k = Karvy(wdb_session, customer_uid)
    try:
        result = k.get_kyc_url(aadhaar, aadhaar_email, aadhaar_mobile)
    except Exception as ex:
        LOG.error(ex)
        self.retry(countdown=1, exc=ex)
    return result


@celery_app.task(base=DBTask, max_retries=3, bind=True, priority=9,
                 queue='priority_queue')
def create_zero_balance_folio(self, investment_group_id):
    rel = RelianceMoneyManager(investment_group_id, None, wdb_session)

    rdb = redis.StrictRedis.from_url(config.REDIS_URL)
    key = ''.join([C.REDIS_ZBF_TASK_ID_PREFIX, investment_group_id])
    try:
        job_id = self.request.id
        rdb.set(key, job_id)
        # LOG.debug("in create zbf {} - {} ".format(key, job_id))
        rel.create_zbf()
    except Exception as ex:
        LOG.error(ex)


@celery_app.task(base=DBTaskOnce, bind=True, max_retries=5,
                 once={'graceful': True}, priority=5, queue='priority_queue')
def fetch_daily_nav(self):
    """
    Runs daily to update NAV in scheme table.
    """
    try:
        nav_reader = NavReader(wdb_session)
        nav_reader.get_nav_amfi(config.AMFIINDIA_URL,
                                config.AMFIINDIA_PART_URL)
        nav_update = NavUpdate(wdb_session)
        nav_update.create_intermediate_table()
        nav_update.update_records_in_navhistory()
        nav_update.update_navs_in_scheme()
        Investment.update_nav(wdb_session)
        Investment.update_current_value(wdb_session)
    except Exception as ex:
        LOG.error('NAV update failed', exc_info=True)
        wdb_session.rollback()
        # retry in 20 minutes
        self.retry(countdown=1200, exc=ex)


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=6,
                 queue='priority_queue')
def update_all_rmm_folios():
    """
    wrapper around investment.update_reliance_balances
    """
    # fetch rows which have "stale" NAV data
    yesterday = datetime.utcnow() - timedelta(hours=24)
    product = wdb_session.query(Product).\
        filter(Product.internal_name == C.LIQUID).first()
    rows = wdb_session.query(Investment).\
        filter(
            Investment.product_id == product.id,
            or_(Investment.scheme_nav_date <= yesterday,
                Investment.scheme_nav_date is None)
        ).all()
    for r in rows:
        # kicks of more workers.
        update_rmm_balance.delay(r.group_id, None)


@celery_app.task(bind=True, base=DBTask, max_retries=5, priority=0,
                 queue='priority_queue')
def validate_pan_number(self, customer_uid, pan_number):
    """
    Thin wrapper over Karvy.validate_pan_number

    In case of failure, sends an email to operations so that this can be taken
    up later.
    """
    k = Karvy(wdb_session, customer_uid)
    try:
        # check if pan is valid according to Income tax database.
        is_valid = k.validate_pan_number(pan_number)
        if not is_valid and not config.DEBUG:
            # Notify admin using email.
            # TODO - use config variables.
            c = Customer.get_profile_for_uid(customer_uid, wdb_session)
            sp.transmissions.send(
                recipients=['operations@sqrrl.in'],
                template='ops-pan-invalid',
                substitution_data={
                    'uid': customer_uid,
                    'pan_number': pan_number,
                    'name': c['name'],
                    'date_of_birth': str(c['date_of_birth'])
                }
            )
            # LOG.debug(response)
        return is_valid
    except Exception as ex:
        LOG.error(ex)
        try:
            self.retry(countdown=5, exc=ex)
        except MaxRetriesExceededError as m_ex:
            # TODO: Send an email as could not validate PAN in 5 retries.
            LOG.warn(m_ex)


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=5,
                 queue='priority_queue')
def process_feed_out_karvy():
    """
    Process pending transactions, generate Karvy feed out dbf, and email.
    """
    f = FeedOutWorker(wdb_session)
    len_new_rows = f.insert_karvy_transactions()

    # stop here if there are no rows to upload
    if not len_new_rows:
        LOG.info('Karvy: No transactions to upload')
        return

    try:
        # create feed out XML
        feed_out_worker_obj = FeedOutWorker(wdb_session)
        karvy_file_list = feed_out_worker_obj.create_karvy_xml()
        # upload several karvy XMLs in parallel
        LOG.info('Karvy XML upload', extra={
            'file_count': len(karvy_file_list)
        })
        count = 0
        for amc_code, fpath in karvy_file_list:
            # space out xml uploads by 60 seconds
            countdown = count * 60
            count = count + 1
            upload_karvy_xml.apply_async((amc_code, fpath),
                                         countdown=countdown)
    except Exception as ex:
        LOG.error('Karvy XML upload failed, use dbf', exc_info=True)

    if len_new_rows:
        try:
            file_name = f.create_karvy_dbf()
            if not config.DEBUG:
                response = slack.files.upload(file_name, channels=[C.SLACK_OPS_CHANNEL])  # NOQA
                slack.chat.post_message(C.SLACK_OPS_CHANNEL,
                                        'Karvy feed out for {} transactions '
                                        'generated'.format(len_new_rows))
        except Exception as ex:
            raise ex
    else:
        LOG.info('Karvy: No transactions to upload')


@celery_app.task(bind=True, base=DBTask, priority=6, queue='priority_queue',
                 max_retries=1)
def upload_karvy_xml(self, amc_code, filepath):
    """
    calls KarvyFeedUpload.upload_file() and uploads karvy XML
    """
    try:
        # LOG.info('Uploading to Karvy {} - {}'.format(amc_code, filepath))
        KarvyFeedUpload.upload_file(wdb_session, amc_code, filepath)
    except Exception as ex:
        LOG.warn('Error uploading Karvy XML, will retry', exc_info=True)
        # retry in 2 minutes
        self.retry(exc=ex, countdown=120)


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=5,
                 queue='priority_queue')
def process_feed_out_cams():
    """
    Process pending transactions, generate CAMS feed out dbf file.
    Uploads the dbf files to the cams server and emails it to operations.
    """
    f = FeedOutWorker(wdb_session)
    len_new_rows = f.insert_cams_transactions()
    f.create_cams_dbf()
    pattern = C.REDIS_CAMS_FEEDFILE_PREFIX + '*'
    r_keys = redis_client.keys(pattern)
    key_list = [key.decode().split('/', 1)[-1]
                for key in r_keys]

    # upload all dbf files to slack
    # for key in key_list:
    #     upload_to_slack.delay(key)

    try:
        LOG.info('Starting upload of Cams dbf files.......')
        count = 0
        for key in key_list:
            # space the uploads by 60 seconds
            countdown = count * 60
            count = count + 1
            upload_cams_feed.apply_async((key,), countdown=countdown)
    except Exception as ex:
        LOG.error('Cams dbf upload failed, upload in manual mode',
                  exc_info=True)
        raise ex

    if len_new_rows:
        try:
            slack.chat.post_message(C.SLACK_OPS_CHANNEL,
                                    'CAMS Feed out for {} transactions '
                                    'generated'.format(len_new_rows))
        except Exception as ex:
            raise ex
    else:
        LOG.info('CAMS: No transactions to upload')


@celery_app.task(bind=True, base=DBTask, priority=6,
                 max_retries=5, queue='priority_queue')
def upload_cams_feed(self, key):
    """
    calls KarvyFeedUpload.upload_file() and uploads karvy XML
    """
    try:
        worker_obj = CamsWorker(wdb_session)
        worker_obj.upload_file(key)
    except Exception as ex:
        LOG.error(ex)
        # self.retry(exc=ex, countdown=30)


@celery_app.task(base=DBTask, priority=8, queue='priority_queue')
def upload_to_slack(key):
    """
    Uploads the file provided to slack channel
    """
    file_name = '/' + key
    try:
        s3_file_download(key, file_name)
        response = slack.files.upload(file_name, channels=[C.SLACK_OPS_CHANNEL])  # NOQA
        if response.successful:
            LOG.debug('{} file uploaded to Slack'.format(file_name))
    except Exception as ex:
        LOG.error('Could not upload file to slack', exc_info=True, extra={
            'param_key': key
        })


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=5,
                 queue='priority_queue')
def process_feed_out_FT():
    """
    Process pending transactions, generate FT feed out dbf and textfile.
    """
    LOG.info('Processing FT transactions (out)...')
    f = FeedOutWorker(wdb_session)
    f.insert_FT_transactions()
    # f.create_FT_dbf()
    filename = f.create_FT_txtfile()
    FTFeedUpload.upload_FT_txtfile(filename)
    LOG.info('Processing FT transactions (out)...COMPLETED')


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=9,
                 queue='priority_queue')
def delete_demo_investments():
    """
    Delete all investments made by demo accounts
    """
    demo_accounts = config.DEMO_ACCOUNTS
    if not demo_accounts:
        return
    for mobile in demo_accounts:
        records = wdb_session.query(Customer.id, Investment.group_id).\
                filter(Customer.mobile == mobile).\
                filter(Customer.id == Investment.customer_id).all()
        # remove duplicate entries
        records = list(set(records))
        for record in records:
            investment_group_id = record.group_id
            customer_id = record.id
            Investment.delete_group(investment_group_id, customer_id,
                                    wdb_session)


@celery_app.task(base=DBTaskOnce, max_retries=3, priority=5,
                 queue='priority_queue')
def karvy_feed_in(s3_key, filepath, feed_type=C.PROCESSED):
    # first download the file from s3 to filepath
    s3_file_download(s3_key, filepath)
    len_new_rows = 0
    if feed_type == C.PROCESSED:
        StgKarvyFeedIn.import_dbf(wdb_session, filepath)
        IntKarvyFeedIn.import_stg(wdb_session)
        len_new_rows = IntKarvyFeedIn.update_transactions(wdb_session)
    elif feed_type == C.KARVY_REJECTED:
        # diff. process for karvy rejections.
        StgKarvyRejectFeedIn.import_dbf(wdb_session, filepath)
        IntKarvyRejectFeedIn.import_stg(wdb_session)
        len_new_rows = IntKarvyRejectFeedIn.update_transactions(wdb_session)
    try:
        slack.chat.post_message(C.SLACK_OPS_CHANNEL,
                                'Karvy feed in for {} transactions processed.'.
                                format(len_new_rows))
    except Exception as ex:
        LOG.error('slack post failed', exc_info=True)


@celery_app.task(base=DBTaskOnce, max_retries=3, priority=5,
                 queue='priority_queue')
def cams_feed_in(s3_key, filepath, feed_type=C.PROCESSED):
    # first download the file from s3 to filepath
    s3_file_download(s3_key, filepath)
    len_new_rows = 0
    if feed_type == C.PROCESSED:
        # feed for PROCESSED transactions
        StgCamsFeedIn.import_dbf(wdb_session, filepath)
        IntCamsFeedIn.import_stg(wdb_session)
        len_new_rows = IntCamsFeedIn.update_transactions(wdb_session)
    elif feed_type == C.REJECTED:
        # rejected feed.
        StgCamsRejectFeedIn.import_dbf(wdb_session, filepath)
        IntCamsRejectFeedIn.import_stg(wdb_session)
        len_new_rows = IntCamsRejectFeedIn.update_transactions(wdb_session)
    try:
        slack.chat.post_message(C.SLACK_OPS_CHANNEL,
                                'CAMS feed in for {} {} trx'.
                                format(len_new_rows, feed_type))
    except Exception as ex:
        # TODO: Retry
        LOG.error('slack post failed', exc_info=True)


@celery_app.task(base=DBTaskOnce, max_retries=3, priority=5,
                 queue='priority_queue')
def ft_feed_in(s3_key, filepath):
    """
    Processes incoming Franklin feed file.
    """
    LOG.info('Processing FT feed in...')
    s3_file_download(s3_key, filepath)
    StgFTFeedIn.import_dbf(wdb_session, filepath)
    IntFTFeedIn.import_stg(wdb_session)
    len_new_rows = IntFTFeedIn.update_transactions(wdb_session)
    try:
        slack.chat.post_message(C.SLACK_OPS_CHANNEL,
                                'FT feed in for {} trx'.format(len_new_rows))
    except Exception as ex:
        # TODO: Retry
        LOG.error('slack post failed', exc_info=True)
    LOG.info('Processing FT feed in...FINISHED')


@celery_app.task(base=DBTaskOnce, once={'graceful': True},  priority=5,
                 queue='priority_queue')
def mail_pan_kyc_docs():
    """
    Mail pan_kyc docs from s3 bucket to operations
    """
    s = S3MailerWorker(wdb_session)
    s.get_s3_objects()


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=5,
                 queue='priority_queue')
def mail_setup_feed():
    """
    mails setup feed of customers to operations
    """
    s = SetupLogWorker(wdb_session)
    s.create_setup_log()


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=5,
                 queue='priority_queue')
def update_billdesk_failed_transactions():
    LOG.info('Check failed Billdesk transactions...')
    bd_obj = app.workers.BilldeskWorker(wdb_session)
    rows = bd_obj.get_billdesk_failed_transactions()
    LOG.debug('Found {} rows'.format(len(rows or [])))
    group(bd_query_transaction.s(row.Customer.uid,
                                 row.Transaction.group_id)
          for row in rows)()
    LOG.info('Check failed Billdesk transactions...COMPLETED')


@celery_app.task(base=DBTask, priority=5, queue='priority_queue')
def bd_query_transaction(customer_uid, group_id):
    # created async tasks for each time Billdesk.query_transaction is called
    bd_obj = Billdesk(customer_uid, wdb_session)
    bd_obj.query_transaction(group_id)


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=3,
                 queue='priority_queue')
def update_ekyc_status():
    """
    This task retreives the records where customer.kyc_status is '01'.
    The status of fetched rows is then passed to karvy.is_kyc_certified.
    Our goal is to make sure that all customers with
    is_kyc_verified True should have kyc_status '02'.
    """
    rows = wdb_session.query(Customer).filter(
                                      Customer.kyc_status == '01',
                                      Customer.is_kyc_verified).all()
    for row in rows:
        karvy_obj = Karvy(wdb_session, row.uid)
        karvy_obj.is_kyc_certified(row.pan_number)


@celery_app.task(base=DBTask, priority=2, queue='priority_queue')
def send_verification_email(doc, url):
    """
    If email is provided in the patch request to the customer_resource API,
    this worker is called which sends a verification link to the user email.
    """
    sp.transmissions.send(
            recipients=[doc['email']],
            template='verify-email',
            substitution_data={
                'confirmation_url': url,
                'name': doc['name'],
            }
    )


# @celery_app.task(base=DBTask)
# def process_nach_register_feed_out():
#     """
#     Do NACH registration for all the customers who have uploaded there
#     signature and have their Customer.nach_mandate_status as 'INITIATED'.
#     """
#     feedout_obj = FeedOutWorker(wdb_session)
#     file_name = feedout_obj.create_nach_registration_feed()
#     nach_worker_obj = NachRegistrationWorker(wdb_session)
#     nach_worker_obj.mail_to_billdesk(file_name)


@celery_app.task(base=DBTask, max_retries=3, priority=6,
                 queue='priority_queue')
def nach_register_feed_in(s3_key, filepath):
    StgNachRegisterFeedIn.insert_data(wdb_session, filepath)
    IntNachRegisterFeedIn.import_stg(wdb_session)
    IntNachRegisterFeedIn.update_customer_nach_status(wdb_session)
    # TO DO int table and update


@celery_app.task(bind=True, base=DBTask, priority=6,
                 queue='priority_queue', max_retries=3)
def generate_nach_trxn_feedout(self):
    """
    Generates feed out file of the transactions where trxn.mode = 'NACH'
    """
    try:
        nach_worker_obj = NachWorker(wdb_session)
        nach_worker_obj.insert_nach_transactions()
        nach_worker_obj.create_nach_feedout()
    except Exception as ex:
        LOG.error('Could not generate NACH transaction feed', exc_info=True)
        self.retry(exc=ex, countdown=60)


# @celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=6,
#                  queue='priority_queue')
# def create_karvy_xml():
#     """
#     creates karvy xml files and calls the upload_karvy_xml worker
#     """
#     feed_out_worker_obj = FeedOutWorker(wdb_session)
#     # karvy_feed_upload_obj = KarvyFeedUpload()
#     karvy_file_list = feed_out_worker_obj.create_karvy_xml()
#     group(upload_karvy_xml.s(file_info[0], file_info[1])
#           for file_info in karvy_file_list)()


@celery_app.task(base=DBTask, priority=7, queue='priority_queue')
def update_ckyc(customer_id):
    """
    Update CKYC ID for a given customer.
    """
    if not customer_id:
        raise ValueError(
            "Customer id is required but not provided for this task."
        )

    c = wdb_session.query(Customer).get(customer_id)
    _temp_ = Karvy.get_ckyc_id(c.pan_number)    # CKYC ID returned by get_ckyc_id may be None.  # NOQA

    if _temp_:
        try:
            c.ckyc_id = _temp_
            wdb_session.add(c)
            wdb_session.commit()
            LOG.info(
                "CKYC ID updated successfully for the customer with id {}.".
                format(customer_id)
            )

            # Removing the customer ids from the Redis whose CKYC ID has been found and updated in the Db.  # NOQA
            redis_client.srem(C.CUSTOMERS_MISSING_CKYC_ID, str(customer_id))

            return customer_id
        except:
            # An error might occur while updating the customer record.
            wdb_session.rollback()

    return None


@celery_app.task(base=DBTaskOnce, max_retries=2, once={'graceful': True},
                 priority=8, queue='priority_queue')
def fetch_missing_ckyc_ids():
    """
    This is a cron task, which is supposed to run once in every two days.
    For all the customer ids present in the CUSTOMERS_MISSING_CKYC_ID key
    of redis, this function launches a new task `update_ckyc` which in turn
    finds the CKYC ID for each customer against their PAN and
    updates it in the database.

    :return: None
    """
    customers_missing_ckyc_id = set(redis_client.smembers(C.CUSTOMERS_MISSING_CKYC_ID))
    for id in customers_missing_ckyc_id:
        update_ckyc.delay(id.decode('ascii'))


@celery_app.task(base=DBTaskOnce, max_retries=2, once={'graceful': True}, priority=9, queue='priority_queue')
def all_customers_with_missing_ckycids():
    """
    This task is called during the API Startup - a bootstrap task. Finds all the customers who
    have been ckyc-ed through Sqrrl, have a valid PAN, but their ckyc_id is missing.
    For all such customers, their id (not uid) is stored in the Redis.

    :return: None
    """

    LOG.info("Querying the DB for Customers whose ckyc_id is missing, but have been ckyc-ed through Sqrrl Platform.")

    # The wrong way to Query the Db in Sqlalchemy
    # target_customers = wdb_session.query(Customer).filter(Customer.is_kyc_verified == True,
    #                                                       Customer.kyc_transaction_id is not None,
    #                                                       Customer.pan_number is not None,
    #                                                       Customer.ckyc_id is None
    #                                                       ).all()

    target_customers = wdb_session.query(Customer).filter(Customer.is_kyc_verified == True,
                                            Customer.kyc_transaction_id.isnot(None),
                                            Customer.pan_number.isnot(None),
                                            Customer.ckyc_id.is_(None)).all()

    LOG.info("{} customers found with their CKYC Id missing but have already "
             "been kyc verified. Populating their ids into redis."
             .format(len(target_customers)))

    exact_customer_ids = [customer.id for customer in target_customers]

    # Adding those selected customer ids into Redis against the key CUSTOMERS_MISSING_CKYC_ID
    redis_client.sadd(C.CUSTOMERS_MISSING_CKYC_ID, *exact_customer_ids)


@celery_app.task(base=DBTask, priority=2, queue='priority_queue')
def communications_mail(mail_type, recipient_mail_id, sender_mail_id = None,
                        cc = None, bcc = None, subject = None, tag = None, attachments = None, ** kwargs):

    if mail_type and recipient_mail_id:
        return email.send_template_mail(mail_type=mail_type,
                                        recipient=recipient_mail_id,
                                        sender=sender_mail_id,
                                        cc=cc,
                                        bcc=bcc,
                                        subject=subject,
                                        tag=tag,
                                        attachments=attachments,
                                        **kwargs)
    return False


@celery_app.task(base=DBTask, max_retries=3, priority=2, bind=True,
                 queue='priority_queue')
def communications_sms(self, sms_type, *mobile_numbers, **message_params):

    response_code = send_bulk_transaction_sms(sms_type,
                                              *mobile_numbers,
                                              **message_params)

    if self.request.retries == self.max_retries:
        LOG.error("Maximum retries reached. Cannot send {} sms to {} phone "
                  "numbers.".format(sms_type, len(mobile_numbers)),
                  exc_info=True,
                  extra={
                      mobile_numbers: list(mobile_numbers)
                  })

    elif response_code != 200:
        # Countdown - 60, 120, 180 seconds
        LOG.debug("Retrying to send {} sms".format(sms_type))
        self.retry(countdown=60*(self.request.retries+1))


@celery_app.task(base=DBTask, max_retries=3,
                 priority=2, queue='priority_queue')
def communications_mobile_push(task_name, recipients, title, message_body,
                               product_id=None, tag_group=None, ** kwargs):

    # Compulsory conditions: recipients and title should not be None.
    if recipients and title and hasattr(recipients, '__iter__'):

        # Construct deeplinks for android and ios.
        dl_android = kwargs.get('deep_link_android', None)
        if not dl_android:
            dl_android = kwargs.get('deep_link', None)

        dl_ios = kwargs.get('deep_link_ios', None)
        if not dl_ios:
            dl_ios = kwargs.get('deep_link', None)

        return mobile_push.send_bulk_push_notifications(
            task_name=task_name, records=list(recipients), title=title,
            message_body=message_body, product_id=product_id,
            tag_group=tag_group, deep_link_android=dl_android,
            deep_link_ios=dl_ios, **kwargs)

    else:
        LOG.debug("Cannot send push notifications for task: {}, as recipients"
                  " list is empty, or title not provided.".format(task_name),
                  exc_info=True,
                  extra={
                      'recipients': recipients,
                      'task name': task_name,
                      'title': title})
        return False


@celery_app.task(base=DBTask, max_retries=3,
                 priority=2, queue='priority_queue')
def payment_reminders(channel=C.MOBILE_PUSH, product=C.LIQUID, days_away=0,
                      title='Payment Reminder!', message='Are you ready?', ** kwargs):

    product_record = Product.\
                    get_product_by_internal_name(session=wdb_session,
                                                 product_internal_name=product)
    product_id = product_record.id

    records = Investment.due_by_n_days_from_now(session=wdb_session,
                                                product_id=product_id,
                                                days=days_away)

    if len(records):
        LOG.info("{} customers are due for their {} instalment in {} days from"
                 " now".format(len(records), product_record.display_name,
                               days_away))

        if channel == C.MOBILE_PUSH:

            task_name = product_record.display_name
            if days_away > 0:
                task_name += ' Upcoming Payment Reminder who are due in {} ' \
                             'days'.format(days_away)
            elif days_away < 0:
                task_name += ' Missed Payment Reminder who are due since {} ' \
                             'days'.format(days_away)
            else:
                task_name += ' Payment Reminder who are due today.'

            communications_mobile_push.delay(
                task_name=task_name,
                recipients=records,
                title=title,
                message_body=message,
                product_id=product_id,
                tag_group=product_record.display_name,
                notification_type=NC.PAYMENT_REMINDER_DEEP_LINK)

        elif channel == C.EMAIL:
            pass

        elif channel == C.SMS:
            pass

        else:
            LOG.warn("Channel {} unrecognized.".format(channel))


@celery_app.task(base=DBTask, priority=9, queue='priority_queue')
def rmm_switch_fund():
    """
    Ad-hoc task for switching reliance fund.
    """
    # LOG.info('Running rmm_switch_fund')
    rsm = RelianceSwitchManager(wdb_session)
    rows = rsm.get_customers_for_switch()
    LOG.info('rmm_switch_fund with {} rows'.format(len(rows)))
    # call rmm_switch_customer in parallel for each rows
    subtasks = (
        rmm_switch_customer.s(
            r.customer_id, r.ip_address,
            int(r.params['from_scheme']),
            int(r.params['to_scheme'])
        ) for r in rows
    )
    result = group(subtasks).apply_async()
    LOG.info('Completed rmm_switch_fund, exiting')
    return result


@celery_app.task(base=DBTask, priority=9, queue='priority_queue')
def rmm_switch_customer(customer_id, ip_address, from_scheme, to_scheme):
    """
    Switches individual customer from Reliance Money Manager to Reliance
    Liquid Fund.
    """
    LOG.info('Switching fund for customer: {}, ip -> {}'.format(customer_id, ip_address))  # NOQA
    rsm = RelianceSwitchManager(wdb_session)
    success = rsm.switch_customer(
        customer_id, ip_address, from_scheme, to_scheme
    )
    LOG.info('rmm_switch_customer for customer {} -> {}'
             .format(customer_id, success))


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=5,
                 queue='priority_queue')
def create_nach_transactions(product_id):
    """
    creates transaction rows for customers with nach_mandate_status
    as SUCCESS.
    """
    obj = NachWorker(wdb_session)
    obj.create_nach_trxns(product_id)


@celery_app.task(base=DBTaskOnce, once={'graceful': True}, priority=1,
                 max_retries=2, queue='priority_queue')
def remote_uri_call(url, method='GET', payload=None, form_data=None, ignore_response=False, **kwargs):

    LOG.info("Received a request to hit {} over {}".format(url, method))

    if method == 'GET':
        pass

    elif method == 'POST':
        resp = None

        if payload:
            resp = requests.post(url=url,
                                 json=payload,
                                 **kwargs) if isinstance(payload, dict) \
                    else requests.post(url=url,
                                       data=payload,
                                       **kwargs)
        elif form_data:
            resp = requests.post(url=url,
                                 data=form_data,
                                 **kwargs)
        else:
            resp = requests.post(url=url, **kwargs)

        return None if ignore_response else resp


# @celery_app.task(base=DBTask, priority=2, ignore_result=True,
#                  max_retries=2, queue='priority_queue')
# def generate_monthly_account_statements():
#     # Steps:
#     # 1. Find start and end-date of the previous month.
#     # 2. Find all the successful transactions which happened in the last month
#     # 3. For all such transactions, find all the unique customers.
#     # 4. For all such customers, find all investments which have a folio.
#     # 5. Using pdf task, send the necessary data and generate pdf file.
#     # 6. Upload the successfully generated pdf files to s3 bucket.
#     # 7. Send emails, sending the generated account statements as attachment.
#
#     datetime_now = datetime.now()
#     current_year, current_month = datetime_now.year, datetime_now.month
#
#     if current_month == 1:
#         days_in_previous_month = monthrange(current_year-1, 12)[1]
#         start_date = datetime(year=current_year-1, month=12, day=1)
#     else:
#         days_in_previous_month = monthrange(current_year, current_month-1)[1]
#         start_date = datetime(year=current_year, month=current_month-1, day=1)
#
#     end_date = start_date + timedelta(days=days_in_previous_month-1,
#                                       hours=23, minutes=59, seconds=59)
#
#     # eta_for_mail_send = datetime(day=max(4, datetime_now.day),
#     #                              month=current_month,
#     #                              year=current_year,
#     #                              hour=7)
#     # LOG.debug("Account Statements Mail sending ETA: {} IST".format(
#     #     to_ist(eta_for_mail_send).strftime('%a %d %B | %I:%M %p')))
#
#     date_format = '%d %B, %Y'
#     LOG.info("Generating account statements for the period : "
#              "{} to {}".format(start_date.strftime(date_format),
#                                end_date.strftime(date_format)))
#
#     from app.model import Transaction
#     all_transactions = wdb_session.query(Transaction)\
#         .filter(Transaction.status == C.TR_STATUS_SUCCESS)\
#         .filter(Transaction.created_on.between(start_date, end_date))\
#         .order_by(Transaction.created_on.asc())\
#         .all()
#
#     all_customers = sorted(list(set([transaction.customer
#                                      for transaction in all_transactions])),
#                            key=lambda customer: customer.id)
#
#     statement_month = start_date.strftime('%B %Y')
#     LOG.info("{} customers did {} successful transactions in the last month "
#              "of {}".format(len(all_customers),
#                             len({i.group_id: None for i in all_transactions}),
#                             statement_month))
#
#     all_investments = \
#         Investment.get_investment_rows_by_customer_ids(
#             wdb_session, *[customer.id for customer in all_customers])
#
#     # print(all_investments[0].__dict__)
#
#     # Filter all those investments which have a folio,
#     # that means those investment rows where transactions have been made.
#     all_investments = [i for i in all_investments if i.scheme_folio]
#
#     statement_period = start_date.strftime(date_format) + " - " + end_date.strftime(date_format)
#     statement_date = to_ist(datetime.now()).strftime(date_format)
#
#     _key_ = lambda trx: trx.investment_id
#     grouped_transactions = \
#         dict((k, list(g)) for k, g in
#              groupby(sorted(all_transactions, key=_key_), _key_))
#
#     _key_ = lambda inv: inv.group_id
#
#     date_format = '%a %d %B | %I:%M %p'
#
#     # output_pdf_directory = \
#     #     os.path.join(PDF_DIRECTORY_PATH, 'statements', str(start_date.year),
#     #                  start_date.strftime('%B'))
#
#     # Create a temporary folder
#     output_pdf_directory = tempfile.mkdtemp(prefix='_'.
#                                             join(['statements',
#                                                   str(start_date.year),
#                                                   start_date.strftime('%B_')]))
#
#     s3_key_prefix = '/'.join(['statements', str(start_date.year),
#                               start_date.strftime('%B')])
#
#     for customer in all_customers:
#         statement_params = {
#             "statement_period": statement_period,
#             "statement_date": statement_date,
#             "customer_name": customer.pan_name,
#             "customer_email": customer.email,
#             "customer_phone_number": customer.mobile,
#             "customer_pan_number": customer.pan_number,
#         }
#         current_customer_investments = [inv for inv in all_investments
#                                         if inv.customer_id == customer.id]
#         products = []
#
#         # Groupby needs the data to be sorted
#         grouped_investments = groupby(
#             sorted(current_customer_investments, key=_key_), _key_)
#
#         current_customer_transactions = []
#         for _, group_item in grouped_investments:
#             group_inv = list(group_item)
#             products.append(
#                 {
#                     "product_name": group_inv[0].product.display_name,
#                     "description": group_inv[0].description,
#                     "schemes": [
#                         {
#                             "name": i.scheme.name,
#                             "allocation_percent": float(str(i.allocation_percent)),
#                             "units": float(str(i.scheme_units)),
#                             "amount_invested":  str(max(i.net_investment, 0.0)),
#                             "amount_withdrawn": str(i.total_withdrawal),
#                             "current_value": str(i.scheme_units * i.scheme_nav),
#                             "folio_number": i.scheme_folio,
#                             "investment_id": i.id,
#                         }
#                         for i in group_inv
#                     ]
#                 })
#
#             product_wise = [s for s in products[-1]["schemes"]]
#             if product_wise[0]["investment_id"] not in grouped_transactions:
#                 # Not transaction occurred under this investment
#                 pass
#             else:
#                 for scheme_item in product_wise:
#                     current_customer_transactions.append(
#                         {
#                             "product_name": products[-1]["product_name"],
#                             "description": products[-1]["description"],
#                             "folio_number": scheme_item["folio_number"],
#                             "scheme": scheme_item["name"],
#                             "transactions": [
#                                 {
#                                     "date_time": to_ist(trx.created_on).strftime(date_format),
#                                     "type":
#                                         'Switch' if 'SWITCH' in trx.transaction_type
#                                         else trx.transaction_type.title(),
#                                     "amount": str(trx.amount),
#                                     "nav": str(trx.amount/trx.units),
#                                     "units_involved": str(trx.units),
#                                     "balance_units": "???",
#                                 }
#                                 for trx in grouped_transactions[scheme_item["investment_id"]]
#                                 if (trx.units > 0.0)
#                             ]
#                         })
#
#         statement_params["summary"] = products
#         statement_params["transactions"] = current_customer_transactions
#
#         (pdf_creator.s(template_name='statement_template.html',
#                         pdf_file_name=customer.id,
#                         pdf_directory=os.path.join(output_pdf_directory,
#                                                     str(customer.id)),
#                         template_params=statement_params,
#                         upload_to_s3=True,
#                         s3_key_prefix=s3_key_prefix) |
#          email_monthly_account_statements.subtask(
#              # eta=eta_for_mail_send,
#             countdown=1200,
#             kwargs={
#                 "customer_name": customer.pan_name,
#                 "email_id": customer.email,
#                 "month": statement_month
#             }
#         )).apply_async()
#
#     # Delete the temporary directory from the machine after 2 days.
#     delete_temporary_directory.apply_async(
#         countdown=172800,
#         kwargs={
#             "temp_folder_path": output_pdf_directory
#     })
#
#
# @celery_app.task(base=DBTask, priority=2, ignore_result=False, max_retries=2,
#                  queue='priority_queue', rate_limit='10/m')
# def pdf_creator(template_name, pdf_directory=None, pdf_file_name=None,
#                 template_params={}, upload_to_s3=False, s3_key_prefix=None,
#                 **kwargs):
#     output_pdf_file_path = statements_pdf_maker.generate_pdf(template_name,
#                                                              pdf_directory,
#                                                              pdf_file_name,
#                                                              template_params,
#                                                              **kwargs)
#
#     if file_exists(output_pdf_file_path):
#         pdf_file_name = os.path.basename(output_pdf_file_path)
#         LOG.debug("{} created at {} using template: {}".
#                   format(pdf_file_name, output_pdf_file_path, template_name))
#
#         if upload_to_s3:
#             key_name = '/'.join([s3_key_prefix, pdf_file_name]) \
#                 if s3_key_prefix else pdf_file_name
#
#             # Upload the statement file to S3.
#             s3_file_upload(key_name, output_pdf_file_path)
#
#             return output_pdf_file_path, key_name
#
#     else:
#         LOG.warn("Failed to create the pdf file: {} using template: {}"
#                  .format(pdf_file_name, template_name))
#
#     return output_pdf_file_path, None
#
#
# @celery_app.task(base=DBTask, priority=2, ignore_result=True, max_retries=2,
#                  queue='priority_queue')
# def email_monthly_account_statements(statement_file_path, customer_name,
#                                      email_id, ** kwargs):
#     LOG.debug("Sending - {} to {} at {}".format(statement_file_path,
#                                                 customer_name, email_id))
#
#     statement_file_local_path, s3_key = statement_file_path
#     statement_dir = \
#         os.path.dirname(os.path.abspath(statement_file_local_path))
#
#     statement_file_local_path=os.path.join(statement_dir,
#                                            'Sqrrl Statement-June.pdf')
#     print(statement_file_local_path)
#
#     try:
#         # Download the pdf file from S3
#         if s3_key:
#             s3_file_download(s3_key, statement_file_local_path)
#             LOG.debug("Downloaded object at {} to {}".
#                       format(s3_key, statement_file_local_path))
#
#         if os.path.isabs(statement_file_local_path) and file_exists(statement_file_local_path):
#             email.send_template_mail(
#                 mail_type=app.communications.email_constants.TYPE_4,
#                 recipient=email_id,
#                 attachments=[statement_file_local_path],
#                 customer_name=customer_name.title(),
#                 **kwargs)
#
#     except Exception as exception:
#         LOG.error("An error occurred while downloading the statement file({}) "
#                   "from S3 [OR] while sending the mail to {}. "
#                   "The exception is: {}".format(s3_key, email_id, exception),
#                   exc_info=True)


@celery_app.task(base=DBTask, priority=8, ignore_result=True, max_retries=2,
                 queue='priority_queue')
def delete_temporary_directory(temp_folder_path):
    if dir_exists(temp_folder_path):
        try:
            os.removedirs(temp_folder_path)
        except Exception as e:
            LOG.warn("Could not delete the directory: {}".
                     format(temp_folder_path),
                     exc_info=True)


@celery_app.task(base=DBTaskOnce, max_retries=3, once={'graceful': True},
                 priority=4, queue='priority_queue')
def update_pan_data_for_all_customers():
    pan_data_missing_customers = \
        wdb_session.query(Customer).filter(
            and_(Customer.is_kyc_verified,
                 Customer.kra_name.isnot(None),
                 or_(Customer.permanent_city.is_(None),
                     Customer.correspondence_city.is_(None)))).all()

    result = {'is_kyc_verified': True}
    LOG.info("Customers who are KYC verified but missing PAN data - {}".
             format(len(pan_data_missing_customers)))

    for c in pan_data_missing_customers:
        fetch_pan_data.delay(result, c.uid)


@celery_app.task(base=DBTask, priority=5, ignore_result=True, max_retries=2,
                 queue='priority_queue')
def update_outdated_next_investment_on(product=C.LIQUID):

    product_row = Product.\
        get_product_by_internal_name(session=wdb_session,
                                     product_internal_name = product)
    product_id = product_row.id

    today = datetime.utcnow().date()

    records = wdb_session.query(Investment).\
        filter(and_(Investment.product_id == product_id,
                    Investment.scheme_units > 0,
                    Investment.instalment_period == 'weekly',
                    func.date_trunc('day', Investment.next_investment_on) < today)).all()

    if len(records):
        LOG.info("{} {} customers didn't pay their weekly instalment this "
                 "week, updating their next_investment_on date".
                 format(len(records), product_row.display_name))
        for investment_record in records:
            try:
                investment_record.update_next_investment_on()
            except Exception as e:
                LOG.warn("Could not update next_investment_on for "
                         "customer(uid={}) due to the following exception: {}".
                         format(investment_record.customer.uid, e), exc_info=1)


@celery_app.task(base=DBTaskOnce, priority=2, ignore_result=True,
                 soft_time_limit=3600, rate_limit='10/m',
                 max_retries=2, queue='priority_queue')
def generate_and_send_statements(pdf_directory, pdf_file_name, template_params,
                                 customer_name, customer_email, customer_id,
                                 template_name='statement_template.html',
                                 s3_key_prefix='statements',
                                 **kwargs):

    # Create the statement pdf file using PdfMaker
    statement_pdf_file_path = \
        statements_pdf_maker.create_html_file(template_name,
                                              pdf_directory=pdf_directory,
                                              pdf_file_name=pdf_file_name,
                                              template_params=template_params)

    # Check if the file has been successfully created or not
    if statement_pdf_file_path and file_exists(statement_pdf_file_path):
        LOG.debug("Statement file successfully created for {}({}) at {}".
                  format(customer_name, customer_id, statement_pdf_file_path))

        # If Upload to S3 is set True, then upload the statement file to S3
        # for historical records
        if C.STATEMENTS_UPLOAD_TO_S3:
            pdf_file_name = os.path.basename(statement_pdf_file_path)
            key_name = '{}/{}'.format(s3_key_prefix, pdf_file_name) \
                if s3_key_prefix else pdf_file_name

            # Upload the statement file to S3.
            s3_file_upload(key_name, statement_pdf_file_path)

        # TODO: Rename the statements file
        month = None
        if 'statement_month' in kwargs:
            month = kwargs['statement_month']
        elif 'month' in kwargs:
            month = kwargs['month']

        statement_file_new_name = "Sqrrl Statement - {}.pdf".\
            format(month if month else customer_name.title())

        src = statement_pdf_file_path
        dst = os.path.join(pdf_directory, statement_file_new_name)
        try:
            os.rename(src, dst)
        except (OSError, IOError):
            shutil.copy2(src, dst)

        statement_pdf_file_path = dst

        # Send the mail
        email.send_template_mail(
            mail_type=app.communications.email_constants.TYPE_4,
            recipient=customer_email,
            attachments=[statement_pdf_file_path],
            customer_name=customer_name.title(),
            month=month,
            **kwargs)


@celery_app.task(base=DBTask, priority=2, ignore_result=True,
                 soft_time_limit=3600, max_retries=2, queue='priority_queue')
def trigger_monthly_account_statements_generation():
    from app.utils import StatementGenerator
    StatementGenerator(wdb_session).start_processing()
