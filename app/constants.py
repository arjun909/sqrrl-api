# constants.py
from enum import Enum

from app.config import MAILCHIMP_BASE_URL, MAILCHIMP_SQRRL_CUSTOMERS_LIST_ID

WEEKLY = 'weekly'
MONTHLY = 'monthly'
PRINCIPAL = 'principal'
RETURNS = 'returns'
INVEST = 'INVEST'
REDEEM = 'REDEEM'
SWITCH_OUT = 'SWITCH_OUT'
SWITCH_IN = 'SWITCH_IN'

# constants for product internal names.
LIQUID = 'liquid'
TAX = 'tax_saving'
GOAL = 'goal_based'

# constants for some funds.
RELIANCE_LIQUID_FUND_ISIN = 'INF204K01UN9'
AXIS_LONG_TERM_FUND_ISIN = 'INF846K01131'
ICICI_LONG_TERM_FUND_ISIN = 'INF109K01464'


# transaction status
TR_STATUS_INITIATED = 'INITIATED'
# PENDING means those NACH transactions that need confirmation from billdesk
# before being forwarded to RTA.
# TR_STATUS_QUEUED = 'QUEUED'
TR_STATUS_PENDING = 'PENDING'
# PROCESSING means transaction has happened, but now needs a verification from
# RTA to be called a SUCCESS or FAILURE.
TR_STATUS_PROCESSING = 'PROCESSING'
TR_STATUS_SUCCESS = 'SUCCESS'
TR_STATUS_FAILURE = 'FAILURE'
# TR_RMM_PENDING = 'RMM_PENDING'

# nach statuses
NACH_REGISTRATION_INITIATED = 'INITIATED'
NACH_REGISTRATION_SIGNED = 'SIGNED'
NACH_REGISTRATION_PROCESSING = 'PROCESSING'
NACH_REGISTRATION_SUCCESS = 'SUCCESS'
NACH_REGISTRATION_FAILURE = 'FAILURE'

# some redis keys.
RDB_KARVY_ENC_PASS = 'RDB_KARVY_ENC_PASS'

# riskometer - index 1 to 5 map to these values.
RISKOMETER = [None, 'Low', 'Moderately Low', 'Moderate',
              'Moderately High', 'High']

# FATCA / PAN code-values
OCCUPATION = {
    '01': 'Business',
    '02': 'Service',
    '03': 'Professional',
    '04': 'Agriculturist',
    '05': 'Retired',
    '06': 'Housewife',
    '07': 'Student',
    '08': 'Others',
    '09': 'Doctor',
    '41': 'Private Sector Service',
    '42': 'Public Sector Service',
    '43': 'Forex Dealer',
    '44': 'Government Service',
    '99': 'Unknown / Not Applicable'
}
KARVY_OCCUPATION = {
    'Service': '01',
    'Business': '02',
    'Student': '03',
    'Housewife': '04',
    'Professional': '05',
    'Agriculturist': '06',
    'Retired': '07',
    'Others': '08',
}

INCOME_GROUP = {
    '31': 'Below 1 Lakh',
    '32': '> 1 <=5 Lacs',
    '33': '>5 <=10 Lacs',
    '34': '>10 <= 25 Lacs',
    '35': '> 25 Lacs < = 1 Crore',
    '36': 'Above 1 Crore'
}

SOURCE_OF_WEALTH = {
    '01': 'Salary',
    '02': 'Business Income',
    '03': 'Gift',
    '04': 'Ancestral Property',
    '05': 'Rental Income',
    '06': 'Prize Money',
    '07': 'Royalty',
    '08': 'Others'
}

# Income tax database search string
NOT_IN_ITD = 'Not in ITD Database'
ITD_VALID = 'Existing and Valid'

# Billdesk constants
BILLDESK_AUTH_SUCCESS = '0300'
BILLDESK_AUTH_ERR = '0399'
BILLDESK_AUTH_INVALID_INPUT = 'NA'
BILLDESK_AUTH_ERR_PENDING = '0002'
BILLDESK_AUTH_ERR_GENERIC = '0001'
BILLDESK_CODE_DEBIT = 'HCC'
BILLDESK_CODE_RUPAY = 'UR2'

# user settings
SETTINGS_K_REMIND_DAYS = 'remind_days'
SETTINGS_K_SUMMARY_DAY = 'summary_day'
SETTINGS_K_TIPS_ENABLED = 'tips_enabled'
SETTINGS_K_PUSH_ENABLED = 'push_enabled'
SETTINGS_K_PAYMENT_PREF = 'payment_preference'

# Payment modes
PAYMENT_MODE_DEBIT = 'D'
PAYMENT_MODE_NB = 'N'
PAYMENT_MODE_RUPAY = 'R'

# Redis key for blacklisted tokens
BLACKLIST = 'blklst'
# redis key for tips
TIPS_CACHE = 'cached:tips'

# fallback dummy MICR
FALLBACK_MICR = '110110110'

# all KRAs
ALL_KRA = ('CAMS', 'CVL', 'DOTEX', 'KARVY', 'NDML')

# For redis locks
REDIS_BD_LOCK_PREFIX = 'sql-bd-lock-'

# Redis key prefix for celery async task of create_zbf()
REDIS_ZBF_TASK_ID_PREFIX = 'zbf-job-id-'

# Redis mis utr prefix
REDIS_UTR_PVN_PREFIX = 'MIS_UTR_'

# Redis key prefix for cams feed out files
REDIS_CAMS_FEEDFILE_PREFIX = 'cams-file'

# For feed
KARVY = 'Karvy'
CAMS = 'CAMS'
FT = 'FT'

# Others
OTHERS = 'Others'
# FOR SLACKER API
SLACK_OPS_CHANNEL = '#ops-tech'

# For transaction sms.
SMS_SUCCESS_TEMPLATE = ('INR {amount} successfully {transaction_type} '
                        '{product} via ref: {group_id}. '
                        'It will be processed within one business day.')
SMS_FAILURE_TEMPLATE = ('Your transaction of INR {amount} with Sqrrl was not '
                        'confirmed by your bank. If any amount was debited, '
                        'your bank will refund it within 1-2 days.')
TR_NOTIFIED_CUSTOMER = 'NOTIFIED_CUSTOMER'
TR_BILLDESK_STATUS = 'BILLDESK_STATUS'

PAID = 'PAID'
PROCESSED = 'Processed'
REJECTED = 'REJECTED'
KARVY_REJECTED = 'KARVY_REJECTION'
ACKNOWLEDGED = 'ACKNOWLEDGED'

NACH = 'NACH'
# For FT feed, this is starting id
FT_MIN_TR_ID = 200000


CUSTOMERS_MISSING_CKYC_ID = "CUSTOMERS_MISSING_CKYC_ID"


class Channel(Enum):
    MOBILE_PUSH = 1
    EMAIL = 2
    SMS = 3


MOBILE_PUSH = 'mobile'
EMAIL = 'email'
SMS = 'sms'

MAILCHIMP_ADD_NEW_MEMBER_URL = \
    '/'.join([MAILCHIMP_BASE_URL, 'lists',
             MAILCHIMP_SQRRL_CUSTOMERS_LIST_ID, 'members'])


# admin actions
class AdminAction(Enum):
    UPDATE_NAV = 'update_nav'
    REFRESH_PAN_DATA = 'refresh_pan_data'
    UPDATE_BANK = 'update_bank'
