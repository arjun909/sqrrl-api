# json_translator.py
# adapted from falcon quickstart
import simplejson as json
import falcon
from app.log import get_logger
LOG = get_logger()


class JSONTranslator(object):

    def process_request(self, req, resp):
        # req.stream corresponds to the WSGI wsgi.input environ variable,
        # and allows you to read bytes from the request body.
        #
        # See also: PEP 3333
        if (req.content_length in (None, 0)
                or 'application/json' not in req.content_type):
            # Nothing to do
            # LOG.warn('content_length: {}; content_type {}'.
            #          format(req.content_length, req.content_type))
            return

        body = req.stream.read()
        # LOG.debug('body - {}'.format(body))
        if not body:
            raise falcon.HTTPBadRequest('Empty request body',
                                        'A valid JSON document is required.')

        try:
            req.context['doc'] = json.loads(body.decode('utf-8'))

        except (ValueError, UnicodeDecodeError):
            raise falcon.HTTPError(falcon.HTTP_753,
                                   'Malformed JSON',
                                   'Could not decode the request body. The '
                                   'JSON was incorrect or not encoded as '
                                   'UTF-8.')

    def process_response(self, req, resp, resource):
        if 'result' not in req.context:
            return

        # convert date/datetime objects to isoformat
        resp.body = json.dumps(req.context['result'],
                               default=lambda d: d.isoformat())
