# auth_middleware.py
# Checks if the request has valid auth headers
import falcon
from jose import jwt
import redis
from app.config import JWT_SALT, REDIS_URL, API_KEYS, DEBUG, ADMIN_KEYS
from app.constants import BLACKLIST
from app.model import Customer
from app import log
LOG = log.get_logger()

# no need to check auth for these paths.
ALLOWED_PATHS = [
    r'',
    r'/',
    r'/favicon.ico',
    r'/v1/auth',
    r'/v1/tips',
    r'/v1/insights/f',
    r'/callback/billdesk',
    r'/callback/ekyc',
    r'/callback/ekyc/success',
    r'/callback/ekyc/failure',
    r'/callback/postmark',
    r'/callback/zapier',
    r'/callback/karvy_rejection',
    r'/callback/email_verify',
    r'/callback/nach_trxn_feedin',
    r'/callback/nach_register_feedin',
    r'/r',
    r'/u/switch',
    r'/billdesk/mock/pgimerchantrequesthandler',
    r'/fake/reliance/redeem',
    r'/callback/clevertap_webhook'
]


class AuthMiddleware(object):
    """
    Make sure the auth tokens are valid, else throw 401 error.
    """
    def process_request(self, req, resp):
        # DEBUG: God-mode. Allows us to pretend to be another user
        # by placing the customer id in header.
        if DEBUG:
            debug_customer_id = req.get_header('x-debug-id')
            if debug_customer_id:
                session = req.context['session']
                c = session.query(Customer).get(debug_customer_id)
                req.context['customer_id'] = debug_customer_id
                req.context['customer_uid'] = c.uid
                LOG.warn('DEBUG: Using API as {}'.format(c.name))
                # skip further token checks.
                return

        # check user token.
        if (req.path.lower() not in ALLOWED_PATHS or
           (req.path.lower() == r'/v1/auth' and req.method == 'DELETE')):
            # check if header has the allowed API key.
            api_key = req.get_header('API-KEY', required=True)
            if api_key and api_key not in API_KEYS:
                LOG.warn('Invalid API key')
                raise falcon.HTTPUnauthorized(title='Authentication required',
                                              description='Invalid API key',
                                              challenges=[])
            # Authorization: Bearer token123
            token = req.get_header('Authorization')
            try:
                # argh - framework should do this. :-/
                if token:
                    token = token.split('Bearer ')[1]
                if token is None:
                    msg = 'Token is invalid'
                    LOG.warn(msg)
                    raise IndexError(msg)
            except IndexError:
                # bad request, must have token.
                LOG.warn('Auth token required but missing')
                description = ('Please provide an auth token '
                               'as part of the request.')
                raise falcon.HTTPUnauthorized('Auth token required',
                                              description,
                                              challenges=[])

            # for admin, check the token against admin keys
            is_admin = '/v1/admin' in req.path.lower()
            if is_admin:
                if token not in ADMIN_KEYS:
                    desc = 'Admin action needs special tokens'
                    raise falcon.HTTPUnauthorized('Invalid token', desc, [])
                else:
                    LOG.debug('Entering admin mode')
                    return
            # validate token
            if not self._validate_jwt(token, req):
                LOG.warn('Invalid token')
                description = ('The provided auth token is not valid. '
                               'Please request a new token and try again.')
                raise falcon.HTTPUnauthorized('Authentication required',
                                              description,
                                              challenges=[])

    def _validate_jwt(self, token, req):
        """
        Validates token. If token is valid, loads current user id in the
        request context
        """
        # check if token is blacklisted.
        rdb = redis.from_url(REDIS_URL)
        if rdb.sismember(BLACKLIST, token):
            # LOG.warn('User has already logged out')
            return False

        # decode jwt
        try:
            payload = jwt.decode(token, JWT_SALT, algorithms=['HS256'])
            uid = payload['user']
            c = req.context['session'].query(Customer) \
                .filter(Customer.uid == uid).first()
            # LOG.debug('c {}'.format(c))
            # no such user - token is invalid
            if not c or not c.is_active:
                return False
            else:
                # load current user's uid, id in the req context,
                # to be available downstream
                req.context['customer_uid'] = uid
                req.context['customer_id'] = c.id
                return True
        except Exception as ex:
            LOG.error(ex)
            return False
        # should never come here, just in case
        return False
