import sqlalchemy.orm.scoping as scoping
from sqlalchemy.exc import SQLAlchemyError
from falcon import HTTPInternalServerError


class DatabaseSessionManager(object):
    def __init__(self, db_session):
        self._session_factory = db_session
        self._scoped = isinstance(db_session, scoping.ScopedSession)

    def process_request(self, req, res, resource=None):
        """
        Handle post-processing of the response (after routing).
        """
        req.context['session'] = self._session_factory()

    def process_response(self, req, res, resource=None):
        """
        Handle post-processing of the response (after routing).
        """
        session = req.context['session']

        # auto-commit changes
        try:
            session.commit()
        except SQLAlchemyError as ex:
            session.rollback()
            raise HTTPInternalServerError('Database Error') from ex

        if self._scoped:
            session.rollback()
        else:
            session.close()
