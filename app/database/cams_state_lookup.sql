--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: cams_state_lookup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY cams_state_lookup (id, created_on, modified_on, code, value, amc_code, kyc_state_code) FROM stdin;
1	\N	\N	TE	Telangana	B	\N
2	\N	\N	AP	Andhra Pradesh	B	028
3	\N	\N	KE	Kerala	B	032
4	\N	\N	KA	Karnataka	B	029
5	\N	\N	GO	Goa	B	030
6	\N	\N	WB	West Bengal	B	019
7	\N	\N	UP	Uttar Pradesh	B	009
8	\N	\N	TN	Tamil Nadu	B	033
9	\N	\N	MP	Madhya Pradesh	B	023
10	\N	\N	GU	Gujarat	B	024
11	\N	\N	ND	Delhi	B	007
12	\N	\N	HP	Himachal Pradesh	B	002
13	\N	\N	MA	Maharashtra	B	027
14	\N	\N	PO	Puducherry	B	034
15	\N	\N	KR	Jammu and Kashmir	B	001
16	\N	\N	SI	Sikkim	B	011
17	\N	\N	AS	Assam	B	013
18	\N	\N	MN	Manipur	B	014
19	\N	\N	ME	Meghalaya	B	017
20	\N	\N	RA	Rajasthan	B	008
21	\N	\N	PU	Punjab	B	003
22	\N	\N	MI	Mizoram	B	015
23	\N	\N	AN	Andaman and Nicobar Islands	B	035
24	\N	\N	NA	Nagaland	B	018
25	\N	\N	TR	Tripura	B	016
26	\N	\N	AR	Arunachal Pradesh	B	012
27	\N	\N	BH	Bihar	B	010
28	\N	\N	HA	Haryana	B	006
29	\N	\N	OT	Others	B	099
30	\N	\N	CH	Chandigarh	B	004
31	\N	\N	JD	Jharkhand	B	020
32	\N	\N	UR	Uttarakhand	B	005
33	\N	\N	CG	Chhattisgarh	B	022
34	\N	\N	DN	Dadra and Nagar Haveli	B	026
35	\N	\N	OD	ODISHA	B	021
36	\N	\N	DD	Daman and Diu	B	025
37	\N	\N	LD	Lakshadweep	B	031
38	\N	\N	AP	Andhra Pradesh	D	028
39	\N	\N	KE	Kerala	D	032
40	\N	\N	KA	Karnataka	D	029
41	\N	\N	GO	Goa	D	030
42	\N	\N	WB	West Bengal	D	019
43	\N	\N	UP	Uttar Pradesh	D	009
44	\N	\N	TN	Tamil Nadu	D	033
45	\N	\N	MP	Madhya Pradesh	D	023
46	\N	\N	GU	Gujarat	D	024
47	\N	\N	ND	Delhi	D	007
48	\N	\N	HP	Himachal Pradesh	D	002
49	\N	\N	MA	Maharashtra	D	027
50	\N	\N	PO	Puducherry	D	034
51	\N	\N	KR	Jammu and Kashmir	D	001
52	\N	\N	SI	Sikkim	D	011
53	\N	\N	AS	Assam	D	013
54	\N	\N	MN	Manipur	D	014
55	\N	\N	ME	Meghalaya	D	017
56	\N	\N	RA	Rajasthan	D	008
57	\N	\N	PU	Punjab	D	003
58	\N	\N	MI	Mizoram	D	015
59	\N	\N	AN	Andaman and Nicobar Islands	D	035
60	\N	\N	NA	Nagaland	D	018
61	\N	\N	TR	Tripura	D	016
62	\N	\N	AR	Arunachal Pradesh	D	012
63	\N	\N	BH	Bihar	D	010
64	\N	\N	HA	Haryana	D	006
65	\N	\N	OT	Others	D	099
66	\N	\N	CH	Chandigarh	D	004
67	\N	\N	JD	Jharkhand	D	020
68	\N	\N	UR	Uttarakhand	D	005
69	\N	\N	CG	Chhattisgarh	D	022
70	\N	\N	DN	Dadra and Nagar Haveli	D	026
71	\N	\N	OD	ODISHA	D	021
72	\N	\N	DD	Daman and Diu	D	025
73	\N	\N	LD	Lakshadweep	D	031
74	\N	\N	TE	Telangana	D	\N
75	\N	\N	TE	Telangana	H	\N
76	\N	\N	AP	Andhra Pradesh	H	028
77	\N	\N	KE	Kerala	H	032
78	\N	\N	KA	Karnataka	H	029
79	\N	\N	GO	Goa	H	030
80	\N	\N	WB	West Bengal	H	019
81	\N	\N	UP	Uttar Pradesh	H	009
82	\N	\N	TN	Tamil Nadu	H	033
83	\N	\N	MP	Madhya Pradesh	H	023
84	\N	\N	GU	Gujarat	H	024
85	\N	\N	ND	Delhi	H	007
86	\N	\N	HP	Himachal Pradesh	H	002
87	\N	\N	MA	Maharashtra	H	027
88	\N	\N	PO	Puducherry	H	034
89	\N	\N	KR	Jammu and Kashmir	H	001
90	\N	\N	SI	Sikkim	H	011
91	\N	\N	AS	Assam	H	013
92	\N	\N	MN	Manipur	H	014
93	\N	\N	ME	Meghalaya	H	017
94	\N	\N	RA	Rajasthan	H	008
95	\N	\N	PU	Punjab	H	003
96	\N	\N	MI	Mizoram	H	015
97	\N	\N	AN	Andaman and Nicobar Islands	H	035
98	\N	\N	NA	Nagaland	H	018
99	\N	\N	TR	Tripura	H	016
100	\N	\N	AR	Arunachal Pradesh	H	012
101	\N	\N	BH	Bihar	H	010
102	\N	\N	HA	Haryana	H	006
103	\N	\N	OT	Others	H	099
104	\N	\N	CH	Chandigarh	H	004
105	\N	\N	JD	Jharkhand	H	020
106	\N	\N	UR	Uttarakhand	H	005
107	\N	\N	CG	Chhattisgarh	H	022
108	\N	\N	DN	Dadra and Nagar Haveli	H	026
109	\N	\N	OD	ODISHA	H	021
110	\N	\N	DD	Daman and Diu	H	025
111	\N	\N	LD	Lakshadweep	H	031
112	\N	\N	TE	Telangana	P	\N
113	\N	\N	AP	Andhra Pradesh	P	028
114	\N	\N	KE	Kerala	P	032
115	\N	\N	KA	Karnataka	P	029
116	\N	\N	GO	Goa	P	030
117	\N	\N	WB	West Bengal	P	019
118	\N	\N	UP	Uttar Pradesh	P	009
119	\N	\N	TN	Tamil Nadu	P	033
120	\N	\N	MP	Madhya Pradesh	P	023
121	\N	\N	GU	Gujarat	P	024
122	\N	\N	ND	New Delhi	P	007
123	\N	\N	HP	Himachal Pradesh	P	002
124	\N	\N	MA	Maharashtra	P	027
125	\N	\N	PO	Pondicherry	P	034
126	\N	\N	KR	Jammu and Kashmir	P	001
127	\N	\N	SI	Sikkim	P	011
128	\N	\N	AS	Assam	P	013
129	\N	\N	MN	Manipur	P	014
130	\N	\N	ME	Meghalaya	P	017
131	\N	\N	RA	Rajasthan	P	008
132	\N	\N	PU	Punjab	P	003
133	\N	\N	MI	Mizoram	P	015
134	\N	\N	AN	Andaman and Nicobar Islands	P	035
135	\N	\N	NA	Nagaland	P	018
136	\N	\N	TR	Tripura	P	016
137	\N	\N	AR	Arunachal Pradesh	P	012
138	\N	\N	BH	Bihar	P	010
139	\N	\N	HA	Haryana	P	006
140	\N	\N	OT	Others	P	099
141	\N	\N	CH	Chandigarh	P	004
142	\N	\N	JD	Jharkhand	P	020
143	\N	\N	UR	Uttarakhand	P	005
144	\N	\N	CG	Chhattisgarh	P	022
145	\N	\N	DN	Dadra and Nagar Haveli	P	026
146	\N	\N	OD	Orissa	P	021
147	\N	\N	DD	Daman and Diu	P	025
148	\N	\N	LD	Lakshadweep	P	031
149	\N	\N	TE	Telangana	G	\N
150	\N	\N	AP	Andhra Pradesh	G	028
151	\N	\N	KE	Kerala	G	032
152	\N	\N	KA	Karnataka	G	029
153	\N	\N	GO	Goa	G	030
154	\N	\N	WB	West Bengal	G	019
155	\N	\N	UP	Uttar Pradesh	G	009
156	\N	\N	TN	Tamil Nadu	G	033
157	\N	\N	MP	Madhya Pradesh	G	023
158	\N	\N	GU	Gujarat	G	024
159	\N	\N	ND	Delhi	G	007
160	\N	\N	HP	Himachal Pradesh	G	002
161	\N	\N	MA	Maharashtra	G	027
162	\N	\N	PO	Puducherry	G	034
163	\N	\N	KR	Jammu and Kashmir	G	001
164	\N	\N	SI	Sikkim	G	011
165	\N	\N	AS	Assam	G	013
166	\N	\N	MN	Manipur	G	014
167	\N	\N	ME	Meghalaya	G	017
168	\N	\N	RA	Rajasthan	G	008
169	\N	\N	PU	Punjab	G	003
170	\N	\N	MI	Mizoram	G	015
171	\N	\N	AN	Andaman and Nicobar Islands	G	035
172	\N	\N	NA	Nagaland	G	018
173	\N	\N	TR	Tripura	G	016
174	\N	\N	AR	Arunachal Pradesh	G	012
175	\N	\N	BH	Bihar	G	010
176	\N	\N	HA	Haryana	G	006
177	\N	\N	OT	Others	G	099
178	\N	\N	CH	Chandigarh	G	004
179	\N	\N	JD	Jharkhand	G	020
180	\N	\N	UR	Uttarakhand	G	005
181	\N	\N	CG	Chhattisgarh	G	022
182	\N	\N	DN	Dadra and Nagar Haveli	G	026
183	\N	\N	OD	ODISHA	G	021
184	\N	\N	DD	Daman and Diu	G	025
185	\N	\N	LD	Lakshadweep	G	031
186	\N	\N	AP	Andhra Pradesh	K	028
187	\N	\N	KE	Kerala	K	032
188	\N	\N	KA	Karnataka	K	029
189	\N	\N	GO	Goa	K	030
190	\N	\N	WB	West Bengal	K	019
191	\N	\N	UP	Uttar Pradesh	K	009
192	\N	\N	TN	Tamil Nadu	K	033
193	\N	\N	MP	Madhya Pradesh	K	023
194	\N	\N	GU	Gujarat	K	024
195	\N	\N	ND	Delhi	K	007
196	\N	\N	HP	Himachal Pradesh	K	002
197	\N	\N	MA	Maharashtra	K	027
198	\N	\N	PO	Puducherry	K	034
199	\N	\N	KR	Jammu and Kashmir	K	001
200	\N	\N	SI	Sikkim	K	011
201	\N	\N	AS	Assam	K	013
202	\N	\N	MN	Manipur	K	014
203	\N	\N	ME	Meghalaya	K	017
204	\N	\N	RA	Rajasthan	K	008
205	\N	\N	PU	Punjab	K	003
206	\N	\N	MI	Mizoram	K	015
207	\N	\N	AN	Andaman and Nicobar Islands	K	035
208	\N	\N	NA	Nagaland	K	018
209	\N	\N	TR	Tripura	K	016
210	\N	\N	AR	Arunachal Pradesh	K	012
211	\N	\N	BH	Bihar	K	010
212	\N	\N	HA	Haryana	K	006
213	\N	\N	OT	Others	K	099
214	\N	\N	CH	Chandigarh	K	004
215	\N	\N	JD	Jharkhand	K	020
216	\N	\N	UR	Uttarakhand	K	005
217	\N	\N	CG	Chhattisgarh	K	022
218	\N	\N	DN	Dadra and Nagar Haveli	K	026
219	\N	\N	OD	ODISHA	K	021
220	\N	\N	DD	Daman and Diu	K	025
221	\N	\N	LD	Lakshadweep	K	031
222	\N	\N	TE	Telangana	K	\N
223	\N	\N	AO	APO	F	\N
224	\N	\N	TE	Telangana	F	\N
225	\N	\N	AP	Andhra Pradesh	F	028
226	\N	\N	KE	Kerala	F	032
227	\N	\N	KA	Karnataka	F	029
228	\N	\N	GO	Goa	F	030
229	\N	\N	WB	West Bengal	F	019
230	\N	\N	UP	Uttar Pradesh	F	009
231	\N	\N	TN	Tamil Nadu	F	033
232	\N	\N	MP	Madhya Pradesh	F	023
233	\N	\N	GU	Gujarat	F	024
234	\N	\N	ND	Delhi	F	007
235	\N	\N	HP	Himachal Pradesh	F	002
236	\N	\N	MA	Maharashtra	F	027
237	\N	\N	PO	Puducherry	F	034
238	\N	\N	KR	Jammu and Kashmir	F	001
239	\N	\N	SI	Sikkim	F	011
240	\N	\N	AS	Assam	F	013
241	\N	\N	MN	Manipur	F	014
242	\N	\N	ME	Meghalaya	F	017
243	\N	\N	RA	Rajasthan	F	008
244	\N	\N	PU	Punjab	F	003
245	\N	\N	MI	Mizoram	F	015
246	\N	\N	AN	Andaman and Nicobar Islands	F	035
247	\N	\N	NA	Nagaland	F	018
248	\N	\N	TR	Tripura	F	016
249	\N	\N	AR	Arunachal Pradesh	F	012
250	\N	\N	BH	Bihar	F	010
251	\N	\N	HA	Haryana	F	006
252	\N	\N	OT	Others	F	099
253	\N	\N	CH	Chandigarh	F	004
254	\N	\N	JD	Jharkhand	F	020
255	\N	\N	UR	Uttarakhand	F	005
256	\N	\N	CG	Chhattisgarh	F	022
257	\N	\N	DN	Dadra and Nagar Haveli	F	026
258	\N	\N	OD	ODISHA	F	021
259	\N	\N	DD	Daman and Diu	F	025
260	\N	\N	LD	Lakshadweep	F	031
261	\N	\N	TE	Telangana	L	\N
262	\N	\N	AP	Andhra Pradesh	L	028
263	\N	\N	KE	Kerala	L	032
264	\N	\N	KA	Karnataka	L	029
265	\N	\N	GO	Goa	L	030
266	\N	\N	WB	West Bengal	L	019
267	\N	\N	UP	Uttar Pradesh	L	009
268	\N	\N	TN	Tamil Nadu	L	033
269	\N	\N	MP	Madhya Pradesh	L	023
270	\N	\N	GU	Gujarat	L	024
271	\N	\N	ND	Delhi	L	007
272	\N	\N	HP	Himachal Pradesh	L	002
273	\N	\N	MA	Maharashtra	L	027
274	\N	\N	PO	Puducherry	L	034
275	\N	\N	KR	Jammu and Kashmir	L	001
276	\N	\N	SI	Sikkim	L	011
277	\N	\N	AS	Assam	L	013
278	\N	\N	MN	Manipur	L	014
279	\N	\N	ME	Meghalaya	L	017
280	\N	\N	RA	Rajasthan	L	008
281	\N	\N	PU	Punjab	L	003
282	\N	\N	MI	Mizoram	L	015
283	\N	\N	AN	Andaman and Nicobar Islands	L	035
284	\N	\N	NA	Nagaland	L	018
285	\N	\N	TR	Tripura	L	016
286	\N	\N	AR	Arunachal Pradesh	L	012
287	\N	\N	BH	Bihar	L	010
288	\N	\N	HA	Haryana	L	006
289	\N	\N	OT	Others	L	099
290	\N	\N	CH	Chandigarh	L	004
291	\N	\N	JD	Jharkhand	L	020
292	\N	\N	UR	Uttarakhand	L	005
293	\N	\N	CG	Chhattisgarh	L	022
294	\N	\N	DN	Dadra and Nagar Haveli	L	026
295	\N	\N	OD	ODISHA	L	021
296	\N	\N	DD	Daman and Diu	L	025
297	\N	\N	LD	Lakshadweep	L	031
298	\N	\N	AP	Andhra Pradesh	T	028
299	\N	\N	KE	Kerala	T	032
300	\N	\N	KA	Karnataka	T	029
301	\N	\N	GO	Goa	T	030
302	\N	\N	WB	West Bengal	T	019
303	\N	\N	UP	Uttar Pradesh	T	009
304	\N	\N	TN	Tamil Nadu	T	033
305	\N	\N	MP	Madhya Pradesh	T	023
306	\N	\N	GU	Gujarat	T	024
307	\N	\N	ND	Delhi	T	007
308	\N	\N	HP	Himachal Pradesh	T	002
309	\N	\N	MA	Maharashtra	T	027
310	\N	\N	PO	Puducherry	T	034
311	\N	\N	KR	Jammu and Kashmir	T	001
312	\N	\N	SI	Sikkim	T	011
313	\N	\N	AS	Assam	T	013
314	\N	\N	MN	Manipur	T	014
315	\N	\N	ME	Meghalaya	T	017
316	\N	\N	RA	Rajasthan	T	008
317	\N	\N	PU	Punjab	T	003
318	\N	\N	MI	Mizoram	T	015
319	\N	\N	AN	Andaman and Nicobar Islands	T	035
320	\N	\N	NA	Nagaland	T	018
321	\N	\N	TR	Tripura	T	016
322	\N	\N	AR	Arunachal Pradesh	T	012
323	\N	\N	BH	Bihar	T	010
324	\N	\N	HA	Haryana	T	006
325	\N	\N	OT	Others	T	099
326	\N	\N	CH	Chandigarh	T	004
327	\N	\N	JD	Jharkhand	T	020
328	\N	\N	UR	Uttarakhand	T	005
329	\N	\N	CG	Chhattisgarh	T	022
330	\N	\N	DN	Dadra and Nagar Haveli	T	026
331	\N	\N	OD	ODISHA	T	021
332	\N	\N	DD	Daman and Diu	T	025
333	\N	\N	LD	Lakshadweep	T	031
334	\N	\N	TE	Telangana	T	\N
\.


--
-- Name: cams_state_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('cams_state_lookup_id_seq', 334, true);


--
-- PostgreSQL database dump complete
--

