--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: -
--

COPY product (id, created_on, modified_on, internal_name, display_name, product_type, schemes, allow_multiple, instalment_strategy, allowed_periods) FROM stdin;
1	\N	\N	liquid	Sqrrl Away	Mutual Fund	{14}	f	principal	{weekly,monthly}
3	\N	\N	tax_saving	Axe Tax	Mutual Fund	{11,12}	t	principal	{monthly}
2	\N	\N	goal_based	BYOD	Mutual Fund	{2,3,4,5,6,7,8,9,10,13}	t	returns	{monthly}
\.


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('product_id_seq', 3, true);


--
-- PostgreSQL database dump complete
--

