--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: billdesk_amc_lookup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY billdesk_amc_lookup (id, created_on, modified_on, code, value) FROM stdin;
1	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	BNPPMFD	BNP Paribas Asset Management India Pvt Ltd
2	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	BSLMFD	Birla Sun Life Asset Management
3	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	CNBROBMFD	Canara Robeco Asset Management Company Ltd
4	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	DSPMFD	DSP BlackRock Investment Managers Pvt.  Ltd.
5	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	FTMFD	Franklin Templeton Asset Management (India) Pvt Ltd.
6	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	SCBMFD	IDFC Asset Management Company Ltd
7	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	INDBMF	Indiabulls Asset Management Company Ltd.
8	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	JPMMFD	JM Financial Asset Management Pvt Limited
9	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	KOTAKMFD	Kotak Mahindra Asset Management Company Ltd.
10	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	LNTMFD	L&T Investment Management Ltd.
11	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	SBIMFD	SBI Funds Management Private Ltd.
12	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	SBNPMFD	Sundaram Asset Management Company Ltd
13	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	TATAMFD	Tata Asset Management Ltd
14	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	TAURUSMFD	Taurus Asset Management Company Ltd
15	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	AXAMFD	BOI AXA Investment Managers Pvt Ltd
16	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	AXISMFD	Axis Asset Management Company Ltd.
17	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	BOBMFD	Baroda Pioneer Asset Management Company Ltd
18	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	EDELMFD	Edelweiss Asset Management Ltd
19	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	HDFCMFD	HDFC Asset Management Company Ltd
20	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	IPRUMFD	ICICI Prudential Asset Management Company Ltd
21	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	LICMFD	LIC NOMURA Mutual Fund Asset Management Company Ltd.
22	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	MIRAEMFD	Mirae Asset Global Investments (India) Pvt. Ltd.
23	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	MOTILALMFD	Motilal Oswal Asset Management Company Ltd.
24	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	PPFASMFD	Parag Parikh Financial Advisory Services Mutual fund
25	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	PRAMMFD	Pramerica Asset Managers Private Ltd
26	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	PRINCIPALD	Principal PNB Asset Management Co. Pvt. Ltd.
27	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	QUANTUMMFD	Quantum Asset Management Company Private Ltd.
28	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	RELIGAREMFD	Religare Asset Management Company Private Ltd.
29	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	RMFD	Reliance Capital Asset Management Ltd.
30	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	UKBCMFD	Union KBC Asset Management Company Pvt Ltd
31	2016-11-01 09:36:20.752184	2016-11-01 09:36:20.752184	UTIMFD	UTI Asset Management Company Ltd
\.


--
-- Name: billdesk_amc_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('billdesk_amc_lookup_id_seq', 31, true);


--
-- PostgreSQL database dump complete
--

