--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: goal_scheme_lookup; Type: TABLE DATA; Schema: public; Owner: -
--
TRUNCATE goal_scheme_lookup RESTART IDENTITY;

COPY goal_scheme_lookup (id, created_on, modified_on, duration_bucket_code, aggression_code, target_roi, scheme_rta_code, allocation_percent) FROM stdin;
1	\N	\N	0	M	0.0700	1525	100
2	\N	\N	0	A	0.0725	132G	100
8	\N	\N	1	A1	0.0825	32	100
10	\N	\N	2	M	0.0831	24	75
12	\N	\N	2	A	0.0875	85NIG	50
13	\N	\N	2	A	0.0875	MTGP	50
14	\N	\N	2	A1	0.0875	85NIG	100
15	\N	\N	3	M	0.0920	NIGP	20
16	\N	\N	3	M	0.0920	MTG	80
17	\N	\N	3	M1	0.1200	313G	100
18	\N	\N	3	A	0.0983	CPGP	33.5
19	\N	\N	3	A	0.0983	85NIG	66.5
20	\N	\N	3	A1	0.1050	313G	100
21	\N	\N	4	M	0.1083	CPGP	33.5
22	\N	\N	4	M	0.1083	MTGP	66.5
23	\N	\N	4	M1	0.1050	313G	100
24	\N	\N	4	A	0.1281	CPGP	65
25	\N	\N	4	A	0.1281	MTGP	35
26	\N	\N	4	A1	0.1200	313G	100
27	\N	\N	5	M	0.1430	CPGP	60
28	\N	\N	5	M	0.1430	MTGP	40
30	\N	\N	5	A	0.1615	CPGP	80
31	\N	\N	5	A	0.1615	MTGP	20
5	\N	\N	1	M1	0.0775	148IG	100
11	\N	\N	2	M1	0.0875	24	100
29	\N	\N	5	M1	0.12	313G	100
32	\N	\N	5	A1	0.18	CPGP	100
4	\N	\N	1	M	0.075	148IG	50
3	\N	\N	1	M	0.075	132G	50
7	\N	\N	1	A	0.08	148IG	50
6	\N	\N	1	A	0.08	32	50
9	\N	\N	2	M	0.0831	148IG	25
\.


--
-- Name: goal_scheme_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('goal_scheme_lookup_id_seq', 32, true);


--
-- PostgreSQL database dump complete
--

