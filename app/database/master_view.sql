 CREATE VIEW 
master_view AS 
(SELECT 
customer_transaction.id, 
customer_transaction.created_on, 
customer_transaction.modified_on, 
customer_transaction.customer_id, 
customer.pan_name as customer_name, 
customer.pan_number as pan_number, 
COALESCE(customer.correspondence_city,customer.permanent_city,'NOT AVAILABLE') as city, 
customer.bank_name as bank_name, 
customer_transaction.amount, 
customer_transaction.units, 
customer_transaction.group_amount, 
customer_transaction.transaction_type, 
customer_transaction.transaction_subtype, 
customer_transaction.status, 
customer_transaction.mode, 
customer_transaction.transaction_reference, 
customer_transaction.bank_transaction_id, 
customer_transaction.bank_transaction_on, 
customer_transaction.note, 
investment_elaborate.investment_id, 
investment_elaborate.product_id, 
investment_elaborate.scheme_id, 
customer_transaction.group_id as transaction_group_id, 
investment_elaborate.investment_group_id, 
investment_elaborate.display_name, 
investment_elaborate.amc_code, 
investment_elaborate.amc_name, 
investment_elaborate.rta_code, 
investment_elaborate.scheme_type, 
investment_elaborate.scheme_name, 
customer_transaction.is_first 
FROM customer_transaction 
INNER JOIN customer ON customer.id=customer_transaction.customer_id 
INNER JOIN (SELECT 
investment.id as investment_id, 
investment.product_id,
investment.scheme_id,
investment.group_id as investment_group_id,
product.display_name, 
scheme.amc_code, 
scheme.amc_name, 
scheme.rta_code,
scheme.scheme_type, 
scheme.name as scheme_name 
FROM investment 
INNER JOIN product ON product.id = investment.product_id 
INNER JOIN scheme ON scheme.id=investment.scheme_id) investment_elaborate ON investment_elaborate.investment_id=customer_transaction.investment_id 
WHERE customer_transaction.status NOT IN ('INITIATED') 
ORDER BY id DESC)