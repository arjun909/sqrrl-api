# main.py
# The mothership! Sets up api, middleware and routes.
from wsgiref import simple_server
import falcon

from app import log, config
from app.database import init_session, db_session
from app.middleware import (
    DatabaseSessionManager,
    JSONTranslator,
    AuthMiddleware
)
from app.api.v1 import (
    FinancialInsights,
    Authorization,
    CustomerResource,
    CustomerDashboardResource,
    ProductResource,
    ProductRecommendationResource,
    InvestmentResource,
    InvestmentListResource,
    SchemeResource,
    SchemeListResource,
    TipsResource,
    JobResource,
    TransactionListResource,
    TransactionResource,
    AttachmentsResource,
    KarvyAadharCallback,
    BilldeskCallback,
    ZapierCallback,
    KarvyRejectionCallback,
    EmailVerificationCallback,
    NachRegisterFeedinCallback,
    NachTrxnFeedinCallback,
    PostmarkCallback,
    RedirectionResource,
    UserActionResource,
    ReferralCodesResource,
    AdminResource,
    BankResource,
    ClevertapWebhook
)
from app.model import Customer

LOG = log.get_logger()


class RootResource(object):
    """
    Handler for GET / - Just points to documentation
    """
    def on_get(self, req, resp):
        # ignore favicon - better for brower testing.
        if req.path == '/favicon.ico':
            raise falcon.HTTPNotFound()
        req.context['result'] = {
            'documentation_url': config.DOC_URL
        }


# setup db
init_session()

#
# # optionally - bootstrap database
# if int(config.BOOTSTRAP_SCHEMES):
#     # LOG.debug('BOOTSTRAP_SCHEMES {}'.format(config.BOOTSTRAP_SCHEMES))
#     load_schemes.delay()

# setup middleware
middleware = [
    JSONTranslator(),
    DatabaseSessionManager(db_session),
    AuthMiddleware()
]

# create server
LOG.info('API Server is starting')
app = falcon.API(middleware=middleware)

# routes
app.add_route('/', RootResource())
app.add_route('/v1/insights/f', FinancialInsights())
app.add_route('/v1/auth', Authorization())
app.add_route('/v1/customer/me', CustomerResource())
app.add_route('/v1/customer/me/dashboard', CustomerDashboardResource())
app.add_route('/v1/customer/me/attachments', AttachmentsResource())
app.add_route('/v1/products', ProductResource())
app.add_route('/v1/product/{product_id}/recommendations',
              ProductRecommendationResource())
app.add_route('/v1/investments', InvestmentListResource())
app.add_route('/v1/investment/{investment_group_id}', InvestmentResource())
app.add_route('/v1/scheme/{scheme_id}', SchemeResource())
app.add_route('/v1/schemes', SchemeListResource())
app.add_route('/v1/investment/{investment_group_id}/transactions',
              TransactionListResource())
app.add_route('/v1/transaction/{transaction_group_id}', TransactionResource())

# misc APIs
app.add_route('/v1/tips', TipsResource())
app.add_route('/v1/job/{job_id}', JobResource())
app.add_route('/v1/banks', BankResource())

# callback
app.add_route('/callback/ekyc/{handler}', KarvyAadharCallback())
app.add_route('/callback/eKYC/{handler}', KarvyAadharCallback())
app.add_route('/callback/billdesk', BilldeskCallback())
app.add_route('/callback/zapier', ZapierCallback())
app.add_route('/callback/postmark', PostmarkCallback())
app.add_route('/callback/karvy_rejection', KarvyRejectionCallback())
app.add_route('/callback/email_verify', EmailVerificationCallback())
app.add_route('/callback/nach_trxn_feedin', NachTrxnFeedinCallback())
app.add_route('/callback/nach_register_feedin', NachRegisterFeedinCallback())
app.add_route('/v1/referral_code', ReferralCodesResource())
app.add_route('/callback/clevertap_webhook', ClevertapWebhook())

# redirector
app.add_route('/r', RedirectionResource())

# for arbitrary user actions
app.add_route('/u/{action}', UserActionResource())
# admin
app.add_route('/v1/admin', AdminResource())

# If DEBUG mode on, then only add the Billdesk-Dummy endpoint.
if config.DEBUG:
    from app.api.v1 import BilldeskDummyResource
    from app.api.fake import RelianceDummyResource
    app.add_route('/billdesk/mock/PGIMerchantRequestHandler', BilldeskDummyResource())  # NOQA
    app.add_route('/fake/reliance/{action}', RelianceDummyResource())


# capture exceptions, send them to Sentry
# ref: https://github.com/falconry/falcon/issues/266#issuecomment-206300970
if config.SENTRY_DSN:
    from raven import Client
    client = Client(config.SENTRY_DSN)

    def internal_error_handler(ex, req, resp, params):

        # By-pass this for HTTPStatus (redirections)
        if isinstance(ex, falcon.HTTPStatus):
            raise ex

        data = {
            'request': {
                'url': req.url,
                'method': req.method,
                'query_string': req.query_string,
                'env': req.env,
                'data': req.params,
                'headers': req.headers,
            }
        }

        # if customer information is available, set it in user_context
        ident = None
        try:
            if 'customer_uid' in req.context:
                uid = req.context['customer_uid']
                c = Customer.get_profile_for_uid(uid, db_session)
                # LOG.debug('setting user context in sentry for: {}'.format(uid))  # NOQA
                client.context.activate()
                client.user_context({
                    'email': c['email'],
                    'username': c['name'] or '',
                    'id': c['id'],
                    'mobile': c['mobile']
                })
                message = (isinstance(ex, falcon.HTTPError) and
                           ex.title or str(ex))
                ident = client.captureException(message=message, data=data)
        except Exception as ex_inner:
            LOG.error(ex_inner)
        finally:
            client.context.clear()

        if not isinstance(ex, falcon.HTTPError):
            resp.status = falcon.HTTP_500
            resp.body = ('A server error occurred'
                         ' (reference code: "%s").' % ident)
        else:
            raise ex

    app.add_error_handler(Exception, internal_error_handler)

if __name__ == "__main__":
    server = simple_server.make_server("localhost", 5000, app)
    server.serve_forever()
