from app import config, constants
from celery.schedules import crontab
from kombu import Queue

result_backend = config.REDIS_URL
broker_url = config.REDIS_URL
task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
# restrict max number of redis connections.
redis_max_connections = config.CELERY_REDIS_MAX_CONNECTIONS
# ref: http://stackoverflow.com/a/23563018
broker_pool_limit = None
broker_transport_options = {
    'max_connections': redis_max_connections,
}
# raise an exception if any task goes beyond 2 minutes
task_soft_time_limit = 120
timezone = 'UTC'
ONCE_REDIS_URL = config.REDIS_URL

task_queue_max_priority = 10
broker_transport_options = {
    'priority_steps': list(range(10)),
}

# Setting for renaming the default queue
# task_default_queue = 'priority_queue'

# Creating a priority queue
task_queues = (
     Queue(name='priority_queue',
           durable=True,
           max_priority=10,
           routing_key='priority_queue',
           queue_arguments={'x-max-priority': 10}
           ),
)


# setup periodic jobs
# this is better in v4.0, right now using old style
# ref: http://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html#crontab-schedules  # NOQA
beat_schedule = {
    'generate-nach-transactions-feed-out': {
        'task': 'app.tasks.generate_nach_trxn_feedout',
        'schedule': crontab(hour='02', minute='15', day_of_week='*')
    },
    # executes every weekday, 10:30pm IST
    'update-nav-daily': {
        'task': 'app.tasks.fetch_daily_nav',
        'schedule': crontab(hour='17', minute='0', day_of_week='*')
    },
    # everyday 6:30 am IST
    # 'update-rmm-balances': {
    #     'task': 'app.tasks.update_all_rmm_folios',
    #     'schedule': crontab(hour='1', minute='0', day_of_week='*')
    # },
    'feed-out-karvy': {
        'task': 'app.tasks.process_feed_out_karvy',
        'schedule': crontab(hour='8', minute='15', day_of_week='mon-fri')
    },
    'feed-out-karvy-2': {
        'task': 'app.tasks.process_feed_out_karvy',
        'schedule': crontab(hour='9', minute='15', day_of_week='mon-fri')
    },
    'feed-out-cams': {
        'task': 'app.tasks.process_feed_out_cams',
        'schedule': crontab(hour='8', minute='45', day_of_week='mon-fri')
    },
    # everyday, midnight
    'delete-investment-demo-accounts': {
        'task': 'app.tasks.delete_demo_investments',
        'schedule': crontab(hour='18', minute='30', day_of_week='*')
    },
    # everyday, 3:30 PM IST
    'pan-kyc-docs-mailer': {
        'task': 'app.tasks.mail_pan_kyc_docs',
        'schedule': crontab(hour='10', minute='0', day_of_week='mon-fri')
    },
    # everyday 5:30 PM IST
    'customer-setup-elogs': {
        'task': 'app.tasks.mail_setup_feed',
        'schedule': crontab(hour='12', minute='0', day_of_week='*')
    },
    # everyday 1:30 PM IST
    'update-billdesk-failed-transactions': {
        'task': 'app.tasks.update_billdesk_failed_transactions',
        'schedule': crontab(hour='8', minute='0', day_of_week='mon-fri')
    },
    # everyday 12:30 PM IST
    'update-ekyc-status': {
        'task': 'app.tasks.update_ekyc_status',
        'schedule': crontab(hour='8', minute='0', day_of_week='mon-fri')
    },
    'feed-out-FT': {
        'task': 'app.tasks.process_feed_out_FT',
        'schedule': crontab(hour='8', minute='45', day_of_week='mon-fri')
    },
    # Timings are in UTC
    # Once in every two days, at 16:30 PM IST
    'fetch-missing-ckyc-ids': {
        'task': 'app.tasks.fetch_missing_ckyc_ids',
        'schedule': crontab(hour='11', minute='00', day_of_month='*/2')
    },
    'sqrrl-away-nach-trx': {
        'task': 'app.tasks.create_nach_trxns',
        'schedule': crontab(hour='17', minute='00', day_of_week='tue'),
        'kwargs': {'product_id': 1}
    },
    'byod-nach-trx': {
        'task': 'app.tasks.create_nach_trxns',
        'schedule': crontab(hour='17', minute='00', day_of_month='5'),
        'kwargs': {'product_id': 2}
    },
    'axe-tax-nach-trx': {
        'task': 'app.tasks.create_nach_trxns',
        'schedule': crontab(hour='18', minute='00', day_of_month='5'),
        'kwargs': {'product_id': 3}
    },
    # Send upcoming payment reminder notifications to all customers whose Sqrrl Away instalment are due in 2 days.
    # Time: 10:30 UTC <=> 16:00 IST
    'sqrrl-away-payment-reminders-due-in-2-days': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='10', minute='30', day_of_week='*'),
        'kwargs': {
            'channel' : constants.MOBILE_PUSH,
            'product' : constants.LIQUID,
            'days_away' :  2,
            'title' : 'Hey, Don\'t forget!',
            'message' : 'Your SqrrlAway weekly payment date is right around the corner.'
        }
    },
    # Send upcoming payment reminder notifications to all customers whose Sqrrl Away instalment are due today.
    # Time: 05:30 UTC <=> 11:00 IST
    'sqrrl-away-payment-reminders-due-today': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='5', minute='30', day_of_week='*'),
        'kwargs': {
            'channel': constants.MOBILE_PUSH,
            'product': constants.LIQUID,
            'days_away': 0,
            'title' : 'It\'s Sqrrl o\' Clock!',
            'message' : 'Time to complete your SqrrlAway transaction!'
        }
    },
    # Send pending payment reminder notifications to all customers whose Sqrrl Away instalment are due since yesterday.
    # Time: 05:30 UTC <=> 11:00 IST
    'sqrrl-away-payment-reminders-due-since-yesterday': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='5', minute='30', day_of_week='*'),
        'kwargs': {
            'channel': constants.MOBILE_PUSH,
            'product': constants.LIQUID,
            'days_away': -1,
            'title': 'Miss something?',
            'message': 'Yes! Your SqrrlAway due instalment. It\'s still not that late. 💁'
        }
    },
    # Send upcoming payment reminder notifications to all customers whose BYOD instalment are due in 2 days.
    # Time: 10:30 UTC <=> 16:00 IST
    'byod-payment-reminders-due-in-2-days': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='10', minute='30', day_of_week='*'),
        'kwargs': {
            'channel' : constants.MOBILE_PUSH,
            'product' : constants.GOAL,
            'days_away':  2,
            'title' : 'Hey dreamer!',
            'message' : 'It\'s your dreams and they are waiting. Only two days remain for your monthly BYOD transaction.'
                        ' Make it and your dreams will thank you!'
        }
    },
    # Send upcoming payment reminder notifications to all customers whose BYOD instalment are due today.
    # Time: 05:30 UTC <=> 11:00 IST
    'byod-payment-reminders-due-today': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='5', minute='30', day_of_week='*'),
        'kwargs': {
            'channel': constants.MOBILE_PUSH,
            'product': constants.GOAL,
            'days_away': 0,
            'title' : 'Payment Reminder!',
            'message' : 'Your BYOD transaction is due today!'
        }
    },
    # Send pending payment reminder notifications to all customers whose BYOD instalment are due since yesterday.
    # Time: 05:30 UTC <=> 11:00 IST
    'byod-payment-reminders-due-since-yesterday': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='5', minute='30', day_of_week='*'),
        'kwargs': {
            'channel': constants.MOBILE_PUSH,
            'product': constants.GOAL,
            'days_away': -1,
            'title': 'We think you missed something!',
            'message': 'Completing your BYOD transaction for this month. It\'s still not that late. 💁'
        }
    },
    # Send upcoming payment reminder notifications to all customers whose Axe Tax instalment are due in 2 days.
    # Time: 10:30 UTC <=> 16:00 IST
    'axe-tax-payment-reminders-due-in-2-days': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='10', minute='30', day_of_week='*'),
        'kwargs': {
            'channel' : constants.MOBILE_PUSH,
            'product' : constants.TAX,
            'days_away' : 2,
            'title' : 'All set to axe the tax?',
            'message': 'Two days to go for your monthly AxeTax transaction!'
        }
    },
    # Send upcoming payment reminder notifications to all customers whose Axe Tax instalment are due today.
    # Time: 05:30 UTC <=> 11:00 IST
    'axe-tax-payment-reminders-due-today': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='5', minute='30', day_of_week='*'),
        'kwargs': {
            'channel': constants.MOBILE_PUSH,
            'product': constants.TAX,
            'days_away': 0,
            'title' : 'Are you ready?',
            'message' : 'Your monthly AxeTax transaction is due today!'
        }
    },
    # Send pending payment reminder notifications to all customers whose Axe Tax instalment are due since yesterday.
    # Time: 05:30 UTC <=> 11:00 IST
    'axe-tax-payment-reminders-due-since-yesterday': {
        'task': 'app.tasks.payment_reminders',
        'schedule': crontab(hour='5', minute='30', day_of_week='*'),
        'kwargs': {
            'channel': constants.MOBILE_PUSH,
            'product': constants.TAX,
            'days_away': -1,
            'title': 'Did you miss something?',
            'message': 'Yup! Your monthly AxeTax transaction. Don\'t delay. 💁'
        }
    },
    # Generating and sending monthly account statements
    # Time: 06:30 UTC <=> 12:00 IST, on 4th/5th of every month
    'monthly-account-statements': {
        'task': 'app.tasks.trigger_monthly_account_statements_generation',
        'schedule': crontab(hour='6', minute='30', day_of_month=5)
    },
    # Fetching and updating PAN data for such customers who are KYC verified
    # but their pan data is missing from the database.
    # Time: 07:45 UTC <=> 13:15 IST, daily
    'update-pan-date-for-all-kyc-verified-customers': {
        'task': 'app.tasks.update_pan_data_for_all_customers',
        'schedule': crontab(hour='7', minute='45', day_of_week='*')
    },
    # Update the outdated next_investment_on values (Sqrrl Away)
    # Time: 07:00 UTC <=> 12:30 IST, on every on Sunday
    'update-outdated-next_investment_on': {
        'task': 'app.tasks.update_outdated_next_investment_on',
        'schedule': crontab(hour='7', minute='0', day_of_week='sun')
    },
}
