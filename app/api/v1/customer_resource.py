# customer_resource.py
# GET and PATCH currently logged in customer info.
from copy import deepcopy

from falcon import before, HTTPBadRequest, HTTP_202, HTTP_400
from cerberus import Validator
from iso8601 import parse_date
from fuzzywuzzy import fuzz
from distutils.util import strtobool

from app.model import (Customer, Investment, ReferralCode,
                       ReferralCodeCustomerMapping, BilldeskBankLookup)
from app import model as m
from app.utils.helpers import errors_to_desc
from app.config import UPCOMING_CUTOFF, EMAIL_VERIFICATION_URL
from app.constants import (OCCUPATION,
                           INCOME_GROUP, SOURCE_OF_WEALTH,
                           SETTINGS_K_REMIND_DAYS, SETTINGS_K_SUMMARY_DAY,
                           SETTINGS_K_TIPS_ENABLED, SETTINGS_K_PUSH_ENABLED,
                           SETTINGS_K_PAYMENT_PREF, PAYMENT_MODE_NB,
                           PAYMENT_MODE_DEBIT, PAYMENT_MODE_RUPAY,
                           NACH_REGISTRATION_SUCCESS)
from app.tasks import (validate_pan, update_bank_from_ifsc, get_kyc_url,
                       validate_pan_number, send_verification_email,
                       fetch_pan_data)
import uuid
from app import log
LOG = log.get_logger()


# validate request body for PATCH
def validate_patch_input(req, resp, resource, params):
    """
    Validates that the customer is passing valid fields and types
    for updating customer info.
    """
    doc = req.context['doc']

    # The validation schema
    schema = {
        'name': {'type': 'string', 'empty': False},
        'email': {
            'type': 'string',
            'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
        },
        'pan_number': {
            'type': 'string',
            'regex': '^[a-zA-Z]{5}[0-9]{4}[a-zA-Z]$',
            'dependencies': ['date_of_birth']
        },
        'date_of_birth': {'type': 'date', 'coerce': parse_date},
        'bank_account_name': {'type': 'string', 'empty': False},
        'bank_ifsc': {
            'type': 'string',
            'regex': '^[a-zA-Z]{4}[0][a-zA-Z0-9]{6}$'
        },
        'bank_account_no': {'type': 'string', 'empty': False},
        'aadhaar': {
            'type': 'string',
            'regex': '^[0-9]{12}$'
        },
        'aadhaar_mobile': {
            'type': 'string',
            'regex': '\d{10}'
        },
        'aadhaar_email': {
            'type': 'string',
            'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
        },
        'referred_by': {
            'type': 'string'
        },
        'pan_kyc_docs_uploaded': {'type': 'boolean'},
        'occupation_code': {'type': 'string', 'empty': False},
        'annual_income_code': {'type': 'string', 'empty': False},
        'source_of_wealth_code': {'type': 'string', 'empty': False},
        'politically_exposed': {'type': 'boolean'},
        'mother_name': {
            'type': 'string',
            'nullable': True
        },
        'nomination_status': {
            'type': 'boolean'
        },
        'nominee_name': {
            'type': 'string'
        },
        'nominee_date_of_birth': {
            'type': 'date',
            'coerce': parse_date
        },
        'nominee_relationship': {
          'type': 'string'
        },
        'nominee_guardian': {
            'type': 'string'
        },
        'marital_status': {
            'type': 'string',
            'nullable': False,
            'allowed': ['01', '02', '03']
        },
        'settings': {
            'type': 'dict',
            'schema': {
                SETTINGS_K_PUSH_ENABLED: {'type': 'boolean'},
                SETTINGS_K_REMIND_DAYS: {
                    'type': 'integer',
                    'min': 1, 'max': 7
                },
                SETTINGS_K_SUMMARY_DAY: {
                    'type': 'string',
                    'allowed': ['sunday', 'monday', 'tuesday', 'wednesday',
                                'thursday', 'friday', 'saturday']
                },
                SETTINGS_K_TIPS_ENABLED: {'type': 'boolean'},
                SETTINGS_K_PAYMENT_PREF: {
                    'type': 'string',
                    'allowed': [
                        PAYMENT_MODE_NB,
                        PAYMENT_MODE_DEBIT,
                        PAYMENT_MODE_RUPAY
                    ]
                }
            }
        }
    }
    error_message = {
        'email': 'not a valid email address',
        'pan_number': 'invalid PAN (Permanent Account Number)'
                      ' or date_of_birth missing',
        'bank_ifsc': 'not a valid IFSC code',
        'aadhaar': 'not a valid Aadhaar'
    }
    # validate input
    try:
        v = Validator(schema)
        if not v.validate(doc):
            # replace in-built messages with friendlier ones
            errors = v.errors
            error_keys = (k for k in error_message if k in v.errors)
            for k in error_keys:
                errors[k] = [error_message[k]]
            raise HTTPBadRequest(title='Some parameters are invalid',
                                 description=errors_to_desc(errors))
    except Exception as ex:
        if (isinstance(ex, HTTPBadRequest)):
            raise ex
        raise HTTPBadRequest(title='Bad request',
                             description='Error validating request')


class CustomerResource(object):
    """
    /v1/customer/me
    """
    def on_get(self, req, resp):
        """
        Returns info about currently logged in user.
        By default: returns few important fields.
        Use with ?extended=True for more fields.
        """
        try:
            extended = req.get_param('extended')
            uid = req.context['customer_uid']
            # LOG.debug('uid: {}'.format(uid))
            # fetch customer record.
            session = req.context['session']
            c = session.query(Customer).filter(Customer.uid == uid).first()
            req.context['result'] = c.get_profile(extended)
        except Exception as ex:
            LOG.error(ex)
            msg = 'Could not fetch customer profile. \n{}'.format(ex)
            raise HTTPBadRequest(title='Bad request', description=msg)

    @before(validate_patch_input)
    def on_patch(self, req, resp):
        """
        PATCH /customer
        """
        doc = req.context['doc']
        uid = req.context['customer_uid']
        # for key in req.context:
        #     LOG.debug('{} - {}'.format(key, req.context[key]))
        session = req.context['session']
        c = session.query(Customer).filter(Customer.uid == uid).first()

        # if patch request has email, send a verification email to the user
        if 'email' in doc:
            token = str(uuid.uuid1())
            url = EMAIL_VERIFICATION_URL + token
            doc['email_verification_token'] = token
            send_verification_email.delay(doc, url)

        if 'referred_by' in doc:
            referred_by_code = doc['referred_by']
            # Just to cross-verify that the referral code is valid
            rc_is_valid, _ = \
                ReferralCode.is_referral_code_valid(session, referred_by_code)
            if rc_is_valid:
                # Add a new mapping in referral_code_customer_mapping table.
                ReferralCodeCustomerMapping.add_row(session=session,
                                                    ref_code=referred_by_code,
                                                    customer_id=c.id)

        # some validation for pan_number
        if 'pan_number' in doc:
            # make sure pan is uppercased.
            doc['pan_number'] = doc['pan_number'].upper()
            # if the patch request if for a duplicate pan_number
            # dont process the request
            cust_pan = session.query(Customer).\
                filter(Customer.pan_number == doc['pan_number'],
                       Customer.uid != uid).first()
            if cust_pan:
                resp.status = HTTP_400
                raise HTTPBadRequest(title='Invalid parameters',
                                     description='The submitted PAN already exists in our database.')  # NOQA
            # it seems we can safely update PAN
            # check kyc status, and update data from pan master as needed
            job = validate_pan.apply_async((c.uid, doc['pan_number']),
                                           link=fetch_pan_data.s(c.uid))
            resp.status = HTTP_202
            resp.location = '/v1/job/{}'.format(job.id)

        # if FATCA fields are submitted, also populate values.
        if 'occupation_code' in doc:
            doc['occupation'] = OCCUPATION[str(doc['occupation_code'])]
        if 'annual_income_code' in doc:
            doc['annual_income'] = INCOME_GROUP[str(doc['annual_income_code'])]
        if 'source_of_wealth_code' in doc:
            doc['source_of_wealth'] =\
                SOURCE_OF_WEALTH[str(doc['source_of_wealth_code'])]
        # settings are stored as hstore, hence all values must be strings
        if 'settings' in doc:
            for k, v in doc['settings'].items():
                doc['settings'][k] = str(v)
            # merge with existing settings, else this will overwrite all keys
            if c.settings:
                new_settings = deepcopy(c.settings)
                new_settings.update(doc['settings'])
                doc['settings'] = new_settings
            LOG.debug('updated settings: {}'.format(doc['settings']))

        # customer sensitive info popped out and saved in local vars instead
        # if key not present save None instead
        aadhaar = doc.get('aadhaar', None)
        aadhaar_email = doc.pop('aadhaar_email', None)
        aadhaar_mobile = doc.pop('aadhaar_mobile', None)

        mother_name = doc.get('mother_name', None)
        if mother_name:
            doc['mother_name'] = str(mother_name).title()

        if 'nomination_status' in doc:
            nomination_status = doc.get('nomination_status')
            if not isinstance(nomination_status, bool):
                nomination_status = strtobool(nomination_status)
            doc['nomination_status'] = nomination_status
        if ('nominee_name' in doc and 'nominee_date_of_birth' in doc
                and 'nominee_relationship' in doc):
            doc['nominee_name'] = str(doc['nominee_name']).title()
            doc['nominee_relationship'] = \
                str(doc['nominee_relationship']).lower()
            doc['nomination_status'] = True

        if 'bank_account_name' in doc and c.pan_name:
            # we want to make sure this account matches the PAN at least.
            score = fuzz.token_set_ratio(c.pan_name.upper(),
                                         doc['bank_account_name'].upper())
            if score < 90:
                LOG.warn('Bank and PAN names mismatch!', extra={
                    'bank_account_name': doc['bank_account_name'],
                    'pan_name': c.pan_name,
                    'c_name': c.name,
                    'c_uid': c.uid
                })

        # validate if provided bank_ifsc is supported
        if 'bank_ifsc' in doc:
            is_supported = BilldeskBankLookup.is_supported(
                doc['bank_ifsc'], session
            )
            # LOG.debug('is_supported: {}'.format(is_supported))
            if not is_supported:
                LOG.warn('Bank is not supported by customer', extra={
                    'data': {'bank_ifsc': doc['bank_ifsc']},
                    'user': {
                        'id': c.id,
                        'mobile': c.mobile,
                        'email': c.email
                    }
                })
                # TEMPORARY: Uncomment once apps can show custom error text
                # raise HTTPInvalidParam(
                #     msg='This bank is not supported for online transactions,'
                #         ' please try another.',
                #     param_name='IFSC'
                # )

        # all seems good, can update
        for key in doc:
            setattr(c, key, doc[key])
        session.commit()

        # LOG.debug('profile saved to database')

        # if IFSC code is updated, then we need to update related bank info
        if 'bank_ifsc' in doc:
            update_bank_from_ifsc.delay(c.id)

        # aadhar - this should kick-off new KYC process
        if aadhaar:
            # creates an async job - poll it to get aadhar kyc url
            job = get_kyc_url.delay(c.uid,
                                    aadhaar, aadhaar_email, aadhaar_mobile)
            resp.status = HTTP_202
            resp.location = '/v1/job/{}'.format(job.id)
            # in parallel, validate the customer's pan number from IT database
            validate_pan_number.delay(c.uid, c.pan_number)
        # return updated profile
        req.context['result'] = c.get_profile()


class CustomerDashboardResource(object):
    """
    Powers /customer/me/dashboard API - a customer dashboard / summary
    with investment, recent transactions etc.
    """
    def on_get(self, req, resp):
        uid = req.context['customer_uid']
        # LOG.debug('uid: {}'.format(uid))
        # fetch customer record.
        session = req.context['session']
        c = session.query(Customer).filter(Customer.uid == uid).first()
        # structure the dashboard as the customer API
        # {
        #     "uid": "...",
        #     "name": "...",
        #     "investments": [
        #
        #     ],
        #     "pending_transactions": [],
        #     "upcoming_transactions": [],
        # }

        # 1. user profile
        result = c.get_profile(extended=True)

        # 2. investments with recent transactions.
        investments = Investment.get_by_customer(session, c.id)
        # LOG.debug('investment: {} rows'.format(len(investments)))
        result['investments'] =\
            Investment.populate_transaction_summary(investments, session)

        # populate min_purchase_amount
        result['investments'] = Investment.populate_min_purchase_amount(
            result['investments'], session
        )

        # if the customer has NACH, populate upcoming_nach_transactions
        if (c.nach_mandate_id and
                c.nach_mandate_status == NACH_REGISTRATION_SUCCESS):
            result['investments'] = \
                Investment.populate_upcoming_nach_transactions(
                result['investments'], session
            )

        # populate scheme related info
        result['investments'] = Investment.populate_schemes(
            result['investments'], session
        )

        # upcoming transactions (next n days)
        upcoming_transactions = Investment.upcoming_transactions(
            session, c.id, UPCOMING_CUTOFF
        )

        # tips - general (Home screen) and product-wise
        tips = m.Tips.get_all()
        result['tips'] = tips['general']

        current_value = net_gain = None
        for p in result['investments']:
            if ('p_net_gain_percentage' in p and
                    p['p_net_gain_percentage'] is not None):
                p['p_net_gain_percentage'] =\
                    round(p['p_net_gain_percentage'], 2)
            # LOG.debug('{}'.format(p))
            if type(p) is dict:
                try:
                    # TIPS
                    p['tips'] = tips[p['internal_name']]
                    # also computing total current_value and net_gain
                    if p['p_current_value']:
                        current_value = (current_value or 0) \
                            + p['p_current_value']
                    if p['p_net_gain']:
                        net_gain = (net_gain or 0) + p['p_net_gain']
                except:
                    p['tips'] = []

                # upcoming_transaction
                # search relevant row in array and insert into item
                p['upcoming_transaction'] = next(
                    (t for t in upcoming_transactions
                     if t.group_id == p['group_id']),
                    None
                )
                # TODO: delete group_id from the upcoming transaction.

                # retry_transaction
                p['retry_transaction'] = Investment.retry_transaction(
                    session, p['group_id']
                )

        # totals
        result['totals'] = {
            'p_current_value': current_value,
            'p_net_gain': net_gain
        }

        # response json
        req.context['result'] = result
