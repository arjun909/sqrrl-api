from app.model import Tips


class TipsResource(object):
    def on_get(self, req, resp):
        """
        GET /tips
        """
        try:
            tips = Tips.get_all()
            req.context['result'] = tips
        except:
            req.context['result'] = []
