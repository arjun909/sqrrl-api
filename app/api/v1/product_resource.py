# product_resource.py
# GET /products
from copy import deepcopy
from falcon import HTTPNotFound, before, HTTPBadRequest
from cerberus import Validator

from app.utils.helpers import errors_to_desc
from app.model import Product, Scheme
from app.api.v1.investment_resource import INVESTMENT_SCHEMA
from app.log import get_logger
LOG = get_logger()


class ProductResource(object):
    """
    Wraps around the Product model - /products
    """
    def on_get(self, req, resp):
        """
        GET /products
        """
        session = req.context['session']
        products = session.query(Product).order_by(Product.id).all()
        req.context['result'] = []
        for p in products:
            req.context['result'].append({
                'id': p.id,
                'internal_name': p.internal_name,
                'display_name': p.display_name,
                'allow_multiple': p.allow_multiple,
                'instalment_strategy': p.instalment_strategy,
                'allowed_periods': p.allowed_periods
            })


def validate_get_input(req, resp, resource, params):
    """
    The input parameters for getting recommended schemes and allocations
    should be validate exactly like POST /investments call, as the same
    parameters are helpful to determine what the allocation ought to be.
    """
    try:
        schema = deepcopy(INVESTMENT_SCHEMA)
        schema.pop('product_id', None)
        v = Validator(schema)
        doc = req.params
        doc.pop('product_id', None)
        # manually converting some fields to int
        for k in ('target_instalment_amount', 'total_target_amount',
                  'target_instalment_count'):
            if k in doc:
                doc[k] = req.get_param_as_int(k)
        # API-29: Format description as string
        if 'description' in doc:
            d = req.get_param_as_list('description')
            doc['description'] = ', '.join(d)
        # LOG.debug(doc)
    except Exception as ex:
        LOG.error('Could not validate input', exc_info=True)
        raise HTTPBadRequest(title='Invalid parameters',
                             description='Could not parse parameters')
    if not v.validate(doc):
        raise HTTPBadRequest(title='Invalid parameters',
                             description=errors_to_desc(v.errors))

    # one of these need to be provided
    if not ('total_target_amount' in doc or 'target_instalment_amount' in doc):
        errors = {
            'target_instalment_amount':  'one of total_target_amount or '
                                         'target_instalment_amount needs'
                                         ' to be provided'
        }
        raise HTTPBadRequest(title='Invalid parameters',
                             description=errors)


class ProductRecommendationResource(object):
    @before(validate_get_input)
    def on_get(self, req, resp, product_id):
        """
        GET /product/{product_id}/recommendations

        This is a pseudo-resource, it does not actually exist in the db.
        For a given query, it returns list of recommended schemes and
        their allocation.
        """
        # ref: http://restful-api-design.readthedocs.io/en/latest/methods.html
        doc = req.params
        # LOG.debug('doc {}'.format(doc))
        session = req.context['session']
        product = session.query(Product).get(product_id)
        if not product:
            raise HTTPNotFound('No such product: {}'.format(id))
        try:
            allocation = Product.get_recommended_schemes(product, doc, session)
            # LOG.debug('allocation: {}'.format(allocation))
            Scheme.populate_summary(allocation, session)
            req.context['result'] = allocation
        except Exception as ex:
            # some field was probably missing.
            LOG.error(ex)
            raise HTTPBadRequest(title='Unable to process request',
                                 description='{}'.format(ex))
