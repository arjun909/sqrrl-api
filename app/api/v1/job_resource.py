# job_resource.py
from falcon import HTTPNotFound

from app.tasks import celery_app
from app.log import get_logger
LOG = get_logger()


class JobResource(object):
    """
    Thin wrapper over celery task results.
    """
    def on_get(self, req, resp, job_id):
        # LOG.debug('job_id {}:'.format(job_id))
        job = celery_app.AsyncResult(job_id)
        if job:
            # LOG.debug('{} - {}'.format(job.status, job.result))
            # TODO: Remove exception info in production.
            if isinstance(job.result, Exception):
                job_result = {
                    'error': str(job.result)
                }
            else:
                job_result = job.result
            req.context['result'] = {
                'id': job_id,
                'status': job.status,
                'result': job_result
            }
            return
        else:
            # not found, return empty 404
            raise HTTPNotFound()
