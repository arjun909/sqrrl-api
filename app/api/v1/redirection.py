# app/api/v1/redirection.py
from falcon import HTTPInvalidParam, HTTPFound

from app.log import get_logger
LOG = get_logger()


class RedirectionResource():
    """
    Simply redirects to the provided url, used to redirect to
    mobile links which begin with sqrrlfintech://
    """
    def on_get(self, req, resp):
        url = req.get_param('u')
        if not url:
            raise HTTPInvalidParam(msg='redirection url is required',
                                   param_name='u')

        # 302 temporary redirect.
        # using "temporary" so that clients can continue to open this
        # location instead of the target, which is not http / https but a
        # custom mobile location at sqrrlfintech://
        # LOG.debug('redirecting to {}'.format(url))
        raise HTTPFound(url)
