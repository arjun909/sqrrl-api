# app/api/v1/bank_resource.py
from falcon import HTTPBadRequest

from app.model import BilldeskBankLookup
from app.log import get_logger
LOG = get_logger()


class BankResource():
    def on_get(self, req, resp):
        db = req.context['session']
        try:
            ifsc_codes = BilldeskBankLookup.get_supported_ifsc_codes(db)
            req.context['result'] = {
                'ifsc_codes': ifsc_codes
            }
        except BaseException as ex:
            LOG.error('Could not fetch supported IFSC codes', exc_info=True)
            raise HTTPBadRequest(
                title='Server error',
                description='Could not fetch supported IFSC codes'
            )
