# referral_codes_resource.py

from falcon import HTTPBadRequest, HTTP_200
from app.model import ReferralCode

from app import log
LOG = log.get_logger()


class ReferralCodesResource(object):
    """
    /v1/referral_code
    """
    def on_get(self, req, resp):
        """
        Returns True or False, depending on whether the Referral Code passed
        is a valid code or not, and if not, then a message explaining the cause
        """
        try:
            ref_code = req.get_param('code')
            uid = req.context['customer_uid']
            LOG.debug('Validating referral code: {} for uid: {}'.
                      format(ref_code, uid))

            session = req.context['session']

            is_valid, message = \
                ReferralCode.is_referral_code_valid(session, ref_code)

            resp.status = HTTP_200
            result = {
                'ref_code_valid': is_valid,
                'message': message
            }
            req.context['result'] = result

        except Exception as ex:
            msg = 'Referral Code could not be validated. ' \
                  'Following exception occurred:\n{}'.format(ex)
            LOG.error(msg)
            raise \
                HTTPBadRequest(title='Bad request',
                               description='Referral Code could not be validated.')
