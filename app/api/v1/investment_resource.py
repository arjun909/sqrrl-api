# investment_resource.py
from falcon import (
    before,
    HTTPBadRequest,
    HTTPInvalidParam,
    HTTP_201,
    HTTP_204,
    HTTPForbidden,
    HTTPNotFound
)
from cerberus import Validator

from app.model import Customer, Investment, Product, Scheme
from app.utils.allocation_helper import CATEGORY_SCORES
# from app.tasks import create_zero_balance_folio
# from app.constants import LIQUID
from app import log
LOG = log.get_logger()

INVESTMENT_SCHEMA = {
    'product_id': {'type': 'integer', 'required': True},
    'category': {
        'type': 'string',
        'allowed': list(CATEGORY_SCORES.keys())
    },
    'description': {'type': 'string', 'empty': False},
    'target_instalment_amount': {
        'type': 'integer',
        'min': 100,
        'dependencies': ['instalment_period']
    },
    'total_target_amount': {'type': 'integer', 'min': 100},
    'target_completion_on': {'type': 'date'},
    'instalment_period': {
        'type': 'string',
        'allowed': ['weekly', 'monthly']
    },
    'target_instalment_count': {
        'type': 'integer', 'min': 1, 'dependencies': ['instalment_period']
    }
}


def validate_post_input(req, resp, resource, params):
    """
    Validates input for creating a new "investment"
    """
    # validate input
    v = Validator(INVESTMENT_SCHEMA)
    if not v.validate(req.context['doc']):
        raise HTTPBadRequest(title='Invalid parameters',
                             description=v.errors)

    # one of these need to be provided
    if not ('total_target_amount' in req.context['doc']
            or 'target_instalment_amount' in req.context['doc']):
        errors = {
            'target_instalment_amount':  'one of total_target_amount or '
                                         'target_instalment_amount needs'
                                         ' to be provided'
        }
        raise HTTPBadRequest(title='Invalid parameters',
                             description=errors)


def load_current_user(req, resp, resource, params):
    """
    Loads the current user into the context
    """
    uid = req.context['customer_uid']
    session = req.context['session']
    c = session.query(Customer).filter(Customer.uid == uid).first()
    req.context['customer'] = c


class InvestmentListResource(object):
    """
    Handles /investment(s)
    """
    @before(load_current_user)
    def on_get(self, req, resp):
        """
        GET /investments
        """
        c = req.context['customer']
        session = req.context['session']
        req.context['result'] = Investment.get_by_customer(session, c.id)

    @before(validate_post_input)
    @before(load_current_user)
    def on_post(self, req, resp):
        """
        POST /investments

        Called when a user creates a new product.
        """
        doc = req.context['doc']
        session = req.context['session']
        customer = req.context['customer']

        # fetch product
        product = session.query(Product).get(doc['product_id'])
        if not product:
            raise HTTPInvalidParam(msg='No such product',
                                   param_name='product_id')
        elif ('instalment_period' in doc and
                doc['instalment_period'] not in product.allowed_periods):
            raise HTTPInvalidParam(
                msg='Must be one of {}'.product.allowed_periods,
                param_name='instalment_period'
            )
        try:
            # create a savepoint - we want to rollback if an error
            # occurs at any point

            # also we should ensure this entire block has no commits
            # until the end.
            session.begin_nested()

            # create new investment(s) based on given input.
            doc['ip_address'] = req.access_route[-1].strip()
            result = Investment.create_new(customer, product,
                                           doc, session)
            # save to database if all is good.
            session.commit()

            # expand scheme_id to scheme { id, name }
            Scheme.populate_summary(result['investments'], session)
            req.context['result'] = result

            # create an async call to reliance.create_zbf()
            # if product.internal_name and product.internal_name == LIQUID:
            #     investment_group_id = result['group_id']
            #     LOG.debug(investment_group_id)
            #     create_zero_balance_folio.delay(investment_group_id)

        except Exception as ex:
            LOG.error(ex)
            session.rollback()
            raise HTTPBadRequest(title='Bad request',
                                 description=('Error creating investments. '
                                              '{}'.format(ex)))
        resp.status = HTTP_201


class InvestmentResource(object):
    def on_delete(self, req, resp, investment_group_id):
        """
        DELETE /investment
        """
        if not investment_group_id:
            raise HTTPInvalidParam(msg='investment_group_id is required',
                                   param_name='investment_group_id')

        session = req.context['session']
        customer_id = req.context['customer_id']
        is_valid_group_id = Investment.is_valid_group_id(investment_group_id,
                                                         customer_id,
                                                         session)
        if is_valid_group_id:
            try:
                Investment.delete_group(investment_group_id, customer_id,
                                        session)
                resp.status = HTTP_204
            except ValueError as ex:
                raise HTTPForbidden(title='Bad request',
                                    description='{}'.format(ex))
        else:
            raise HTTPNotFound(title='No such investment group')
