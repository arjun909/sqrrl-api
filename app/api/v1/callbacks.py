# callbacks.py
#
# Handles some third party callbacks.
from falcon import (HTTPBadRequest, uri, HTTPInternalServerError,
                    HTTPInvalidParam)

from app.model import (Customer, StgNachTrxnFeedIn, IntNachTrxnFeedIn)
from app.tasks import (validate_pan, cams_feed_in,
                       karvy_feed_in)
from app.tasks import nach_register_feed_in, ft_feed_in, remote_uri_call
from app.utils.billdesk import Billdesk
from app.utils.helpers import s3_file_upload
from app.log import get_logger
from app.workers import BilldeskWorker

from app.config import (CAMS_DBF_PASS, KARVY_DBF_PASS, FT_DBF_PASS,
                        MAILCHIMP_USERNAME, MAILCHIMP_PASSWORD,
                        CLEVERTAP_UNINSTALL_CAMPAIGNS)

from app.utils import RedisWrapper
from postmark_inbound import PostmarkInbound
from app import constants as C

import re
import json
import requests
import zipfile
import io
from datetime import datetime

from sparkpost import SparkPost
sp = SparkPost()

LOG = get_logger()


class KarvyAadharCallback():
    """
    Handles /callback/ekyc/{success/failure}
    """
    def on_get(self, req, resp, handler):
        # get the kyc_transaction_id so we know which customer's callback
        # this is.
        kyc_transaction_id = req.get_param_as_int('refno')
        is_success = handler.lower() == 'success'
        session = req.context['session']
        try:
            c = Customer.post_ekyc_callback(kyc_transaction_id,
                                            is_success, session)
            if is_success:
                # Fetch the ckyc_id from the ckyc database,
                # using the SOAP API provided.
                # And update the ckyc_id field of the customer table.
                # ckyc_id can be fetched for only those users who have been
                # ckyc-ed through Sqrrl Platform.
                # update_ckyc.delay(c.id)
                RedisWrapper().get_redis_client().\
                    sadd(C.CUSTOMERS_MISSING_CKYC_ID, c.id)

                # we have successfully created a customer's KYC
                # we can update data from PAN database.
                # do this after 1 min to give Karvy database some time
                validate_pan.apply_async((c.uid, c.pan_number),
                                         countdown=60)

        except Exception as ex:
            LOG.error('Aadhaar eKYC callback failed!', exc_info=True, extra={
                'kyc_transaction_id': kyc_transaction_id
            })
            raise HTTPBadRequest(title='Bad request',
                                 description='{}'.format(ex))


class BilldeskCallback(object):
    """
    Handles POST /callback/billdesk
    """
    def on_post(self, req, resp):
        body = req.stream.read().decode()
        params = uri.parse_query_string(body)
        session = req.context['session']
        # LOG.debug('{}'.format(params))
        bd = Billdesk(None, session)
        bd.update_transaction(params['msg'])

    # simply defined to allow GET
    def on_get(self, req, resp):
        pass

    # simply defined to allow HEAD
    def on_head(self, req, resp):
        pass


class PostmarkCallback(object):
    """
    Handled POST /callback/postmark
    POST request contains data/attachments received in the postmark
    inbound email
    """
    def on_post(self, req, resp):

        doc = req.context['doc']
        session = req.context['session']
        label = doc['BccFull'][0]['MailboxHash']
        json_data = json.dumps(doc)

        if label == 'billdesk_mis':
            PostmarkCallback.process_billdesk_mis(session, json_data)
        elif label == 'nach_trxn_feedin':
            PostmarkCallback.process_nach_trxn_feedin(session, json_data)
        else:
            LOG.warn('The postmark callback POST request did not have'
                     ' a matching label. Label - {}'.format(label))

    @staticmethod
    def process_billdesk_mis(session, json_data):
        """
        calls BilldeskWorker's MIS processing functions according to the
        content type and name of attachment.
        """
        LOG.info('Processing billdesk mis')

        inbound = PostmarkInbound(json=json_data)
        if inbound.has_attachments():
            billdesk_obj = BilldeskWorker(session)
            attachments = inbound.attachments()
            for attachment in attachments:
                filename = attachment.name()
                content_type = attachment.content_type()
                attachment.download('/tmp/')
                filepath = '/tmp/' + filename

                if 'SQRRL_UTR_' in filename and\
                        content_type == 'application/vnd.ms-excel':
                    billdesk_obj.process_utr(filepath)
                elif content_type == 'application/octet-stream':
                    suffix = datetime.utcnow().strftime('%Y%m%dT%H%M%S')
                    dir_path = '/tmp/billdesk_mis_pv_number{}'.format(
                                                                suffix)
                    # extract the zipfile to a directory inside /tmp
                    archive = zipfile.ZipFile(filepath)
                    archive.extractall(path=dir_path)
                    billdesk_obj.process_mis_pv_number(dir_path)
                else:
                    LOG.error('Unknown Filename or content type.'
                              ' filename - content type {} - {}'.format(
                                                    filename, content_type))
        else:
            LOG.info('Received billdesk MIS. No attachments were found.')

    @staticmethod
    def process_nach_trxn_feedin(session, json_data):
        """
        The attachment can contain 3 types of files:
        filetype 1: Reverse nach-trxn-feed-out.xls
        Action = Mark Pending Nach transactions as Acknowledged by billdesk
        filetype 2: SQL_Remittance_ddmmyyyy.xls
        Action = Mark Pending Nach transactions as Processing and sent to AMC
                 and update pv_number
        filetype 3: UTR_SQL_Remittance_ddmmyyyy.xls
        Action = Update UTR against the transaction
        """
        LOG.info('Processing NACH transaction feed in')
        inbound = PostmarkInbound(json=json_data)

        if inbound.has_attachments():
            attachments = inbound.attachments()

            for attachment in attachments:
                filename = attachment.name()
                content_type = attachment.content_type()
                attachment.download('/tmp/')
                filepath = '/tmp/' + filename

                if 'Reverse' in filename and\
                        content_type == 'application/vnd.ms-excel':
                    LOG.info('Received {}'.format(filename))
                    StgNachTrxnFeedIn.mark_as_acknowledged(session, filepath)

                elif 'UTR SQL_Remittance_' in filename and\
                        content_type == 'application/vnd.ms-excel':
                    LOG.info('Received {}'.format(filename))
                    StgNachTrxnFeedIn.update_utr(session, filepath)
                    bd_obj = BilldeskWorker(session)
                    bd_obj.email_utr_rows()

                elif 'SQL_Remittance_' in filename and\
                        content_type == 'application/vnd.ms-excel':
                    StgNachTrxnFeedIn.import_data(session, filepath)
                    IntNachTrxnFeedIn.import_stg(session)
                    IntNachTrxnFeedIn.update_transaction(session)
                    LOG.info('NACH trxn feed in from billdesk processed......')

                else:
                    LOG.info('{} filename could not be recognised in'
                             ' Nach feedin'.format(filename))


class BilldeskMisCallback(object):
    """
    Handles POST /callback/billdesk_mis
    POST request contains the zip file having billdesk MIS and PV number
    """
    def on_post(self, req, resp):

        LOG.info('Processing Billdesk MIS.........')

        session = req.context['session']
        bd_obj = BilldeskWorker(session)

        doc = req.context['doc']
        if not doc or 'file' not in doc or not doc['file']:
            raise HTTPBadRequest(title='Bad Request',
                                 description='Invalid JSON'
                                 ' or missing file parameter in JSON body')
        file_link = doc['file']

        if 'title' not in doc or not doc['title']:
            raise HTTPBadRequest(title='Bad Request',
                                 description='Invalid JSON'
                                 ' or missing file parameter in JSON body')
        req_title = doc['title']

        # if file URL found, download the zipfile, response obj is created
        r = requests.get(file_link)

        if 'SQRRL_UTR_' in req_title:
            try:
                feed_file = io.BytesIO(r.content)
                suffix = datetime.utcnow().strftime('%Y%m%d%H%M%S.xls')
                file_path = '/tmp/SQRRL_UTR_{}'.format(suffix)
                with open(file_path, 'wb') as f:
                    f.write(feed_file.read())
            except:
                raise HTTPBadRequest(
                            title='connection refused',
                            description='could not request the file link'
                            )
            bd_obj.process_utr(file_path)
        else:
            try:
                # r.content used to access the bytes
                # io.BytesIO() creates file like obj for bytes
                archive = zipfile.ZipFile(io.BytesIO(r.content))
                zip_file = [member for member in archive.namelist()
                            if '.zip' in member]
                if zip_file:
                    zip_file = zip_file[0]
                else:
                    raise Exception('Link does not have billdesk MIS'
                                    ' attachment or UTR attachment')
                suffix = datetime.utcnow().strftime('%Y%m%dT%H%M%S')
                dir_path = '/tmp/billdesk_mis_utr_{}'.format(suffix)
                archive.extractall(path=dir_path)
                # billdesk_archive_path = filepath + '/' + zip_file
                # billdesk_archive_obj = zipfile.ZipFile(billdesk_archive_path)
                # billdesk_archive_obj.extractall(
                #     path=billdesk_archive_path.split('.zip')[0]
                #     )
            except Exception as ex:
                LOG.info(ex)
                raise HTTPBadRequest(
                            title='connection refused',
                            description='could not request the file link'
                            )
            bd_obj.process_billdesk_mis_pv_number(
                                    dir_path)


class ZapierCallback(object):
    """
    Extracts DBF from the Zapier API response for incoming feed files.
    /callback/zapier
    """
    def on_post(self, req, resp):
        LOG.info('Processing feed in...')
        doc = req.context['doc']
        body = doc['body']
        urls_extracted = re.findall(r'(https?://[^\s]+\.zip)', body)
        if not urls_extracted:
            LOG.warn('No zip download link found, trying FT')
            urls_extracted = re.findall(r'(https?://zapier\.com.*)', body)
            if not urls_extracted:
                LOG.warn('No attachment found either')
                return

        zip_url = urls_extracted[0]
        # LOG.debug('zip_url: {}'.format(zip_url))
        if 'cams' in zip_url:
            password = CAMS_DBF_PASS.encode()
        elif 'karvy' in zip_url:
            password = KARVY_DBF_PASS.encode()
        elif 'zapier' in zip_url:
            password = FT_DBF_PASS.encode()
        else:
            LOG.warn('Zip file url unrecognized')
            return

        try:
            r = requests.get(zip_url)
        except requests.exceptions.RequestException as e:
            LOG.error(e)
        try:
            z = zipfile.ZipFile(io.BytesIO(r.content))
        except zipfile.BadZipFile as e:
            raise HTTPBadRequest(
                title='Bad request',
                description=('Bad Zipfile found at {}. File is not a '
                             ' Zipfile'.format(zip_url)))

        filename = list(z.NameToInfo.keys())[0]
        filepath = ''.join(['/tmp/', filename])
        LOG.debug('extracting to: {}'.format(filepath))
        z.extractall(path='/tmp', pwd=password)

        # upload to s3
        key = 'tmp/{}'.format(filename)
        s3_file_upload(key, filepath)

        # kick off tasks
        if 'cams' in zip_url:
            cams_feed_in.delay(key, filepath, doc['feed_type'])
        elif 'karvy' in zip_url:
            karvy_feed_in.delay(key, filepath, doc['feed_type'])
        else:
            ft_feed_in.delay(key, filepath)
        LOG.info('Finished processing feed in.')


class KarvyRejectionCallback(object):

    def on_post(self, req, resp):
        """
        Handles POST /callback/karvy_rejection via Zapier
        Downloads the provided GDrive file, processes those transactions.
        """
        LOG.info('Processing Karvy *rejected* transactions...')
        doc = req.context['doc']
        # LOG.debug(doc)
        f_link = doc['file']
        f_name = doc['title']
        if not f_name:
            LOG.warn('No file provided in /callback/karvy_rejection')
            return

        try:
            # download the provided file to /tmp
            r = requests.get(f_link)
            suffix = datetime.utcnow().strftime(r'%Y%m%dT%H%M%S.dbf')
            file_path = '/tmp/karvy_rejection_{}'.format(suffix)
            with open(file_path, 'wb') as f:
                f.write(r.content)
            # upload the file_path to s3
            key = file_path[1:]
            s3_file_upload(key, file_path)

            # kick-off worker
            karvy_feed_in.delay(key, file_path, doc['feed_type'])
        except requests.exceptions.RequestException as e:
            LOG.error(e, exc_info=True)

        LOG.info('Finished processing Karvy rejected transactions')


class EmailVerificationCallback(object):
    def on_get(self, req, resp):
        """
        Called when a customer hits the verification email. On successful POST
        request, is_email_verified is updated.
        """
        token = req.get_param('token')
        session = req.context['session']
        c = session.query(Customer).filter(Customer.email_verification_token ==
                                           token).first()
        try:
            c.is_email_verified = True
            session.add(c)
            session.commit()
            # Add the customer to the weekly newsletter Mailchimp list

            try:
                remote_uri_call(C.MAILCHIMP_ADD_NEW_MEMBER_URL,
                                method='POST',
                                payload={
                                    'email_address': c.email,
                                    'status': 'subscribed',
                                    'merge_fields': {
                                        'MMERGE3': c.uid,
                                        'MMERGE4': (c.pan_name if c.pan_name
                                                    else c.name),
                                        'MMERGE5': c.mobile,
                                        'MMERGE6': c.id
                                    }
                                },
                                auth=(MAILCHIMP_USERNAME, MAILCHIMP_PASSWORD))

            except Exception as e:
                LOG.warn("Customer(uid={}) could not be added to the Mailchimp"
                         " list of Sqrrl Customers due to exception: {}".
                         format(c.uid, e), exc_info=True)

            resp.content_type = 'text/html'
            # req.context['result'] = 'Thank you for verifying your email.'
            resp.body = '''
            <html>
            <head>
                <title>Sqrrl: Email is verified</title>
                <meta http-equiv="refresh" content="2;url=http://l.sqrrl.in/email_verified" />
            </head>
            <body>
                <p>Thank you for verifying your email.
                You will be redirected back to the app.</p>
            </body>
            </html>
            '''
        except Exception as ex:
            LOG.error('Could not verify email with token={}'.format(token),
                      exc_info=True, extra={'token': token})

            raise HTTPBadRequest(title='Bad request',
                                 description=('The requested URL does not exist.\
                                               Please check the URL'))


class NachRegisterFeedinCallback(object):

    def on_post(self, req, resp):
        """
        Handles POST /callback/nach_register_feedin via Zapier
        Called when operations@sqrrl.in receives email from billdesk
        containing Feedin file for NACH registrations.
        """
        LOG.info('Processing NACH registration Feed IN......')
        doc = req.context['doc']
        # LOG.debug(doc)
        if not doc or 'file' not in doc:
            raise HTTPBadRequest(title='Bad request',
                                 description='Invalid json or missing file '
                                 'parameter in the json')
        file_link = doc['file']
        if not file_link:
            raise Exception(
                'No file provided in /callback/nach_register_feedin')
        try:
            # download the provided file to /tmp
            r = requests.get(file_link)
            feed_file = io.BytesIO(r.content)
            suffix = datetime.utcnow().strftime('%Y%m%dT%H%M%S.xls')
            file_path = '/tmp/nach_register_feedin_{}'.format(suffix)
            with open(file_path, 'wb') as f:
                f.write(feed_file.read())
            # upload the file_path to s3
            key = file_path[1:]
            try:
                s3_file_upload(key, file_path)
            except Exception as ex:
                LOG.error('S3 upload error', exc_info=True)
                raise HTTPInternalServerError(
                    title='S3 upload failed',
                    description='Could not process NACH feed in'
                )

            # kick-off worker
            nach_register_feed_in.delay(key, file_path)
        except requests.exceptions.RequestException as e:
            LOG.error(e, exc_info=True)
        LOG.info('Finished processing NACH transactions Feed In')


class NachTrxnFeedinCallback(object):

    def on_post(self, req, resp):
        """
        Handles POST /callback/nach_trxn_feedin via Zapier
        Zapier sends API request to the URL when operations@sqrrl.in receives
        email from Karvy containing feed in file.
        """
        LOG.info('Processing Feed IN for  NACH transactions......')
        doc = req.context['doc']
        session = req.context['session']
        # LOG.debug(doc)
        if not doc or 'file' not in doc:
            raise HTTPBadRequest(title='Bad request',
                                 description='Invalid json or missing file '
                                 'parameter in the json')
        file_link = doc['file']
        if not file_link:
            raise Exception(
                'No file URL provided in /callback/nach_trxn_feedin')

        # if file URL found, download the zipfile, response obj is created
        try:
            r = requests.get(file_link)
            # r.content used to access the bytes
            # io.BytesIO() creates file like obj for bytes
            archive = zipfile.ZipFile(io.BytesIO(r.content))
        except Exception as ex:
            raise HTTPBadRequest(title='Bad request',
                                 description='The URL could not be processed')

        feed_file = [member for member in archive.namelist()
                     if '.xls' in member]
        if feed_file:
            # convert list to string
            feed_file = feed_file[0]
            has_feed_file = True

        if has_feed_file:
            try:
                r = requests.get(file_link)
                dir_path = '/tmp/nach_trxn_feedin'
                archive.extract(feed_file, path=dir_path)
                file_path = dir_path + '/' + feed_file
            except:
                raise HTTPBadRequest(
                            title='connection refused',
                            description='could not request the file link'
                            )
            StgNachTrxnFeedIn.import_data(session, file_path)
            IntNachTrxnFeedIn.import_stg(session)
            IntNachTrxnFeedIn.update_transaction(session)
        LOG.info('NACH trxn feed in from billdesk processed...........')


class ClevertapWebhook():
    """
    Clevertap webhook resource
    """
    def on_post(self, req, resp):
        doc = req.context['doc']
        if 'is_test' in doc and doc['is_test']:
            return

        campaign_id = doc.get('targetId', '')
        if not campaign_id:
            raise HTTPInvalidParam(param_name='targetId', msg='')

        if str(campaign_id) not in CLEVERTAP_UNINSTALL_CAMPAIGNS:
            LOG.warn('Invalid targetId', extra={'data': campaign_id})
            return

        LOG.info('Updating uninstall date', extra={'data': campaign_id})
        db = req.context['session']
        Customer.update_uninstalls(doc, db)
        req.context['result'] = {'success': True}
