# app/api/v1/user_actions.py
# a handler for generic user actions, e.g., switch funds.
from falcon import HTTPInvalidParam, uri, HTTPTemporaryRedirect

from app.model import Customer, CustomerAction
from app.log import get_logger
from app.config import USER_ACTION_DEFAULT_CALLBACK
LOG = get_logger()


class UserActionResource():
    """
    We can use /u/{action} to define arbitrary user actions like switch
    funds, etc.
    """
    # can use this to filter the url
    allowed_actions = ('switch')

    def on_get(self, req, resp, action):
        """
        GET /u/{action}
        """
        if action not in self.allowed_actions:
            raise HTTPInvalidParam(msg='Invalid parameter',
                                   param_name='action')

        # all /u/{action} calls must include a valid uid
        uid = req.get_param('uid', required=True)
        session = req.context['session']
        customer = session.query(Customer).filter(Customer.uid == uid).first()
        if not customer:
            raise HTTPInvalidParam(msg='No such customer',
                                   param_name='uid')

        if action == 'switch':
            self.handle_switch(customer, action, req)

    def handle_switch(self, customer, action, req):
        """
        Handles switch actions
        """
        # get IP address
        session = req.context['session']
        ip_address = req.access_route[-1].strip()

        # parse query parameters into a dict
        # TODO: upgrade falcon to 1.2.0 and test thoroughly before uncommenting
        # params = uri.parse_query_string(req.query_string, parse_qs_csv=False)
        params = uri.parse_query_string(req.query_string)

        # where to redirect
        cb = params.pop('cb')
        if not cb:
            cb = USER_ACTION_DEFAULT_CALLBACK

        # create a new row
        row = CustomerAction(
            customer_id=customer.id,
            action=action.lower(),
            ip_address=ip_address,
            params=params
        )
        session.add(row)
        session.commit()

        # redirect to callback
        raise HTTPTemporaryRedirect(cb)
