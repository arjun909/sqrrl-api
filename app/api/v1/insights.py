# insights.py
# handles /insights/* API calls
import asyncio

from falcon import HTTPInvalidParam, HTTPBadRequest, HTTP_204
from cerberus import Validator

from app.model import CustomerSms
from app import log
LOG = log.get_logger()

schema = {
    'mobile': {'type': 'string', 'regex': '\d{10}', 'required': True},
    'texts': {
        'required': True,
        'type': 'list',
        'schema': {
            'type': 'dict',
            'schema': {
                'sender_code': {'type': 'string', 'required': True},
                'body': {'type': 'string', 'required': True}
            }
        }
    }
}


class FinancialInsights(object):
    def on_post(self, req, res):
        """
        Parse incoming request - and push into CustomerSms model asynchronously
        """
        # validate input
        doc = req.context['doc']

        try:
            v = Validator(schema)
            if not v.validate(doc):
                # LOG.info('errors %s' % v.errors)
                if 'mobile' in v.errors:
                    raise HTTPInvalidParam(param_name='mobile',
                                           msg='Should be a 10 digit number')
                else:
                    raise HTTPInvalidParam(param_name='texts',
                                           msg=('Should be an array of texts'
                                                ' with sender_code and body'))
        except Exception:
            raise HTTPBadRequest(title='Bad request',
                                 description='Error validating request')

        # asynchronously process mobile text
        task = process_sms(doc, req.context['session'])
        loop = asyncio.get_event_loop()
        loop.run_until_complete(task)
        res.status = HTTP_204


async def process_sms(doc, db_session):
    """
    An asynchronous process to transfer sms data to db.
    """
    # LOG.info('[async] Processing sms: %s' % doc['mobile'])
    for item in doc['texts']:
        cs = CustomerSms()
        cs.mobile = doc['mobile']
        cs.raw_text = item['body']
        cs.sms_shortcode = item['sender_code']
        db_session.add(cs)
    db_session.commit()
