# attachments_resource.py
import boto3

from app.model import Customer
from app import config
from app.log import get_logger
LOG = get_logger()


class AttachmentsResource(object):
    """
    Handles uploading of customer attachments.
    """

    def on_patch(self, req, resp):
        """
        PATCH /customer/me/attachments

        Using S3 for actual file uploads. This action simply creates
        placeholders and direct upload links to the right S3 locations.
        """
        s3 = boto3.client('s3', config.AWS_REGION)
        result = {}
        customer_uid = req.context['customer_uid']
        doc = req.context['doc']
        attachments = {}
        # LOG.debug('keys: {}'.format(doc['keys']))

        # right now hard-coding what attachments need to be uploaded.
        # later we should get these in parameters
        # here we generate 'pre-signed' POST urls that allow clients
        # to directly upload their files to the right destination
        # ref: http://boto3.readthedocs.io/en/latest/reference/services/s3.html#S3.Client.generate_presigned_post  # NOQA
        for k in doc['keys']:
            key = 'customer-{}/{}'.format(customer_uid, k)
            # LOG.debug('key: {}'.format(key))
            # generates a url with fields that can be used to POST files
            # TODO: Add conditions to restrict file size, mime types
            post = s3.generate_presigned_post(config.ATTACHMENT_BUCKET, key)
            result[k] = post
            # in the db store bucket+key - that will help us identify
            # the object and download it later.
            attachments[k] = '{}+{}'.format(config.ATTACHMENT_BUCKET, key)

        # save to db.
        # NOTE: existing attachment files are always overwritten
        # this may be changed later
        session = req.context['session']
        Customer.update_attachments(customer_uid, attachments, session)

        # set json response
        req.context['result'] = result
