# scheme_resource.py
from falcon import HTTPNotFound, HTTPInvalidParam
from app.model import Scheme
from app.log import get_logger

LOG = get_logger()


class SchemeResource(object):
    """
    Scheme resource for GET /schemes
    """
    def on_get(self, req, resp, scheme_id):
        # returns mandatory fields for every scheme
        session = req.context['session']
        try:
            req.context['result'] = Scheme.get_details(session,
                                                       scheme_id=scheme_id)
        except Exception as ex:
            LOG.error(ex)
            # treat it as 404
            raise HTTPNotFound(title='Scheme not found',
                               description='Could not find scheme with id {}'.
                               format(scheme_id))


class SchemeListResource(object):
    """
    Scheme list for /schemes?id=1,2,3
    """
    def on_get(self, req, resp):
        session = req.context['session']
        # get the ids from query params
        # LOG.debug('params {}'.format(req.params))
        ids = req.get_param_as_list('id')
        # LOG.debug('ids: {}'.format(ids))
        result = []
        if ids:
            try:
                scheme_ids = [int(s) for s in ids]
                # LOG.debug('scheme_ids: {}'.format(scheme_ids))
            except ValueError:
                # Non integers passed.
                # one more attempt
                scheme_ids = [int(s) for s in req.get_param('id').split(',')]
            except Exception:
                raise HTTPInvalidParam(msg='Bad ids {}'.
                                           format(req.get_param('id')),
                                       param_name='id')
            if scheme_ids:
                result = Scheme.get_details(session, scheme_ids=scheme_ids)
                req.context['result'] = result
                return
        else:
            # return all rows
            result = Scheme.get_details(session)
            req.context['result'] = result
            return
