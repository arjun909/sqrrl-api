# flake8: noqa
from .insights import FinancialInsights
from .auth import Authorization
from .customer_resource import CustomerResource, CustomerDashboardResource
from .product_resource import ProductResource, ProductRecommendationResource
from .investment_resource import InvestmentResource, InvestmentListResource
from .scheme_resource import SchemeResource, SchemeListResource
from .tips_resource import TipsResource
from .job_resource import JobResource
from .transaction_resource import TransactionResource, TransactionListResource
from .attachments_resource import AttachmentsResource
from .callbacks import (KarvyAadharCallback, BilldeskCallback, ZapierCallback,
KarvyRejectionCallback, EmailVerificationCallback, NachTrxnFeedinCallback,
NachRegisterFeedinCallback, PostmarkCallback, ClevertapWebhook)
from .redirection import RedirectionResource
from .user_actions import UserActionResource

from .billdesk_dummy import BilldeskDummyResource
from .referral_codes_resource import ReferralCodesResource
from .admin import AdminResource
from .bank_resource import BankResource
