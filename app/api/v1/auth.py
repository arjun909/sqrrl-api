# auth.py
# Authentication - uses third-party OTP service to verify mobile number.
# Authorization - based on Json Web Tokens (JWT)

from datetime import datetime
from time import time
from jose import jwt
import requests
import redis
from falcon import before
from falcon import HTTPInvalidParam, HTTPBadRequest
from falcon import HTTP_UNAUTHORIZED, HTTP_204, HTTP_201
from cerberus import Validator

from app.tasks import send_otp
from app.model import Customer
from app.config import JWT_SALT, OTP_APPLICATION_KEY, OTP_VERIFY_URL, REDIS_URL
from app.config import DEMO_ACCOUNTS
from app.constants import BLACKLIST
from app import log
LOG = log.get_logger()


# validate incoming request object
def validate_auth_input(req, resp, resource, params):
    """
    Input should be of the form { "mobile": "9988776655", "otp": "1234" }
    "otp" is optional.
    """
    doc = req.context['doc']
    schema = {
        'mobile': {'type': 'string', 'required': True, 'regex': '\d{10}'},
        'otp': {'type': 'string', 'regex': '\d{4}'}
    }
    # validate input
    try:
        v = Validator(schema)
        if not v.validate(doc):
            if 'm' in v.errors:
                raise HTTPInvalidParam(param_name='mobile',
                                       msg='Should be a 10 digit number')
            else:
                raise HTTPInvalidParam(param_name='otp',
                                       msg='Should be a 4 digit number')
    except Exception:
        raise HTTPBadRequest(title='Bad request',
                             description='Error validating request')


class Authorization(object):
    """
    Handles /v1/auth endpoint requests
    """
    @before(validate_auth_input)
    def on_post(self, req, res):
        """
        If only "mobile" is provided, send an OTP to verify mobile number.
        Else, generate JWT token and send it back.
        """
        doc = req.context['doc']
        mobile = doc['mobile']
        session = req.context['session']
        if DEMO_ACCOUNTS and mobile in DEMO_ACCOUNTS:
            # if mobile is demo account mobile then send customer profile and
            # response code 201 for both the POST(auth) ie
            # auth[mobile] and auth[mobile + OTP]
            LOG.debug('[/auth] Demo account')
            # get client's IP address
            # ref: http://stackoverflow.com/a/7839576
            # ref: http://stackoverflow.com/a/22936947
            client_ip = req.access_route[-1].strip()
            uid, token = self.generate_token(mobile,
                                             session,
                                             client_ip)
            # include basic profile in the response
            customer = Customer.get_profile_for_uid(uid, session)
            req.context['result'] = {
                'token': token,
                'customer': customer
            }
            res.status = HTTP_201
        else:
            if 'otp' not in doc:
                # send otp
                LOG.debug('[/auth] Sending OTP')
                send_otp.delay(mobile)
                res.status = HTTP_204
            else:
                # otp included, verify it.
                LOG.debug('[/auth] Verifying OTP')
                # verify otp, and inspect result to see if it was valid.
                # NOTE: using a third-party call here, this API might be a
                # little slow. It can't be helped however, as we need to check
                # validity of mobile number.
                result = self.verify_otp(mobile, doc['otp'])
                response_code = result['response']['code']
                if result['status'] == 'success' and\
                        response_code == 'NUMBER_VERIFIED_SUCCESSFULLY':
                    # get client's IP address
                    # ref: http://stackoverflow.com/a/7839576
                    # ref: http://stackoverflow.com/a/22936947
                    client_ip = req.access_route[-1].strip()
                    # LOG.debug('IP address: {}'.format(client_ip))
                    uid, token = self.generate_token(mobile,
                                                     session,
                                                     client_ip)
                    # include basic profile in the response
                    customer = Customer.get_profile_for_uid(uid, session)
                    req.context['result'] = {
                        'token': token,
                        'customer': customer
                    }
                    res.status = HTTP_201
                else:
                    # OTP was probably invalid.
                    req.context['result'] = {
                        'error_code': 'UNAUTHORIZED',
                        'error_description': 'Invalid OTP, mobile not verified.'    # NOQA
                    }
                    res.status = HTTP_UNAUTHORIZED

    def generate_token(self, mobile, session, client_ip):
        """
        Check if mobile exists in db - and then generate a JWT
        token with user uid
        """
        # check if this mobile is already in customer table
        c = session.query(Customer).filter(Customer.mobile == mobile).first()
        is_new = False
        if not c:
            # create a new customer entry
            # LOG.debug('Creating new customer - mobile {}'.format(mobile))
            c = Customer(mobile=mobile, mobile_verified=True)
            session.add(c)
            is_new = True

        # update last_login_on, mobile_verified
        c.last_login_on = datetime.utcnow()
        if not c.ip_address:
            c.ip_address = client_ip
        if not c.mobile_verified:
            c.mobile_verified = True
        session.commit()
        session.refresh(c)

        if is_new:
            c.construct_customer_attribution_code(session)

        # at this point, c contains customer record
        # encode customer uid and generate JWT token
        # iat - issued at - use Unix time in seconds (int)
        payload = {'user': c.uid, 'iat': int(time())}
        token = jwt.encode(payload, JWT_SALT)
        return c.uid, token

    def verify_otp(self, mobile, otp):
        """
        verifies mobile number and otp using third-party service
        """
        # LOG.debug('[verify_otp] %s' % mobile)
        # ref: https://sendotp.msg91.com/doc
        headers = {'application-Key': OTP_APPLICATION_KEY}
        data = {
            'countryCode': '91',
            'mobileNumber': mobile,
            'oneTimePassword': otp
        }
        r = requests.post(OTP_VERIFY_URL,
                          headers=headers,
                          json=data).json()
        return r

    def on_delete(self, req, res):
        """
        DELETE /auth - logs a user out.
        """
        # extract the token from the Authorization header
        token = None
        try:
            token = req.get_header('Authorization').split('Bearer ')[1]
        except Exception as ex:
            LOG.error(ex)
            raise HTTPBadRequest(title='Bad request',
                                 description='Could not extract token '
                                             'from Authorization header')
        if token:
            # push this token to the blacklist of tokens in redis.
            rdb = redis.from_url(REDIS_URL)
            rdb.sadd(BLACKLIST, token)
            res.status = HTTP_204
