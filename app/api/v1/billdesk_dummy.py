from falcon import uri, HTTP_200, HTTP_400, HTTP_401, HTTP_307, HTTPMovedPermanently
import hmac
from hashlib import sha256
from app import config
from distutils.util import strtobool
from app.log import get_logger
from base64 import b64encode
from os import urandom
from app.tasks import remote_uri_call


LOG = get_logger()


class BilldeskDummyResource():

    notes = {
        "0300": 'Success',
        "0399": 'Invalid Authentication at Bank',
        "NA": 'Invalid Input in the Request Message',
        "0002": 'BillDesk is waiting for Response from Bank',
        "0001": 'Error at BillDesk'
    }

    def on_post(self, req, resp):
        LOG.info("Transaction request received by Billdesk dummy")

        req_body = req.stream.read().decode()
        req_params = uri.parse_query_string(req_body)

        msg = req_params.get('msg', None)
        if not msg:
            resp.status = HTTP_400
            resp.content_type = 'text/plain'
            resp.body = "Bad Request: Request does not contains msg parameter. Cannot process this request."
            return resp

        hidRequestId = req_params.get('hidRequestId', None)
        hidOperation = req_params.get('hidOperation', None)

        LOG.info("Ignoring the validation of the values of "
                 "hidRequestId={hidRequestId} and hidOperation={hidOperation}".
                 format(hidRequestId=hidRequestId, hidOperation=hidOperation))

        # Additional parameter to reject a transaction explicitly.
        transaction_reject = strtobool(req_params.get('trxreject', 'False'))
        transaction_reject_reason = req_params.get('transaction_reject_reason', '0001')

        # Tokens from the msg
        msg_tokens = BilldeskDummyResource.parse_msg(msg)

        # There should be 23 pipe-separated arguments in the msg value,
        # including checksum.
        if len(msg_tokens) != 23:
            resp.status = HTTP_400
            resp.content_type = 'text/plain'
            resp.body = "Bad Request: Request not formatted properly according to defined rules." \
                        "Some parameters may be missing."
            return resp

        biller_id = msg_tokens[0]
        additional_info1 = msg_tokens[1]
        txtMerchantUserRefNo = msg_tokens[2]
        txn_amount = msg_tokens[3]
        txtBankID = msg_tokens[4]
        currency = msg_tokens[7]
        billerid_lower = msg_tokens[10]
        additional_info2 = msg_tokens[14]
        additional_info3 = msg_tokens[15]
        additional_info4 = msg_tokens[16]
        additional_info5 = msg_tokens[17]
        additional_info6 = msg_tokens[18]

        _temp_ = msg_tokens[19]
        _temp_tokens = _temp_.split('-')

        additional_info7 = _temp_tokens[0]
        additional_info9 = _temp_tokens[1]
        additional_info10 = _temp_tokens[2]
        additional_info11 = _temp_tokens[3]
        additional_info12 = _temp_tokens[4]
        sourceid = _temp_tokens[5]

        additional_info8 = msg_tokens[20]
        RU = msg_tokens[21]

        # checksum_received = msg_tokens[22]
        checksum_received = msg_tokens[-1]

        msg_checksum = BilldeskDummyResource.calculate_checksum(msg_tokens)

        if checksum_received != msg_checksum:
            resp.status = HTTP_400
            resp.content_type = 'text/plain'
            resp.body = "Bad Request: Checksum didn't match. Request may have been modified or " \
                        "your key has changed."
            return resp

        if BilldeskDummyResource.is_biller_id_invalid(biller_id, billerid_lower):
            resp.status = HTTP_401
            resp.content_type = 'text/plain'
            resp.body = "Unauthorized: Merchant ID is invalid. " \
                        "Permission denied for further processing."
            return resp

        additional_info8_tokens = additional_info8.split('-')
        txn_datetime = additional_info8_tokens[0]

        # if txn_amount != sum([float(t) for t in additional_info8_tokens[1:]
        #                       if t != 'NA']):
        #     resp.status = HTTP_401
        #     resp.content_type = 'text/plain'
        #     resp.body = "Unauthorized: Transaction amount does not match."
        #     return resp

        resp.status = HTTP_307
        resp.content_type = 'text/plain'
        resp.location = RU
        resp.set_header('location', RU)

        if transaction_reject:
            resp.body = "Request received. Transaction has been rejected by the bank. " \
                        "Wait for redirection."
        else:
            resp.body = "Request received. Transaction was successful, under processing now. " \
                        "Redirecting now."

        # TODO: Submit for server-to-server POST over the RU, after 1-2 sec.
        transaction_response = \
            BilldeskDummyResource.construct_response(biller_id, txtMerchantUserRefNo,
                                                     txn_amount, txtBankID, currency,
                                                     txn_datetime, additional_info1,
                                                     additional_info2, additional_info3,
                                                     additional_info4, transaction_reject,
                                                     transaction_reject_reason)

        remote_uri_call.apply_async(countdown=2,
                                    expires=300,
                                    priority=0,
                                    kwargs={
                                        'url': RU,
                                        'method': 'POST',
                                        'form_data': {
                                            'msg': transaction_response
                                        },
                                        'ignore_response': True
                                    })
        LOG.info("Submitted for Server-to-Server call to inform about transaction status.")

        raise HTTPMovedPermanently(RU)

    @staticmethod
    def calculate_checksum(tokens, consider_last=False):
        key = config.BILLDESK_CHECKSUM_KEY.encode('UTF-8')
        return hmac.new(key, '|'.join(tokens if consider_last else tokens[:-1]).
                        encode('UTF-8'),
                        digestmod=sha256).hexdigest().upper()


    @staticmethod
    def parse_msg(msg, split_character='|'):
        return msg.split(split_character)


    @staticmethod
    def is_biller_id_invalid(id, id_lower):
        return ((id != config.BILLDESK_MERCHANTID.upper()) or \
               (id_lower != config.BILLDESK_MERCHANTID.lower()))


    @staticmethod
    def construct_response(biller_id, txtMerchantUserRefNo, txn_amount, txtBankID,
                           currency, txn_datetime, additional_info1, additional_info2,
                           additional_info3, additional_info4,
                           transaction_reject=False, transaction_reject_reason='0001'):

        auth_status = transaction_reject_reason if transaction_reject else '0300'

        msg = {
            "biller_id": str(biller_id),
            "transaction_reference_id": str(additional_info1),
            "pg_transaction_id": 'DUMMY-' +
                                 additional_info1 + "-" +
                                 b64encode(urandom(4))\
                                     .decode('utf-8'),
            "bank_transaction_id":'DUMMY-' + txtBankID +
                                  b64encode(urandom(6))\
                                      .decode('utf-8'),
            "transaction_amount": txn_amount,
            "bank_id": txtBankID,
            "bank_merchant_id": txtMerchantUserRefNo,
            "txn_type": 'NA',
            "currency_name": str(currency).upper(),
            "item_code": 'NA',
            "security_type": 'NA',
            "security_id": 'NA',
            "security_password": 'NA',
            "txn_date": txn_datetime,
            "auth_status": auth_status,
            "settlement_type": 'NA',
            "additional_info1": 'NA',
            "additional_info2": additional_info2,
            "additional_info3": additional_info3,
            "additional_info4": additional_info4,
            "additional_info5": 'NA',
            "additional_info6": 'NA',
            "additional_info7": 'NA',
            "error_status": 'NA',
            "error_description": BilldeskDummyResource.notes.get(auth_status,
                                                                 'Unrecognized operation')
        }

        formatted_msg = "{biller_id}|{transaction_reference_id}|" \
                          "{pg_transaction_id}|{bank_transaction_id}|" \
                          "{transaction_amount}|{bank_id}|{bank_merchant_id}|" \
                          "{txn_type}|{currency_name}|{item_code}|{security_type}|" \
                          "{security_id}|{security_password}|{txn_date}|{auth_status}|" \
                          "{settlement_type}|{additional_info1}|{additional_info2}|" \
                          "{additional_info3}|{additional_info4}|{additional_info5}|" \
                          "{additional_info6}|{additional_info7}|{error_status}|" \
                          "{error_description}".format(**msg)

        msg["checksum"] = BilldeskDummyResource.\
            calculate_checksum(list(BilldeskDummyResource.
                                    parse_msg(formatted_msg)), consider_last=True)

        return formatted_msg + "|" + msg["checksum"]
