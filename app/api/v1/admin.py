# app/api/v1/admin.py
import falcon

from app.tasks import (
    fetch_daily_nav, update_bank_from_ifsc, refresh_pan_data
)
from app.constants import AdminAction
from app.log import get_logger
LOG = get_logger()


class AdminResource():
    """
    Contains admin actions, most of which are wrappers on top of celery tasks
    in tasks.py
    """
    def on_post(self, req, resp):
        """
        POST /v1/admin sends an action to be executed by the admin
        """
        # db = req.context['session']
        doc = req.context['doc']
        action = doc.get('action')
        if not action:
            raise falcon.HTTPInvalidParam(
                msg="It cannot be empty.", param_name="action"
            )
        params = doc.get('params', {})
        try:
            self.handle_action(action, params)
            req.context['result'] = {
                'success': '{} sent to queue'.format(action)
            }
        except Exception as ex:
            LOG.error('handle_action failed', exc_info=True, extra={
                'data': doc
            })
            req.context['result'] = {'error': '{} failed'.format(action)}
            resp.status = falcon.HTTP_BAD_REQUEST

    def handle_action(self, action, params):
        """
        How to handle a certain action
        """
        LOG.debug('action: {}, params: {}'.format(action, params))
        # LOG.debug('AdminAction: {}'.format(AdminAction.UPDATE_NAV))
        if action == AdminAction.UPDATE_NAV.value:
            fetch_daily_nav.delay()
        elif action == AdminAction.UPDATE_BANK.value:
            update_bank_from_ifsc.delay(**params)
        elif action == AdminAction.REFRESH_PAN_DATA.value:
            refresh_pan_data.delay(**params)
