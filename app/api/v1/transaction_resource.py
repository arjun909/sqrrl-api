# transaction_resource.py
from falcon import (HTTPNotFound, HTTP_202, HTTPForbidden, HTTP_200,
                    before, HTTPBadRequest, HTTPInvalidParam)
from cerberus import Validator

from app.model import Investment, Transaction, Customer
from app.constants import (TR_STATUS_PROCESSING, TR_STATUS_FAILURE,
                           TR_STATUS_INITIATED, NACH,
                           NACH_REGISTRATION_SUCCESS,
                           LIQUID, INVEST, REDEEM, REDIS_BD_LOCK_PREFIX,
                           PAYMENT_MODE_NB, SETTINGS_K_PAYMENT_PREF,
                           PAYMENT_MODE_RUPAY, PAYMENT_MODE_DEBIT)
from app.utils.helpers import errors_to_desc
from app.tasks import (get_billdesk_transaction_url, rmm_redeem,
                       update_rmm_balance, get_rmm_transaction_status)
from app.utils.reliance import RelianceMoneyManager
from app import config
from app.utils.billdesk import Billdesk
from app.log import get_logger
LOG = get_logger()


def validate_investment_ownership(req, resp, params):
    """
    For a given investment_group_id, checks if the logged in user
    has access.
    """
    session = req.context['session']
    customer_id = req.context['customer_id']
    investment_group_id = params.get('investment_group_id')
    rows = Investment.fetch(investment_group_id, customer_id, session)
    if not rows:
        raise HTTPNotFound(title='Invalid investment group id {}'.
                           format(investment_group_id))
    else:
        req.context['investments'] = rows

    # customer must be kyc-ok for purchase in investment
    # i.e., Customer's FATCA and bank_info should be up-to-date
    customer = session.query(Customer).get(customer_id)
    if not customer.ok_for_investment():
        raise HTTPForbidden(title='Cannot complete transaction',
                            description='Customer registration is incomplete')


def validate_transaction_ownership(req, resp, params):
    """
    Validate this transaction is owned by current user
    """
    session = req.context['session']
    customer_id = req.context['customer_id']
    transaction_group_id = params.get('transaction_group_id')

    # check if the transaction_group_id belongs to this customer
    if not Transaction.belongs_to_customer(customer_id,
                                           transaction_group_id,
                                           session):
        raise HTTPForbidden(title='Transaction group invalid',
                            description='Only own transactions allowed')


def validate_patch_input(req, resp, resource, params):
    """
    Validates input for updating a transaction
    """
    schema = {
        'reference': {'type': 'string', 'empty': False},
        'status': {
            'type': 'string',
            'allowed': [
                TR_STATUS_PROCESSING, TR_STATUS_FAILURE
            ]
        },
        'bank_transaction_id': {'type': 'string', 'empty': False},
        'note': {'type': 'string'}
    }
    # validate input
    v = Validator(schema)
    # TODO: Make errors go back better
    if not v.validate(req.context['doc']):
        raise HTTPBadRequest(title='Invalid parameters',
                             description=errors_to_desc(v.errors))


def validate_post_input(req, resp, resource, params):
    """
    Validates input for POST /transactions call
    """
    schema = {
        'transaction_type': {
            'required': True,
            'type': 'string',
            'allowed': [INVEST, REDEEM]
        },
        'amount': {
            'required': True,
            'type': 'integer',
            'min': 100
        },
        'payment_mode': {
            'type': 'string',
            'allowed': [
                '', PAYMENT_MODE_NB, PAYMENT_MODE_RUPAY, PAYMENT_MODE_DEBIT,
                NACH
            ],
        }
    }
    # validate input
    v = Validator(schema)
    if not v.validate(req.context['doc']):
        raise HTTPBadRequest(title='Invalid parameters',
                             description=errors_to_desc(v.errors))


class TransactionListResource():

    @before(validate_investment_ownership)
    @before(validate_post_input)
    def on_post(self, req, resp, investment_group_id):
        """
        List of transactions
        POST /investment/investment_group_id/transactions
        """
        doc = req.context['doc']
        session = req.context['session']
        amount = doc['amount']
        transaction_type = doc['transaction_type']
        customer_uid = req.context['customer_uid']
        customer_id = req.context['customer_id']
        investment_rows = req.context['investments']
        product = investment_rows[0].product
        record = session.query(Customer).get(customer_id)
        if record:
            # If it's a demo account then transactions are not allowed
            mobile = record.mobile
            if config.DEMO_ACCOUNTS and mobile in config.DEMO_ACCOUNTS:
                raise HTTPForbidden(
                        title='Transactions forbidden for Demo Accounts',
                        description='The city of Gotham is not for nice people'
                                    + ', you are forbidden')

        # payment_mode - netbanking, debit or rupay card.
        payment_mode = doc.get('payment_mode')
        if not payment_mode:
            LOG.debug('no payment_mode provided')
            # if not provided as a param, use customer settings
            # default is netbanking for backward compatibility
            payment_mode = record.settings.get(SETTINGS_K_PAYMENT_PREF,
                                               PAYMENT_MODE_NB)

        if (transaction_type == INVEST and
            record and
            payment_mode == NACH and
                record.nach_mandate_status != NACH_REGISTRATION_SUCCESS):
            raise HTTPForbidden(
                    title='NACH transactions not enabled',
                    description='Transactions through NACH are not allowed for'
                                + ' this account'
                )

        LOG.debug('payment_mode {}'.format(payment_mode))
        # initiate a new transaction
        client_ip = req.access_route[-1].strip()
        try:
            tr_group_id, tr_status, tr_mode = Transaction.initiate(
                                                        investment_group_id,
                                                        amount,
                                                        transaction_type,
                                                        session, client_ip,
                                                        payment_mode)
        except ValueError as ex:
            raise HTTPBadRequest(title='Bad Request', description=str(ex))
        # INVEST

        # nach transctions have status: initiated and mode: NACH
        if transaction_type == INVEST:
            # this code executes for non NACH transactions
            if tr_status == TR_STATUS_INITIATED and tr_mode != NACH:
                # using Billdesk
                job = get_billdesk_transaction_url.delay(
                    customer_uid,
                    investment_group_id,
                    tr_group_id,
                    amount,
                    payment_mode=payment_mode
                )

            # return HTTP 202 Accepted with location of job
                resp.status = HTTP_202
                resp.location = '/v1/job/{}'.format(job.id)

            # NACH transactions
            elif tr_status == TR_STATUS_INITIATED and tr_mode == NACH:
                resp.status = HTTP_200
                req.context['result'] = Transaction.fetch(customer_id,
                                                          tr_group_id,
                                                          session,
                                                          grouped=True)
        else:
            # REDEEM or WITHDRAW scenario
            LOG.debug('{} - {}'.format(transaction_type, amount))
            # Check balances - are we within the allowed balance
            # for this amount?
            if not Transaction.can_redeem(tr_group_id, session):
                # TODO: decline or delete this transaction in the db.
                raise HTTPInvalidParam(msg='{} is greater than available '
                                       'withdrawal balance'.format(amount),
                                       param_name='amount')
            if product.internal_name == LIQUID:
                # call reliance in background
                client_ip = req.access_route[-1].strip()
                # arguments for redeem API call
                redeem_args = (investment_group_id, tr_group_id,
                               amount, client_ip)
                # if this is successful, update available balance
                # adding some safety for updating values from Reliance API
                # as there tends to be a lag between actual redemption
                # and transaction status
                safety = 120    # in seconds.
                job_update = get_rmm_transaction_status.subtask(
                    (investment_group_id, tr_group_id), countdown=safety
                )
                # redeem, and on success, update transaction status
                job = rmm_redeem.apply_async(redeem_args, link=job_update)

                # return HTTP 202 Accepted with location of job
                resp.status = HTTP_202
                resp.location = '/v1/job/{}'.format(job.id)

                # changing the transaction flag to PROCESSING, as the app
                # shows this customer that this transaction succeeded,
                # and we don't want him to make another request if this
                # doesn't show up in history.
                Transaction.update_group(tr_group_id, session,
                                         status=TR_STATUS_PROCESSING)
            else:
                # simply update the status of the newly initiated
                # transaction group to 'PROCESSING', and we will let the
                # transaction feed file process take over.
                Transaction.update_group(tr_group_id, session,
                                         status=TR_STATUS_PROCESSING)
                req.context['result'] = Transaction.fetch(customer_id,
                                                          tr_group_id,
                                                          session,
                                                          grouped=True)

    @before(validate_investment_ownership)
    def on_get(self, req, resp, investment_group_id):
        """
        GET /investment/investment_group_id/transactions
        """
        session = req.context['session']
        investment_rows = req.context['investments']
        if not investment_rows:
            raise HTTPNotFound('No such investment: {}'.
                               format(investment_group_id))
        transactions = Transaction.fetch_by_investment(session,
                                                       investment_group_id)
        # LOG.debug('got {} rows'.format(len(transactions)))
        req.context['result'] = transactions or []


class TransactionResource():
    """
    Single transaction
    """
    @before(validate_transaction_ownership)
    @before(validate_patch_input)
    def on_patch(self, req, resp, transaction_group_id):
        doc = req.context['doc']
        session = req.context['session']
        customer_id = req.context['customer_id']

        # TODO: Need to check if transaction is currently pending.
        # TODO: One time token for updating a transaction?

        # update transaction
        Transaction.update_group(transaction_group_id, session, **doc)
        tr_rows = Transaction.fetch(customer_id, transaction_group_id,
                                    session, grouped=True)
        req.context['result'] = tr_rows

        # if this is a liquid / reliance transaction update, then
        # refresh transaction status from reliance
        if tr_rows:
            investment_group_id = tr_rows['investment_group_id']
            inv_rows = Investment.fetch(investment_group_id,
                                        customer_id, session)
            is_liquid = inv_rows[0].product.internal_name == LIQUID
            if is_liquid:
                if 'reference' in doc:
                    # TODO: Eventually combine this with update_group call
                    # above.
                    Transaction.update_group(transaction_group_id, session,
                                             status=TR_STATUS_PROCESSING)
                    try:
                        # Reliance money manager - transaction reference was
                        # updated. Update transaction status from Reliance
                        # synchronously, then refresh response (for withdrawal)
                        # LOG.debug(tr_rows)
                        if tr_rows['transaction_type'] == REDEEM:
                            rmm = RelianceMoneyManager(investment_group_id,
                                                       transaction_group_id,
                                                       session)
                            rmm.transaction_status()
                            # kick off a task to update the balance, in 5 sec.
                            update_rmm_balance.apply_async(
                                (investment_group_id, transaction_group_id),
                                countdown=5
                            )
                        result = Transaction.fetch(customer_id,
                                                   transaction_group_id,
                                                   session, grouped=True)
                        # LOG.debug(result)
                        req.context['result'] = result
                    except Exception as ex:
                        LOG.error(ex)
                        raise ex
                        # TODO: Fail silently, or notify via response?

    @before(validate_transaction_ownership)
    def on_get(self, req, resp, transaction_group_id):
        session = req.context['session']
        customer_id = req.context['customer_id']

        # uses redis to check if we need to wait for billdesk to complete
        # updating this transaction.
        rdb = Billdesk.get_redis_client()
        lock = '{}{}'.format(REDIS_BD_LOCK_PREFIX, transaction_group_id)
        # LOG.debug('check if exists: {}'.format(lock))
        if rdb.exists(lock):
            LOG.warn('billdesk updating, lock: {}'.format(lock))
            while True:
                # do a blocking pop from the lock (with a timeout of 2 seconds)
                # when the process completes, this function will return 1
                # or None (if times out)
                k = rdb.blpop(lock, 2)
                if k is None or int(k[1]) == 1:
                    break
            # delete the lock, no longer needed.
            rdb.delete(lock)
        # populate transaction in json response.
        req.context['result'] = Transaction.fetch(customer_id,
                                                  transaction_group_id,
                                                  session, grouped=True)
