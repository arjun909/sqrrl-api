# api/fake/reliance_dummy.py
from datetime import datetime
import simplejson as sj

from app.utils.helpers import to_ist
from app.log import get_logger
LOG = get_logger()


class RelianceDummyResource():
    """
    Returns fake redeem responses
    """

    def on_get(self, req, resp, action):
        """
        GET /api/fake/reliance/{action}

        action = RedInvbankDetails_V1
        action = Redemptionsave
        """
        LOG.info('Inside RelianceDummy with action: {}'.format(action))
        amount = req.get_param_as_int('UnitAmtValue', 0)

        # build the dummy success response, return
        now = datetime.utcnow()
        t = int(now.timestamp() * 1000)
        dt = to_ist(now).ctime()
        r1 = {
            'Amount': amount,
            'Date_Time': dt,    # note: diff format from what's expected
            'OptionCode': 'GROWTH',
            'PlanCode': 'GROWTH PLAN - GROWTH OPTION',
            'SchemeCode': 'RELIANCE LIQUID FUND - TREASURY PLAN',
            'UNITS': '0',
            'Return_code': '0',
            'REFNO': 'MOCK{}'.format(t),
            'Return_Msg': 'PROCESSED via fake reliance'
        }
        resp.body = sj.dumps(r1)
