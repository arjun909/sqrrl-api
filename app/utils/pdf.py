from jinja2 import FileSystemLoader, Environment, Template
from jinja2.exceptions import TemplateNotFound
from weasyprint import HTML
from bs4 import BeautifulSoup
from .os_utils import dir_exists, create_dirs, file_exists
import os
import errno
from app.config import TEMPLATE_DIRECTORY_PATH, PDF_DIRECTORY_PATH
from app import log
import tempfile

LOG = log.get_logger()


class PdfMaker(object):

    template_environment = None

    def __init__(self, path_to_templates=None, pdf_directory=None,
                 template_name=None, follow_links=True, **kwargs):

        self.templates_directory = \
            path_to_templates if path_to_templates else TEMPLATE_DIRECTORY_PATH

        self.target_pdf_directory = \
            pdf_directory if pdf_directory else PDF_DIRECTORY_PATH

        if not dir_exists(self.templates_directory):
            # create_dirs(self.templates_directory)
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),
                                    self.templates_directory)

        if not dir_exists(self.target_pdf_directory):
            # Target directory does not exists. Run mkdir -p
            create_dirs(self.target_pdf_directory)

        # As of now, the loader will be a file-system loader.
        # In future implementation, a package-loader can also be configured.
        LOG.debug("Initializing the Environment and the Loader")

        loader = self.__get_file_system_loader__(follow_links)
        self.template_environment = Environment(loader=loader, **kwargs)

        self.template = None
        self.template_name = template_name
        if template_name:
            self.template = self.get_template(template_name)

    # Private method
    def __get_file_system_loader__(self, follow_links):
        return FileSystemLoader(self.templates_directory,
                                followlinks=follow_links)

    def get_template(self, template_name):
        # check if the template exists in that directory
        if template_name in self.template_environment.list_templates():
            return self.template_environment.get_template(template_name)
        else:
            error_message = "Template: {} does not exists in the " \
                            "templates directory {}.".\
                            format(template_name, self.templates_directory)
            LOG.error(error_message)
            raise TemplateNotFound(name=template_name, message=error_message)

    def generate_pdf(self, template_name, pdf_directory=None,
                     pdf_file_name=None, template_params={}, **kwargs):

        # Decide the directory where the pdf will be stored
        pdf_directory = \
            pdf_directory if pdf_directory else self.target_pdf_directory

        if not dir_exists(pdf_directory):
            create_dirs(pdf_directory)

        # Decide the file name of the pdf file, and
        # if pdf_file_name is not provided, generate a random file name
        pdf_file_name = str(pdf_file_name if pdf_file_name
                            else next(tempfile._get_candidate_names()))

        # Create the absolute path of the pdf file
        absolute_path_to_pdf_file = os.path.join(pdf_directory, pdf_file_name)
        if not pdf_file_name.endswith('.pdf'):
            absolute_path_to_pdf_file += ".pdf"

        try:
            # If template_params is provided and some keyword arguments also,
            # then merge the two dictionaries.
            # Leaving it to the caller, it is caller's responsibility to ensure
            # that template_params is a dictionary or any of dict's subtype.
            if kwargs:
                template_params = {**template_params, **kwargs}

            # Generate the pdf file
            if self.template and (self.template_name == template_name):
                HTML(string=BeautifulSoup(self.template
                                          .render(**template_params))
                     .prettify()).write_pdf(absolute_path_to_pdf_file)
            else:
                HTML(string=
                     BeautifulSoup(self.get_template(template_name)
                                   .render(**template_params)).prettify())\
                    .write_pdf(absolute_path_to_pdf_file)

        except Exception as e:
            LOG.error("An exception has occurred while generating a pdf file, "
                      "with the exception being: {}".format(e))

        return absolute_path_to_pdf_file

    def create_html_file(self, template_name, pdf_directory=None,
                         pdf_file_name=None, template_params={}, **kwargs):

        if kwargs:
            template_params = {**template_params, **kwargs}

        if self.template and (self.template_name == template_name):
            x = self.template.render(**template_params)
        else:
            x = self.get_template(template_name).render(**template_params)

        x = BeautifulSoup(x).prettify()

        if not dir_exists(pdf_directory):
            create_dirs(pdf_directory)

        html_file_path = os.path.join(pdf_directory, "{}.html".format(str(pdf_file_name)))
        with open(html_file_path, "w") as text_file:
            text_file.write(x)

        # return html_file_path
        return self.generate_pdf_from_html_file(html_file_path, pdf_directory, pdf_file_name)

    def generate_pdf_from_html_file(self, html_file_path,
                                    pdf_directory=None, pdf_file_name=None):

        try:
            if file_exists(html_file_path):

                absolute_path_to_pdf_file = os.path.join(pdf_directory, str(pdf_file_name))

                if not absolute_path_to_pdf_file.endswith('.pdf'):
                    absolute_path_to_pdf_file += ".pdf"

                html = HTML(filename=html_file_path)

                if html:
                    # Actual html to pdf conversion
                    html.write_pdf(target=absolute_path_to_pdf_file)

                return absolute_path_to_pdf_file

            else:
                LOG.warn("File: {} DOES NOT exists.".format(html_file_path))

        except Exception as e:
            LOG.error("An exception occurred while generating the pdf: {}".format(e))

        return None
