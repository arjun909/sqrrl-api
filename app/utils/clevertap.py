# clevertap.py
# wrapper over Clevertap-related utilities.
from itertools import groupby
import requests
import simplejson as sj

from app import config
from app.log import get_logger
LOG = get_logger()


class Clevertap():
    """
    Clevertap-related utilities.

    Clevertap is used for analytics + engagement (push notifications etc.)
    """
    session = None      # the requests session

    @classmethod
    def get_requests_session(cls):
        if not cls.session:
            # create a session which we can re-use.
            cls.session = requests.Session()
            cls.session.headers.update({
                'X-CleverTap-Account-Id': config.CLEVERTAP_ACCOUNT_ID,
                'X-CleverTap-Passcode': config.CLEVERTAP_PASSCODE,
                'Content-Type': 'application/json'
            })
        return cls.session

    @staticmethod
    def due_today(product_id, records):
        """
        Sends out push notifications for Sqrrl Away transactions that are
        due today.

        params:
            records: output of Investment.due_today
        """
        LOG.info('Sending {} payment reminders via PUSH'.format(len(records)))

        # we can batch API calls by amount
        # sort by amount, then group by amount.
        results = sorted(records, key=lambda r: r['amount'])
        for amount, group in groupby(results, lambda r: r['amount']):
            # extract mobiles to which this amount is to be sent
            # LOG.debug('amount: {}'.format(amount))
            identities = [r['customer_mobile'] for r in list(group)]
            # LOG.debug('identities: {}'.format(identities))

            # construct request payload
            deep_link = config.PUSH_1_DEEP_LINK.format(
                product_id=product_id, amount=amount
            )
            payload = {
                'name': 'Payment Reminder',
                'to': {'Identity': identities},
                'content': {
                    'title': config.PUSH_1_TITLE,
                    'body': config.PUSH_1_BODY,
                    'platform_specific': {
                        'android': {
                            'background_image': config.PUSH_1_ANDROID_IMG,
                            'default_sound': True,
                            'deep_link': deep_link
                        },
                        'ios': {'deep_link': deep_link}
                    }
                }
            }

            # LOG.debug(payload)
            s = Clevertap.get_requests_session()

            # TODO: this API allows max 1000 requests in a second.
            # handle this limit later.

            result = s.post(config.CLEVERTAP_API_PUSH_URL, sj.dumps(payload))
            if result.status_code != 200:
                LOG.warn('Push API failed', extra={
                    'status_code': result.status_code,
                    'payload': payload,
                    'result': result.json()
                })
