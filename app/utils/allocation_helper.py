# allocation_helper.py
from collections import namedtuple
import re

from app.utils.helpers import get_duration_bucket
from app.log import get_logger

LOG = get_logger()

# we'll store moderate / aggressive scores in this.
Score = namedtuple('Score', 'm a')

# TODO: Make these strings come from constants
CATEGORY_SCORES = {
    'purchase': Score(m=0.7, a=0.3),
    'experience': Score(m=0.4, a=0.6),
    'occasion': Score(m=0.9, a=0.1),
    'trip': Score(m=0.6, a=0.4),
    'custom': Score(m=0.5, a=0.5),
}
DURATION_SCORES = (
    Score(m=1, a=0),        # 0-6 months
    Score(m=0.8, a=0.2),    # 6-12 months
    Score(m=0.6, a=0.4),    # 12-24 months
    Score(m=0.3, a=0.7),    # 24-36 months
    Score(m=0.2, a=0.8),        # > 36 months
)
KEYWORD_SCORES = {
    'holiday': Score(m=0.6, a=0.4),
    'phone': Score(m=0.4, a=0.6),
    'wedding': Score(m=0.9, a=0.1),
    'gift': Score(m=0.7, a=0.3),
    'laptop': Score(m=0.4, a=0.6),
    'handbag': Score(m=0.4, a=0.6),
    'jewellery': Score(m=0.7, a=0.3),
    'bike': Score(m=0.4, a=0.6),
    'car': Score(m=0.4, a=0.6),
    'road': Score(m=0.4, a=0.6),
    'vacation': Score(m=0.6, a=0.4),
    'trip': Score(m=0.6, a=0.4),
}


def be_aggressive(category, target_date, description):
    """
    Runs some scores to decide which portfolio - moderate or aggressive,
    to go for.

    @return True for aggressive, false for moderate
    """
    # step 1 - use category to get m/a scores.
    try:
        category_score = CATEGORY_SCORES[category]
    except KeyError:
        category_score = Score(m=0.5, a=0.5)
    # LOG.debug('Category - {}'.format(category_score))

    # step 2 - duration score
    index = get_duration_bucket(target_date)
    try:
        duration_score = DURATION_SCORES[index]
    except IndexError:
        # same values for > 36 months
        duration_score = DURATION_SCORES[-1]
    # LOG.debug('Duration {} - {}'.format(index, duration_score))

    # step 3 - keyword score.
    # this is a very rudimentary whole-word search that will fail
    # in some common scenarios. In future we'll make this fancy.
    keyword_score = Score(m=0.6, a=0.4)
    if description:
        # find the last word in description that matches list of keywords
        description = description.lower()
        # split description into words, then try to find a keyword
        match = None
        for word in re.split('\W+', description, flags=re.IGNORECASE):
            # LOG.debug('searching for {}'.format(word))
            if word in KEYWORD_SCORES.keys():
                match = word
                # LOG.debug('Matched: {}'.format(match))
                break
        if match:
            keyword_score = KEYWORD_SCORES[match]
        else:
            # this is the default
            keyword_score = Score(m=0.6, a=0.4)

    # LOG.debug('Keyword - {}'.format(keyword_score))

    # multiply to get final score
    final_score = Score(
        m=category_score.m*duration_score.m*keyword_score.m,
        a=category_score.a*duration_score.a*keyword_score.a,
    )
    # LOG.debug('Final - {}'.format(final_score))

    return final_score.a > final_score.m
