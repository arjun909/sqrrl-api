from app import model as m
from app import constants as C
from app import log
from sqlalchemy import and_
from datetime import datetime, timedelta
from itertools import groupby
from app.utils import PdfMaker
from calendar import monthrange
import tempfile
from app.utils.helpers import to_ist
# from app.utils.os_utils import create_dirs
import os


LOG = log.get_logger()


class StatementGenerator(object):

    def __init__(self, session):

        datetime_now = datetime.now()
        current_year, current_month = datetime_now.year, datetime_now.month

        # Find the start and end date of the statement generation
        if current_month == 1:
            days_in_previous_month = monthrange(current_year - 1, 12)[1]
            self.start_date = datetime(year=current_year - 1,
                                       month=12, day=1)
        else:
            days_in_previous_month = monthrange(current_year,
                                                current_month - 1)[1]

            self.start_date = datetime(year=current_year,
                                       month=current_month - 1,
                                       day=1)

        self.end_date = self.start_date + timedelta(days=days_in_previous_month - 1,
                                                    hours=23, minutes=59, seconds=59)

        date_format = '%d %B, %Y'
        LOG.info("Generating account statements for the period : "
                 "{} to {}".format(self.start_date.strftime(date_format),
                                   self.end_date.strftime(date_format)))

        self.session = session
        self.template_name = 'statement_template.html'
        self.output_base_pdf_directory = None

        self.statement_period = "{} - {}".\
            format(self.start_date.strftime(date_format),
                   self.end_date.strftime(date_format))

        self.statement_date = to_ist(datetime.now()).strftime(date_format)
        self.statement_month = self.start_date.strftime('%B %Y')

        self.s3_key_prefix = 'statements/{}/{}'.\
            format(self.start_date.year,self.start_date.strftime('%B'))

        self.pdf_maker = PdfMaker(template_name=self.template_name)

    # Create a temporary folder
    def create_temporary_folder(self):
        # cwd = os.getcwd()
        # temp_pdf_dir = os.path.join(cwd, 'pdf', 'statements')
        # create_dirs(temp_pdf_dir)
        # self.output_base_pdf_directory = temp_pdf_dir
        self.output_base_pdf_directory = \
            tempfile.mkdtemp(prefix='statements_{}_{}'.
                             format(self.start_date.year,
                                    self.start_date.strftime('%B_')))

    def shortlist_investments(self):
        investment_rows = self.session.query(m.Investment).\
            filter(and_(m.Investment.scheme_folio.isnot(None),
                        m.Investment.scheme_units.isnot(None))).all()
        LOG.debug("{} investment rows".format(len(investment_rows)))
        return investment_rows

    def shortlist_transactions(self):
        return self.session.query(m.Transaction)\
            .filter(m.Transaction.status == C.TR_STATUS_SUCCESS) \
            .filter(m.Transaction.created_on.between(self.start_date,
                                                   self.end_date)) \
            .order_by(m.Transaction.created_on.asc()) \
            .all()

    @staticmethod
    def by_customer_id(x):
        return x.customer_id

    @staticmethod
    def by_product_id(x):
        return x.product_id

    @staticmethod
    def by_investment_id(x):
        return x.investment_id

    def get_records(self):
        # 1. Find all the investment rows which have a folio and units not null
        investment_rows = self.shortlist_investments()

        # 2. Get all the unique customer_ids from the investment_rows
        unique_customers = sorted(set([i.customer_id for i in investment_rows]))
        LOG.debug("{} unique customers".format(len(unique_customers)))

        # 3. Group-by those investment rows on customer_id
        investment_rows_grouped = groupby(sorted(investment_rows,
                                                 key=StatementGenerator.by_customer_id),
                                          StatementGenerator.by_customer_id)

        records = {}

        # 4. For each customer, group-by their investment rows on product_ids
        for id, grouped_inv_rows in investment_rows_grouped:
            investments = list(grouped_inv_rows)
            investments_grouped = groupby(sorted(investments,
                                                 key=StatementGenerator.by_product_id),
                                          StatementGenerator.by_product_id)
            records[id] = {
                "investments": {product_id: list(rows)
                                for product_id, rows in investments_grouped}
            }

        # 6. Find all the transactions which are success and happened last month
        all_trans = self.shortlist_transactions()

        # 7. Group-by the transactions on the basis of customer ids
        transaction_rows_grouped = groupby(sorted(all_trans,
                                                  key=StatementGenerator.by_customer_id),
                                           StatementGenerator.by_customer_id)

        # for customer_id in records.keys():
        for customer_id, trans in transaction_rows_grouped:
            transactions = list(trans)

            trans_grouped_by_investments = groupby(
                sorted(transactions,
                       key=StatementGenerator.by_investment_id),
                StatementGenerator.by_investment_id)

            if customer_id not in records:
                # Somehow, the records does not contain any key equivalent to this
                # customer_id but there are transactions made by this customer.
                # This is an impossible case, still here for sanity.
                LOG.warn("Goof up with customer_id {}".format(customer_id))
            else:
                records[customer_id]['transactions'] = {}
                for investment_id, t in trans_grouped_by_investments:
                    records[customer_id]['transactions'][investment_id] = list(t)

        return records

    def start_processing(self):
        records = self.get_records()

        # Create a date format, this will be used in transactions
        date_format = '%a %d %B | %I:%M %p'

        # Create a temporary folder
        self.create_temporary_folder()
        LOG.info("Statements folder: {}".format(self.output_base_pdf_directory))

        from app import tasks

        for customer_id in records.keys():
            # Get the customer row
            customer = \
                list(records[customer_id]["investments"].values())[0][0].customer

            # Construct statement params
            statement_params = {
                "statement_period": self.statement_period,
                "statement_date": self.statement_date,
                "customer_name": (customer.pan_name if customer.pan_name
                                  else customer.name),
                "customer_email": customer.email,
                "customer_phone_number": customer.mobile,
                "customer_pan_number": customer.pan_number,
            }

            products_invested = []
            for product_id, investment_rows in records[customer_id]["investments"].items():
                products_invested.append(
                    {
                        "product_name": investment_rows[0].product.display_name,
                        "description": investment_rows[0].description,
                        "schemes": [
                            {
                                "name": i.scheme.name,
                                "allocation_percent": float(str(i.allocation_percent)),
                                "units": float(str(i.scheme_units)),
                                "amount_invested": str(max(i.net_investment, 0.0)),
                                "amount_withdrawn": str(i.total_withdrawal),
                                "current_value": str(i.scheme_units * i.scheme_nav),
                                "folio_number": i.scheme_folio,
                                "investment_id": i.id,
                            }
                            for i in investment_rows]
                    })
            statement_params["summary"] = products_invested

            last_month_transactions = []
            if 'transactions' in records[customer_id]:
                for inv_id in records[customer_id]['transactions'].keys():
                    t = records[customer_id]['transactions'][inv_id]
                    investment_obj = t[0].investment

                    last_month_transactions.append({
                        "product_name": investment_obj.product.display_name,
                        "description": investment_obj.description,
                        "folio_number": investment_obj.scheme_folio,
                        "scheme": investment_obj.scheme.name,
                        "transactions": [
                            {
                                "date_time": to_ist(trx.created_on).strftime(date_format),
                                "type":
                                    'Switch' if 'SWITCH' in trx.transaction_type
                                    else trx.transaction_type.title(),
                                "amount": str(trx.amount),
                                "nav": str(trx.amount / trx.units),
                                "units_involved": str(trx.units),
                                "balance_units": "???"
                            }
                            for trx in t
                            if trx.units > 0.0 and trx.amount
                        ]
                    })

            statement_params["transactions"] = last_month_transactions

            pdf_directory = os.path.join(self.output_base_pdf_directory,
                                         str(customer_id))

            if customer.is_email_verified:
                tasks.generate_and_send_statements.apply_async(
                    countdown=15,
                    kwargs={
                        "pdf_directory": pdf_directory,
                        "pdf_file_name": customer_id,
                        "template_params": statement_params,
                        "customer_name": statement_params["customer_name"],
                        "customer_email": statement_params["customer_email"],
                        "customer_id": customer_id,
                        "s3_key_prefix": self.s3_key_prefix,
                        "statement_month": self.statement_month
                    })

        # Delete the temporary directory from the machine after 2 days.
        tasks.delete_temporary_directory.apply_async(
            countdown=180000,
            kwargs={
                "temp_folder_path": self.output_base_pdf_directory
            })

