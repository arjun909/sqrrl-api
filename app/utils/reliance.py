# reliance.py
# from urllib import parse
from decimal import Decimal, localcontext
from datetime import datetime
import requests

from app import config
from app.model import Investment, Transaction
from app.utils.helpers import get_ist_now, from_ist
from app.log import get_logger
from app.constants import (TR_STATUS_PROCESSING, TR_STATUS_SUCCESS,
                           TR_STATUS_FAILURE)

LOG = get_logger()


class RelianceMoneyManager():
    """
    Wraps all functionality for Reliance Money Manager API.
    Designed to support single customer/investment use cases.
    No batch support.

    NOTE: callback url
    https://investeasy.reliancemutual.com/online/SimplySavePartner/Confirmation?Msg=Success&Folio=xxxxxxxxxxxx&arncode=ARN-xxxxxx&pan=ABCD1111A&Name=BRUCE%20WAYNE&scheme=LP&plan=IG&option=G&email=bruce@gotham.com&mobile=9999999999&dob=01/01/1980&RI=Y&PEP=N&RPEP=N&PTI=Y&BII=Y&TAX=individual&OCCUP=1&INCOME=5-10L&IFSC=ICIC0000031&ACTYPE=Savings&ACNUM=xxxxxxxxxxxx&EdFlag=Y
    """
    # the requests session
    r_session = None

    @classmethod
    def get_request_session(cls):
        """
        Returns a re-usable requests session to be used for API calls
        to Reliance. Request sessions make calls more performant.
        """
        if not cls.r_session:
            cls.r_session = requests.Session()
            if config.DEBUG:
                cls.r_session.verify = False
        return cls.r_session

    def __init__(self, investment_group_id, transaction_group_id, session):
        self.session = session
        self.transaction_group_id = transaction_group_id
        investment = session.query(Investment).\
            filter(Investment.group_id == investment_group_id).first()
        self.investment = investment

        # 'LP' -> Money Manager
        # 'LF' -> Liquid Fund Treasury Plan
        try:
            self.scheme_code = self.investment.scheme.rta_code[:2]
        except:
            LOG.warn('Could not get scheme_code, using LF')
            self.scheme_code = 'LF'

        if not self.investment:
            raise Exception('{} not found'.format(investment_group_id))

    # def get_purchase_url(self, amount):
    #     """
    #     Returns the transaction url that has to be launched in browser or
    #     webview to complete purchase of amount.
    #     """
    #     # LOG.info('Purchase {}'.format(amount))
    #     # folio must already exist before this can happen.
    #     # if folio does not exist, use get_setup_url
    #     assert self.investment.scheme_folio
    #
    #     # API endpoint (API #1 in reference document v1.0)
    #     url = '{}{}'.format(config.RELIANCE_BASE_URL,
    #                         config.RELIANCE_PURCHASE)
    #     # LOG.info('url: {}'.format(url))
    #
    #     # build params
    #     params = {
    #         'Folio': self.investment.scheme_folio,
    #         'TransactionType': 'Purchase',
    #         'SchemeCode': self.scheme_code,
    #         'PlanCode': 'IG',
    #         'OptionCode': 'G',
    #         'Amount': amount,
    #         'ARNCODE': 'ARN-{}'.format(config.ARN),
    #         'EUIN': config.EUIN,
    #         'Channel': 'DIGIAPI_PART_PUR',
    #         'deviceid': 'PARTNERAPI',
    #         'appVersion': '1.0.1',
    #         'appName': config.RELIANCE_APP_NAME,
    #         'apikey': config.RELIANCE_API_KEY
    #     }
    #
    #     # make the request
    #     r = requests.get(url, params, timeout=10)
    #     # LOG.debug(r.json())
    #     if (r.status_code == 200 and 'Return_Code' in r.json()
    #             and int(r.json()['Return_Code']) == 0):
    #         try:
    #             # extract the transaction url from response and return
    #             transaction_url = r.json().get('ShortUrl', '')
    #             Transaction.update_group(tr_group_id=self.transaction_group_id,
    #                                      session=self.session,
    #                                      status=TR_STATUS_PROCESSING)
    #             return transaction_url
    #         except Exception as ex:
    #             LOG.error(ex)
    #             raise ex
    #     else:
    #         # something is not right
    #         msg = r.json()['Return_Message'] \
    #             or 'status_code: {}'.format(r.status_code)
    #         LOG.error(msg)
    #         raise Exception('{}'.format(msg))

    def redeem(self, amount, ip):
        """
        Redeem - transfer money back to customer account.
        """
        LOG.debug('Redeem {}'.format(amount))

        # do everything using Decimals with high prec
        # else risk rounding errors!
        with localcontext() as ctx:
            ctx.prec = 12
            amount = Decimal(amount, ctx)

            # we need some params
            bank_name = self.get_bank_name()

            # today's date in IST, in MM/DD/YYYY format as per docs
            now = get_ist_now().strftime(r'%m/%d/%Y')

            # now we talk to the reliance API
            if config.RELIANCE_FAKE_REDEEM:
                url = config.RELIANCE_FAKE_REDEEM_URL
            else:
                url = '{}{}'.format(config.RELIANCE_BASE_URL,
                                    config.RELIANCE_REDEEM)
            params = {
                'Cuttime': '',
                'Fname': '',
                'Lname': '',
                'Mname': '',
                'Mstatus': '',
                'RedFlag': 'P',
                'Tpin': 'A',
                'UnitAmtValue': amount,
                'UnitamtFlag': 'A',
                'acno': self.investment.scheme_folio,
                'appName': config.RELIANCE_APP_NAME,
                'appVersion': '1.0.1',
                'arncode': 'ARN-{}'.format(config.ARN),
                'bank': bank_name,
                'branch': config.RELIANCE_BRANCH_CODE,
                'deviceid': 'PARTNERAPI',
                'entdate': now,
                'fund': 'RMF',
                'ip': ip,
                'oldihno': '0',
                'options': 'G',
                'pangno': '',
                'plan': 'IG',
                'proxybranch': '',
                'scheme': self.scheme_code,
                'trdate': now,
                'userid': '',
                'apikey': config.RELIANCE_API_KEY
            }
            try:
                r_session = RelianceMoneyManager.get_request_session()
                LOG.debug(params)
                r = r_session.get(url, params=params, timeout=10)
                result = r.json()
                status = None
                reference = None
                note = None
                if int(result['Return_code']) == 0:
                    # SUCCESS
                    reference = result['REFNO']
                    status = TR_STATUS_PROCESSING
                else:
                    # there was an error in redemption
                    note = result['Return_Msg']
                    status = TR_STATUS_FAILURE

                # update db, return updated rows
                self.update_transaction_group(trx_status=status,
                                              reference=reference,
                                              note=note)

                tr = Transaction.fetch(self.investment.customer_id,
                                       self.transaction_group_id,
                                       self.session)
                return {
                    'transactions': tr
                }
            except requests.exceptions.ConnectionError as c_err:
                LOG.error(c_err, exc_info=True)
                # Update Transaction status to FAILURE.
                self.update_transaction_group(
                    trx_status=TR_STATUS_FAILURE,
                    reference=None,
                    note='Reliance API Connection Failure')

                raise c_err

    def transaction_status(self):
        """
        Fetches status of a transaction
        """
        # get transaction reference from db
        t_row = self.session.query(Transaction).\
            filter(Transaction.group_id == self.transaction_group_id).first()
        if not t_row:
            # this should never happen
            return
        t = t_row.transaction_reference

        # build params for RMM API call
        params = {
            'fund': 'RMF',
            'deviceid': 'PARTNERAPI',
            'appVersion': '1.0.1',
            'appName': config.RELIANCE_APP_NAME,
            'apikey': config.RELIANCE_API_KEY,
            'refno': t
        }

        # LOG.info('params {}'.format(params))
        # API endpoint (API #5 in reference document v1.0)
        url = '{}{}'.format(config.RELIANCE_BASE_URL,
                            config.RELIANCE_REF_STATUS)

        r_session = RelianceMoneyManager.get_request_session()
        r = r_session.get(url, params=params, timeout=60)
        if r.status_code == 200:
            # LOG.info('{}'.format(r.json()))
            r_json = r.json()
            # LOG.debug(r_json)
            if r_json and int(r_json.get('ReturnCode', '-1')) == 0:
                # at least reference number is correct!
                if r_json.get('ReturnMsg', '') == 'SUCCESS':
                    # LOG.info('SUCCESSFUL!')
                    # update the corresponding transaction
                    trxn_status = TR_STATUS_PROCESSING
                    # in case of insta-redemption, we need to temporarily
                    # calculate units based on latest NAV, else all
                    # computations will go bonkers.
                    units = None
                    if r_json.get('InstaStatus', '') == 'SUCCESS':
                        try:
                            trxn_status = TR_STATUS_SUCCESS
                            units = round(
                                t_row.amount/self.investment.scheme_nav, 4
                            )
                            # LOG.debug('temp. units: {}'.format(units))
                        except Exception as ex:
                            LOG.error('Insta redemption update failed',
                                      exc_info=True)
                    Transaction.update_group(self.transaction_group_id,
                                             self.session,
                                             reference=t,
                                             units=units,
                                             status=trxn_status)
                else:
                    # trxn_status = TR_STATUS_FAILURE
                    LOG.warn('NOT SUCCESSFUL', extra={
                        'rmm_refno': t,
                        'rmm_tr_group_id': self.transaction_group_id
                    })

                # update investment table with up-to-date computations
                Investment.post_verification(self.session,
                                             self.investment.group_id)
        else:
            LOG.warn('NOT SUCCESSFUL')

    def get_withdrawal_balance(self):
        """
        Checks RMM API to fetch available withdrawal balances.
        """
        # make a request to Reliance API.
        url = '{}{}'.format(config.RELIANCE_BASE_URL, config.RELIANCE_BALANCE)
        params = {
            'acno': self.investment.scheme_folio,
            'apikey': config.RELIANCE_API_KEY,
            'appName': config.RELIANCE_APP_NAME,
            'appVersion': '1.0.1',
            'arncode': 'ARN-{}'.format(config.ARN),
            'branch': config.RELIANCE_BRANCH_CODE,
            'deviceid': 'PARTNERAPI',
            'plan': 'IG',
            'proxybranch': '',
            'scheme': self.scheme_code
        }
        r_session = RelianceMoneyManager.get_request_session()
        r = r_session.get(url, params=params, timeout=10)
        if r.status_code == 200:
            resp_json = r.json()
            # LOG.debug(resp_json)
            try:
                # fetch available balance - amount and units.
                total_amount = resp_json['TotalAmt']
                total_units = resp_json['Totalunits']
                nav = resp_json['Nav']
                # e.g. for format: "1/6/2017 12:00:00 AM"
                nav_date_str = resp_json['Nav_Date']
                nav_date = datetime.strptime(nav_date_str,
                                             r'%m/%d/%Y %I:%M:%S %p')
                nav_date_utc = from_ist(nav_date)
                # update investment table.
                # using Decimal with precision 4
                with localcontext() as ctx:
                    ctx.prec = 12
                    self.investment.available_withdrawal_balance =\
                        Decimal(total_amount, ctx)
                    self.investment.scheme_units = Decimal(total_units, ctx)
                    self.investment.current_value = Decimal(total_amount, ctx)
                    self.investment.scheme_nav = Decimal(nav, ctx)
                    self.investment.scheme_nav_date = nav_date_utc
                    # LOG.debug('nav {} on {}'.
                    #           format(nav, nav_date_utc.isoformat()))
                    # TODO: what to do with has_processing_transactions?
                    # save to database
                    self.session.commit()
            except KeyError as kerr:
                LOG.error(kerr)
                raise kerr
        else:
            raise Exception('Could not fetch status from Reliance API.\n{}'.
                            format(r.json()))

    # def create_zbf(self):
    #     """
    #     Creates a zero-balance folio.
    #
    #     Uses a hacky direct form submit to directly generate folio
    #     with Reliance if needed.
    #     """
    #     # the following has been reverse engineered from Reliance
    #     # API website + Javascript.
    #     # in case it breaks, will need to check Javascript again
    #     c = self.investment.customer
    #     pep = 'Y' if c.politically_exposed else 'N'
    #
    #     # get bank info from Reliance's internal IFSC lookup API
    #     bank_name = c.bank_name.title()
    #     bank_micr = '0'
    #     bank_city = c.bank_branch_city
    #     try:
    #         # NOTE: Extremely brittle, may break any time
    #         r_bank = requests.post(config.RELIANCE_IFSC_LOOKUP, {
    #             'strIFSC': c.bank_ifsc,
    #             'strMICR': '0',
    #             'strValue': '0'
    #         })
    #         if r_bank.status_code == 200:
    #             bank_data = r_bank.json()['data'][0]
    #             bank_name = bank_data['BankName']
    #             # Use a fallback 9 digit MICR otherwise this will not work.
    #             bank_micr = bank_data['MICRNo'] or FALLBACK_MICR
    #             bank_city = bank_data['BankCity']
    #     except Exception as ex:
    #         LOG.error(ex)
    #
    #     # LOG.debug('bank_name: {} ; bank_micr: {}'.format(bank_name, bank_micr))  # NOQA
    #     dummy_email = 'customer+{}@sqrrl.in'.format(c.uid)
    #     dummy_mobile = config.DUMMY_MOBILE
    #     params = {
    #       'PAN': c.pan_number,
    #       'Name': c.pan_name,
    #       'Email': dummy_email,
    #       'Mobile': dummy_mobile,
    #       'DOB': c.date_of_birth.strftime(r'%d/%m/%Y'),
    #       'RI': 'Y',
    #       'countrytaxid': 'India',
    #       'Ftaxid': '',
    #       'PEP': pep,
    #       'RPEP': pep,
    #       'Above18': 'Y',            # TODO: Compute from DOB?
    #       'GuardPAN': '',            # only for minors.
    #       'GuardName': '',
    #       'GuardRelation': 'Father',
    #       'PayInd': 'Y',
    #       'BornInd': 'Y',
    #       'taxstatus': 'individual',
    #       'occupation': c.occupation_code,
    #       'income': '1-5L',          # TODO: Get from FATCA
    #       'Nominee': 'I do not wish to Nominate',
    #       'NomDOB': '',
    #       'NomPer1': 0,
    #       'Nominee2': '',
    #       'NomDOB2': '',
    #       'NomPer2': 0,
    #       'Nominee3': '',
    #       'NomDOB3': '',
    #       'NomPer3': 0,
    #       'nomadd1': '',
    #       'nomadd2': '',
    #       'nomadd3': '',
    #       'nomcity': '',
    #       'nomstate': '',
    #       'nompin': '0',
    #       'bnkifsc': c.bank_ifsc,
    #       'bnkname': bank_name,
    #       'bnkcity': bank_city,
    #       'bnkmicr': bank_micr,                    # we don't have this info.
    #       'bnkactype': 'Savings',
    #       'bnkacno': c.bank_account_no,
    #       'Arncode': 'ARN-{}'.format(config.ARN),
    #       'scheme': self.scheme_code,
    #       'plan': 'IG',
    #       'option': 'G'
    #     }
    #     # LOG.debug('params: \n{}'.format(params))
    #     try:
    #         # for this request using a proxy is throwing an SSL error
    #         # hence explicitly disabling proxy.
    #         r = requests.post(config.RELIANCE_ZBF_FORM_URL,
    #                           data=params, proxies={})
    #         # LOG.debug('content: {}'.format(r.content))
    #         # LOG.debug('status_code {}'.format(r.status_code))
    #         if r.status_code == 200:
    #             folio = r.json()['Folio']
    #             self.investment.scheme_folio = folio
    #             self.session.commit()
    #         else:
    #             raise Exception('RMM folio creation failed!')
    #             self.session.rollback()
    #     except Exception as ex:
    #         LOG.error(ex)
    #         raise ex

    # def get_transaction_url(self, amount):
    #     """
    #     Generates a transaction url for displaying the payment gateway
    #     for a transaction.
    #     """
    #     # 1. Check if a folio exists
    #
    #     if self.investment.scheme_folio:
    #         # directly return the purchase url for this scheme
    #         return self.get_purchase_url(amount)
    #     else:
    #         self.create_zbf()
    #         return self.get_purchase_url(amount)

    def get_bank_name(self):
        """
        Calls Reliance API to fetch bank details - these details
        need to be passed to redeem API.

        Based on conversation with Reliance: This call is somewhat
        redundant today. The bank_name parameter for the redeem API call
        is ignored at present. The idea behind this API is to eventually
        allow a customer to redeem into a bank of his/her choice, and
        this framework is simply being put in place for that flexibility.

        While this parameter will be ignored by Reliance, we should still
        stick with calling this API so that later if we need to introduce
        withdrawal bank selection we're already up to date.
        """
        # API-109 - after discussion with Reliance, since this API is
        # timing out and it's value is not really being used in the redemption
        # API, let's just return our own internal value of bank name.
        return self.investment.customer.bank_name

        # Following code is the actual logic for getting bank name.
        # TODO: Cache the response as bank details rarely change.
        # url = '{}{}'.format(config.RELIANCE_BASE_URL,
        #                     config.RELIANCE_BANK_DETAILS)
        # params = {
        #     'acno': self.investment.scheme_folio,
        #     'apikey': config.RELIANCE_API_KEY,
        #     'appName': config.RELIANCE_APP_NAME,
        #     'appVersion': '1.0.1',
        #     'arncode': 'ARN-{}'.format(config.ARN),
        #     'deviceid': 'PARTNERAPI',
        #     'plan': 'IG',
        #     'scheme': self.scheme_code
        # }
        # try:
        #     r_session = RelianceMoneyManager.get_request_session()
        #     r = r_session.get(url, params=params, timeout=30)
        #     bank_details = r.json()
        #     LOG.debug(bank_details)
        #     # This is an array of all bank accounts of this folio
        #     # We pick the "primary" bank as all redemptions happen into
        #     # this account (till bank selection is implemented)
        #     bank_name = next(b['BankName'] for b in bank_details
        #                      if b['PrimaryBank'] == 'Y')
        #     LOG.debug('bank_name: {}'.format(bank_name))
        #     return bank_name
        # except requests.exceptions.ConnectionError as c_err:
        #     # some connection error, or timeout.
        #     LOG.error(c_err)
        #     raise c_err
        # except Exception as ex:
        #     LOG.error('Could not get bank details', exc_info=True)
        #     raise ex

    def switch_fund(self):
        """
        API to switch fund - from Reliance Money Manager to Reliance
        Liquid Fund (Treasury)

        ref: https://sqrrldotin.atlassian.net/browse/API-67
        """

        # construct the url
        url = '{}{}'.format(config.RELIANCE_BASE_URL,
                            config.RELIANCE_SWITCH_FUND)
        LOG.debug('url: {}'.format(url))

        # API params - some hard coded for RMM -> Reliance Liquid Fund
        params = {
            'fund': 'RMF',
            'scheme': 'LP',     # for money manager fund
            'plan': 'IG',       # regular growth
            'option': 'G',      # growth
            'acno': self.investment.scheme_folio,
            'RedFlg': 'F',      # full switch
            'UntAmtFlg': 'Y',   # units
            'UntAmtValue': self.investment.extras['switch_units'],
            'Tfund': 'RMF',
            'Tscheme': 'LF',    # liquid fund - treasury plan
            'Tplan': 'IG',
            'Toption': 'G',
            'Tacno': self.investment.scheme_folio,
            'Agent': 'ARN-{}'.format(config.ARN),
            'SubAgent': '',
            'oldihno': 0,
            'EUINno': config.EUIN,
            'EUINflag': 'Y',
            'branch': config.RELIANCE_BRANCH_CODE,
            'deviceid': 'PARTNERAPI',
            'appVersion': '1.0.1',
            'appname': config.RELIANCE_APP_NAME,
            'apikey': config.RELIANCE_API_KEY,
        }

        # make the API call
        r = requests.get(url, params)
        LOG.debug('status_code: {}'.format(r.status_code))
        try:
            result = r.json()
        except:
            LOG.error('Could not parse response', extra={
                'r_content': r.content
            })
            result = None
        success = False
        reference = ''
        if r.status_code == 200 and result:
            if 'ReturnCode' in result and result['ReturnCode'] == '0':
                reference = result['REFNO']
                LOG.info('Fund switch successful', extra={
                    'folio': self.investment.scheme_folio,
                    'reference': reference,
                    'units': result['AmountPaid']
                })
                success = True
            else:
                LOG.warn('Reliance switch failed', extra={
                    'folio': self.investment.scheme_folio,
                    'result_json': result
                })
        else:
            LOG.error('Reliance switch failed, no response', extra={
                'request_url': r.url,
                'status_code': r.status_code
            })
        return success, reference

    def update_transaction_group(self, trx_status, reference, note):
        Transaction.update_group(tr_group_id=self.transaction_group_id,
                                 session=self.session,
                                 reference=reference,
                                 status=trx_status,
                                 note=note)
