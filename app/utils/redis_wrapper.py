# redis_wrapper.py

import redis
from redis import ConnectionError
from app import config
from app.log import get_logger

LOG = get_logger()


class RedisWrapper(object):
    """
    This is a wrapper class over the Redis.
    """

    __rdb_client__ = None


    def __init__(self):
        """
        Initialize the Redis client.
        """
        self.__rdb_client__ = redis.StrictRedis.from_url(config.REDIS_URL)


    @staticmethod
    def raise_redis_connection_error(self, ce):
        """
        :param ce: ConnectionError object from the redis package.  
        :return: raises a ConnectionError whenever this method is called.
        """
        LOG.error("There has been a connection error while connecting with the Redis database.")
        raise ce


    def get_value_from_redis(self, key):
        """
        Fetch a value from Redis against a given Key.
        """
        if key:
            try:
                return self.__rdb_client__.get(key)
            except ConnectionError as ce:
                RedisWrapper.raise_redis_connection_error(ce)
        else:
            return None


    def remove_value_from_set(self, set_name, value):
        if set_name and value:
            try:
                self.__rdb_client__.srem(set_name, str(value))
            except ConnectionError as ce:
                RedisWrapper.raise_redis_connection_error(ce)
            except Exception as e:
                LOG.error("An exception has occurred while removing the value from the Set named {} : {}".format(set_name, e.message))


    def get_redis_client(self):
        return self.__rdb_client__