# shortuuid.py
from datetime import datetime
from hashids import Hashids

from app.config import UUID_SALT, UUID_MIN_LENGTH, UUID_ALPHABET

# reduce min_length by 1 to provide for a prefix 'C'
hashids = Hashids(salt=UUID_SALT,
                  min_length=UUID_MIN_LENGTH - 1,
                  alphabet=UUID_ALPHABET)


def generate(prefix=None):
    """
    Generate url-friendly uids based on current unix UTC timestamp.

    IMPORTANT: Use UTC timestamps or there is no guarantee that every time this
    function is called, it will generate a unique hash. The timestamp used
    needs to be unique every time this function is called.

    Ref: http://hashids.org/python/

    This library converts integers to unique, url-friendly, profanity-free
    short codes. The alternative is to use uuid4 and base64, but this library
    does some work in removing similar letters like l and 1, and so on.

    prefix can be used to pad the id with something like 'C' for customer,
    which makes the uids slightly prettier to look at.
    """
    t = datetime.utcnow().timestamp() * 1000
    if prefix:
        return '{}{}'.format(prefix, hashids.encode(int(t)))
    else:
        return hashids.encode(int(t))
