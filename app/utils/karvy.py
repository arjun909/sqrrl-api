# karvy.py
from datetime import datetime, timedelta
from urllib.parse import urljoin
import requests
from suds.client import Client
import redis
from xml.etree import ElementTree
from fuzzywuzzy import fuzz

import simplejson as sj
from app.utils import RedisWrapper

from app import config
from app.constants import (
    RDB_KARVY_ENC_PASS,
    NOT_IN_ITD,
    ITD_VALID,
    ALL_KRA,
    OCCUPATION
)
from app.model import Customer, CamsStateLookup
from app.log import get_logger
LOG = get_logger()


class Karvy(object):
    """
    Wrapper over Karvy APIs - includes PAN validation, eKYC creation etc.
    """
    # the redis client - at class level because we want to re-use connection.
    rdb = None
    request_session = None

    @classmethod
    def get_redis_client(cls):
        if not cls.rdb:
            # new connection.
            cls.rdb = redis.StrictRedis.from_url(config.REDIS_URL)
        return cls.rdb

    @classmethod
    def get_request_session(cls):
        if not cls.request_session:
            # new request session
            cls.session = requests.Session()
            if config.QUOTAGUARDSTATIC_URL:
                # optionally specify proxy to use with the session
                proxies = {
                    'http': config.HTTP_PROXY,
                    'https': config.HTTPS_PROXY
                }
                cls.session.proxies = proxies
        return cls.session

    def __init__(self, session, customer_uid):
        self.customer_uid = customer_uid
        # the sqlalchemy session
        self.session = session

    def is_kyc_certified(self, pan_number):
        """
        Checks if a PAN number is KYC certified.
        ref: https://www.karvykra.com/service/pansearchallkras.asmx
        """
        # two steps here - 1 is to get an encrypted password
        # 2 is to use this password for fetching PAN details
        # First, see if encrypted password can be re-used from last time.
        rdb = Karvy.get_redis_client()
        encrypted_password = rdb.get(RDB_KARVY_ENC_PASS)
        if not encrypted_password:
            encrypted_password = Karvy.get_encrypted_password()

        # search pan with all KRAs
        result = self.search_all_kras(pan_number, encrypted_password)

        # result is not expected
        if 'is_kyc_verified' not in result:
            raise ValueError('Could not verify details with Income Tax'
                             ' Department, please check if PAN and '
                             'date of birth are correct.')
        return result

    def search_all_kras(self, pan_number, encrypted_password):
        """
        Calls Karvy API to fetch KYC status across all KRAs
        """
        params = {
            'PanNumber': pan_number,
            'UserName': config.KARVY_USERNAME,
            'EncPassword': encrypted_password,
            'POSCode': config.KARVY_POS_CODE
        }
        url = '{}{}'.format(config.KARVY_BASE_URL,
                            config.KARVY_SEARCH_ALL_KRAS)
        s = Karvy.get_request_session()
        # this API frequently times out, hence using very high time-out
        r = s.get(url, params=params, timeout=30)
        if r.status_code == 200:
            # LOG.debug('\n{}\n'.format(r.text))
            # check sample XML to understand hierarchy of elements
            # we're interested in children of a tag <APP_PAN_INQ>
            # in this xml.
            pan_data = {}
            try:
                tree = ElementTree.fromstring(r.text.strip())
                root = ElementTree.fromstring(tree.text)
                # iterate through <APP_PAN_INQ>
                # and collect values in a dict.
                for elem in root[0].iter():
                    # skip self, empty tags
                    if elem.text and not elem.tag == 'APP_PAN_INQ':
                        pan_data[elem.tag] = elem.text
            except ElementTree.ParseError as p_err:
                LOG.error('Could not parse xml', exc_info=True, extra={
                    'request_url': r.url,
                    'response_text': r.text,
                    'data': {
                        'perr_customer_uid': self.uid,
                        'perr_customer_pan': pan_number
                    }
                })
                raise p_err

            # debug
            # LOG.debug('{}'.format(pan_data))

            # get current customer row
            c = self.session.query(Customer).\
                filter(Customer.uid == self.customer_uid).first()

            # this data includes status for all KRAs (CLV, Karvy etc.)
            # pull all status codes into a dict
            key = None
            status_codes = {}
            for key in ALL_KRA:
                kyc_status = pan_data.get('APP_{}_STATUS'.format(key), '')
                status_codes[key] = str(kyc_status)

            # LOG.debug('\n{}\n'.format(status_codes))
            # These will be returned.
            is_kyc_verified = ekyc_allowed = pan_kyc_allowed = False
            message = ''    # helpful message to user.

            # convert dict to convenient list
            items = [{'kra': k, 'status': s} for k, s in status_codes.items()]

            if all(i['status'] in ('', '05') for i in items):
                # no entry in KRA database, this customer goes to ekyc
                ekyc_allowed = True
                pan_kyc_allowed = True  # in case Aadhaar eKYC fails
                c.kyc_status = '05'
            else:
                # customer has entry in KRA database, act accordingly
                # writing all search generators
                banned = (i for i in items if i['status'] == '06')
                kyc_ok = (i for i in items if i['status'] in ('01', '02'))
                other = (i for i in items if i['status']
                         not in ('01', '02', '06'))

                # recursively finding a match
                item = next(banned, next(kyc_ok, next(other, None)))

                # this should not happen, but if it does, it gets thrown
                if not item:
                    error_message = 'Something unexplained occurred!'
                    LOG.error(error_message, exc_info=True, extra={
                        'status_code_items': '{}'.format(items),
                        'data': {
                            'customer_uid': self.uid,
                            'customer_pan': pan_number
                        }
                    })
                    raise ValueError(error_message)

                # now we check for: banned, kyc_ok, other <- in this order
                kra = item['kra']
                status = item['status']
                # banned_status
                if status == '06':
                    # '06' -> blocked by SEBI (e.g., Vijay Mallya)
                    # ALERT! This guy needs to be blocked everywhere
                    message = ('PAN is deactivated by {}, cannot '
                               'make any investments'.format(kra))
                elif status in ('01', '02'):
                    # this guy has KYC OK
                    is_kyc_verified = True
                else:
                    # some scenario where this guy has a KRA record
                    # but not 01, 02, 06 - so we need to send him to PAN
                    # based cKYC.
                    pan_kyc_allowed = True

                # update customer KRA info from item
                c.kra_name = kra = item['kra']
                c.kyc_status = item['status']
                # when this KYC was created and last updated.
                entry_dt_key = 'APP_{}_ENTRYDT'.format(kra)
                c.kyc_date = pan_data.get(entry_dt_key, None)
                if c.kyc_date:
                    c.kyc_created_on =\
                        datetime.strptime(c.kyc_date, r'%d-%m-%Y %H:%M:%S')  # NOQA
                mod_dt_key = 'APP_{}_MODDT'.format(kra)
                if mod_dt_key in pan_data and pan_data[mod_dt_key]:
                    c.kyc_updated_on =\
                        datetime.strptime(pan_data[mod_dt_key],
                                          r'%d-%m-%Y %H:%M:%S')
                # convert dates to UTC
                ist_offset = timedelta(hours=5, minutes=30)
                if c.kyc_created_on:
                    c.kyc_created_on = c.kyc_created_on - ist_offset
                if c.kyc_updated_on:
                    c.kyc_updated_on = c.kyc_updated_on - ist_offset

            # overwriting PAN number to make sure there's no
            # mismatch of data across columns.
            c.is_kyc_verified = is_kyc_verified
            c.pan_number = pan_number
            c.pan_name = pan_data.get('APP_NAME')
            if c.pan_name:
                # a crude check to flag name mismatch
                score = fuzz.token_set_ratio(c.pan_name.upper(),
                                             c.name.upper())
                if score < 90:
                    # this will also catch names which could have
                    # changed e.g., married women.
                    LOG.warn('PAN name mismatch!', extra={
                        'c_name': c.name,
                        'c_pan_name': c.pan_name,
                        'c_pan_number': c.pan_number,
                    })
            self.session.add(c)
            self.session.commit()

            if not any((is_kyc_verified, ekyc_allowed,  pan_kyc_allowed)):
                # Nothing is allowed, need to take this up offline
                if not message:
                    message = ('Cannot proceed with online investment, someone'
                               ' from our team will get in touch shortly')

            ret_val = {
                'is_kyc_verified': is_kyc_verified,
                'ekyc_allowed': ekyc_allowed,
                'pan_kyc_allowed': pan_kyc_allowed,
                'message': message,
            }
            # LOG.debug(ret_val)
            return ret_val
        else:
            # Karvy API status code is not 200
            error_message = 'Invalid status code - {}'.format(r.status_code)
            LOG.error(error_message, exc_info=True, extra={
                'r_status_code': r.status_code,
                'r_content': r.content,
                'data': {
                    'customer__uid': self.uid,
                    'customer__pan': pan_number
                }
            })
            raise ValueError(error_message)

    def get_all_pan_data(self):
        """
        Gets PAN master data from relevant KRA.
        ref: http://www.karvykra.com/service/pandownloadanykra.asmx
        """
        # get customer row
        # get current customer row
        c = self.session.query(Customer).\
            filter(Customer.uid == self.customer_uid).first()

        # customer should be KYC certified to fetch data
        if not c.is_kyc_verified:
            return

        url = '{}{}'.format(config.KARVY_BASE_URL, config.KARVY_PAN_FETCH)
        params = {
            'UserName': config.KARVY_USERNAME,
            'Password': config.KARVY_PASSWORD,
            'POSCode': config.KARVY_POS_CODE,
            'Type': c.kra_name,
            'Panno': c.pan_number,
            'DOB': c.date_of_birth.strftime(r'%d-%m-%Y'),
            'Userid': config.KARVY_USERID
        }

        s = Karvy.get_request_session()
        # using a very high timeout for this pan fetch.
        r = s.get(url, params=params, timeout=60)
        pan_data = {}
        if r.status_code == 200:
            try:
                tree = ElementTree.fromstring(r.text.strip())
                root = ElementTree.fromstring(tree.text)
                for elem in root[0].iter():
                    # skip self, empty tags
                    if elem.text and not elem.tag == 'APP_PAN_INQ':
                        pan_data[elem.tag] = elem.text
            except ElementTree.ParseError as p_ex:
                LOG.error('Could not parse {}'.format(r.text))
                raise p_ex
        else:
            LOG.warn("Karvy API failed to respond. Could not fetch pan data "
                     "for customer uid {}, response: {}".format(c.uid, r.text),
                     extra={
                         'api_response': r.text,
                         'uid': c.uid
                     })
            return
        # debug
        # LOG.info(pan_data)

        # update database with fetched values.
        # we're interested in only these fields
        # name as per pan
        c.pan_name = pan_data.get('APP_NAME', None)

        # gender: e.g 'F'
        c.gender = pan_data['APP_GEN']

        # correspondence address
        c.correspondence_address_1 = pan_data['APP_COR_ADD1']
        c.correspondence_address_2 = pan_data.get('APP_COR_ADD2')
        c.correspondence_city = pan_data['APP_COR_CITY']
        c.correspondence_pin = pan_data['APP_COR_PINCD']
        # this is a code, needs to be translated into text
        c.correspondence_state =\
            CamsStateLookup.get_state_from_pan_code(self.session,
                                                    pan_data['APP_COR_STATE'])

        # permanent address
        c.permanent_address_1 = pan_data.get('APP_PER_ADD1')
        c.permanent_address_2 = pan_data.get('APP_PER_ADD2')
        c.permanent_city = pan_data.get('APP_PER_CITY')
        c.permanent_pin = pan_data.get('APP_PER_PINCD')

        # this is a code, needs to be translated into text
        permanent_state = pan_data.get('APP_PER_STATE')
        if permanent_state:
            c.permanent_state =\
                CamsStateLookup.get_state_from_pan_code(self.session,
                                                        permanent_state)

        # marital status - convert code to text
        c.marital_status = pan_data.get('APP_MAR_STATUS')
        # occupation, convert code to text
        c.occupation_code = pan_data.get('APP_OCC')
        try:
            c.occupation = OCCUPATION[c.occupation_code]
        except KeyError:
            c.occupation = OCCUPATION['99']     # Unknown
        # LOG.debug('occupation {} - {}'.
        #           format(c.occupation_code, c.occupation))

        # Need to make sure that the PAN is of a resident individual.
        # This information is only available after a full PAN data download.
        try:
            if (pan_data['APP_TYPE'] != 'I'
                    or pan_data['APP_RES_STATUS'] != 'R'):
                LOG.warn('PAN: Customer is not Resident Indian', extra={
                    'APP_TYPE': pan_data['APP_TYPE'],
                    'APP_RES_STATUS': pan_data['APP_RES_STATUS'],
                    'c__pan_name': c.pan_name,
                    'c__pan_number': c.pan_number,
                })
                # setting is_kyc_verified to False to prevent transactions
                c.is_kyc_verified = False
        except Exception as ex:
            LOG.error('PAN: Resident Indian check failed', exc_info=True)

        # save to db.
        self.session.commit()

    @staticmethod
    def get_encrypted_password():
        """
        Fetches an encrypted password from Karvy, to be used in
        search_all_kras
        """
        # get encrypted password
        url = '{0}{1}'.format(config.KARVY_BASE_URL,
                              config.KARVY_GET_ENC_PASSWORD)
        params = {
            'PassKey': 'KARVY',
            'Password': config.KARVY_PASSWORD
        }
        s = Karvy.get_request_session()
        r = s.get(url, params=params)
        encrypted_password = None
        if r.status_code == 200:
            root = ElementTree.fromstring(r.content)
            encrypted_password = root.text
            # LOG.info('Encrypted password: {}'.format(encrypted_password))
            rdb = Karvy.get_redis_client()
            rdb.set(RDB_KARVY_ENC_PASS, encrypted_password, ex=86400)
        else:
            LOG.warn('Could not get encrypted password from Karvy')
        return encrypted_password

    def get_kyc_url(self, aadhaar, aadhaar_email, aadhaar_mobile):
        """
        Used for new KYC generation flow.
        """
        # fetch customer row from db.
        c = self.session.query(Customer).\
            filter(Customer.uid == self.customer_uid).first()

        if not c:
            raise Exception('Invalid customer_uid {}'.
                            format(self.customer_uid))

        # must have PAN (duh!)
        if not c.pan_number:
            raise Exception('Please provide PAN number before Aadhaar eKYC')

        # customer should not be kyc to go ahead
        # if c.is_kyc_verified:
        #     return {
        #         'error': 'Customer is already KYC verified'
        #     }

        # new transaction id to track kyc application.
        # this needs to be a unique integer, hence we piggy-back on the
        # customer's unique integer id.
        kyc_transaction_id = c.id

        # SOAP 1.2 request (hello, 1995!)
        # ref: http://ekyc.karvykra.com/partners/clients.asmx?op=RequestToken
        # don't ask how we do this.
        k_client = Client('{}?wsdl'.format(config.KARVY_KYC_TOKEN_URL))

        # Step 1. Generate a temporary token
        token = k_client.service.RequestToken(kyc_transaction_id,
                                              config.KARVY_KYC_TOKEN_PASS)
        # LOG.debug('token: {}'.format(token))

        c.kyc_transaction_id = kyc_transaction_id
        c.ekyc_submitted_on = datetime.utcnow()
        # save changes to database.
        self.session.commit()

        # Step 2. Get request id using the token and all params
        # NOTE: AgencyCode is actually POSCode, though docs do not say so.

        # may not know the pan_name at this stage.
        name = c.pan_name if c.pan_name else c.name
        email = aadhaar_email or c.email
        mobile = aadhaar_mobile or c.mobile
        request_id = k_client.service.GenerateDataRequest(
            config.KARVY_USERID,
            aadhaar,
            config.KARVY_POS_CODE,
            kyc_transaction_id,
            c.pan_number,
            name,
            mobile,
            email,
            token
        )

        try:
            kyc_webview_url = '{}?requestid={}'.\
                format(config.KARVY_KYC_WEBVIEW_URL, request_id)
            # LOG.debug('url: {}'.format(kyc_webview_url))
        except Exception as ex:
            LOG.error(ex)
            raise ex
        # LOG.debug('url: {}'.format(kyc_webview_url))
        # Include the success/failure/stop urls so client will know
        # when to hide webview etc.
        success_url = urljoin(config.KARVY_EKYC_CALLBACK_URL, 'success')
        failure_url = urljoin(config.KARVY_EKYC_CALLBACK_URL, 'failure')
        return {
            'url': kyc_webview_url,
            'success_url': success_url,
            'failure_url': failure_url,
            'stop_url': config.KARVY_EKYC_CALLBACK_URL
        }

    def validate_pan_number(self, pan_number):
        """
        Uses a service that hits the income tax database and tells us if
        this user's PAN exist.
        """
        # build params and send API request
        params = {
            'Adminusername': 'ARN-{}'.format(config.ARN),
            'Adminpassword': config.KARVY_ADMIN_PASSWORD,
            'fund': 128,
            'pan': pan_number.lower()
        }
        # use this for propagating exception.
        failed_message = 'PAN check in ITD failed'
        try:
            s = Karvy.get_request_session()
            r = s.get(config.KARVY_PAN_CHECK_URL, params=params, timeout=30)
            if r.status_code == 200:
                LOG.debug(r.content)
                # the xml response has two possibilities.
                # no such PAN in database, or valid PAN number.
                if NOT_IN_ITD in r.content.decode():
                    return False
                elif ITD_VALID in r.content.decode():
                    return True
            else:
                LOG.error(failed_message, extra={
                    'r_status_code': r.status_code,
                    'r_content': r.content,
                    'request_pan': pan_number
                }, exc_info=True)
                raise ValueError(failed_message)
        except Exception as ex:
            LOG.error('Could not check PAN in IT database', exc_info=True,
                      extra={'requested_pan': pan_number})
            raise ex

    @staticmethod
    def get_ckyc_id(pan_number):
        """
        Fetch the ckyc_id from the ckyc database using the karvykra API.
        This service will not be useful to fetch the 14-digit ckyc id's for those investor who
        have done CKYC through other intermediary; only those who are ckyc-ed through Sqrrl Platfrom.
        """

        # 1. Get encrypted password
        # Karvy.get_encrypted_password()
        redis_client = RedisWrapper().get_redis_client()
        encrypted_password = redis_client.get(RDB_KARVY_ENC_PASS)
        if encrypted_password:
            encrypted_password = encrypted_password.decode('ascii')
        else:
            encrypted_password = Karvy.get_encrypted_password()

        LOG.debug("The encrypted password is: {}".format(encrypted_password))

        # 2. Create a suds client for this request
        ckyc_api_client = Client('{}?wsdl'.format(config.CKYC_BASE_URL))

        ckyc_api_response = ckyc_api_client.service.GetCkycDetailsByPan(
                                                    Username=config.KARVY_USERNAME,
                                                    EncryptedPassword=encrypted_password,
                                                    Passkey='KARVY',
                                                    PanNo=pan_number)

        LOG.debug("The CKYC API has been queried for the PAN No.: {} ".format(pan_number))

        #LOG.debug("The response received: \n{} ".format(ckyc_api_response))

        # 3. Freeing the resources for easy GC
        rdb, encrypted_password, ckyc_api_client = None, None, None

        try:
            parsed_ckyc_api_response = sj.loads(ckyc_api_response)

            ckyc_id = [ row_item['CkycNumber'] for row_item in parsed_ckyc_api_response if 'CkycNumber' in row_item and row_item['CkycNumber'] and len(row_item['CkycNumber'])== 14][0]
            return ckyc_id

        except (IndexError, KeyError):
            # API gave the output. The ckyc_id may not be available right now for the customer though.
            return None

        except Exception as e:
            # Logging this exception may be very useful for debugging the issue concisely.
            LOG.error("An exception has occurred while fetching the ckyc_id from the CKYC API. The exception is: {}".format(e))
            return None
