# helpers.py
# some helper methods.
from datetime import datetime, timedelta
from math import ceil
import simplejson as json
import requests
import boto3
from boto3.s3.transfer import TransferConfig
from botocore.client import Config

from app import config


# this is the offset to convert IST to UTC and vice versa.
IST_OFFSET = timedelta(hours=5, minutes=30)


def monthdelta(target_date, today=None):
    """
    Computes number of months till given date, including that month and
    current month.
    """
    # 'today' can be passed as start date for computing the delta.
    if not today:
        today = datetime.utcnow()
    assert target_date > today
    # first get the months if same year
    if target_date.year == today.year:
        return target_date.month - today.month + 1
    elif target_date.month < today.month:
        # less than one year if just looking at months.
        return (target_date.year - today.year - 1) * 12\
            + (12 - today.month + 1) + target_date.month
    else:
        return (target_date.year - today.year) * 12\
                + (target_date.month - today.month + 1)


def weekdelta(target_date, today=None):
    """
    Computes number of weeks till given date.
    """
    if not today:
        today = datetime.utcnow()
    assert target_date > today
    date_delta = target_date - today
    return ceil(date_delta.days / 7.0)


def get_duration_bucket(target_date):
    """
    For a given date figure out which of the following buckets
    it falls in:
    0 - 6 months, 6 - 12, 12 - 24, 24 - 36, and so on till 120 months.

    Returns an integer 0-9 for the corresponding bucket.
    """
    # today.
    today = datetime.utcnow()
    # target should be in the future.
    assert target_date > today

    months = [0, 6, 12, 24, 36, 60, 72, 84, 108, 120]
    # find the buckets by searching from behind where target_date falls.
    try:
        result = next(i for i in reversed(range(len(months)))
                      if target_date > today + timedelta(days=30*months[i]))
    except StopIteration:
        # should never come here, but if it does...
        result = 0
    return result


def get_ifsc_info(ifsc):
    """
    For a given ifsc code, ping razorpay API to get branch etc.
    """
    # TODO: implement caching.
    # Let caller implement exception handling
    r = requests.get('https://ifsc.razorpay.com/{}'.format(ifsc.upper()))
    if r.status_code == 200:
        ret_val = {}
        # need to clean up some non-ASCII characters in Razorpay response
        for key, value in r.json().items():
            try:
                if value:
                    ret_val[key] = value.encode('ASCII', errors='ignore').\
                                         decode('ASCII')
            except Exception:
                ret_val[key] = value
        return ret_val


def get_ist_now():
    """
    Returns UTC now after converting it to IST
    """
    now = datetime.utcnow()
    # simply add 5:30 to UTC, don't need daylight-savings-aware libs
    ist_offset = timedelta(hours=5, minutes=30)
    return now + ist_offset


def round_up(a, b):
    """
    ref: http://stackoverflow.com/a/8866377
    For a non-negative a and positive b, round a to the nearest multiple of b
    """
    if a >= 0 and b > 0:
        return (a + b - 1) // b * b
    else:
        # silently return un-modified value
        return a


def to_ist(dt):
    """
    Converts from UTC to IST
    """
    if isinstance(dt, datetime):
        return dt + IST_OFFSET
    else:
        raise ValueError('{} is not a valid datetime'.format(dt))


def from_ist(dt):
    """
    Converts IST to UTC
    """
    if isinstance(dt, datetime):
        return dt - IST_OFFSET
    else:
        raise ValueError('{} is not a valid datetime'.format(dt))


def send_transaction_sms(sms):
    """
    Uses msg91 API to send transaction sms.
    Ref: https://control.msg91.com/apidoc/textsms/send-sms-v1.php

    @params
    sms - array in the format [{'message': 'hello', 'to': ['99xxx']}]
    """
    assert config.SMS_API_URL and config.SMS_SENDER
    assert config.SMS_API_AUTHKEY
    if not sms:
        raise ValueError('No sms parameter provided!')

    headers = {
        'Content-Type': 'application/json',
        'authkey': config.SMS_API_AUTHKEY
    }

    # API request body
    body = {
        'sender': config.SMS_SENDER,
        'route': '4',   # 4 - transactions, 1 - promotions.
        'country': '91',    # hard-coded.
        'flash': 0,
        'sms': sms
    }

    if not config.DEBUG:
        r = requests.post(url=config.SMS_API_URL,
                          data=json.dumps(body), headers=headers)
        if r.status_code != 200:
            raise Exception('Sms sending failed!')


class S3Helper():
    """
    Utility that wraps upload and download from S3
    """
    s3client = None

    @classmethod
    def get_client(cls):
        if not cls.s3client:
            cfg = Config(signature_version='s3v4')
            cls.s3client = boto3.client('s3', config.AWS_REGION, config=cfg)
        return cls.s3client

    def __init__(self):
        # disable threads, they cause issues with Celery sometimes
        self.tcfg = TransferConfig(use_threads=False)

    def upload_file(self, key, file_path):
        s3 = S3Helper.get_client()
        s3.upload_file(file_path, config.ATTACHMENT_BUCKET, key,
                       Config=self.tcfg)

    def download_file(self, key, file_path):
        # disable threads, they cause issues with Celery sometimes
        s3 = S3Helper.get_client()
        s3.download_file(config.ATTACHMENT_BUCKET, key, file_path,
                         Config=self.tcfg)


# this keeps things backward compatible, without replacing usage
s3h = S3Helper()
s3_file_upload = s3h.upload_file
s3_file_download = s3h.download_file


def xirr(transactions):
    """
    Based on http://stackoverflow.com/a/11503492

    Computes the annualised rate of return for a set of transactions,
    i.e., purchases and redemptions, along with their dates, and the current
    value of the investment computed from NAV and total units.

    Args:
        transactions (list): list of tuples of date, amount. The final element
        should be tuple of (nav_date, current_value) of the investment.
        NOTE: investments (purchases) should have negative sign, redemptions
        should have no sign, i.e., positive.

    Returns:
        float: The calculated annualised rate of return.
    """
    years = [(ta[0] - transactions[0][0]).days / 365.0 for ta in transactions]
    residual = 1
    step = 0.05
    guess = 0.05
    epsilon = 0.0001
    limit = 10000
    while abs(residual) > epsilon and limit > 0:
        limit -= 1
        residual = 0.0
        for i, ta in enumerate(transactions):
            residual += ta[1] / pow(guess, years[i])
        if abs(residual) > epsilon:
            if residual > 0:
                guess += step
            else:
                guess -= step
                step /= 2.0
    return guess-1


def mask_string(input_string, unmasked_pos=[], with_char='*'):
    """
    For a given input string, replace all characters with the given character,
    except those in position unmasked_pos
    """
    if input_string:
        chars = [with_char] * len(input_string)
        for i in unmasked_pos:
            chars[i] = input_string[i]
        return ''.join(chars)


def errors_to_desc(errors):
    """
    Takes cerberus error dict and converts it to a string that can be displayed
    to the user.
    """
    msg = []

    for k in errors:
        msg.append('\"{}\": {}'.format(k, ';'.join(errors[k])))

    return '. '.join(msg)
