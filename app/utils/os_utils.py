import os
import errno
from pathlib import Path
from app import log

LOG = log.get_logger()


def dir_exists(directory_path):
    return os.path.exists(directory_path)


def file_exists(file_path):
    return os.path.isfile(file_path) or Path(str(file_path)).is_file()


def create_dirs(directory_path):
    try:
        os.makedirs(directory_path)
        LOG.debug("Directory {} created.".format(directory_path))

    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc

        LOG.debug("Directory {} already exists, cannot create new."
                  .format(directory_path))
        pass
