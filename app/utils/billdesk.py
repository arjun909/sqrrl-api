# billdesk.py
from datetime import datetime, timedelta
import hmac
from hashlib import sha256
import redis
import requests

from app import config
from app.utils.helpers import get_ist_now
from app.model import (Investment, Customer, BilldeskBankLookup,
                       BilldeskAmcLookup, Transaction)
from app.constants import (
    BILLDESK_AUTH_SUCCESS, TR_STATUS_FAILURE, TR_STATUS_PROCESSING,
    REDIS_BD_LOCK_PREFIX, BILLDESK_AUTH_ERR_PENDING,
    PAYMENT_MODE_NB,
)
from app.log import get_logger


LOG = get_logger()


class Billdesk(object):
    """
    Wrapper over billdesk service.
    """
    # the redis client - at class level because we want to re-use connection.
    rdb = None
    r_session = None

    @classmethod
    def get_redis_client(cls):
        if not cls.rdb:
            # new connection.
            cls.rdb = redis.StrictRedis.from_url(config.REDIS_URL)
        return cls.rdb

    @classmethod
    def get_request_session(cls):
        """
        A class-level variable for requests session.
        """
        if not cls.r_session:
            cls.r_session = requests.Session()
        return cls.r_session

    def __init__(self, customer_uid, session):
        self.session = session
        if customer_uid:
            self.customer_uid = customer_uid
            self.customer = self.session.query(Customer).\
                filter(Customer.uid == self.customer_uid).first()
            if not self.customer:
                raise Exception(msg='No customer for uid {}'.
                                format(customer_uid))

    def get_transaction_url(self, investment_group_id,
                            transaction_group_id, amount,
                            payment_mode=PAYMENT_MODE_NB):
        """
        Gets a transaction url for investing `amount` into investment_group_id
        """
        self.investment_group_id = investment_group_id
        LOG.debug('Invest {} in {}'.format(amount, investment_group_id))

        # this amount will be split across the diff. schemes
        # need AMC - amount split for this transaction
        investments = self.session.query(Investment).\
            filter(Investment.group_id == investment_group_id)
        t_split = []
        for row in investments:
            # TODO: check rounding issues here
            t_amount = row.allocation_percent * amount / 100
            t_amc = BilldeskAmcLookup.find_by_amc(row.scheme.amc_name,
                                                  self.session)
            t_split.append((t_amc,
                            '{:.2f}'.format(t_amount),
                            row.scheme.rta_code))
        # LOG.debug('t_split {}'.format(t_split))
        params = self.generate_transaction_params(transaction_group_id,
                                                  t_split, amount, payment_mode)  # NOQA
        # This is a POST request, so return params separately
        if payment_mode == PAYMENT_MODE_NB:
            url = config.BILLDESK_URL
        else:
            url = config.BILLDESK_DEBIT_URL
        return url, params

    def generate_transaction_params(self, tr_group_id, t_split, amount,
                                    payment_mode=PAYMENT_MODE_NB):
        """
        Generates params for the Billdesk API call.
        """
        # need to generate a | separated string with all params as per doc

        # sourceid - internal to billdesk, pass 'NA'
        # billerid is merchant id (why can't they be consistent?)
        params = {
            'sourceid': 'NA',
            'billerid': config.BILLDESK_MERCHANTID.upper(),
        }
        # need lowercase merchant id as well.
        params['billerid_lower'] = params['billerid'].lower()

        # amount, xx.xx format
        params['txn_amount'] = '{:.2f}'.format(amount)

        # return url
        params['RU'] = config.BILLDESK_RETURN_URL

        # for netbanking...
        if payment_mode == PAYMENT_MODE_NB:
            # get 3 character bank code (e.g., IDB)
            try:
                bank_code = BilldeskBankLookup.find_by_ifsc(
                    self.customer.bank_ifsc, self.session)
                if not bank_code:
                    raise ValueError('No bank code found for {}, '
                                     'try debit card'.
                                     format(self.customer.bank_name))
            except Exception as ex:
                LOG.error('No bank_code found', exc_info=True)
                raise ex
        else:
            # rupay/debit card
            bank_code = 'NA'
        params['txtBankID'] = bank_code

        # order number: use transaction_goup_id
        params['additional_info1'] = tr_group_id

        # folio no. - Billdesk doesn't use this ever since they started
        # supporting multiple folio transactions, this is for internal
        # reconciliation. Pass investment_group_id here.
        params['additional_info2'] = self.investment_group_id

        # User id - to identify the customer.
        params['additional_info3'] = self.customer_uid

        # distributor id ARN-xxx
        params['additional_info4'] = 'ARN-{}'.format(config.ARN)

        # AMC1-AMC2-AMC3-AMC4, use NA where not available.
        # same with amounts and scheme codes.
        amc_codes = []
        amc_amounts = []
        scheme_codes = []
        for i in range(4):
            try:
                amc_codes.append(t_split[i][0])
                amc_amounts.append(t_split[i][1])
                scheme_codes.append(t_split[i][2])
            except IndexError:
                amc_codes.append('NA')
                amc_amounts.append('NA')
                scheme_codes.append('NA')

        # AMC codes.
        params['additional_info5'] = '-'.join(amc_codes)

        # transaction time and amount, e.g, 20080115091010-AMT1-AMT2-AMT3-AMT4
        # this transaction time MUST be in IST (not UTC)
        now = datetime.utcnow()
        # simply add 5:30 to UTC, don't need daylight-savings-aware libs
        ist_offset = timedelta(hours=5, minutes=30)
        ist_now = (now + ist_offset).strftime(r'%Y%m%d%H%M%S')
        combined = [ist_now]
        combined.extend(amc_amounts)
        params['additional_info8'] = '-'.join(combined)

        # fund type, default to NONLIQUID as LIQUID is handled by Reliance API
        # NOTE: this is hard-coded in Billdesk, not very important.
        params['additional_info6'] = 'NONLIQUID'

        # investor tax status
        params['additional_info7'] = 'RESIDENT'

        # scheme codes from RTA
        params['additional_info9'] = '-'.join(scheme_codes)

        # SIP registration codes. Pass 'NA'
        params['additional_info10'] = 'NA'

        # Transaction Type: 'L' for Lumpsum Purchase & 'S' for SIP Purchase.
        # this is hard-coded as well
        params['additional_info11'] = 'L'

        # AMC Code, NA, already passed in additional_info5
        params['additional_info12'] = 'NA'

        # some parameters differ across net banking and debit card.
        if payment_mode == PAYMENT_MODE_NB:
            # Bank account no needed for Net banking
            params['txtMerchantUserRefNo'] = self.customer.bank_account_no
            # billdesk “product code” - to be hard-coded
            params['product_code'] = 'DIRECT'
        else:
            params['txtMerchantUserRefNo'] = 'NA'
            params['product_code'] = 'NA'

        # msg param created with the above params as a | separated string.
        # in this manner:
        # SQRRL|additional_info1|txtMerchantUserRefNo|txn_amount|txtBankID|
        # NA|NA|INR|DIRECT|R|sqrrl|NA|NA|F|additional_info2|additional_info3|
        # additional_info4|additional_info5|addition al_info6|
        # additional_info7-additional_info9-additonal_info10-additional_info11-
        # additional_info12-sourceid|additional_info8|RU|Checksum
        msg = ('{billerid}|{additional_info1}|{txtMerchantUserRefNo}|'
               '{txn_amount}|{txtBankID}|NA|NA|INR|{product_code}|R|'
               '{billerid_lower}|NA|NA|F|{additional_info2}|'
               '{additional_info3}|{additional_info4}|{additional_info5}|'
               '{additional_info6}|{additional_info7}-{additional_info9}-'
               '{additional_info10}-{additional_info11}-{additional_info12}'
               '-{sourceid}|{additional_info8}|{RU}').format(**params)

        # API-56 - saved debit cards
        token_details = ''
        if payment_mode != PAYMENT_MODE_NB:
            # need to suffix request token to this request msg
            token_id = self.get_request_token(tr_group_id)
            token_details = 'CP1005!{merchant_id}!{token}!NA!NA!NA'.\
                format(merchant_id=config.BILLDESK_MERCHANTID.upper(),
                       token=token_id)
            LOG.debug('token_details: {}'.format(token_details))

        # compute checksum using SHA256, make uppercase, append to msg
        key = config.BILLDESK_CHECKSUM_KEY.encode('UTF-8')
        if token_details:
            # in case of debit card, use msg|token_details for checksum
            value = '{}|{}'.format(msg, token_details)
            msg_template = '{msg}|{checksum}|{token_details}'
        else:
            # non-debit card scenario - use just the msg for checksum
            value = msg
            msg_template = '{msg}|{checksum}'

        # compute checksum using key
        checksum = hmac.new(key, value.encode('UTF-8'),
                            digestmod=sha256).hexdigest().upper()

        # now combine the msg, checksum and (optional) token_details
        # for the new msg
        final_msg = msg_template.format(msg=msg, checksum=checksum,
                                        token_details=token_details)
        # LOG.debug('msg={}'.format(final_msg))

        # Billdesk will need a POST request with the following form / params
        # in order to redirect to the netbanking/debit url.
        if payment_mode == PAYMENT_MODE_NB:
            return {
                'hidRequestId': config.BILLDESK_PARAM_REQUEST_ID,
                'hidOperation': config.BILLDESK_PARAM_OPERATION,
                'msg': final_msg
            }
        else:
            return {
                'hidRequestId': config.BILLDESK_DEBIT_REQUEST_ID,
                'msg': final_msg
            }

    def update_transaction(self, msg, ignore_checksum=False):
        """
        Processes response from Billdesk - checks whether transaction went
        through successfully, and updates transaction
        """
        # LOG.info('Inside update_transaction with msg: {}'.format(msg))

        msg_tokens = msg.split('|')
        transaction_group_id = msg_tokens[1]
        LOG.debug('transaction_group_id: {}'.format(transaction_group_id))

        # a redis-based "lock" to make a GET for this transaction wait
        # till this update is complete.
        if transaction_group_id:
            rdb = Billdesk.get_redis_client()
            lock = '{}{}'.format(REDIS_BD_LOCK_PREFIX, transaction_group_id)
            if rdb.exists(lock):
                # already locked, return
                LOG.warn('already locked: {}'.format(lock))
                return
            else:
                # TODO: Add expiry time?
                rdb.lpush(lock, 0)  # function is initiated.

        if transaction_group_id == 'NA':
            transaction_group_id = None

        if not transaction_group_id:
            # nothing to do!
            LOG.warn('Nothing to process; msg: {}'.format(msg))
            return

        pg_transaction_id = msg_tokens[2]
        bank_transaction_id = msg_tokens[3]

        # TODO this is in IST, convert to UTC
        try:
            bank_transaction_on = datetime.strptime(msg_tokens[13],
                                                    r'%d-%m-%Y %H:%M:%S')
        except:
            bank_transaction_on = None

        auth_status = msg_tokens[14]
        status = TR_STATUS_FAILURE
        note = msg_tokens[-2]

        if auth_status == BILLDESK_AUTH_SUCCESS:
            LOG.debug('billdesk: success! {}'.format(transaction_group_id))
            # we still need to validate the checksum of the response
            # against the passed checksum, to validate this transaction.
            status = TR_STATUS_PROCESSING
            if not ignore_checksum:
                key = config.BILLDESK_CHECKSUM_KEY.encode('UTF-8')
                checksum = hmac.new(key, '|'.join(msg_tokens[:-1]).
                                    encode('UTF-8'),
                                    digestmod=sha256).hexdigest().upper()
                if checksum != msg_tokens[-1]:
                    note = 'Payment rejected (checksum mismatch)'
                    status = TR_STATUS_FAILURE
                    LOG.warn(note, extra={
                        'msg_param': msg
                    })
        else:
            if auth_status == BILLDESK_AUTH_ERR_PENDING:
                message = 'Billdesk transaction 0002'
                note = '0002: Billdesk got no confirmation from bank.'
                try:
                    # notify the customer via sms.
                    Transaction.notify_failed_transaction(
                        self.session, transaction_group_id
                    )
                except Exception as ex:
                    LOG.error('0002 sms notification error', exc_info=True)
            else:
                message = 'Billdesk transaction failed'
            data = {
                'response_msg': msg,
                'transaction_group_id': transaction_group_id,
                'auth_status': auth_status,
            }
            # sentry_client.captureMessage(message=message, data=data)
            LOG.debug(data)
            LOG.error('{}'.format(message), extra=data)

        # update transaction in db
        try:
            Transaction.update_group(transaction_group_id, self.session,
                                     status=status, note=note,
                                     bank_transaction_id=bank_transaction_id,
                                     bank_transaction_on=bank_transaction_on,
                                     reference=pg_transaction_id)
        except Exception as ex:
            LOG.error('[billdesk] transaction update error', exc_info=True)
        finally:
            # clear the lock
            LOG.debug('deleting lock: {}'.format(lock))
            rdb.delete(lock)

    def query_transaction(self, transaction_group_id):
        """
        For a given transaction_group_id, checks the status from billdesk
        API.

        Use this for a 0002 billdesk transaction within 48 hours to check
        if this transaction has succeeded.
        """
        LOG.debug('Checking status for {}'.format(transaction_group_id))
        params = {
            'merchant': config.BILLDESK_MERCHANTID.upper(),
            'transaction': transaction_group_id,
            'timestamp': ''
        }
        # current timestamp in IST
        now = datetime.utcnow() + timedelta(hours=5, minutes=30)
        params['timestamp'] = now.strftime(r'%Y%m%d%H%M%S')
        msg = '0122|{merchant}|{transaction}|{timestamp}'.format(**params)

        # compute checksum using SHA256, make uppercase, append to msg
        key = config.BILLDESK_CHECKSUM_KEY.encode('UTF-8')
        checksum = hmac.new(key, msg.encode('UTF-8'),
                            digestmod=sha256).hexdigest().upper()
        msg = '{msg}|{checksum}'.format(msg=msg, checksum=checksum)

        # LOG.debug('msg: {}'.format(msg))
        r = requests.post(config.BILLDESK_QUERY_URL, data={'msg': msg})
        if r.status_code == 200:
            # r.content is a byte object
            # LOG.debug('response {}'.format(r.content))
            try:
                response_msg = r.content.decode('utf-8')
                # LOG.debug(response_msg)
                bd_response = response_msg.split('|')
                transaction_reference = bd_response[3]
                auth_status = bd_response[15]
                LOG.debug('auth_status: {}'.format(auth_status))
                # if the status of this transaction has changed to success,
                # update the db.
                if auth_status == BILLDESK_AUTH_SUCCESS:
                    LOG.info('Billdesk transaction succeeded', extra={
                        'transaction_reference': transaction_reference
                    })
                    # transform this msg, which has an additional status,
                    # to a format which works with update_transaction
                    # additionally, we turn of checksum check as this will
                    # not match for just a part of the msg.
                    msg_param = response_msg.split('|', 1)[1]
                    self.update_transaction(msg_param, True)
            except Exception as ex:
                LOG.error(ex, exc_info=True)

    def get_request_token(self, tr_group_id):
        """
        In order to implement saved debit cards there is an extra step
        to fetch a request token for a customer, then include that token with
        the msg parameter for redirecting customer to payment store.
        """

        # this parameter is to be POST-ed to Billdesk.
        msg = (
            'CS1006|{source_id}|{trace_no}|{user_id}|'
            'ALL|{request_date}|NA|NA|NA'
        )

        # passing these parameters
        params = {
            'source_id': config.BILLDESK_MERCHANTID.upper(),
            'trace_no': tr_group_id,
            'user_id': self.customer_uid
        }

        # convert params to a key, use that to set fetched token in cache
        # a cache prevents the odd scenario where a request for the same trx
        # is triggered a second time, in which case Billdesk will throw an
        # error.
        cache_key = 'BDTOKEN|{user_id}|{trace_no}'.format(**params)
        rdb = Billdesk.get_redis_client()

        cached_value = rdb.get(cache_key)
        if cached_value:
            LOG.debug('returning cached token: {}'.format(cached_value))
            return cached_value

        # nothing is cached, use fresh token from server
        # date time stamp in YYYYMMDDHHMMSS format
        params['request_date'] = get_ist_now().strftime(r'%Y%m%d%H%M%S')

        msg = msg.format(**params)
        # LOG.debug('msg before checksum: \n{}'.format(msg))

        # compute checksum using SHA256, make uppercase, append to msg
        key = config.BILLDESK_CHECKSUM_KEY.encode('UTF-8')
        checksum = hmac.new(key, msg.encode('UTF-8'),
                            digestmod=sha256).hexdigest().upper()
        msg = '{msg}|{checksum}'.format(msg=msg, checksum=checksum)
        # LOG.debug('msg: \n{}'.format(msg))

        s = Billdesk.get_request_session()
        result = s.get(config.BILLDESK_MANAGE_URL, params={'msg': msg})
        if result.status_code == 200:
            LOG.debug(result.content.decode('utf-8'))
            # parse result, get important stuff
            result_msg_tokens = result.content.decode('utf-8').split('|')
            status = result_msg_tokens[3]
            if status != 'Y':
                # API call failed
                error_code = result_msg_tokens[4]
                error_description = result_msg_tokens[5]
                trace_no = result_msg_tokens[2]
                LOG.error('Unable to fetch token id from Billdesk', extra={
                    'error_code': error_code,
                    'error_description': error_description,
                    'trace_no': trace_no
                })
            else:
                token_id = result_msg_tokens[-2]
                LOG.debug('Got token_id: {}'.format(token_id))
                # cache for a minute, handles duplicate requests
                rdb.set(cache_key, token_id, 60)
                return token_id
        else:
            LOG.error('Could not get request token', extra={
                'result_status_code': result.status_code,
                'result_content': result.content
            })
