# Sqrrl API

API to power [Sqrrl](https://sqrrl.in) mobile apps. 

Current status: Creating specifications.

## Development notes

- The `blueprint` contains API specifications in [API Blueprint][1] format.

[1]: https://apiblueprint.org
