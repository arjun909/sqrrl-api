from datetime import datetime
from fabric.api import local, env, run, prefix, settings, cd, get
from fabric.operations import put

env.use_ssh_config = True
env.colorize_errors = True

APP_HOME = '~/api'
TAR_HOME = '/tmp'
VENV = 'api'
LOGFILE = '/var/log/sqrrl/web-1.log'
GREP_CMD = ('grep -io "^[^#]*DATABASE_URL.*\(postgres.*\)" .env '
            '| grep -io "postgres[^\']*"')


def prepare(env_file='.env'):
    """
    Steps to run before deployment
    """
    local('git archive master --format tar -o latest.tar')
    if env_file != '.env':
        # temporarily backup .env file, then push the provided .env
        # into the generated tar
        local('cp .env .env.bak && cp {} .env'.format(env_file))
        local('tar -u -f latest.tar .env')
        local('cp .env {} && mv .env.bak .env'.format(env_file))


def build():
    """
    post local tar code to server(s), and build it there.
    """
    put('latest.tar', TAR_HOME)
    # run('source ~/.virtualenvs/api/bin/activate')
    # run('pwd')
    # run('which python')
    with prefix('workon {}'.format(VENV)):
        run('tar xf {}/latest.tar'.format(TAR_HOME), quiet=True)
        run('rm {}/latest.tar'.format(TAR_HOME), quiet=True)
        run('pip -q install -r requirements.txt')
        # run any db migrations
        with settings(warn_only=True):
            run('alembic upgrade heads')
            # stop and start web server, workers
            run('sudo stop sqrrl')
        run('sudo start sqrrl')


def clean():
    """
    Clean up!
    """
    local('rm latest.tar')


def update_config(env_file='.env'):
    """
    Updates .env file and restarts app server.
    """
    put(env_file, APP_HOME)
    with cd(APP_HOME):
        run('mv {} .env'.format(env_file))
        with settings(warn_only=True):
            # stop and start web server, workers
            run('sudo stop sqrrl')
        run('sudo start sqrrl')


def deploy(env_file='.env'):
    """
    Putting it all together
    """
    prepare(env_file)
    build()
    clean()


def run_sql(sql_file):
    """
    Runs an sql file against DATABASE_URL given in .env file
    """
    with cd(APP_HOME):
        # grep command excludes comments then searches postgres db
        db_url = run(GREP_CMD, quiet=True)
        # print('Using database {}'.format(db_url))
        run('psql -d {} < {}'.format(db_url, sql_file))


def dump_livedb(localdb, localdbuser):
    """
    Dumps the live database into localdb
    """
    with prefix('workon {}'.format(VENV)):
        db_url = run(GREP_CMD, quiet=True)
        print('db_url: {}'.format(db_url))
        out_fname = '/tmp/{}.tar'.format(datetime.utcnow().isoformat())
        run('pg_dump -O {} > {}'.format(db_url, out_fname))
    # refresh localdb
    # fetch tar dump into same file location.
    get(out_fname, out_fname)
    with settings(warn_only=True):
        local('dropdb {}'.format(localdb))
    local('createdb -O {} {}'.format(localdbuser, localdb))
    local('psql {} -c \'create extension hstore;\''.format(localdb))
    local('psql -U {} {} < {}'.format(localdbuser, localdb, out_fname))
    local('rm {}'.format(out_fname))
    run('rm {}'.format(out_fname))


def tail(grep=None, logfile=LOGFILE):
    if not grep:
        run('tail {}'.format(logfile))
    else:
        run('tail {} | grep -i \'{}\''.format(logfile, grep))
