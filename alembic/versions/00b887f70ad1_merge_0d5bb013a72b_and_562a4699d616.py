"""merge 0d5bb013a72b and 562a4699d616

Revision ID: 00b887f70ad1
Revises: 0d5bb013a72b, 562a4699d616
Create Date: 2017-03-28 13:17:24.238710

"""

# revision identifiers, used by Alembic.
revision = '00b887f70ad1'
down_revision = ('0d5bb013a72b', '562a4699d616')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
