"""empty message

Revision ID: d6e6063a11a4
Revises: 99c281d3ec64, c9c1803379cf
Create Date: 2017-08-18 13:55:11.634639

"""

# revision identifiers, used by Alembic.
revision = 'd6e6063a11a4'
down_revision = ('99c281d3ec64', 'c9c1803379cf')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
