"""investment.category

Revision ID: d225893d066b
Revises: 2cb89fae0c19
Create Date: 2016-11-25 12:06:20.528732

"""

# revision identifiers, used by Alembic.
revision = 'd225893d066b'
down_revision = '2cb89fae0c19'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('investment', sa.Column('category', sa.String(), nullable=True))
    op.add_column('investment', sa.Column('target_roi', sa.Numeric(scale=4), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('investment', 'target_roi')
    op.drop_column('investment', 'category')
    ### end Alembic commands ###
