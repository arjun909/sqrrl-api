"""scheme.rta

Revision ID: fcaf36a0509a
Revises: 5a6240fd6bc6
Create Date: 2017-01-02 15:26:32.136155

"""

# revision identifiers, used by Alembic.
revision = 'fcaf36a0509a'
down_revision = '5a6240fd6bc6'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('scheme', sa.Column('rta', sa.String(), nullable=False, server_default='Karvy'))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('scheme', 'rta')
    ### end Alembic commands ###
