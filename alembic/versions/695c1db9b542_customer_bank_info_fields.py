""" customer bank info fields

Revision ID: 695c1db9b542
Revises: ee25f4076f15
Create Date: 2016-09-20 14:25:34.200707

"""

# revision identifiers, used by Alembic.
revision = '695c1db9b542'
down_revision = 'ee25f4076f15'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('customer', sa.Column('bank_account_name', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_account_no', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_branch_address', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_branch_city', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_branch_name', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_ifsc', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_mandate_status', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_nach_mandate_number', sa.String(), nullable=True))
    op.add_column('customer', sa.Column('bank_name', sa.String(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('customer', 'bank_name')
    op.drop_column('customer', 'bank_nach_mandate_number')
    op.drop_column('customer', 'bank_mandate_status')
    op.drop_column('customer', 'bank_ifsc')
    op.drop_column('customer', 'bank_branch_name')
    op.drop_column('customer', 'bank_branch_city')
    op.drop_column('customer', 'bank_branch_address')
    op.drop_column('customer', 'bank_account_no')
    op.drop_column('customer', 'bank_account_name')
    ### end Alembic commands ###
