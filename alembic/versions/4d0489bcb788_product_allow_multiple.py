"""product.allow_multiple

Revision ID: 4d0489bcb788
Revises: b1fcf31c7ce7
Create Date: 2016-10-07 11:00:49.079621

"""

# revision identifiers, used by Alembic.
revision = '4d0489bcb788'
down_revision = 'b1fcf31c7ce7'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('customer_sms', 'balance_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('customer_sms', 'transaction_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'available_withdrawal_balance',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'current_value',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'net_gain',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'net_gain_percentage',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'net_investment',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'scheme_units',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=3),
               existing_nullable=True)
    op.alter_column('investment', 'target_instalment_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'total_investment',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'total_target_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('investment', 'total_withdrawal',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.add_column('product', sa.Column('allow_multiple', sa.Boolean(), nullable=True))
    op.alter_column('scheme', 'minimum_initial_investment',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_redemption_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_redemption_units',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_sip_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_subsequent_investment',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'purchase_minimum_increment_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'redemption_incremental_units',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'stp_in_minimum_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'stp_out_minimum_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'switch_in_minimum_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'switch_out_minimum_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    op.alter_column('scheme', 'swp_minimum_amount',
               existing_type=sa.NUMERIC(),
               type_=sa.Numeric(scale=2),
               existing_nullable=True)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('scheme', 'swp_minimum_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'switch_out_minimum_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'switch_in_minimum_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'stp_out_minimum_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'stp_in_minimum_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'redemption_incremental_units',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'purchase_minimum_increment_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_subsequent_investment',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_sip_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_redemption_units',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_redemption_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('scheme', 'minimum_initial_investment',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.drop_column('product', 'allow_multiple')
    op.alter_column('investment', 'total_withdrawal',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'total_target_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'total_investment',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'target_instalment_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'scheme_units',
               existing_type=sa.Numeric(scale=3),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'net_investment',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'net_gain_percentage',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'net_gain',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'current_value',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('investment', 'available_withdrawal_balance',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('customer_sms', 'transaction_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    op.alter_column('customer_sms', 'balance_amount',
               existing_type=sa.Numeric(scale=2),
               type_=sa.NUMERIC(),
               existing_nullable=True)
    ### end Alembic commands ###
