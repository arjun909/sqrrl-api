"""scheme returns

Revision ID: c4bd87189461
Revises: 8edf82d78ada
Create Date: 2016-11-15 22:58:19.082473

"""

# revision identifiers, used by Alembic.
revision = 'c4bd87189461'
down_revision = '8edf82d78ada'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('scheme', sa.Column('amfi_code', sa.String(), nullable=True))
    op.add_column('scheme', sa.Column('five_years_return', sa.Numeric(scale=2), nullable=True))
    op.add_column('scheme', sa.Column('isin', sa.String(), nullable=True))
    op.add_column('scheme', sa.Column('objective', sa.String(), nullable=True))
    op.add_column('scheme', sa.Column('one_month_return', sa.Numeric(scale=2), nullable=True))
    op.add_column('scheme', sa.Column('one_year_return', sa.Numeric(scale=2), nullable=True))
    op.add_column('scheme', sa.Column('six_months_return', sa.Numeric(scale=2), nullable=True))
    op.add_column('scheme', sa.Column('three_months_return', sa.Numeric(scale=2), nullable=True))
    op.add_column('scheme', sa.Column('three_years_return', sa.Numeric(scale=2), nullable=True))
    op.drop_column('scheme', 'scheme_account_ifsc')
    op.drop_column('scheme', 'scheme_account_no')
    op.drop_column('scheme', 'pg_scheme_code')
    op.drop_column('scheme', 'scheme_bank_name')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('scheme', sa.Column('scheme_bank_name', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.add_column('scheme', sa.Column('pg_scheme_code', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.add_column('scheme', sa.Column('scheme_account_no', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.add_column('scheme', sa.Column('scheme_account_ifsc', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.drop_column('scheme', 'three_years_return')
    op.drop_column('scheme', 'three_months_return')
    op.drop_column('scheme', 'six_months_return')
    op.drop_column('scheme', 'one_year_return')
    op.drop_column('scheme', 'one_month_return')
    op.drop_column('scheme', 'objective')
    op.drop_column('scheme', 'isin')
    op.drop_column('scheme', 'five_years_return')
    op.drop_column('scheme', 'amfi_code')
    ### end Alembic commands ###
