"""transaction.note

Revision ID: 8edf82d78ada
Revises: 83a7802afaad
Create Date: 2016-11-15 22:32:49.884842

"""

# revision identifiers, used by Alembic.
revision = '8edf82d78ada'
down_revision = '83a7802afaad'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('customer_transaction', sa.Column('bank_transaction_id', sa.String(), nullable=True))
    op.add_column('customer_transaction', sa.Column('bank_transaction_on', sa.DateTime(), nullable=True))
    op.add_column('customer_transaction', sa.Column('note', sa.String(), nullable=True))
    op.drop_column('customer_transaction', 'pg_transaction_id')
    op.drop_column('customer_transaction', 'pg_transaction_on')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('customer_transaction', sa.Column('pg_transaction_on', postgresql.TIMESTAMP(), autoincrement=False, nullable=True))
    op.add_column('customer_transaction', sa.Column('pg_transaction_id', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.drop_column('customer_transaction', 'note')
    op.drop_column('customer_transaction', 'bank_transaction_on')
    op.drop_column('customer_transaction', 'bank_transaction_id')
    ### end Alembic commands ###
