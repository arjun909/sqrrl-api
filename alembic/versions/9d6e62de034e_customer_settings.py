"""customer.settings

Revision ID: 9d6e62de034e
Revises: 36d5181973e1
Create Date: 2016-11-30 15:33:36.956642

"""

# revision identifiers, used by Alembic.
revision = '9d6e62de034e'
down_revision = '36d5181973e1'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('customer', sa.Column('settings', postgresql.HSTORE(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('customer', 'settings')
    ### end Alembic commands ###
