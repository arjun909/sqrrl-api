"""Fixed typo in customer_sms relationship

Revision ID: 4c097a15bf95
Revises: 13b1f2851a39
Create Date: 2016-09-21 14:21:56.364381

"""

# revision identifiers, used by Alembic.
revision = '4c097a15bf95'
down_revision = '13b1f2851a39'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('product', sa.Column('schemes', postgresql.ARRAY(sa.Integer()), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('product', 'schemes')
    ### end Alembic commands ###
