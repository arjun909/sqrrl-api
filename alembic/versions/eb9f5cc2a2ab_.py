"""empty message

Revision ID: eb9f5cc2a2ab
Revises: b947988dbe6a, ba49944fb3ef
Create Date: 2017-04-14 13:11:42.224403

"""

# revision identifiers, used by Alembic.
revision = 'eb9f5cc2a2ab'
down_revision = ('b947988dbe6a', 'ba49944fb3ef')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
