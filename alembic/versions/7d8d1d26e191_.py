"""empty message

Revision ID: 7d8d1d26e191
Revises: 437a958a7be5, 5c0a5025bcb8, 80d5a4488093
Create Date: 2017-06-06 12:28:06.070674

"""

# revision identifiers, used by Alembic.
revision = '7d8d1d26e191'
down_revision = ('437a958a7be5', '5c0a5025bcb8', '80d5a4488093')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
